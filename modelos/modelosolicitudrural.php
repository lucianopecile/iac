<?php

class ModeloSolicitudRural 
{
    private $intIdSolicitud;
    private $intIdSolicitudRural;
	private $intIdUsrCreador;
	private $intIdUsrMod;
	private $fecUltMod;
	private $booResidenteTierra;
	private $fecFechaResideTierra;
	private $txtResidenciaAnterior;
	private $txtDomicilioReal;
	private $txtProfesion;
	private $txtOcupacionActual;
	private $booIntegraSociedad;
	private $txtDenominacionSociedad;
	private $txtComponentesSociedad;
	private $intIdTrabajoCampo;
	private $booPadresRurales;
	private $intIdProvinciaAnterior;
    private $booEsTecnico;
    private $txtTitulo;
    private $txtEjercioProfesionEn;
    private $booCertificadoProductor;
    private $booOtrasTierras;
    private $txtTierrasDeclaradas;
    private $booOcupanteTierraFiscal;
    private $txtUbicacionTierraFiscal;
    private $booOtrasAdjudicaciones;
    private $txtDetalleAdjudicaciones;
    private $booTransfirioTierras;
    private $txtDestinatario;
    private $intIdArrendamiento;
	private $txtReferencias;
    private $txtNombres;
    private $txtApellido;
	private $intIdPoblador;
    private $intIdTierraRural;
	
	
	
	

	


//------------------------------------------------------------------------------------

	public function db_connect()
	{
		$config = Config::singleton();
		$this->Conexion_ID=mysql_connect($config->get('dbhost'),$config->get('dbuser'), $config->get('dbpass'));
		// $this->Conexion_ID=mysql_connect("localhost","root","");
		if (!$this->Conexion_ID) 
		{
            die('Ha fallado la conexi�n: ' . mysql_error());
            return 0;
        }
        //seleccionamos la base de datos
        if (!@mysql_select_db($config->get('dbname'),$this->Conexion_ID)) 
		{
            echo "Imposible abrir " . $config->get('dbname') ;
            return 0;
        }
        return $this->Conexion_ID;
	}
	
	public function __construct()
	{
	  $this->db_connect();
	}


// ------------------------------------------------------------------------------------

    public function getIdSolicitud()
	{
	    return $this->intIdSolicitud;
	} 

    public function putIdSolicitud($parIdSolicitud)
	{
	    $this->intIdSolicitud = $parIdSolicitud;
	} 

	
// ------------------------------------------------------------------------------------

    public function getIdSolicitudRural()
	{
	    return $this->intIdSolicitudRural;
	} 

    public function putIdSolicitudRural($parIdSolicitudRural)
	{
	    $this->intIdSolicitudRural = $parIdSolicitudRural;
	} 

// ------------------------------------------------------------------------------------

    public function getIdUsrCreador()
	{
	    return $this->intIdUsrCreador;
	} 

    public function putIdUsrCreador($parIdUsrCreador)
	{
	    $this->intIdUsrCreador = $parIdUsrCreador;
	} 

// ------------------------------------------------------------------------------------

    public function getIdUsrMod()
	{
	    return $this->intIdUsrMod;
	} 

    public function putIdUsrMod($parIdUsrMod)
	{
	    $this->intIdUsrMod = $parIdUsrMod;
	} 
// ------------------------------------------------------------------------------------

    public function getUsrMod()
	{
	    return $this->intUsrMod;
	} 

    public function putUsrMod($parUsrMod)
	{
	    $this->intUsrMod = $parUsrMod;
	} 

// ------------------------------------------------------------------------------------

    public function getUltMod()
	{
	    return $this->fecUltMod;
	} 

    public function putUltMod($parUltMod)
	{
	    $this->fecUltMod = $parUltMod;
	} 
	
// ------------------------------------------------------------------------------------

    public function getResidenteTierra()
	{
	    return $this->booResidenteTierra;
	} 

    public function putResidenteTierra($parResidenteTierra)
	{
	    $this->booResidenteTierra = $parResidenteTierra;
	}

// ------------------------------------------------------------------------------------

    public function getFechaResideTierra()
	{
	    return $this->fecFechaResideTierra;
	} 

    public function putFechaResideTierra($parFechaResideTierra)
	{
	    $this->fecFechaResideTierra = $parFechaResideTierra;
	} 

// ------------------------------------------------------------------------------------
	
    public function getResidenciaAnterior()
	{
		return 	$this->txtResidenciaAnterior;
	} 
    
	public function putResidenciaAnterior($parResidenciaAnterior)
	{
	    $this->txtResidenciaAnterior = $parResidenciaAnterior;
	} 


// ------------------------------------------------------------------------------------
	
    public function getDomicilioReal()
	{
		return 	$this->txtDomicilioReal;
	} 
    
	public function putDomicilioReal($parDomicilioReal)
	{
	    $this->txtDomicilioReal = $parDomicilioReal;
	} 

// ------------------------------------------------------------------------------------
	
    public function getProfesion()
	{
		return 	$this->txtProfesion;
	} 
    
	public function putProfesion($parProfesion)
	{
	    $this->txtProfesion = $parProfesion;
	} 

// ------------------------------------------------------------------------------------
	
    public function getOcupacionActual()
	{
		return 	$this->txtOcupacionActual;
	} 
    
	public function putOcupacionActual($parOcupacionActual)
	{
	    $this->txtOcupacionActual = $parOcupacionActual;
	} 

	
// ------------------------------------------------------------------------------------

	public function getIntegraSociedad()
	{
		return $this->booIntegraSociedad;
	}
	
	public function putIntegraSociedad($parIntegraSociedad)
	{
		$this->booIntegraSociedad = $parIntegraSociedad;
	}
	
// ------------------------------------------------------------------------------------

	public function getDenominacionSociedad()
	{
		return $this->txtDenominacionSociedad;
	}
	
	public function putDenominacionSociedad($parDenominacionSociedad)
	{
		$this->txtDenominacionSociedad = $parDenominacionSociedad;
	}

// ------------------------------------------------------------------------------------

	public function getComponentesSociedad()
	{
		return $this->txtComponentesSociedad;
	}
	
	public function putComponentesSociedad($parComponentesSociedad)
	{
		$this->txtComponentesSociedad = $parComponentesSociedad;
	}

// ------------------------------------------------------------------------------------

	public function getIdTrabajoCampo()
	{
		return $this->intIdTrabajoCampo;
	}
	
	public function putIdTrabajoCampo($parIdTrabajoCampo)
	{
		$this->intIdTrabajoCampo = $parIdTrabajoCampo;
	}

// ------------------------------------------------------------------------------------

	public function getPadresRurales()
	{
		return $this->booPadresRurales;
	}
	
	public function putPadresRurales($parPadresRurales)
	{
		$this->booPadresRurales = $parPadresRurales;
	}
	
// ------------------------------------------------------------------------------------

	public function getIdProvinciaAnterior()
	{
		return $this->intIdProvinciaAnterior;
	}
	
	public function putIdProvinciaAnterior($parIdProvinciaAnterior)
	{
		$this->intIdProvinciaAnterior = $parIdProvinciaAnterior;
	}

// ------------------------------------------------------------------------------------

	public function getEsTecnico()
	{
		return $this->booEsTecnico;
	}
	
	public function putEsTecnico($parEsTecnico)
	{
		$this->booEsTecnico= $parEsTecnico;
	}


// ------------------------------------------------------------------------------------

	public function getTitulo()
	{
		return $this->txtTitulo;
	}
	
	public function putTitulo($parTitulo)
	{
		$this->txtTitulo = $parTitulo;
	}


// ------------------------------------------------------------------------------------

	public function getEjercioProfesionEn()
	{
		return $this->txtEjercioProfesionEn;
	}
	
	public function putEjercioProfesionEn($parEjercioProfesionEn)
	{
		$this->txtEjercioProfesionEn = $parEjercioProfesionEn;
	}

// ------------------------------------------------------------------------------------

	public function getCertificadoProductor()
	{
		return $this->booCertificadoProductor;
	}
	
	public function putCertificadoProductor($parCertificadoProductor)
	{
		$this->booCertificadoProductor= $parCertificadoProductor;
	}

// ------------------------------------------------------------------------------------

	public function getOtrasTierras()
	{
		return $this->booOtrasTierras;
	}
	
	public function putOtrasTierras($parOtrasTierras)
	{
		$this->booOtrasTierras= $parOtrasTierras;
	}


// ------------------------------------------------------------------------------------

	public function getTierrasDeclaradas()
	{
		return $this->txtTierrasDeclaradas;
	}
	
	public function putTierrasDeclaradas($parTierrasDeclaradas)
	{
		$this->txtTierrasDeclaradas = $parTierrasDeclaradas;
	}

// ------------------------------------------------------------------------------------

	public function getOcupanteTierraFiscal()
	{
		return $this->booOcupanteTierraFiscal;
	}
	
	public function putOcupanteTierraFiscal($parOcupanteTierraFiscal)
	{
		$this->booOcupanteTierraFiscal= $parOcupanteTierraFiscal;
	}

// ------------------------------------------------------------------------------------

	public function getUbicacionTierraFiscal()
	{
		return $this->txtUbicacionTierraFiscal;
	}
	
	public function putUbicacionTierraFiscal($parUbicacionTierraFiscal)
	{
		$this->txtUbicacionTierraFiscal= $parUbicacionTierraFiscal;
	}

// ------------------------------------------------------------------------------------

	public function getOtrasAdjudicaciones()
	{
		return $this->booOtrasAdjudicaciones;
	}
	
	public function putOtrasAdjudicaciones($parOtrasAdjudicaciones)
	{
		$this->booOtrasAdjudicaciones= $parOtrasAdjudicaciones;
	}

// ------------------------------------------------------------------------------------

	public function getDetalleAdjudicaciones()
	{
		return $this->txtDetalleAdjudicaciones;
	}
	
	public function putDetalleAdjudicaciones($parDetalleAdjudicaciones)
	{
		$this->txtDetalleAdjudicaciones= $parDetalleAdjudicaciones;
	}


// ------------------------------------------------------------------------------------

	public function getTransfirioTierras()
	{
		return $this->booTransfirioTierras;
	}
	
	public function putTransfirioTierras($parTransfirioTierras)
	{
		$this->booTransfirioTierras= $parTransfirioTierras;
	}


// ------------------------------------------------------------------------------------

	public function getDestinatario()
	{
		return $this->txtDestinatario;
	}
	
	public function putDestinatario($parDestinatario)
	{
		$this->txtDestinatario= $parDestinatario;
	}

// ------------------------------------------------------------------------------------

	public function getIdArrendamiento()
	{
		return $this->intIdArrendamiento;
	}
	
	public function putIdArrendamiento($parIdArrendamiento)
	{
		$this->intIdArrendamiento = $parIdArrendamiento;
	}


// ------------------------------------------------------------------------------------

	public function getReferencias()
	{
		return $this->txtReferencias;
	}
	
	public function putReferencias($parReferencias)
	{
		$this->txtReferencias= $parReferencias;
	}

// ------------------------------------------------------------------------------------

    public function getApellido()
	{
	    return $this->txtApellido;
	} 

    public function putApellido($parApellido)
	{
	    $this->txtApellido = $parApellido;
	}

// ------------------------------------------------------------------------------------

    public function getNombres()
	{
	    return $this->txtNombres;
	} 

    public function putNombres($parNombres)
	{
	    $this->txtNombres = $parNombres;
	}	
// ------------------------------------------------------------------------------------

    public function getIdPoblador()
	{
	    return $this->intIdPoblador;
	} 

    public function putIdPoblador($parIdPoblador)
	{
	    $this->intIdPoblador = $parIdPoblador;
	} 

// ------------------------------------------------------------------------------------

    public function getIdTierraRural()
	{
	    return $this->intIdTierraRural;
	} 

    public function putIdTierraRural($parIdTierraRural)
	{
	    $this->intIdTierraRural = $parIdTierraRural;
	} 

		
// ------------------------------------------------------------------------------------

	public function listadoTotal() 
	//retorna la consulta de todas los solicitudesrurales
	{
		
		
		$query = ('SELECT solicitudesrurales.*, solicitudes.nroexpediente, solicitudes.anioexpediente,solicitudes.letraexpediente,pobladores.apellido,solicitudes.idestadosolicitud, pobladores.nombres, estadosolicitud.descripcion as estado FROM solicitudesrurales,solicitudes, pobladores,estadosolicitud WHERE solicitudesrurales.idsolicitud=solicitudes.id && solicitudes.idpoblador=pobladores.id && solicitudes.idestadosolicitud=estadosolicitud.id ');
		$result_all=mysql_query($query);
		if($result_all)
		{
			while ($varsol = mysql_fetch_object($result_all))
			{
	 			//llenar el array 
				$arrSolicitudRurales[]=array("id"=>$varsol->idsolicitud, // ACA DEVUELVE EN ID EL ID DE SOLICITUD NO DE SOLICITUD RURAL
				            "idsolicitud"=>$varsol->idsolicitud,
							"apellido"=>$varsol->apellido,
							"nombres"=>$varsol->nombres,
							"estado"=>$varsol->estado, 
							"idestadosolicitud"=>$varsol->idestadosolicitud,
 							"nroexpediente"=>$varsol->nroexpediente,
   	 	 	 			    "anioexpediente"=>$varsol->anioexpediente,
							"letraexpediente"=>$varsol->letraexpediente
							);
			} 
		}
		return($arrSolicitudRurales);	
	}
	
// ------------------------------------------------------------------------------------	
	public function traersolicitudrural()
	//retorna los datos de un solicitud rural a partir de un id de  solicitud  rural
{	   
		$query = ("SELECT solicitudesrurales.* FROM solicitudesrurales WHERE solicitudesrurales.id='$this->intIdSolicitudRural' ");
        $result_all=mysql_query($query);			 
	  
        if($result_all ){

	      $this->cargarresultados($result_all);
          return(true);	            
        } 
	  	
	    else{
	       return(false);	
	    }
		
       
		
	}
// ------------------------------------------------------------------------------------	
	public function traersolicitudruralasociada()
	//retorna los datos de un solicitud rural a partir de un id de  solicitud 
{	   
		$query = ("SELECT solicitudesrurales.* FROM solicitudesrurales WHERE solicitudesrurales.idsolicitud='$this->intIdSolicitud' ");
        $result_all=mysql_query($query);			 
	  
        if($result_all ){
      
	      $this->cargarresultados($result_all);
          return(true);	            
        } 
	  	
	    else{
	      return(false);	
	    }
		
       
		
	}
		
// ------------------------------------------------------------------------------------	
     public function borrarsolicitudrural()
     {	
     
	  
      $query=("DELETE FROM solicitudesrurales WHERE id = '$this->intIdSolicitudRural'");
      $result_all=mysql_query($query);
	  $num_rows = mysql_affected_rows();
	  
       if($result_all && $num_rows>0 ){
	      
          return(true);
	   }
	   else{
	      return(false);
	   }	  

	  }
	   

     	
// ------------------------------------------------------------------------------------
     public function modificarsolicitudrural()
     {
	  
	 
    
       $query = ("UPDATE solicitudesrurales SET idsolicitud='$this->intIdSolicitud',residentetierra='$this->booResidenteTierra',idusrmod='$this->intIdUsrMod',fechaultmod=CURRENT_DATE,fecharesidetierra='$this->fecFechaResideTierra', residenciaanterior='$this->txtResidenciaAnterior', domicilioreal='$this->txtDomicilioReal',profesion='$this->txtProfesion', ocupacionactual='$this->txtOcupacionActual',integrasociedad='$this->booIntegraSociedad', denominacionsociedad='$this->txtDenominacionSociedad',componentessociedad='$this->txtComponentesSociedad',idtrabajocampo=$this->intIdTrabajoCampo, padresrurales='$this->booPadresRurales', idprovinciaanterior=$this->intIdProvinciaAnterior, estecnico='$this->booEsTecnico', titulo='$this->txtTitulo', ejercioprofesionen='$this->txtEjercioProfesionEn', certificadoproductor='$this->booCertificadoProductor', otrastierras='$this->booOtrasTierras', tierrasdeclaradas='$this->txtTierrasDeclaradas', ocupantetierrafiscal='$this->booOcupanteTierraFiscal', ubicaciontierrafiscal='$this->txtUbicacionTierraFiscal', otrasadjudicaciones='$this->booOtrasAdjudicaciones', detalleadjudicaciones='$this->txtDetalleAdjudicaciones', transfiriotierras='$this->booTransfirioTierras', destinatario='$this->txtDestinatario',idarrendamiento=$this->intIdArrendamiento, referencias='$this->txtReferencias', idtierrarural='$this->intIdTierraRural'   WHERE id = '$this->intIdSolicitudRural' ");
      $result_all=mysql_query($query);
	  $num_rows = mysql_affected_rows();
          if($result_all){
	               return(true);
           }
	       else{
	             return(false);
	       }
      
}



// ------------------------------------------------------------------------------------
     public function altasolicitudrural()
     {

     
     $query = ("INSERT INTO solicitudesrurales ( idsolicitud,residentetierra,idusrcreador,idusrmod,fechaultmod,fecharesidetierra, residenciaanterior, domicilioreal,profesion, ocupacionactual,integrasociedad, denominacionsociedad,componentessociedad,idtrabajocampo, padresrurales, idprovinciaanterior, estecnico, titulo, ejercioprofesionen, certificadoproductor,otrastierras, tierrasdeclaradas, ocupantetierrafiscal, ubicaciontierrafiscal, otrasadjudicaciones, detalleadjudicaciones, transfiriotierras, destinatario,idarrendamiento, referencias, idtierrarural ) VALUES ('$this->intIdSolicitud','$this->booResidenteTierra','$this->intIdUsrCreador','$this->intIdUsrMod',CURRENT_DATE,'$this->fecFechaResideTierra', '$this->txtResidenciaAnterior','$this->txtDomicilioReal','$this->txtProfesion', '$this->txtOcupacionActual','$this->booIntegraSociedad','$this->txtDenominacionSociedad','$this->txtComponentesSociedad',$this->intIdTrabajoCampo, '$this->booPadresRurales', $this->intIdProvinciaAnterior, '$this->booEsTecnico','$this->txtTitulo', '$this->txtEjercioProfesionEn', '$this->booCertificadoProductor', '$this->booOtrasTierras', '$this->txtTierrasDeclaradas', '$this->booOcupanteTierraFiscal', '$this->txtUbicacionTierraFiscal', '$this->booOtrasAdjudicaciones', '$this->txtDetalleAdjudicaciones', '$this->booTransfirioTierras','$this->txtDestinatario',$this->intIdArrendamiento, '$this->txtReferencias', $this->intIdTierraRural)");
    $result_all=mysql_query($query);
	
	
     if($result_all){
//	   define('IDSOLICITUD',mysql_insert_id());
 // 	   $this->putIdSolicitudRural(IDSOLICITUD);
       return(true);
      }
	  else{
	  
	   return(false);
	  }

  }
  

//------------------------------------------------------------------------------------------------ 
  public function cargarresultados($resultado)
	//coloca los datos del query en las variables de la clase
	{
	  
	    while ($cons = mysql_fetch_object($resultado)) {
	 		    
    $this->putIdSolicitudRural($cons->id);
    $this->putIdSolicitud($cons->idsolicitud);
    $this->putIdUsrCreador($cons->idusrcreador);
    $this->putIdUsrMod($cons->idusrmod);
    $this->putUltMod(fechaacadena($cons->fechaultmod));
	$this->putResidenteTierra($cons->residentetierra);
	$this->putFechaResideTierra($cons->fecharesidetierra);
	$this->putResidenciaAnterior($cons->residenciaanterior);
	$this->putDomicilioReal($cons->domicilioreal);
	$this->putProfesion($cons->profesion);
	$this->putOcupacionActual($cons->ocupacionactual);
	$this->putIntegraSociedad($cons->integrasociedad);
	$this->putDenominacionSociedad($cons->denominacionsociedad);
	$this->putComponentesSociedad($cons->componentessociedad);
	$this->putIdTrabajoCampo($cons->idtrabajocampo);
	$this->putPadresRurales($cons->padresrurales);
	$this->putIdProvinciaAnterior($cons->idprovinciaanterior);
    $this->putEsTecnico($cons->estecnico);
    $this->putTitulo($cons->titulo);
    $this->putEjercioProfesionEn($cons->ejercioprofesionen);
    $this->putCertificadoProductor($cons->certificadoproductor);
    $this->putOtrasTierras($cons->otrastierras);
    $this->putTierrasDeclaradas($cons->tierrasdeclaradas);
    $this->putOcupanteTierraFiscal($cons->ocupantetierrafiscal);
    $this->putUbicacionTierraFiscal($cons->ubicaciontierrafiscal);
    $this->putOtrasAdjudicaciones($cons->otrasadjudicaciones);
    $this->putDetalleAdjudicaciones($cons->detalleadjudicaciones);
    $this->putTransfirioTierras($cons->transfiriotierras);
    $this->putDestinatario($cons->destinatario);
    $this->putIdArrendamiento($cons->idarrendamiento);
	$this->putReferencias($cons->referencias);
    $this->putNombres($cons->nombres);
    $this->putApellido($cons->apellido);
    $this->putIdTierraRural($cons->idtierrarural);

 
		
		}
	}

	
	
}
?>