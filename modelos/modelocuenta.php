<?php

class ModeloCuenta 
{
    private $intIdCuenta;
    private $txtNroCuenta;
    private $intConvenio;
    private $intIdSolicitud;
    private $intIdUsrCreador;
    private $intIdUsrMod;
    private $fecUltMod;
    private $txtCuentaCorriente;
    private $intValorLiquidacion;
    private $intValorHectareaFinal;
    private $intCantCuotas;
    private $intPeriodoCuota;
    private $intTasaInteres;    
    private $intFormalizacion;
    private $intRecuperoMensura;
    private $fecFechaCuenta;
    private $txtObservacion;
    
    private $intSumaIntereses;
    private $intCantCuotasFormalizacion;
    private $intPeriodoDiferidoFormalizacion;
    private $fecFechaVencimientoFormalizacion;
    private $intCuotasParalelas;
    private $intPeriodoDiferidoPlan;
    private $fecFechaVencimientoPlan;
    
    private $intCantMeses; 	/* no esta en la tabla CUENTA, se usa para el calculo de cuotas */
    private $fecPimerVenc; 	/* no esta en la tabla CUENTA, se usa para el calculo de cuotas */
    private $intNroExpediente; /* no esta en la tabla CUENTA, se usa para la busqueda */
    private $intAnioExpediente; /* no esta en la tabla CUENTA, se usa para la busqueda */
    private $txtLetraExpediente; /* no esta en la tabla CUENTA, se usa para la busqueda */
    private $arrElegidas;


//==========================================================================================================================	

	public function db_connect()
	{
		$config = Config::singleton();
		$this->Conexion_ID=mysql_connect($config->get('dbhost'),$config->get('dbuser'), $config->get('dbpass'));
		if (!$this->Conexion_ID) 
		{
			die('Ha fallado la conexi�n: ' . mysql_error());
			return 0;
		}
        //seleccionamos la base de datos
		if (!@mysql_select_db($config->get('dbname'),$this->Conexion_ID)) 
		{
			echo "Imposible abrir " . $config->get('dbname') ;
			return 0;
		}
        return $this->Conexion_ID;
	}
	
//==========================================================================================================================	

	public function __construct()
	{
		$this->db_connect();
	}
	
// ------------------------------------------------------------------------------------

    public function getIdCuenta()
	{
	    return $this->intIdCuenta;
	} 

    public function putIdCuenta($parIdCuenta)
	{
	    $this->intIdCuenta = $parIdCuenta;
	}
	
// ------------------------------------------------------------------------------------

	public function getNroCuenta()
	{
		return $this->txtNroCuenta;
	}
	
	public function putNroCuenta($parNro)
	{
		$this->txtNroCuenta = $parNro;
	}
	
// ------------------------------------------------------------------------------------

	public function getConvenio()
	{
		return $this->intConvenio;
	}
	
	public function putConvenio($parConvenio)
	{
		$this->intConvenio = $parConvenio;
	}
	
// ------------------------------------------------------------------------------------

    public function getIdSolicitud()
	{
	    return $this->intIdSolicitud;
	} 

    public function putIdSolicitud($parIdSolicitud)
	{
	    $this->intIdSolicitud = $parIdSolicitud;
	} 

// ------------------------------------------------------------------------------------

    public function getValorLiquidacion()
	{
	    return $this->intValorLiquidacion;
	} 

    public function putValorLiquidacion($parValorLiquidacion)
	{
	    $this->intValorLiquidacion = $parValorLiquidacion;
	}
	
// ------------------------------------------------------------------------------------

    public function getValorHectareaFinal()
    {
        return $this->intValorHectareaFinal;
    }

    public function putValorHectareaFinal($parValor)
    {
        $this->intValorHectareaFinal = $parValor;
    }

// ------------------------------------------------------------------------------------

    public function getCantCuotas()
	{
	    return $this->intCantCuotas;
	} 

    public function putCantCuotas($parCantCuotas)
	{
	    
	    $this->intCantCuotas = $parCantCuotas;
	}
		
// ------------------------------------------------------------------------------------

    public function getTasaInteres()
	{
	    return $this->intTasaInteres;
	} 

    public function putTasaInteres($parTasaInteres)
	{
	    $this->intTasaInteres = $parTasaInteres;
	}	
            
// ------------------------------------------------------------------------------------

    public function getFechaCuenta()
    {
        return $this->fecFechaCuenta;
    }

    public function putFechaCuenta($parFecha)
    {
        $this->fecFechaCuenta = $parFecha;
    }

// ------------------------------------------------------------------------------------

    public function getIdUsrCreador()
	{
	    return $this->intIdUsrCreador;
	} 

    public function putIdUsrCreador($parIdUsrCreador)
	{
	    $this->intIdUsrCreador = $parIdUsrCreador;
	} 

// ------------------------------------------------------------------------------------

    public function getIdUsrMod()
	{
	    return $this->intIdUsrMod;
	} 

    public function putIdUsrMod($parIdUsrMod)
	{
	    $this->intIdUsrMod = $parIdUsrMod;
	} 

// ------------------------------------------------------------------------------------

    public function getUltMod()
	{
	    return $this->fecUltMod;
	} 

    public function putUltMod($parUltMod)
	{
	    $this->fecUltMod = $parUltMod;
	}

// ------------------------------------------------------------------------------------
    public function getObservacion()
	{
		return 	$this->txtObservacion;
	} 
    public function putObservacion($parObservacion)
	{
	    $this->txtObservacion = $parObservacion;
	} 

// ------------------------------------------------------------------------------------

    public function getPeriodoCuota()
	{
	    return $this->intPeriodoCuota;
	} 

    public function putPeriodoCuota($parPeriodoCuota)
	{
	    $this->intPeriodoCuota = $parPeriodoCuota;
	}
 
// ------------------------------------------------------------------------------------

    public function getCuentaCorriente()
	{
	    return $this->txtCuentaCorriente;
	} 

    public function putCuentaCorriente($parCuentaCorriente)
	{
	    $this->txtCuentaCorriente = $parCuentaCorriente;
	}

// ------------------------------------------------------------------------------------

    public function getCantMeses()
	{
	    return $this->intCantMeses;
	} 

    public function putCantMeses($parCantMeses)
	{
	    $this->intCantMeses = $parCantMeses;
	}
	
// ------------------------------------------------------------------------------------

	public function getPrimerVenc()
	{
		return $this->fecPimerVenc;
	}
	
	public function putPrimerVenc($parVenc)
	{
		$this->fecPimerVenc = $parVenc;
	}
	
// ------------------------------------------------------------------------------------

	public function getNroExpediente()
	{
		return $this->intNroExpediente;
	}
	public function putNroExpediente($parNroExp)
	{
		$this->intNroExpediente = $parNroExp;
	}

// ------------------------------------------------------------------------------------

	public function getAnioExpediente()
	{
		return $this->intAnioExpediente;
	}
	public function putAnioExpediente($parAnioExp)
	{
		$this->intAnioExpediente = $parAnioExp;
	}
	
// ------------------------------------------------------------------------------------

	public function getLetraExpediente()
	{
		return $this->txtLetraExpediente;
	}
	public function putLetraExpediente($parLetraExp)
	{
		$this->txtLetraExpediente = $parLetraExp;
	}

// ------------------------------------------------------------------------------------

    public function getFormalizacion()
    {
        return $this->intFormalizacion;
    }

    public function putFormalizacion($parForm)
    {
        $this->intFormalizacion = $parForm;
    }

// ------------------------------------------------------------------------------------

    public function getRecuperoMensura()
    {
        return $this->intRecuperoMensura;
    }

    public function putRecuperoMensura($parForm)
    {
        $this->intRecuperoMensura = $parForm;
    }
    
// ------------------------------------------------------------------------------------
    public function getElegidas()
	{
		return 	$this->arrElegidas;
	} 
    public function putElegidas($parElegidas=array())
	{
	    $this->arrElegidas=$parElegidas;
	} 	

// ------------------------------------------------------------------------------------

    public function getSumarIntereses()
    {
        return 	$this->intSumaIntereses;
    } 
    public function putSumarIntereses($sumaIntereses)
    {
        $this->intSumaIntereses = $sumaIntereses;
    }
    
// ------------------------------------------------------------------------------------

    public function getCantCuotasFormalizacion()
    {
        return 	$this->intCantCuotasFormalizacion;
    } 
    public function putCantCuotasFormalizacion($cuotasFormaliacion)
    {
        $this->intCantCuotasFormalizacion = $cuotasFormaliacion;
    }
        
// ------------------------------------------------------------------------------------

    public function getPeriodoDiferidoFormalizacion()
    {
        return 	$this->intPeriodoDiferidoFormalizacion;
    } 
    public function putPeriodoDiferidoFormalizacion($periodoDiferido)
    {
        $this->intPeriodoDiferidoFormalizacion = $periodoDiferido;
    }
    
// ------------------------------------------------------------------------------------

    public function getFechaVencimientoFormalizacion()
    {
        return 	$this->fecFechaVencimientoFormalizacion;
    } 
    public function putFechaVencimientoFormalizacion($fechaVencFormalizacion)
    {
        $this->fecFechaVencimientoFormalizacion = $fechaVencFormalizacion;
    }
    
// ------------------------------------------------------------------------------------

    public function getCuotasParalelas()
    {
        return 	$this->intCuotasParalelas;
    } 
    public function putCuotasParalelas($cuotasParalelas)
    {
        $this->intCuotasParalelas = $cuotasParalelas;
    }
    
// ------------------------------------------------------------------------------------

    public function getPeriodoDiferidoPlan()
    {
        return 	$this->intPeriodoDiferidoPlan;
    } 
    public function putPeriodoDiferidoPlan($periodoDiferido)
    {
        $this->intPeriodoDiferidoPlan = $periodoDiferido;
    }
    
// ------------------------------------------------------------------------------------

    public function getFechaVencimientoPlan()
    {
        return 	$this->fecFechaVencimientoPlan;
    } 
    public function putFechaVencimientoPlan($fechaVencPlan)
    {
        $this->fecFechaVencimientoPlan = $fechaVencPlan;
    }
    
// ------------------------------------------------------------------------------------

	public function listadoTotal() 
        //retorna la consulta de todos las cuentas
	{
            $query =('SELECT cuentas.*,solicitudes.anioexpediente,solicitudes.nroexpediente,solicitudes.letraexpediente,solicitudes.tiposolicitud,pobladores.apellido,pobladores.nombres,localidades.descripcion as localidad FROM cuentas,solicitudes,pobladores,localidades WHERE cuentas.idsolicitud=solicitudes.id && solicitudes.idpoblador=pobladores.id && solicitudes.idlocalidad=localidades.id ORDER BY nroexpediente+anioexpediente DESC');
            $result_all = mysql_query($query);
            if($result_all)
            {
                while ($varres = mysql_fetch_object($result_all))
                {
                    if ($varres->tiposolicitud == RURAL)
                            $tipo = "RURAL";
                    else
                            $tipo = "URBANA";

                    //llenar el array
                    $arrCuentas[] = array("id"=>$varres->id,
                        "nrocuenta"=>$varres->nrocuenta,
                        "expediente"=>$varres->nroexpediente." ".$varres->anioexpediente." ".$varres->letraexpediente,
                        "idsolicitud"=>$varres->idsolicitud,
                        "valorliquidacion"=>$varres->valorliquidacion,
                        "solicitante"=>$varres->apellido.", ".$varres->nombres,
                        "tipo"=>$tipo,
                        "localidad"=>$varres->localidad);
                }
            }
            return($arrCuentas);
	}

// ------------------------------------------------------------------------------------

    public function listadoNroCuenta() 
    //retorna la consulta de las cuentas segun un nro de cuenta
    {
        $query = ("SELECT cuentas.*,solicitudes.anioexpediente,solicitudes.nroexpediente,solicitudes.letraexpediente,solicitudes.tiposolicitud,pobladores.apellido,pobladores.nombres FROM cuentas,solicitudes,pobladores WHERE cuentas.idsolicitud=solicitudes.id && solicitudes.idpoblador=pobladores.id && cuentas.nrocuenta LIKE '$this->txtNroCuenta%' ORDER BY cuentas.nrocuenta DESC");
        $result_all = mysql_query($query);
        while ($varres = mysql_fetch_object($result_all))
        {
            if ($varres->tiposolicitud == RURAL)
                $tipo = "RURAL";
            else
                $tipo = "URBANA";

            //llenar el array 
            $arrCuentas[] = array("id"=>$varres->id,
                            "nrocuenta"=>$varres->nrocuenta,
                            "expediente"=>$varres->nroexpediente." ".$varres->anioexpediente." ".$varres->letraexpediente,
                            "idsolicitud"=>$varres->idsolicitud,
                            "valorliquidacion"=>$varres->valorliquidacion,
                            "solicitante"=>$varres->apellido.", ".$varres->nombres,
                            "tipo"=>$tipo );
        } 
        return($arrCuentas);
    }

// ------------------------------------------------------------------------------------

    public function listadoExpediente() 
    //retorna la consulta de las cuentas segun un nro de expdte
    {
        $query =("SELECT cuentas.*,solicitudes.anioexpediente,solicitudes.nroexpediente,solicitudes.letraexpediente,solicitudes.tiposolicitud,pobladores.apellido,pobladores.nombres FROM cuentas,solicitudes,pobladores WHERE cuentas.idsolicitud=solicitudes.id && solicitudes.idpoblador=pobladores.id && solicitudes.nroexpediente LIKE '$this->intNroExpediente%' ORDER BY nroexpediente+anioexpediente DESC");
        $result_all = mysql_query($query);
        while ($varres = mysql_fetch_object($result_all))
        {
            if ($varres->tiposolicitud == RURAL)
                    $tipo = "RURAL";
            else
                    $tipo = "URBANA";

            //llenar el array 
            $arrCuentas[] = array("id"=>$varres->id,
                                "nrocuenta"=>$varres->nrocuenta,
                                "expediente"=>$varres->nroexpediente." ".$varres->anioexpediente." ".$varres->letraexpediente,
                                "idsolicitud"=>$varres->idsolicitud,
                                "valorliquidacion"=>$varres->valorliquidacion,
                                "solicitante"=>$varres->apellido.", ".$varres->nombres,
                                "tipo"=>$tipo );
        } 
        return($arrCuentas);
    }
    
// ------------------------------------------------------------------------------------

	public function traerCuenta()
	//retorna los datos de una cuenta en particular a partir de un id 
	{
            $query = ("SELECT cuentas.*,solicitudes.anioexpediente,solicitudes.nroexpediente,pobladores.apellido,pobladores.nombres,tipoperiodo.cantmeses FROM cuentas,solicitudes,pobladores,tipoperiodo WHERE cuentas.id='$this->intIdCuenta'&& cuentas.idsolicitud=solicitudes.id && solicitudes.idpoblador=pobladores.id && cuentas.periodocuota=tipoperiodo.id");
            $result_all = mysql_query($query);
            $num_rows = mysql_affected_rows();
            if($result_all && $num_rows > 0)
            {
                $this->cargarresultados($result_all);
                return(true);	            
            }else{
                return(false);	
            }
	}

// ------------------------------------------------------------------------------------

	public function traerCuentaSolicitud()
	//retorna los datos de una cuenta en particular a partir de un id
	{
            $query = ("SELECT cuentas.* FROM cuentas WHERE cuentas.idsolicitud='$this->intIdSolicitud'");
            $result_all = mysql_query($query);
            $num_rows = mysql_affected_rows();
            if($result_all && $num_rows > 0)
            {
                $this->cargarresultados($result_all);
                return(true);
            }else{
                return(false);
            }
	}

// ------------------------------------------------------------------------------------	

	public function borrarCuenta()
	{	
            $query=("DELETE FROM cuentas WHERE id = '$this->intIdCuenta'");
            $result_all = mysql_query($query);
            $num_rows = mysql_affected_rows();
            return ($result_all && $num_rows > 0);
	}

// ------------------------------------------------------------------------------------

	public function modificarCuenta()
	{
            $query = ("UPDATE cuentas SET 
                idusrmodificador='$this->intIdUsrMod',
                fechaultmod=CURRENT_DATE,
                observacion='$this->txtObservacion',
                valorliquidacion='$this->intValorLiquidacion',
                valorhectareafinal='$this->intValorHectareaFinal',
                cantcuotas='$this->intCantCuotas',
                periodocuota='$this->intPeriodoCuota',
                tasainteres='$this->intTasaInteres',
                formalizacion='$this->intFormalizacion',
                recuperomensura='$this->intRecuperoMensura',
                sumarintereses='$this->intSumaIntereses',
                cantcuotasformalizacion='$this->intCantCuotasFormalizacion',
                periododiferidoformalizacion='$this->intPeriodoDiferidoFormalizacion',
                fechavencimientoformalizacion='$this->fecFechaVencimientoFormalizacion', 
                cuotasparalelas='$this->intCuotasParalelas', 
                periododiferidoplan='$this->intPeriodoDiferidoPlan',
                fechavencimientoplan='$this->fecFechaVencimientoPlan'
                WHERE id = '$this->intIdCuenta'");
            $result_all = mysql_query($query);
            return $result_all;
	}

// ------------------------------------------------------------------------------------
     
	public function altaCuenta()
	{
            $query = ("INSERT INTO cuentas (idsolicitud,
                                            nrocuenta,
                                            idusrcreador,
                                            idusrmodificador,
                                            fechaultmod,
                                            fechacuenta,
                                            cuentacorriente,
                                            valorliquidacion,
                                            valorhectareafinal,
                                            cantcuotas,
                                            periodocuota,
                                            tasainteres,
                                            formalizacion,
                                            recuperomensura,
                                            observacion,
                                            convenio,
                                            sumarintereses,
                                            cantcuotasformalizacion,
                                            periododiferidoformalizacion,
                                            fechavencimientoformalizacion,
                                            cuotasparalelas,
                                            periododiferidoplan,
                                            fechavencimientoplan)                                            
                            VALUES ('$this->intIdSolicitud',
                                '$this->txtNroCuenta',
                                '$this->intIdUsrCreador',
                                '$this->intIdUsrMod',
                                CURRENT_DATE,
                                '$this->fecFechaCuenta',
                                '$this->txtCuentaCorriente',
                                '$this->intValorLiquidacion',
                                '$this->intValorHectareaFinal',
                                '$this->intCantCuotas',
                                '$this->intPeriodoCuota',
                                '$this->intTasaInteres',
                                '$this->intFormalizacion',
                                '$this->intRecuperoMensura',
                                '$this->txtObservacion',
                                '$this->intConvenio',
                                '$this->intSumaIntereses',
                                '$this->intCantCuotasFormalizacion',
                                '$this->intPeriodoDiferidoFormalizacion',
                                '$this->fecFechaVencimientoFormalizacion',
                                '$this->intCuotasParalelas',
                                '$this->intPeriodoDiferidoPlan',
                                '$this->fecFechaVencimientoPlan')");
            $result_all = mysql_query($query);
            define('IDCUENTA',mysql_insert_id());
            $this->putIdCuenta(IDCUENTA);
            if($result_all)
                return(IDCUENTA);
            else                
                return 0;
	}

// ------------------------------------------------------------------------------------	

    public function borrarCuotasCuenta()
    {	
        $query=("DELETE FROM cuotas WHERE idcuenta = '$this->intIdCuenta'");
        $result_all = mysql_query($query);
        return ($result_all);
    }

// ------------------------------------------------------------------------------------

    public function borrarCuotasCuentaNoPagas()
    {
        $query=("DELETE FROM cuotas WHERE idcuenta = '$this->intIdCuenta' && fechapago='0000-00-00'");
        $result_all = mysql_query($query);
        return ($result_all);
    }

// ------------------------------------------------------------------------------------

    public function ajustarLiquidacion()
    {
        $query = ("UPDATE cuentas SET idusrmodificador='$this->intIdUsrMod',fechaultmod=CURRENT_DATE, observacion='$this->txtObservacion',
            valorliquidacion='$this->intValorLiquidacion',tasainteres='$this->intTasaInteres',cantcuotas='$this->intCantCuotas'
            WHERE id = '$this->intIdCuenta'");
        $result_all = mysql_query($query);
        return $result_all;
    }

// -----------------------------------------------------------------------------------------------

    public function guardarobservacion()
    {
        $query = ("UPDATE cuentas SET observacion='$this->txtObservacion' WHERE id='$this->intIdCuenta'");
        $result_all = mysql_query($query);
        return ($result_all);
    }

//=================================================================================================

    public function cargarresultados($resultado)
    //coloca los datos del query en las variables de la clase
    {
        while ($cons = mysql_fetch_object($resultado))
        {
            $this->putIdCuenta($cons->id);
            $this->putIdSolicitud($cons->idsolicitud);
            $this->putNroCuenta($cons->nrocuenta);
            $this->putIdUsrCreador($cons->idusrcreador);
            $this->putIdUsrMod($cons->idusrmodificador);
            $this->putUltMod($cons->fechaultmod);                
            $this->putFechaCuenta($cons->fechacuenta);
            $this->putCuentaCorriente($cons->cuentacorriente);
            $this->putConvenio($cons->convenio);                
            $this->putValorLiquidacion($cons->valorliquidacion);                
            $this->putCantCuotas($cons->cantcuotas);
            $this->putPeriodoCuota($cons->periodocuota);
            $this->putTasaInteres($cons->tasainteres);                
            $this->putFormalizacion($cons->formalizacion);
            $this->putRecuperoMensura($cons->recuperomensura);
            $this->putObservacion($cons->observacion);

            $this->putSumarIntereses($cons->sumarintereses);
            $this->putCantCuotasFormalizacion($cons->cantcuotasformalizacion);
            $this->putPeriodoDiferidoFormalizacion($cons->periododiferidoformalizacion);
            $this->putFechaVencimientoFormalizacion($cons->fechavencimientoformalizacion);
            $this->putCuotasParalelas($cons->cuotasparalelas);
            $this->putPeriodoDiferidoPlan($cons->periododiferidoplan);
            $this->putFechaVencimientoPlan($cons->fechavencimientoplan);

            $this->putCantMeses($cons->cantmeses);
            $this->putNroExpediente($cons->nroexpediente);
            $this->putAnioExpediente($cons->anioexpediente);

        }
    }
	
// ------------------------------------------------------------------------------------	

	public function traerCuentaExpediente()
	//retorna los datos de una cuenta particular a partir de los datos de un expediente
	{	 
		$condicion="";
		if ($this->intAnioExpediente > 0)
			$condicion .= "solicitudes.anioexpediente LIKE'$this->intAnioExpediente%' && ";

		if ($this->intNroExpediente > 0)
			$condicion .= "solicitudes.nroexpediente LIKE'$this->intNroExpediente%' && ";

		if ($this->txtNroCuenta > 0)
			$condicion .= "cuentas.nrocuenta LIKE'$this->txtNroCuenta%' && ";

		$query = ("SELECT cuentas.*,solicitudes.id as idsolicitud,solicitudes.nroexpediente,solicitudes.letraexpediente,solicitudes.anioexpediente,solicitudes.tiposolicitud,pobladores.apellido,pobladores.nombres FROM cuentas, solicitudes, pobladores WHERE ".$condicion. "cuentas.idsolicitud=solicitudes.id && solicitudes.idpoblador=pobladores.id");
                $result_all = mysql_query($query);
		while ($var = mysql_fetch_object($result_all))
		{
			if ($var->tiposolicitud == RURAL)
				$tipo = "RURAL";
			else
				$tipo = "URBANA";

			//llenar el array 
			$arrCuentas[] = array("id"=>$var->id,
                        "nrocuenta"=>$var->nrocuenta,
						"idsolicitud"=>$var->idsolicitud,
						"solicitante"=>$var->apellido.", ".$var->nombres,
						"expediente"=>$var->nroexpediente." ".$var->anioexpediente." ".$var->letraexpediente,
						"tipo"=>$tipo,
						"valorliquidacion"=>$var->valorliquidacion );
		} 
		return($arrCuentas);	
	} 

// ------------------------------------------------------------------------------------

	public function existeNroCuenta()
	//verifica que el nro de cuenta ingresado no existe para otra cuenta
	{
		$query = ("SELECT id FROM cuentas WHERE nrocuenta='$this->txtNroCuenta' && id <> '$this->intIdCuenta'");
		$result_all = mysql_query($query);
		$num_rows = mysql_affected_rows();
		return ($result_all && ($num_rows > 0));
	}

// ------------------------------------------------------------------------------------

    public function tieneCuotasPagas()
    //verifica si la cuenta tiene al menos un cuota pagada
    {
        $query = ("SELECT id FROM cuotas WHERE cuotas.idcuenta='$this->intIdCuenta' && cuotas.fechapago<>'00-00-0000'");
		$result_all = mysql_query($query);
		$num_rows = mysql_affected_rows();
		return ($result_all && ($num_rows > 0));
    }
   
//============================================================================

	public function ObtenerZonas()
	{
        $parArray[1]="0";
		$parArray=$this->getElegidas();
		$arrElemento=0;
		$condicion="";
		$primero=true;
		if (is_array($parArray)) 
		{
		    foreach($parArray as $arrElemento)
		    {
			$todas=$this->VerDepende($arrElemento);
			 if($primero){
			   $primero=false;
			 }
			 else{
			  $condicion=$condicion." || ";
			 }
			  $condicion=$condicion.$todas;
            } 
			 
		}
		return($condicion);
	}

//----------------------------------------------------------

	public function VerDepende($parId) 
    //consulta de localidades dependientes de la localidad por parametro
	{
		$condicion="";
		$query =('SELECT * FROM localidades WHERE localidades.depende='.$parId.' ORDER BY descripcion');
		$result_all = mysql_query($query);
		$num_rows = mysql_affected_rows();
		if($num_rows > 0)
		{
			$primero = true;
			while ($varloc = mysql_fetch_object($result_all))
			{
				if($primero)
					$primero = false;
				else
					$condicion = $condicion." || ";
			 }
			 $condicion = $condicion." solicitudes.idlocalidad=".$varloc->id;
		}else{
			$condicion = " solicitudes.idlocalidad=".$parId;
		}
		return($condicion);
	}

// ------------------------------------------------------------------------------------

	public function traertotcuentaZonas($parCondicion)
	//retorna la cuentas pertenecientes a una zona especificada en el parametro
	{
		if(empty($parCondicion))
			$query = ("SELECT cuentas.*, solicitudes.anioexpediente,solicitudes.letraexpediente,solicitudes.nroexpediente,solicitudes.tiposolicitud,pobladores.apellido,pobladores.nombres,pobladores.documento, tipodocumento.descripcion as tipodocumento, tipoperiodo.cantmeses,localidades.descripcion as zona, COUNT(*) AS cantcuenta, localidades.depende as comarca  FROM cuentas,solicitudes,pobladores,tipoperiodo ,localidades,tipodocumento WHERE cuentas.idsolicitud=solicitudes.id && solicitudes.idpoblador=pobladores.id &&  cuentas.periodocuota=tipoperiodo.id && solicitudes.idlocalidad=localidades.id && tipodocumento.id=pobladores.idtipodoc  GROUP BY  localidades.depende, localidades.descripcion");
		else
			$query = ("SELECT cuentas.*, solicitudes.anioexpediente,solicitudes.letraexpediente,solicitudes.nroexpediente,solicitudes.tiposolicitud,pobladores.apellido,pobladores.nombres,pobladores.documento, tipodocumento.descripcion as tipodocumento, tipoperiodo.cantmeses,localidades.descripcion as zona, COUNT(*) AS cantcuenta, localidades.depende as comarca  FROM cuentas,solicitudes,pobladores,tipoperiodo ,localidades,tipodocumento WHERE (".$parCondicion.") && cuentas.idsolicitud=solicitudes.id && solicitudes.idpoblador=pobladores.id &&  cuentas.periodocuota=tipoperiodo.id && solicitudes.idlocalidad=localidades.id && tipodocumento.id=pobladores.idtipodoc  GROUP BY  localidades.depende, localidades.descripcion");

		$result_all=mysql_query($query);
		$num_rows = mysql_affected_rows();
		if ($result_all && $num_rows > 0)
			return $result_all;
		else
			return false;
	}

//------------------------------------------------------------------------------------

	public function sumaCapitalCuotasPagas()
	//devuelve la suma de los capitales de las cuotas pagas, o con al menos un pago parcial
	{
		$query = "SELECT sum(capital) as capitaltotal FROM cuotas WHERE cuotas.idcuenta='$this->intIdCuenta' && fechapago<>'0000-00-00'";
		$result_all = mysql_query($query);
		if($result_all)
		{
			$var = mysql_fetch_object($result_all);
			$suma = $var->capitaltotal;
			return $suma;
		}
		return false;
	}

}

?>