<?php


class ModeloUnidad
{
   
    private $intId;
	private $txtDescripcion;
	private $txtLetra;

    
// ------------------------------------------------------------------------------------
	
	public function db_connect()
	{
		$config = Config::singleton();

		$this->Conexion_ID=mysql_connect($config->get('dbhost'),$config->get('dbuser'), $config->get('dbpass'));
  
		if (!$this->Conexion_ID) 
		{
			die('Ha fallado la conexi�n: ' . mysql_error());
			return 0;
		}
        //seleccionamos la base de datos
        if (!@mysql_select_db($config->get('dbname'),$this->Conexion_ID)) 
		{
            echo "Imposible abrir " . $config->get('dbname') ;
            return 0;
        }
		return $this->Conexion_ID;
	}
	
// ------------------------------------------------------------------------------------

	public function __construct()
	{
		$this->db_connect();
	}

// ------------------------------------------------------------------------------------

	public function getId()
	{
		return $this->intId;
	}

	public function putId($parId)
	{
		$this->intId = $parId;
	}

// ------------------------------------------------------------------------------------

	public function getDescripcion()
	{
		return $this->txtDescripcion;
	}

	public function putDescripcion($parDescripcion)
	{
		$this->txtDescripcion = $parDescripcion;
	}

// ------------------------------------------------------------------------------------

	public function getLetra()
	{
		return $this->txtLetra;
	}

	public function putLetra($parLetra)
	{
		$this->txtLetra = $parLetra;
	}
	
//----------------------------------------------------------

	public function traerTodos() 
	//retorna la consulta de todas las unidades
	{
		$query = ('SELECT * FROM unidades');
		$result_all = mysql_query($query);
		while ($varu = mysql_fetch_object($result_all))
		{
	 		//llenar el array 
			$arrUnidades[]=array($varu->id,
								$varu->descripcion,
								$varu->letra);                   
		}
	return $arrUnidades;
	}	
//----------------------------------------------------------

	public function traerTodosTierra() 
	//retorna la consulta de todas las unidades
	{
		$query = ('SELECT * FROM unidades WHERE id=4 || id=5 ');
		$result_all = mysql_query($query);
		while ($varu = mysql_fetch_object($result_all))
		{
	 		//llenar el array 
			$arrUnidades[]=array($varu->id,
								$varu->descripcion,
								$varu->letra);                   
		}
	return $arrUnidades;
	}	

//----------------------------------------------------------

	public function traerLetrasUnidad() 
	//retorna la consulta de todas las letras de las unidades
	{
		$query = ('SELECT id,letra FROM unidades');
		$result_all = mysql_query($query);
		while ($varu = mysql_fetch_object($result_all))
		{
	 		//llenar el array 
			$arrUnidades[]=array($varu->id,
								$varu->letra);                   
		}
	return $arrUnidades;
	}	
	
//============================================================================

	public function traerUnidad()
	// carga las variables con la unidad indicada segun el ID, devuleve true o false segun corresponda 
	{ 
		$id = $this->getId();
		$query = ("SELECT unidades.* FROM unidades WHERE unidades.id='$id'");
		$result_all = mysql_query($query);
 		if($result_all)
 		{
			$this->cargarresultados($result_all);
			return(true);
 		} else {
			echo "No se encontro la unidad";
			return(false);
		}
	}	 

//============================================================================

	public function listadoTotal()  
	//retorna la consulta de todas las tasas de interes
	{
		$query = ("SELECT * FROM unidades ");
		$result_all = mysql_query($query);
		while ($varu = mysql_fetch_object($result_all))
		{
			$arrUnidades[]=array("id" => $varu->id,
								"descripcion" => $varu->descripcion,
								"letra" => $varu->letra);	               
		}
		return $arrUnidades;
	}

//============================================================================
	
	public function borrarunidad()
	{
		$query = ("DELETE FROM unidades WHERE id = '$this->intId'");
		$result_all = mysql_query($query);
		$num_rows = mysql_affected_rows();
		return ($result_all && $num_rows > 0);
	}
     	
//============================================================================

	public function modificarunidad()
	{
		$query = ("UPDATE unidades SET descripcion='$this->txtDescripcion',letra='$this->txtLetra' WHERE id = '$this->intId'");
		$result_all = mysql_query($query);
		$num_rows = mysql_affected_rows();
		return ($result_all && $num_rows > 0);
	}


//============================================================================

	public function altaunidad()
	{
		$query = ("INSERT INTO unidades (descripcion,letra) VALUES ('$this->txtDescripcion','$this->txtLetra')");
		$result_all = mysql_query($query);
		return ($result_all && $num_rows > 0);       
	}

//============================================================================

	public function setvariables()
	//pone a cero y vacio todas las variables de la clase
	{
		$this->putId(0);
		$this->putDescripcion("");
		$this->putLetra("");
	}
  
//============================================================================

	public function cargarresultados($resultado)
	//coloca los datos del query en las variables de la clase
	{
		$this->setvariables();
		while ($cons = mysql_fetch_object($resultado))
		{
			$this->putId($cons->id);
			$this->putDescripcion($cons->descripcion);
			$this->putLetra($cons->letra);       
		}
	}	
	
}
?>