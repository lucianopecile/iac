<?php

class ModeloSolicitudUrbana 
{
    private $intIdSolicitud;
	private $intIdTierraRural;
    private $intIdSolicitudUrbana;
	private $intIdUsrCreador;
	private $intIdUsrMod;
	private $fecUltMod;
	private $txtTierrasDeclaradas;
	private $txtActividadLaboral;
	private $booTieneConcesion;
	private $txtDetalleConcesion;
	private $intIdDestinoTierra;
    private $txtNombres;
    private $txtApellido;
	private $intIdPoblador;
	
	
//------------------------------------------------------------------------------------

	public function db_connect()
	{
		$config = Config::singleton();
		$this->Conexion_ID=mysql_connect($config->get('dbhost'),$config->get('dbuser'), $config->get('dbpass'));
		// $this->Conexion_ID=mysql_connect("localhost","root","");
		if (!$this->Conexion_ID) 
		{
            die('Ha fallado la conexi�n: ' . mysql_error());
            return 0;
        }
        //seleccionamos la base de datos
        if (!@mysql_select_db($config->get('dbname'),$this->Conexion_ID)) 
		{
            echo "Imposible abrir " . $config->get('dbname') ;
            return 0;
        }
        return $this->Conexion_ID;
	}
	
	public function __construct()
	{
	  $this->db_connect();
	}


// ------------------------------------------------------------------------------------

    public function getIdSolicitud()
	{
	    return $this->intIdSolicitud;
	} 

    public function putIdSolicitud($parIdSolicitud)
	{
	    $this->intIdSolicitud = $parIdSolicitud;
	} 

	
// ------------------------------------------------------------------------------------

    public function getIdSolicitudUrbana()
	{
	    return $this->intIdSolicitudUrbana;
	} 

    public function putIdSolicitudUrbana($parIdSolicitudUrbana)
	{
	    $this->intIdSolicitudUrbana = $parIdSolicitudUrbana;
	} 
// ------------------------------------------------------------------------------------
	
    public function getIdTierraUrbana()
	{
	    return $this->intIdTierraUrbana;
	} 

    public function putIdTierraUrbana($parIdTierraUrbana)
	{
	    $this->intIdTierraUrbana = $parIdTierraUrbana;
	} 

// ------------------------------------------------------------------------------------

    public function getIdUsrCreador()
	{
	    return $this->intIdUsrCreador;
	} 

    public function putIdUsrCreador($parIdUsrCreador)
	{
	    $this->intIdUsrCreador = $parIdUsrCreador;
	} 

// ------------------------------------------------------------------------------------

    public function getIdUsrMod()
	{
	    return $this->intIdUsrMod;
	} 

    public function putIdUsrMod($parIdUsrMod)
	{
	    $this->intIdUsrMod = $parIdUsrMod;
	} 
// ------------------------------------------------------------------------------------

    public function getUsrMod()
	{
	    return $this->intUsrMod;
	} 

    public function putUsrMod($parUsrMod)
	{
	    $this->intUsrMod = $parUsrMod;
	} 

// ------------------------------------------------------------------------------------

    public function getUltMod()
	{
	    return $this->fecUltMod;
	} 

    public function putUltMod($parUltMod)
	{
	    $this->fecUltMod = $parUltMod;
	} 
	
// ------------------------------------------------------------------------------------
	
    public function getTierrasDeclaradas()
	{
		return 	$this->txtTierrasDeclaradas;
	} 
    
	public function putTierrasDeclaradas($parTierrasDeclaradas)
	{
	    $this->txtTierrasDeclaradas = $parTierrasDeclaradas;
	} 


// ------------------------------------------------------------------------------------
	
    public function getActividadLaboral()
	{
		return 	$this->txtActividadLaboral;
	} 
    
	public function putActividadLaboral($parActividadLaboral)
	{
	    $this->txtActividadLaboral = $parActividadLaboral;
	} 

// ------------------------------------------------------------------------------------

	public function getTieneConcesion()
	{
		return $this->booTieneConcesion;
	}
	
	public function putTieneConcesion($parTieneConcesion)
	{
		$this->booTieneConcesion = $parTieneConcesion;
	}
	
// ------------------------------------------------------------------------------------

	public function getDetalleConcesion()
	{
		return $this->txtDetalleConcesion;
	}
	
	public function putDetalleConcesion($parDetalleConcesion)
	{
		$this->txtDetalleConcesion = $parDetalleConcesion;
	}

// ------------------------------------------------------------------------------------

	public function getIdDestinoTierra()
	{
		return $this->intIdDestinoTierra;
	}
	
	public function putIdDestinoTierra($parIdDestinoTierra)
	{
		$this->intIdDestinoTierra = $parIdDestinoTierra;
	}

// ------------------------------------------------------------------------------------

    public function getApellido()
	{
	    return $this->txtApellido;
	} 

    public function putApellido($parApellido)
	{
	    $this->txtApellido = $parApellido;
	}

// ------------------------------------------------------------------------------------

    public function getNombres()
	{
	    return $this->txtNombres;
	} 

    public function putNombres($parNombres)
	{
	    $this->txtNombres = $parNombres;
	}	
// ------------------------------------------------------------------------------------

    public function getIdPoblador()
	{
	    return $this->intIdPoblador;
	} 

    public function putIdPoblador($parIdPoblador)
	{
	    $this->intIdPoblador = $parIdPoblador;
	} 

	
// ------------------------------------------------------------------------------------

	public function listadoTotal() 
	//retorna la consulta de todas los solicitudesurbanas
	{
		$query = ('SELECT solicitudesurbanas.*, solicitudes.nroexpediente, solicitudes.anioexpediente,solicitudes.letraexpediente,solicitudes.idestadosolicitud, solicitudes.superficie,pobladores.apellido, pobladores.nombres, estadosolicitud.descripcion as estado FROM solicitudesurbanas,solicitudes, pobladores,estadosolicitud WHERE solicitudesurbanas.idsolicitud=solicitudes.id && solicitudes.idpoblador=pobladores.id && solicitudes.idestadosolicitud=estadosolicitud.id');
		$result_all = mysql_query($query);
		if($result_all)
		{
			while ($varsol = mysql_fetch_object($result_all))
			{
	 			//llenar el array 
				$arrSolicitudUrbana[] = array("id"=>$varsol->idsolicitud, // ACA DEVUELVE EN ID EL ID DE SOLICITUD NO DE SOLICITUD URBANA
							"apellido"=>$varsol->apellido,
							"nombres"=>$varsol->nombres,
							"estado"=>$varsol->estado, 
							"idestadosolicitud"=>$varsol->idestadosolicitud,
 							"nroexpediente"=>$varsol->nroexpediente,
   	 	 	 			    "anioexpediente"=>$varsol->anioexpediente,
							"letraexpediente"=>$varsol->letraexpediente,
							"superficie"=>$varsol->superficie,
							);
			} 
		}
		return($arrSolicitudUrbana);	
	}
	
// ------------------------------------------------------------------------------------	
	public function traersolicitudurbana()
	//retorna los datos de un solicitud urbana a partir de un id de  solicitud 
{	   
	   
		$query = ("SELECT solicitudesurbanas.* FROM solicitudesurbanas WHERE solicitudesurbanas.id='$this->intIdSolicitudUrbana' ");
        $result_all=mysql_query($query);
		$num_rows = mysql_affected_rows();
	  
       if($result_all && $num_rows>0 ){
      
	    $this->cargarresultados($result_all);
       return(true);	            
      } 
	  	
	  else{
	  return(false);	
	  }
		
       
		
	}
// ------------------------------------------------------------------------------------	
	public function traersolicitudurbanaasociada()
	//retorna los datos de un solicitud urbana a partir de un id de  solicitud 
{	   
		$query = ("SELECT solicitudesurbanas.* FROM solicitudesurbanas WHERE solicitudesurbanas.idsolicitud='$this->intIdSolicitud' ");
        $result_all=mysql_query($query);			 
        
        if($result_all ){
      
	      $this->cargarresultados($result_all);
          return(true);	            
        } 
	  	
	    else{
	      return(false);	
	    }
		
       
		
	}
		
// ------------------------------------------------------------------------------------	
     public function borrarsolicitudurbana()
     {	
     
	  
      $query=("DELETE FROM solicitudesurbanas WHERE id = '$this->intIdSolicitudUrbana'");
      $result_all=mysql_query($query);
	  $num_rows = mysql_affected_rows();
	  
       if($result_all && $num_rows>0 ){
	      
          return(true);
	   }
	   else{
	      return(false);
	   }	  

	  }
	   

     	
// ------------------------------------------------------------------------------------
     public function modificarsolicitudurbana()
     {
	  
	 
    
       $query = ("UPDATE solicitudesurbanas SET idsolicitud='$this->intIdSolicitud',idusrmod='$this->intIdUsrMod',fechaultmod=CURRENT_DATE, tierrasdeclaradas='$this->txtTierrasDeclaradas', actividadlaboral='$this->txtActividadLaboral',tieneconcesion='$this->booTieneConcesion', detalleconcesion='$this->txtDetalleConcesion',iddestinotierra=$this->intIdDestinoTierra  WHERE id = '$this->intIdSolicitudUrbana' ");

      $result_all=mysql_query($query);
	  $num_rows = mysql_affected_rows();
          if($result_all){
	               return(true);
           }
	       else{
	             return(false);
	       }
      
}



// ------------------------------------------------------------------------------------
     public function altasolicitudurbana()
     {

     
     $query = ("INSERT INTO solicitudesurbanas ( idsolicitud,idusrcreador,idusrmod,fechaultmod, tierrasdeclaradas,actividadlaboral, tieneconcesion,detalleconcesion, iddestinotierra,idtierraurbana ) VALUES ('$this->intIdSolicitud','$this->intIdUsrCreador','$this->intIdUsrMod',CURRENT_DATE,'$this->txtTierrasDeclaradas', '$this->txtActividadLaboral','$this->booTieneConcesion','$this->txtDetalleConcesion', $this->intIdDestinoTierra, $this->intIdTierraUrbana)");
    
	$result_all=mysql_query($query);
	
	
     if($result_all){
	
       return(true);
      }
	  else{
	  
	   return(false);
	  }

  }
  

//------------------------------------------------------------------------------------------------ 
  public function cargarresultados($resultado)
	//coloca los datos del query en las variables de la clase
	{
	  
	    while ($cons = mysql_fetch_object($resultado)) {
	 		    
    $this->putIdSolicitudUrbana($cons->id);
    $this->putIdSolicitud($cons->idsolicitud);
    $this->putIdUsrCreador($cons->idusrcreador);
    $this->putIdUsrMod($cons->idusrmod);
    $this->putUltMod(fechaacadena($cons->fechaultmod));
	$this->putTierrasDeclaradas($cons->tierrasdeclaradas);
	$this->putActividadLaboral($cons->actividadlaboral);
	$this->putTieneConcesion($cons->tieneconcesion);
	$this->putDetalleConcesion($cons->detalleconcesion);
	$this->putIdDestinoTierra($cons->iddestinotierra);
    $this->putNombres($cons->nombres);
    $this->putApellido($cons->apellido);

 
		
		}
       
		
       
		
	}
	
}
?>