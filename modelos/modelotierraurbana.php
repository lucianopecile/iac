<?php


class ModeloTierraUrbana
{
		private $intIdTierraUrbana;
		private $intIdLocalidad; 				
		private $intIdZonaUrbana;			
		private $intNroZona;			
		private $intNroSolar;				
		private $txtFraccion;
		private $txtObservacion;	
		private $intIdSolicitudUrbana;



    
	public function db_connect()
	{
		$config = Config::singleton();

		$this->Conexion_ID=mysql_connect($config->get('dbhost'),$config->get('dbuser'), $config->get('dbpass'));
		// $this->Conexion_ID=mysql_connect("localhost","root","");
  
		if (!$this->Conexion_ID) 
		{
            die('Ha fallado la conexi�n: ' . mysql_error());
            return 0;
        }
        //seleccionamos la base de datos
        if (!@mysql_select_db($config->get('dbname'),$this->Conexion_ID)) 
		{
            echo "Imposible abrir " . $config->get('dbname') ;
            return 0;
        }

        return $this->Conexion_ID;
	}
	
	
	
	public function __construct()
	{
		$this->db_connect();
	}
	
	
// ------------------------------------------------------------------------------------
	
    public function getIdTierraUrbana()
	{
	    return $this->intIdTierraUrbana;
	} 

    public function putIdTierraUrbana($parIdTierraUrbana)
	{
	    $this->intIdTierraUrbana = $parIdTierraUrbana;
	} 
// ------------------------------------------------------------------------------------

    public function getIdSolicitudUrbana()
	{
	    return $this->intIdSolicitudUrbana;
	} 

    public function putIdSolicitudUrbana($parIdSolicitudUrbana)
	{
	    $this->intIdSolicitudUrbana = $parIdSolicitudUrbana;
	} 


// ------------------------------------------------------------------------------------

    public function getIdLocalidad()
	{
	    return $this->intIdLocalidad;
	} 

    public function putIdLocalidad($parIdLocalidad)
	{
	    $this->intIdLocalidad = $parIdLocalidad;
	}
	
// ------------------------------------------------------------------------------------

    public function getIdZonaUrbana()
	{
	    return $this->intIdZonaUrbana;
	} 

    public function putIdZonaUrbana($parIdZonaUrbana)
	{
	    $this->intIdZonaUrbana = $parIdZonaUrbana;
	} 


	
// ------------------------------------------------------------------------------------

    public function getNroZona()
	{
	    return $this->intNroZona;
	} 

    public function putNroZona($parNroZona)
	{
	    $this->intNroZona = $parNroZona;
	}
// ------------------------------------------------------------------------------------

    public function getNroSolar()
	{
	    return $this->intNroSolar;
	} 

    public function putNroSolar($parNroSolar)
	{
	    $this->intNroSolar = $parNroSolar;
	}

// ------------------------------------------------------------------------------------

    public function getFraccion()
	{
	    return $this->txtFraccion;
	} 

    public function putFraccion($parFraccion)
	{
	    $this->txtFraccion = $parFraccion;
	}

// ------------------------------------------------------------------------------------

    public function getObservacion()
	{
	    return $this->txtObservacion;
	} 

    public function putObservacion($parObservacion)
	{
	    $this->txtObservacion = $parObservacion;
	}


//====================================================================================================
	
	public function listadototal() 
    //retorna la consulta de todos los valores de la tierra
	{
    	$query = ('SELECT tierrasurbanas.* FROM tierrasurbanas ORDER BY id');
	
    	$result_all= mysql_query($query);
      
		while ($varvt = mysql_fetch_object($result_all))
		{
	 		//llenar el array 
			$arrTierraUrbana[] = array("id"=>$varvt->id,
			                        "idlocalidad"=>$varvt->idlocalidad,
 									"idzonaurbana"=>$varvt->idzonaurbana,									                                    "nrozona"=>$varvt->nrozona,
									"nrosolar"=>$varvt->nrosolar,
									"fraccion"=>$varvt->fraccion,
									"observacion"=>$varvt->observacion
												
							 	);
		} 
		return($arrTierraUrbana);	
	}
	


//==================================================================================================== 
	
	public function traertierrasurbanas()
	//retorna la tierra urbana  a partir de un id 
	{
		$query = ("SELECT tierrasurbanas.* FROM tierrasurbanas WHERE tierrasurbanas.id = '$this->intIdTierraUrbana'");
	     
        $result_all = mysql_query($query);
		$num_rows = mysql_affected_rows();
	  
		if($result_all && $num_rows > 0)
		{
      		$this->cargarresultados($result_all);
			return(true);	            
      	} else {
	  		return(false);	
	  	}
	}
	
	
	
	// ------------------------------------------------------------------------------------	
	public function traertierraurbanaasociada()
	//retorna los datos de un solicitud rural a partir de un id de  solicitud 
{	   
		$query = ("SELECT solicitudesurbanas.idtierraurbana,tierrasurbanas.* FROM tierrasurbanas,solicitudesurbanas WHERE tierrasurbanas.id=solicitudesurbanas.idtierraurbana && solicitudesurbanas.id='$this->intIdSolicitudUrbana' ");
        $result_all=mysql_query($query);			 
        if($result_all ){
      
	      $this->cargarresultados($result_all);
          return(true);	            
        } 
	  	
	    else{
	      return(false);	
	    }
		
       
		
	}

	
//====================================================================================================
	
	public function borrartierraurbana()
	{	
		$query=("DELETE FROM tierrasurbanas WHERE id = '$this->intIdTierraUrbana'");
		$result_all=mysql_query($query);
		$num_rows = mysql_affected_rows();
		return ($result_all && $num_rows > 0);
	}
	   
     	
//====================================================================================================	 

	public function modificartierraurbana()
	{
		$query = ("UPDATE tierrasurbanas SET idlocalidad='$this->intIdLocalidad', idzonaurbana='$this->intIdZonaUrbana', nrozona='$this->intNroZona',nrosolar='$this->intNroSolar', fraccion='$this->txtFraccion',observacion='$this->txtObservacion' WHERE id = '$this->intIdTierraUrbana'");
		
		$result_all = mysql_query($query);
		return($result_all );
	}



//==================================================================================================== 

	public function altatierraurbana()
	{
		$query = ("INSERT INTO tierrasurbanas (idlocalidad, idzonaurbana, nrozona, nrosolar, fraccion, observacion) VALUES ($this->intIdLocalidad,'$this->intIdZonaUrbana', '$this->intNroZona','$this->intNroSolar','$this->txtFraccion','$this->txtObservacion')");
		$result_all = mysql_query($query);
	    	 if($result_all)
	    {
	    	define('IDTU',mysql_insert_id()); /* obtengo el id del ultimo insert en la DB */
	    	return IDTU;
	    } else {
			return 0;	    	
	    }
	    
	}

  
//==================================================================================================== 
   
	public function cargarresultados($resultado)
	//coloca los datos del query en las variables de la clase
	{
		
		while ($cons = mysql_fetch_object($resultado))
		{
			$this->putIdTierraUrbana($cons->id);
			$this->putIdLocalidad($cons->idlocalidad);
			$this->putIdZonaUrbana($cons->idzonaurbana);
			$this->putNroZona($cons->nrozona);
			$this->putNroSolar($cons->nrosolar);
			$this->putFraccion($cons->fraccion);
			$this->putObservacion($cons->observacion);

		}
	}
	


}
?>