<?php


class ModeloEstadoCivil
{
	private $intId;
	private $txtDescripcion;

    
// ------------------------------------------------------------------------------------
	
	public function db_connect()
	{
		$config = Config::singleton();
		$this->Conexion_ID=mysql_connect($config->get('dbhost'),$config->get('dbuser'), $config->get('dbpass'));
		//$this->Conexion_ID=mysql_connect("localhost","root","");
  
		if (!$this->Conexion_ID) 
		{
            die('Ha fallado la conexi�n: ' . mysql_error());
            return 0;
        }
        //seleccionamos la base de datos
        if (!@mysql_select_db($config->get('dbname'),$this->Conexion_ID)) 
		{
            echo "Imposible abrir " . $config->get('dbname') ;
            return 0;
        }
 
        return $this->Conexion_ID;
  
	}
	
// ------------------------------------------------------------------------------------
	
	public function __construct()
	{
		$this->db_connect();
    }
    
// ------------------------------------------------------------------------------------

    public function getId()
	{
	    return $this->intId;
	}
	 
    public function putId($parId)
	{
	    $this->intId =$parId;
	} 

// ------------------------------------------------------------------------------------

    public function getDescripcion()
	{
	    return $this->txtDescripcion;
	} 
	
    public function putDescripcion($parDescripcion)
	{
	    $this->txtDescripcion =$parDescripcion;
	} 

//-----------------------------------------------------------------------------

	public function traertodos() 
    //retorna la consulta de todos los estados civiles posibles
	{
		$query = ('SELECT * FROM estadocivil ORDER BY descripcion');
		$result_all = mysql_query($query);
		while ($vartd = mysql_fetch_object($result_all))
		{
	 		//llenar el array 
			$arrestadocivil[]=array($vartd->id,
   									$vartd->descripcion);
		} 
		return $arrestadocivil;
	}	

//============================================================================
	public function traerestadocivil()
	// 
    { 
		$idestadocivil = $this->getId();
		$query = ("SELECT estadocivil.* FROM estadocivil WHERE estadocivil.id='$idestadocivil'");
		$result_all = mysql_query($query);
 
		if($result_all)
		{
			$this->cargarresultados($result_all);
			return(true);
		} else {
			//TODO borrar el echo y poner otra cosa mejor
			echo "No se encontro el estado civil requerido";
			return(false);
		}
	}	 

//============================================================================

	public function listadototal()  
    //retorna la consulta de todas las tasas de interes
	{
		$query = ("SELECT estadocivil.* FROM estadocivil");
		$result_all=mysql_query($query);

		while ($vartd = mysql_fetch_object($result_all))
		{
			$arrestadocivil[]=array("id"=>$vartd->id,
   									"descripcion"=>$vartd->descripcion);
		} 
		return $arrestadocivil;
	}	

//============================================================================

	public function borrarestadocivil()
	{	
		$query = ("DELETE FROM estadocivil WHERE id = '$this->intId' && id not in(select pobladores.idestadocivil from pobladores)");
		$result_all = mysql_query($query);
		$num_rows = mysql_affected_rows();
		return($result_all && $num_rows > 0);
	}
     	
//============================================================================

	public function modificarestadocivil()
	{
		$query = ("UPDATE estadocivil SET descripcion='$this->txtDescripcion' WHERE id = '$this->intId'");
		$result_all=mysql_query($query);
		$num_rows = mysql_affected_rows();
		return($result_all);
	}

//============================================================================

	public function altaestadocivil()
	{
		$query = ("INSERT INTO estadocivil (descripcion) VALUES ('$this->txtDescripcion')");
		$result_all = mysql_query($query);
		return($result_all);
	}
				
//============================================================================

	public function setvariables()
	//pone a cero y vacio todas las variables de la clase
	{
		$this->putId(0);
		$this->putDescripcion("");
	}

//============================================================================

	public function cargarresultados($resultado)
	//coloca los datos del query en las variables de la clase
	{
		$this->setvariables();
		
		while ($cons = mysql_fetch_object($resultado))
		{
			$this->putId($cons->id);
			$this->putDescripcion($cons->descripcion);
		}
	}
	
	
}
?>