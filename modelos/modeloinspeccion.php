<?php

class ModeloInspeccion 
{
	private $intIdInspeccion;
	private $intIdSolicitud;
	private $fecFechaInspeccion;
	private $intSuperficie;
	private $intIdUnidadSup;
	private $intSupInvernada;
	private $intIdUnidadSupInv;
	private $intSupVeranada;
	private $intIdUnidadSupVer;
	private $txtCapacidadGanadera;
	private $txtDistanciaEmbarque;
	private $intValorHectarea;
	private $intIdEmbarque;
	private $intIdZonaAgro;
	private $booEspejoAgua;
	private $booRioArroyo;
	private $booAltasCumbres;
	private $booCentroUrbano;
	private $booAccesos;
	private $txtObservacion;

//------------------------------------------------------------------------------------

	public function db_connect()
	{
		$config = Config::singleton();
		$this->Conexion_ID=mysql_connect($config->get('dbhost'),$config->get('dbuser'), $config->get('dbpass'));
		// $this->Conexion_ID=mysql_connect("localhost","root","");
		if (!$this->Conexion_ID) 
		{
            die('Ha fallado la conexi�n: ' . mysql_error());
            return 0;
        }
        //seleccionamos la base de datos
        if (!@mysql_select_db($config->get('dbname'),$this->Conexion_ID)) 
		{
            echo "Imposible abrir " . $config->get('dbname') ;
            return 0;
        }
        return $this->Conexion_ID;
	}
	
	public function __construct()
	{
	  $this->db_connect();
	}

// ------------------------------------------------------------------------------------

    public function getIdInspeccion()
	{
	    return $this->intIdInspeccion;
	} 

    public function putIdInspeccion($parIdInspeccion)
	{
	    $this->intIdInspeccion = $parIdInspeccion;
	} 
	
// ------------------------------------------------------------------------------------

    public function getIdSolicitud()
	{
	    return $this->intIdSolicitud;
	} 

    public function putIdSolicitud($parIdSolicitud)
	{
	    $this->intIdSolicitud = $parIdSolicitud;
	} 

// ------------------------------------------------------------------------------------

    public function getFechaInspeccion()
	{
	    return $this->fecFechaInspeccion;
	} 

    public function putFechaInspeccion($parFechaInspeccion)
	{
	    $this->fecFechaInspeccion = $parFechaInspeccion;
	}

// ------------------------------------------------------------------------------------

    public function getSuperficie()
	{
	    return $this->intSuperficie;
	} 

    public function putSuperficie($parSuperficie)
	{
	    $this->intSuperficie = $parSuperficie;
	}

// ------------------------------------------------------------------------------------

    public function getIdUnidadSup()
	{
	    return $this->intIdUnidadSup;
	} 

    public function putIdUnidadSup($parIdUnidadSup)
	{
	    $this->intIdUnidadSup = $parIdUnidadSup;
	}

// ------------------------------------------------------------------------------------

    public function getSupInvernada()
	{
	    return $this->intSupInvernada;
	} 

    public function putSupInvernada($parSupInvernada)
	{
	    $this->intSupInvernada = $parSupInvernada;
	}

// ------------------------------------------------------------------------------------

    public function getIdUnidadSupInv()
	{
	    return $this->intIdUnidadSupInv;
	} 

    public function putIdUnidadSupInv($parIdUnidadSupInv)
	{
	    $this->intIdUnidadSupInv = $parIdUnidadSupInv;
	}

// ------------------------------------------------------------------------------------

    public function getSupVeranada()
	{
	    return $this->intSupVeranada;
	} 

    public function putSupVeranada($parSupVernada)
	{
	    $this->intSupVeranada = $parSupVernada;
	}

// ------------------------------------------------------------------------------------

    public function getIdUnidadSupVer()
	{
	    return $this->intIdUnidadSupVer;
	} 

    public function putIdUnidadSupVer($parIdUnidadSupVer)
	{
	    $this->intIdUnidadSupVer = $parIdUnidadSupVer;
	}

// ------------------------------------------------------------------------------------

    public function getCapacidadGanadera()
	{
	    return $this->txtCapacidadGanadera;
	} 

    public function putCapacidadGanadera($parCapacidadGanadera)
	{
	    $this->txtCapacidadGanadera = $parCapacidadGanadera;
	}
	
// ------------------------------------------------------------------------------------

    public function getDistanciaEmbarque()
	{
	    return $this->txtDistanciaEmbarque;
	} 

    public function putDistanciaEmbarque($parDistanciaEmbarque)
	{
	    $this->txtDistanciaEmbarque = $parDistanciaEmbarque;
	}
	
// ------------------------------------------------------------------------------------

    public function getValorHectarea()
	{
	    return $this->intValorHectarea;
	} 

    public function putValorHectarea($parValorHectarea)
	{
	    $this->intValorHectarea= $parValorHectarea;
	}
	
// ------------------------------------------------------------------------------------

    public function getIdEmbarque()
	{
	    return $this->intIdEmbarque;
	} 

    public function putIdEmbarque($parIdEmbarque)
	{
	    $this->intIdEmbarque = $parIdEmbarque;
	}

// ------------------------------------------------------------------------------------

    public function getIdZonaAgro()
	{
	    return $this->intIdZonaAgro;
	} 

    public function putIdZonaAgro($parIdZonaAgro)
	{
	    $this->intIdZonaAgro = $parIdZonaAgro;
	}

// ------------------------------------------------------------------------------------

    public function getEspejoAgua()
	{
	    return $this->booEspejoAgua;
	} 

    public function putEspejoAgua($parEspejoAgua)
	{
	    $this->booEspejoAgua = $parEspejoAgua;
	}

// ------------------------------------------------------------------------------------

    public function getRioArroyo()
	{
	    return $this->booRioArroyo;
	} 

    public function putRioArroyo($parRioArroyo)
	{
	    $this->booRioArroyo = $parRioArroyo;
	}

// ------------------------------------------------------------------------------------

    public function getAltasCumbres()
	{
	    return $this->booAltasCumbres;
	} 

    public function putAltasCumbres($parAltasCumbres)
	{
	    $this->booAltasCumbres = $parAltasCumbres;
	}

// ------------------------------------------------------------------------------------

    public function getCentroUrbano()
	{
	    return $this->booCentroUrbano;
	} 

    public function putCentroUrbano($parCentroUrbano)
	{
	    $this->booCentroUrbano= $parCentroUrbano;
	}
	
// ------------------------------------------------------------------------------------

    public function getAccesos()
	{
	    return $this->booAccesos;
	} 

    public function putAccesos($parAccesos)
	{
	    $this->booAccesos= $parAccesos;
	}
	
// ------------------------------------------------------------------------------------
	
    public function getObservacion()
	{
		return 	$this->txtObservacion;
	} 
    
	public function putObservacion($parObservacion)
	{
	    $this->txtObservacion = $parObservacion;
	} 

// ------------------------------------------------------------------------------------

	public function listadoTotal() 
	//retorna la consulta de todas las inspecciones
	{
		$query = ("SELECT inspeccion.*, unidades.descripcion as unidad FROM inspeccion, unidades WHERE inspeccion.idunidad=unidades.id");
		$result_all = mysql_query($query);
		if($result_all)
		{
			$zonaagro = new ModeloZonasagro();
			$embarque = new ModeloPuntoEmbarque();
			while ($varin = mysql_fetch_object($result_all))
			{
				$zonaagro->putId($varin->idzonaagro);
				$zonaagro->traerZonaAgro();
				$embarque->putId($varin->idpuntoembarque);
				$embarque->traerPuntoEmbarque();
				
	 			//llenar el array 
				$arrInspecciones[] = array("id"=>$varin->id,
                        	"fechainspeccion"=>fechaACadena($varin->fechainspeccion),
							"superficie"=>$varin->superficie,
							"unidad"=>$varin->unidad,
							"zonaagro"=>$zonaagro->getDescripcion(),
							"puntoembarque"=>$embarque->getDescripcion(),
							"capacidadganadera"=>$varin->capacidadganadera,
							"distanciaembarque"=>$varin->distanciaembarque);
			} 
		}
		return($arrInspecciones);	
	}
	
// ------------------------------------------------------------------------------------	

	public function traerInspeccion()
	//retorna los datos de una inspeccion particular a partir de su id
	{
		$query = ("SELECT inspeccion.* FROM inspeccion WHERE inspeccion.id='$this->intIdInspeccion'");
		
		$result_all = mysql_query($query);
	  
		if($result_all)
		{
			$this->cargarresultados($result_all);
			return(true);	            
		} else {
			return(false);	
		}
	}

// ------------------------------------------------------------------------------------	

	public function traerInspeccionAsociada()
	//retorna los datos de una inspeccion en base a la solicitud rural asociada
	{
		$query = ("SELECT inspeccion.* FROM inspeccion WHERE inspeccion.idsolicitudrural='$this->intIdSolicitud'");
		
		$result_all = mysql_query($query);
		$num_rows = mysql_affected_rows();
	  
		if($result_all && $num_rows > 0)
		{
			$this->cargarresultados($result_all);
			return(true);	            
		} else {
			return(false);	
		}
	}

// ------------------------------------------------------------------------------------	
	
	public function borrarinspeccion()
	{	
		$query=("DELETE FROM inspeccion WHERE id = '$this->intIdInspeccion'");
		$result_all=mysql_query($query);
		$num_rows = mysql_affected_rows();
		return ($result_all && $num_rows > 0);
	}
     	
// ------------------------------------------------------------------------------------
	
	public function modificarinspeccion()
	{
		$query = ("UPDATE inspeccion SET idsolicitudrural='$this->intIdSolicitud',fechainspeccion='$this->fecFechaInspeccion',superficie='$this->intSuperficie',
				idunidad=$this->intIdUnidadSup,idpuntoembarque=$this->intIdEmbarque,capacidadganadera='$this->txtCapacidadGanadera',distanciaembarque='$this->txtDistanciaEmbarque',
				valorhectarea='$this->intValorHectarea',supinvernada='$this->intSupInvernada',idunidadinv=$this->intIdUnidadSupInv,supveranada='$this->intSupVeranada',
				idunidadver=$this->intIdUnidadSupVer,idzonaagro=$this->intIdZonaAgro,espejosagua='$this->booEspejoAgua',riosarroyos='$this->booRioArroyo',
				altascumbres='$this->booAltasCumbres',centrosurbanos='$this->booCentroUrbano',accesos='$this->booAccesos',observacion='$this->txtObservacion'
				WHERE id = '$this->intIdInspeccion'");
		$result_all = mysql_query($query);
		return ($result_all);
	}

// ------------------------------------------------------------------------------------

	public function altainspeccion()
	{
		$query = ("INSERT INTO inspeccion (idsolicitudrural,fechainspeccion,superficie,idunidad,idpuntoembarque,capacidadganadera,distanciaembarque,valorhectarea,supinvernada,idunidadinv,supveranada,idunidadver,idzonaagro,espejosagua,riosarroyos,altascumbres,centrosurbanos,accesos,observacion)
				VALUES ('$this->intIdSolicitud','$this->fecFechaInspeccion','$this->intSuperficie',$this->intIdUnidadSup,$this->intIdEmbarque,'$this->txtCapacidadGanadera','$this->txtDistanciaEmbarque','$this->intValorHectarea','$this->intSupInvernada',$this->intIdUnidadSupInv,
				'$this->intSupVeranada',$this->intIdUnidadSupVer,$this->intIdZonaAgro,'$this->booEspejoAgua','$this->booRioArroyo','$this->booAltasCumbres','$this->booCentroUrbano','$this->booAccesos','$this->txtObservacion')");
		$result_all = mysql_query($query);
		$num_rows = mysql_affected_rows();
		return ($result_all && $num_rows > 0);
	}

//------------------------------------------------------------------------------------------------ 

	public function cargarresultados($resultado)
	//coloca los datos del query en las variables de la clase
	{
	    while ($cons = mysql_fetch_object($resultado))
		{
			$this->putIdInspeccion($cons->id);
			$this->putIdSolicitud($cons->idsolicitudrural);
			$this->putFechaInspeccion($cons->fechainspeccion);
			$this->putSuperficie($cons->superficie);
			$this->putIdUnidadSup($cons->idunidad);
	        $this->putSupInvernada($cons->supinvernada);
	        $this->putIdUnidadSupInv($cons->idunidadinv);
	        $this->putSupVeranada($cons->supveranada);
	        $this->putIdUnidadSupVer($cons->idunidadver);
			$this->putIdEmbarque($cons->idpuntoembarque);
			$this->putCapacidadGanadera($cons->capacidadganadera);
			$this->putDistanciaEmbarque($cons->distanciaembarque);
			$this->putValorHectarea($cons->valorhectarea);
			$this->putIdZonaAgro($cons->idzonaagro);
			$this->putEspejoAgua($cons->espejosagua);
			$this->putRioArroyo($cons->riosarroyos);
			$this->putAltasCumbres($cons->altascumbres);
			$this->putCentroUrbano($cons->centrosurbanos);
			$this->putAccesos($cons->accesos);
			$this->putObservacion($cons->observacion);
		}
	}

}
?>