<?php


class ModeloTierraRural
{
		private $intIdTierraRural;
		private $intIdSolicitudRural;
		private $intIdSeccion; 				
		private $intIdFraccion;			
		private $intIdColonia;			
		private $intIdEnsanche;				
		private $intNroLote;
		private $intIdLegua;
		private $txtObservacion;	



    
	public function db_connect()
	{
		$config = Config::singleton();

		$this->Conexion_ID=mysql_connect($config->get('dbhost'),$config->get('dbuser'), $config->get('dbpass'));
		// $this->Conexion_ID=mysql_connect("localhost","root","");
  
		if (!$this->Conexion_ID) 
		{
            die('Ha fallado la conexi�n: ' . mysql_error());
            return 0;
        }
        //seleccionamos la base de datos
        if (!@mysql_select_db($config->get('dbname'),$this->Conexion_ID)) 
		{
            echo "Imposible abrir " . $config->get('dbname') ;
            return 0;
        }

        return $this->Conexion_ID;
	}
	
	
	
	public function __construct()
	{
		$this->db_connect();
	}
	
	
// ------------------------------------------------------------------------------------
	
    public function getIdTierraRural()
	{
	    return $this->intIdTierraRural;
	} 

    public function putIdTierraRural($parIdTierraRural)
	{
	    $this->intIdTierraRural = $parIdTierraRural;
	} 
// ------------------------------------------------------------------------------------

    public function getIdSolicitudRural()
	{
	    return $this->intIdSolicitudRural;
	} 

    public function putIdSolicitudRural($parIdSolicitudRural)
	{
	    $this->intIdSolicitudRural = $parIdSolicitudRural;
	} 

// ------------------------------------------------------------------------------------

    public function getIdSeccion()
	{
	    return $this->intIdSeccion;
	} 

    public function putIdSeccion($parIdSeccion)
	{
	    $this->intIdSeccion = $parIdSeccion;
	}
	
// ------------------------------------------------------------------------------------

    public function getIdFraccion()
	{
	    return $this->intIdFraccion;
	} 

    public function putIdFraccion($parIdFraccion)
	{
	    $this->intIdFraccion = $parIdFraccion;
	} 


	
// ------------------------------------------------------------------------------------

    public function getIdColonia()
	{
	    return $this->intIdColonia;
	} 

    public function putIdColonia($parIdColonia)
	{
	    $this->intIdColonia = $parIdColonia;
	}
// ------------------------------------------------------------------------------------

    public function getIdEnsanche()
	{
	    return $this->intIdEnsanche;
	} 

    public function putIdEnsanche($parIdEnsanche)
	{
	    $this->intIdEnsanche = $parIdEnsanche;
	}

// ------------------------------------------------------------------------------------

    public function getNroLote()
	{
	    return $this->intNroLote;
	} 

    public function putNroLote($parNroLote)
	{
	    $this->intNroLote = $parNroLote;
	}
// ------------------------------------------------------------------------------------

    public function getIdLegua()
	{
	    return $this->intIdLegua;
	} 

    public function putIdLegua($parIdLegua)
	{
	    $this->intIdLegua = $parIdLegua;
	}

// ------------------------------------------------------------------------------------

    public function getObservacion()
	{
	    return $this->txtObservacion;
	} 

    public function putObservacion($parObservacion)
	{
	    $this->txtObservacion = $parObservacion;
	}


//====================================================================================================
	
	public function listadototal() 
    //retorna la consulta de todas las tierras rurales
	{
    	$query = ('SELECT tierrasrurales.* FROM tierrasrurales ORDER BY id');
	
    	$result_all= mysql_query($query);
      
		while ($varvt = mysql_fetch_object($result_all))
		{
	 		//llenar el array 
			$arrTierrasRurales[] = array("id"=>$varvt->id,
			                        "idseccion"=>$varvt->idseccion,
        							"idfraccion"=>$varvt->idfraccion,									                                    "idensanche"=>$varvt->idensanche,
									"idcolonia"=>$varvt->idcolonia,
									"nrolote"=>$varvt->nrolote,
									"idlegua"=>$varvt->idlegua,
									"observacion"=>$varvt->observacion
												
							 	);
		} 
		return($arrTierrasRurales);	
	}
	


//==================================================================================================== 
	
	public function traertierrarural()
	//retorna la tierra rural  a partir de un id 
	{
		$query = ("SELECT tierrasrurales.* FROM tierrarurales WHERE tierrarurales.id = '$this->intIdTierraRural'");
	     
        $result_all = mysql_query($query);
		$num_rows = mysql_affected_rows();
	  
		if($result_all && $num_rows > 0)
		{
      		$this->cargarresultados($result_all);
			return(true);	            
      	} else {
	  		return(false);	
	  	}
	}
// ------------------------------------------------------------------------------------	
	public function traertierraruralasociada()
	//retorna los datos de un solicitud rural a partir de un id de  solicitud 
{	   
		$query = ("SELECT solicitudesrurales.idtierrarural,tierrasrurales.* FROM tierrasrurales,solicitudesrurales WHERE tierrasrurales.id=solicitudesrurales.idtierrarural && solicitudesrurales.id='$this->intIdSolicitudRural' ");
        $result_all=mysql_query($query);			 
        if($result_all ){
      
	      $this->cargarresultados($result_all);
          return(true);	            
        } 
	  	
	    else{
	      return(false);	
	    }
		
       
		
	}
		
//====================================================================================================
	
	public function borrartierrarural()
	{	
		$query=("DELETE FROM tierrasrurales WHERE id = '$this->intIdTierraRural'");
		$result_all=mysql_query($query);
		$num_rows = mysql_affected_rows();
		return ($result_all && $num_rows > 0);
	}
	   
     	
//====================================================================================================	 

	public function modificartierrarural()
	{
		$query = ("UPDATE tierrasrurales SET idseccion=$this->intIdSeccion, idfraccion=$this->intIdFraccion, nrolote='$this->intNroLote',idensanche=$this->intIdEnsanche, idcolonia=$this->intIdColonia,idlegua=$this->intIdLegua,observacion='$this->txtObservacion' WHERE id = '$this->intIdTierraRural'");
		$result_all = mysql_query($query);
		return($result_all );
	}



//==================================================================================================== 

	public function altatierrarural()
	{
		$query = ("INSERT INTO tierrasrurales (idseccion, idfraccion, nrolote, idensanche, idcolonia, idlegua, observacion) VALUES ('$this->intIdSeccion','$this->intIdFraccion', '$this->intNroLote',$this->intIdEnsanche,$this->intIdColonia,$this->intIdLegua,'$this->txtObservacion')");
		$result_all = mysql_query($query);
		 if($result_all)
	    {
	    	define('IDTR',mysql_insert_id()); /* obtengo el id del ultimo insert en la DB */
	    	return IDTR;
	    } else {
			return 0;	    	
	    }
	    
	}

  
//==================================================================================================== 
   
	public function cargarresultados($resultado)
	//coloca los datos del query en las variables de la clase
	{
		
		while ($cons = mysql_fetch_object($resultado))
		{
			$this->putIdTierraRural($cons->id);
			$this->putIdSeccion($cons->idseccion);
			$this->putIdFraccion($cons->idfraccion);
			$this->putNroLote($cons->nrolote);
			$this->putIdColonia($cons->idcolonia);
			$this->putIdEnsanche($cons->idensanche);
			$this->putIdLegua($cons->idlegua);
			$this->putObservacion($cons->observacion);

		}
	}
	


}
?>