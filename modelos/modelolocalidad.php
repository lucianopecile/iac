<?php


class ModeloLocalidad 
{
   
    private $intIdLocalidad;
	private $intIdProvincia;
    private $intCodPos;
    private $txtDescripcion;	
    
// ------------------------------------------------------------------------------------
	
  public function db_connect()
{
        $config = Config::singleton();

    $this->Conexion_ID=mysql_connect($config->get('dbhost'),$config->get('dbuser'), $config->get('dbpass'));
  
		if (!$this->Conexion_ID) 
		{
            die('Ha fallado la conexi�n: ' . mysql_error());
            return 0;
        }
        //seleccionamos la base de datos
        if (!@mysql_select_db($config->get('dbname'),$this->Conexion_ID)) 
		{
            echo "Imposible abrir " . $config->get('dbname') ;
            return 0;
        }
 
        return $this->Conexion_ID;
}
	
// ------------------------------------------------------------------------------------
	
	public function __construct()
	{
	
	  $this->db_connect();
    }
// ------------------------------------------------------------------------------------
    public function getIdLocalidad()
	{
	    return $this->intIdLocalidad;
	} 
// ------------------------------------------------------------------------------------	
    public function putIdLocalidad($parIdLocalidad)
	{
	    $this->intIdLocalidad =$parIdLocalidad;
	} 

// ------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------
    public function getIdProvincia()
	{
	    return $this->intIdProvincia;
	} 
// ------------------------------------------------------------------------------------	
    public function putIdProvincia($parProvincia)
	{
	    $this->intIdProvincia =$parProvincia;
	} 

// ------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------
    public function getCodPos()
	{
	    return $this->intCodPos;
	} 
// ------------------------------------------------------------------------------------	
    public function putCodPos($parCodPos)
	{
	    $this->intCodPos =$parCodPos;
	} 


// ----------------------------------------------------------------------
    public function getDescripcion()
	{
	    return $this->txtDescripcion;
	} 
// ----------------------------------------------------------------------	
    public function putDescripcion($parDescripcion)
	{
	    $this->txtDescripcion =$parDescripcion;
	} 


//----------------------------------------------------------
	
	public function TraerTodosL() 
	//retorna la consulta de todas las localidades
	{
		$query = ('SELECT * FROM localidades ORDER BY descripcion');
		$result_all = mysql_query($query);
		
		while ($varloc = mysql_fetch_object($result_all)) 
		{
	 		//llenar el array 
			$arrLocalidades[] = array($varloc->id,
									$varloc->descripcion);
		} 
		return $arrLocalidades;
	}
		
//----------------------------------------------------------
	
	public function TraerTodos() 
	//retorna la consulta de todas las localidades CON las Comarcas
	{
		$query = ('SELECT * FROM localidades ORDER BY descripcion');
		$result_all = mysql_query($query);
		
		while ($varloc = mysql_fetch_object($result_all)) 
		{
	 		//llenar el array 
			$arrLocalidades[] = array($varloc->id,
									$varloc->descripcion);
		} 
		return $arrLocalidades;
	}
		
//----------------------------------------------------------
	
	public function listadoTotal() 
	//retorna la consulta de todos las localidades
	{
		$query = ('SELECT id, descripcion FROM localidades  ORDER BY descripcion');
		$result_all = mysql_query($query);
	
		while ($varcli = mysql_fetch_object($result_all))
		{
			//llenar el array 
			$arrLocalidades[] = array("id"=>$varcli->id,
									"descripcion"=>$varcli->descripcion);
		} 
		return($arrLocalidades);	
	}
	
//----------------------------------------------------------
	
	public function traerlocalidad()
	//retorna los datos de un emprendedor particular a partir de un id 
	{
		$query = ("SELECT * FROM localidades WHERE id='$this->intIdLocalidad'");
        $result_all=mysql_query($query);
		$num_rows = mysql_affected_rows();

		if($result_all && $num_rows > 0)
		{
			$this->cargarresultados($result_all);
			return(true);	            
		} else {
			return(false);	
		}
	}
	
//============================================================================
	
     public function borrarlocalidad()
     {	
     
	  
      $query=("DELETE FROM localidades WHERE id = '$this->intIdLocalidad'");
      $result_all=mysql_query($query);
	  $num_rows = mysql_affected_rows();
	  
       if($result_all && $num_rows>0 ){
	      
          return(true);
	   }
	   else{
	      return(false);
	   }	  

	  }
	   
     	
//============================================================================

     public function modificarlocalidad()
     {
	  $query = ("UPDATE localidades SET descripcion= '$this->txtDescripcion', codpost='$this->intCodPos',idprovincia=1 WHERE id = '$this->intIdLocalidad'");
      $result_all=mysql_query($query);
	  $num_rows = mysql_affected_rows();

      if($result_all){
	    	 	   return(true);
      }
	  else{
	   return(false);
	  }
}


//============================================================================


     public function altalocalidad()
     {

      
     $query = ("INSERT INTO localidades (descripcion, idprovincia, codpost) VALUES ('$this->txtDescripcion', 1,'$this->intCodPos')");
	 $result_all=mysql_query($query);
     if($result_all){
		    return(true);
		  }
          else{
	         return(false);
	      }
       
      

  }
  

//============================================================================			 
	public function setvariables()
	//pone a cero y vacio todas las variables de la clase
		{
		
	        $this->putIdLocalidad(0);
    	    $this->putDescripcion("");
        	$this->putCodPos(0);
    	    $this->putIdProvincia(0);		
		}


  	
//----------------------------------------------------------
	
	
	  public function cargarresultados($resultado)
	//coloca los datos del query en las variables de la clase
	{
	   
	   	$this->setvariables();
		
	    while ($cons = mysql_fetch_object($resultado)) {
	 		    
        $this->putIdLocalidad($cons->id);
        $this->putDescripcion($cons->descripcion);
        $this->putCodPos($cons->codpost);
        $this->putIdProvincia($cons->idprovincia);		

		}
       
		
       
		
	}
	
//============================================================================
public function TraerElegidas($parElegidas)
{

        $parArray[1]="0";
		$parArray=$parElegidas;
		$arrElemento=0;
		$condicion="";
		$primero=true;
		if (is_array($parArray)) 
		{
		    foreach($parArray as $arrElemento)
		    {
			 if($primero){
			   $primero=false;
			 }
			 else{
			  $condicion=$condicion." || ";
			 }
			  $condicion=$condicion." localidades.id=".$arrElemento;
            } 
			 
		}
		
		
		$ListaZonas=$this->ConsultaElegidas($condicion);
		
		return($ListaZonas);
}		

//============================================================================
	public function ConsultaElegidas($parCondicion) 
	{
	  if(!empty($parCondicion)){	
    	  $query =('SELECT * FROM localidades WHERE '.$parCondicion.' ORDER BY descripcion');
	  }
	  else{
	       $query =('SELECT * FROM localidades WHERE id=0   ORDER BY descripcion');

	  }	 
      $result_all=mysql_query($query);
      
	  while ($varzonas = mysql_fetch_object($result_all)) {
	 		//llenar el array 
			$arrZonas[]=array( "id"=>$varzonas->id,"descripcion"=>$varzonas->descripcion);
					
                   
      } 
	
	return($arrZonas);	
	}


}

?>