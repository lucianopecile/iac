<?php

class ModeloCuota 
{
    private $intIdCuota;
    private $intIdCuenta;
    private $intIdUsrCreador;
    private $intIdUsrMod;
    private $fecUltMod;
    private $intNroCuota;
    private $intMontoCuota;
    private $intCapital;
    private $intInteres;
    private $intSaldo;
    private $intInteresMora;
    private $fecFechaVenc;
    private $fecFechaPago;
    private $fecFechaCalculoMora;
    private $txtSolicitante;
    private $intAnioExpediente;
    private $intNroExpediente;
    private $txtLetraExpediente;
    private $intNroCuenta;
    private $txtCuentaCorriente;
    private $intValorLiquidacion;
    private $intCobrado;
    private $intCuotaFormalizacion;
    private $txtObservacion;
    private $intConceptos;


//==========================================================================================================================	
    
	public function db_connect()
	{
            $config = Config::singleton();
            $this->Conexion_ID=mysql_connect($config->get('dbhost'),$config->get('dbuser'), $config->get('dbpass'));
            if (!$this->Conexion_ID) 
            {
                    die('Ha fallado la conexi�n: ' . mysql_error());
                    return 0;
            }
            //seleccionamos la base de datos
            if (!@mysql_select_db($config->get('dbname'),$this->Conexion_ID)) 
            {
                    echo "Imposible abrir " . $config->get('dbname') ;
                    return 0;
            }
            return $this->Conexion_ID;
	}
	
//==========================================================================================================================	

	public function __construct()
	{
		$this->db_connect();
	}
	
//==========================================================================================================================

	public function getIdCuota()
	{
	    return $this->intIdCuota;
	}
	
        public function putIdCuota($parIdCuota)
	{
	    $this->intIdCuota = $parIdCuota;
	}
	 
// ------------------------------------------------------------------------------------
    
	public function getIdCuenta()
	{
	    return $this->intIdCuenta;
	}
	
        public function putIdCuenta($parIdCuenta)
	{
	    $this->intIdCuenta = $parIdCuenta;
	} 
	
// ------------------------------------------------------------------------------------

    public function getIdUsrCreador()
	{
	    return $this->intIdUsrCreador;
	} 

    public function putIdUsrCreador($parIdUsrCreador)
	{
	    $this->intIdUsrCreador = $parIdUsrCreador;
	} 
	
// ------------------------------------------------------------------------------------

    public function getIdUsrMod()
	{
	    return $this->intIdUsrMod;
	}

    public function putIdUsrMod($parIdUsrMod)
	{
	    $this->intIdUsrMod = $parIdUsrMod;
	} 

// ------------------------------------------------------------------------------------

    public function getUltMod()
	{
	    return $this->fecUltMod;
	} 

    public function putUltMod($parUltMod)
	{
	    $this->fecUltMod = $parUltMod;
	}

// ------------------------------------------------------------------------------------

    public function getNroCuota()
	{
	    return $this->intNroCuota;
	} 

    public function putNroCuota($parNroCuota)
	{
	    
	    $this->intNroCuota = $parNroCuota;
	} 	
	
// ------------------------------------------------------------------------------------

    public function getMontoCuota()
	{
	    return $this->intMontoCuota;
	}

    public function putMontoCuota($parMontoCuota)
	{
	    
	    $this->intMontoCuota = $parMontoCuota;
	}

// ------------------------------------------------------------------------------------

    public function getCapital()
	{
	    return $this->intCapital;
	}
	
    public function putCapital($parCapital)
	{
	    $this->intCapital = $parCapital;
	} 
	
// ------------------------------------------------------------------------------------

    public function getInteres()
	{
	    return $this->intInteres;
	} 

    public function putInteres($parInteres)
	{
	    $this->intInteres = $parInteres;
	} 
	
// ------------------------------------------------------------------------------------

    public function getSaldo()
	{
	    return $this->intSaldo;
	} 

    public function putSaldo($parSaldo)
	{
	    $this->intSaldo = $parSaldo;
	} 

// ------------------------------------------------------------------------------------

    public function getInteresMora()
	{
	    return $this->intInteresMora;
	} 

    public function putInteresMora($parInteresMora)
	{
	    $this->intInteresMora = $parInteresMora;
	} 
	
// ------------------------------------------------------------------------------------

    public function getFechaVenc()
	{
	    return $this->fecFechaVenc;
	} 

    public function putFechaVenc($parFechaVenc)
	{
	    $this->fecFechaVenc = $parFechaVenc;
	} 
	
// ------------------------------------------------------------------------------------

    public function getFechaPago()
	{
	    return $this->fecFechaPago;
	} 

        public function putFechaPago($parFechaPago)
	{
	    $this->fecFechaPago = $parFechaPago;
	}
// ------------------------------------------------------------------------------------

    public function getFechaCalculoMora()
	{
	    return $this->fecFechaCalculoMora;
	} 

        public function putFechaCalculoMora($parFechaCalculoMora)
	{
	    $this->fecFechaCalculoMora = $parFechaCalculoMora;
	}
	
	
// ------------------------------------------------------------------------------------

	public function getSolicitante()
	{
		return 	$this->txtSolicitante;
	} 
    
	public function putSolicitante($parSolicitante)
	{
	    $this->txtSolicitante = $parSolicitante;
	}

// ------------------------------------------------------------------------------------

	public function getAnioExpediente()
	{
		return $this->intAnioExpediente;
	}
	
	public function putAnioExpediente($parAnioExp)
	{
		$this->intAnioExpediente = $parAnioExp;
	}

// ------------------------------------------------------------------------------------

	public function getNroExpediente()
	{
		return $this->intNroExpediente;
	}
	
	public function putNroExpediente($parNroExp)
	{
		$this->intNroExpediente = $parNroExp;
	}

// ------------------------------------------------------------------------------------

	public function getLetraExpediente()
	{
		return $this->txtLetraExpediente;
	}
	
	public function putLetraExpediente($parLetraExp)
	{
		$this->txtLetraExpediente = $parLetraExp;
	}

// ------------------------------------------------------------------------------------

	public function getNroCuenta()
	{
		return $this->intNroCuenta;
	}

	public function putNroCuenta($parNroCuenta)
	{
		$this->intNroCuenta = $parNroCuenta;
	}

// ------------------------------------------------------------------------------------

	public function getCuentaCorriente()
	{
		return $this->txtCuentaCorriente;
	}
	
	public function putCuentaCorriente($parCuentaCorriente)
	{
		$this->txtCuentaCorriente = $parCuentaCorriente;
	}

// ------------------------------------------------------------------------------------

    public function getValorLiquidacion()
	{
	    return $this->intValorLiquidacion;
	}

    public function putValorLiquidacion($parValorLiquidacion)
	{
	    $this->intValorLiquidacion = $parValorLiquidacion;
	}

// ------------------------------------------------------------------------------------

	public function getCobrado()
	{
		return $this->intCobrado;
	}
	
	public function putCobrado($parCobrado)
	{
		$this->intCobrado = $parCobrado;
	}

// ------------------------------------------------------------------------------------

    public function getEsCuotaFormalizacion()
    {
        return $this->intCuotaFormalizacion;
    }
    public function putEsCuotaFormalizacion($intEsCuotaFormalizacion)
    {
        $this->intCuotaFormalizacion = $intEsCuotaFormalizacion;
    }        

// ------------------------------------------------------------------------------------

    public function getObservacion()
	{
		return 	$this->txtObservacion;
	}
    public function putObservacion($parObservacion)
	{
	    $this->txtObservacion = $parObservacion;
	}
// ------------------------------------------------------------------------------------

    public function getConceptos()
	{
	    return $this->intConceptos;
	} 

    public function putConceptos($parConceptos)
	{
	    $this->intConceptos = $parConceptos;
	} 
	

// ------------------------------------------------------------------------------------
	
    public function listadoTotal() 
    //retorna la consulta de todas las cuotas con solicitante, expediente y demas
    {
        $query = ("SELECT cuotas.*,cuentas.valorliquidacion,cuentas.idsolicitud,cuentas.nrocuenta,solicitudes.anioexpediente,solicitudes.letraexpediente,solicitudes.nroexpediente,solicitudes.idpoblador,pobladores.apellido,pobladores.nombres,cuentas.cuentacorriente
                        FROM cuotas,cuentas,pobladores,solicitudes
                        WHERE cuotas.idcuenta='$this->intIdCuenta' && cuentas.id='$this->intIdCuenta' && cuentas.idsolicitud=solicitudes.id && solicitudes.idpoblador=pobladores.id ORDER BY numerocuota ");
        //echo "$query";

        $result_all = mysql_query($query);
        if($result_all)
        {
            while ($varres = mysql_fetch_object($result_all))
            {
                //llenar el array 
                $arrCuotas[] = array("id"=>$varres->id,
                                    "idcuenta"=>$varres->idcuenta,
                                    "capital"=>$varres->capital,
                                    "nrocuota"=>$varres->numerocuota,
                                    "montocuota"=>$varres->montocuota*1,
                                    "fechavencimiento"=>$this->fechaACadena($varres->fechavencimiento),
                                    "fechapago"=>$this->fechaACadena($varres->fechapago),
                                    "saldo"=>$varres->saldo*1,
                                    "interes"=>$varres->interes*1,
                                    "cobrado"=>$varres->cobrado*1,
                                    "interesmora"=>$varres->interesmora*1,
                                    "fechacalculomora"=>$this->fechaACadena($varres->fechacalculomora),
                                    "conceptos"=>$varres->conceptos,
                                    "cuotaformalizacion"=>$varres->cuotaformalizacion,
                                );
                $this->putSolicitante($varres->apellido.", ".$varres->nombres);
                $this->putValorLiquidacion($varres->valorliquidacion*1);
                $this->putCuentaCorriente($varres->cuentacorriente);
                $this->putAnioExpediente($varres->anioexpediente);
                $this->putNroExpediente($varres->nroexpediente);
                $this->putLetraExpediente($varres->letraexpediente);
                $this->putNroCuenta($varres->nrocuenta);
            }
            return($arrCuotas);
        }else{
            return(false);
        }
    }
// ------------------------------------------------------------------------------------

    public function listadoCuotasCuenta()
    //retorna la consulta de todas las cuotas de una cuenta
    {
        $query = ("SELECT cuotas.* FROM cuotas WHERE cuenta.id='$this->intIdCuenta' ORDER BY numerocuota");
        $result_all = mysql_query($query);
        if($result_all)
        {
            while ($varres = mysql_fetch_object($result_all))
            {
                //llenar el array 	  
                $arrCuotas[] = array("id"=>$varres->id,
                                    "nrocuota"=>$varres->numerocuota,
                                    "montocuota"=>$varres->montocuota,
                                    "fechavencimiento"=>$this->fechaACadena($varres->fechavencimiento),
                                    "fechapago"=>$this->fechaACadena($varres->fechapago),
                                    "saldo"=>$varres->saldo,
                                    "cobrado"=>$varres->cobrado,
                                    "interes"=>$varres->interes,
                                );
            }
            return($arrCuotas);
        }else{
            return(false);
        }
    }

// ------------------------------------------------------------------------------------

    public function listadoCuotasCuentaPagas()
    //retorna la consulta de las cuotas que tienen al menos un pago de una cuenta
    {
        $query = ("SELECT cuotas.* FROM cuotas WHERE cuotas.idcuenta='$this->intIdCuenta' && fechapago<>'0000-00-00' ORDER BY numerocuota");
        $result_all = mysql_query($query);
        if($result_all)
        {
            while ($varres = mysql_fetch_object($result_all))
            {
                    //llenar el array
                    $arrCuotas[] = array("id"=>$varres->id,
                                        "nrocuota"=>$varres->numerocuota,
                                        "montocuota"=>$varres->montocuota,
                                        "fechavencimiento"=>$this->fechaACadena($varres->fechavencimiento),
                                        "fechapago"=>$this->fechaACadena($varres->fechapago),
                                        "saldo"=>$varres->saldo,
                                        "cobrado"=>$varres->cobrado,
                                        "interes"=>$varres->interes,
                                        "interesmora"=>$varres->interesmora,
                    );
            }
            return($arrCuotas);
        }else{
                return(false);
        }
    }

// ------------------------------------------------------------------------------------

    public function listadoCuotasCuentaDeuda($condicion)
    //retorna la consulta de las cuotas que se deben de una cuenta
    {
        $query = ("SELECT * FROM cuotas WHERE idcuenta='$this->intIdCuenta' ".$condicion." ORDER BY numerocuota");
        $result_all = mysql_query($query);
        if($result_all)
        {
            while ($varres = mysql_fetch_object($result_all))
            {
                //llenar el array
                $arrCuotas[] = array("id"=>$varres->id,
                                    "nrocuota"=>$varres->numerocuota,
                                    "montocuota"=>$varres->montocuota,
                                    "fechavencimiento"=>$this->fechaACadena($varres->fechavencimiento),
                                    "fechapago"=>$this->fechaACadena($varres->fechapago),
                                    "saldo"=>$varres->saldo,
                                    "cobrado"=>$varres->cobrado,
                                    "interes"=>$varres->interes,
                                    "interesmora"=>$varres->interesmora,
                );
            }
            return($arrCuotas);
        }else{
            return(false);
        }
    }

// ------------------------------------------------------------------------------------

    public function calcularCapitalCobrado()
    //devuelve el total del capital de la cuenta cobrado incluyendo la formalizacion y la cantidad de cuotas cobradas en ese capital
    {
        $query = ("SELECT sum(capital) as capitalcobrado, count(*) as cuotaspagas FROM cuotas WHERE idcuenta='$this->intIdCuenta' && fechapago<>'0000-00-00'");
        $result_all = mysql_query($query);
        if($result_all)
        {
            $varres = mysql_fetch_object($result_all);
            $res = array("capitalcobrado"=>$varres->capitalcobrado,
                        "cantcuotas"=>$varres->cuotaspagas
                    );
            return $res;
        }else{
            return false;
        }
    }

// ------------------------------------------------------------------------------------
	
    public function traerCuota()
    //retorna los datos de un cuota particular a partir de un id 
    {
        $query = ("SELECT cuotas.*,cuentas.nrocuenta,cuentas.idsolicitud,pobladores.apellido,pobladores.nombres FROM cuotas,cuentas,pobladores,solicitudes
        WHERE cuotas.id='$this->intIdCuota' && cuentas.id=cuotas.idcuenta && cuentas.idsolicitud=solicitudes.id && pobladores.id=solicitudes.idpoblador");
        $result_all = mysql_query($query);
        $num_rows = mysql_affected_rows();
        if($result_all && $num_rows > 0)
        {
            $this->cargarresultados($result_all);
            return true;
        }else{
            return false;
        }
    }
	
// ------------------------------------------------------------------------------------	

    public function traerCuotaCuenta()
    //retorna los datos de un cuota particular a partir de un id de cuenta y un numero de cuota
    {        
        $query = ("SELECT * FROM cuotas  WHERE idcuenta='$this->intIdCuenta' && numerocuota='$this->intNroCuota'");
        $result_all = mysql_query($query);
        $num_rows = mysql_affected_rows();
        
        if($result_all && $num_rows > 0)
        {            
            $this->cargarresultados($result_all);
            return(true);	            
        }else{
            return(false);	
        }
    }	
	
// ------------------------------------------------------------------------------------	
     
    public function borrarCuota()
    {	
        $query=("DELETE FROM cuotas WHERE id = '$this->intIdCuota'");
        $result_all = mysql_query($query);
        $num_rows = mysql_affected_rows();
        return ($result_all && $num_rows > 0);
    }
     	
// ------------------------------------------------------------------------------------

    public function modificarCuota()
    {
        $query = ("UPDATE cuotas SET montocuota='$this->intMontoCuota',saldo='$this->intSaldo',idusrmod='$this->intIdUsrMod',fechaultmod=CURRENT_DATE,
                        fechavencimiento='$this->fecFechaVenc',fechapago='$this->fecFechaPago',interes='$this->intInteres',capital='$this->intCapital',
                        fechacalculomora='$this->fecFechaCalculoMora ',interesmora='$this->intInteresMora',cobrado='$this->intCobrado',
                        observacion='$this->txtObservacion' WHERE id = '$this->intIdCuota'");
        $result_all = mysql_query($query);
        return $result_all;
    }

// ------------------------------------------------------------------------------------

    public function editarCuota()
    {
        $query = ("UPDATE cuotas SET montocuota='$this->intMontoCuota',saldo='$this->intSaldo',idusrmod='$this->intIdUsrMod',fechaultmod=CURRENT_DATE,
                interesmora='$this->intInteresMora' WHERE id='$this->intIdCuota'");
        $result_all = mysql_query($query);
        return $result_all;
    }

// ------------------------------------------------------------------------------------
    
    public function modificarMora()
    {
        $query = ("UPDATE cuotas SET fechacalculomora= '$this->fecFechaCalculoMora ', interesmora='$this->intInteresMora',idusrmod='$this->intIdUsrMod',
        fechaultmod=CURRENT_DATE,observacion='$this->txtObservacion' WHERE id='$this->intIdCuota'");
        $result_all = mysql_query($query);
        $num_rows = mysql_affected_rows();
        return ($result_all && $num_rows > 0);
    }

// ------------------------------------------------------------------------------------
    
    public function modificarVencimiento()
    {
            $query = ("UPDATE cuotas SET idusrmod='$this->intIdUsrMod',fechaultmod=CURRENT_DATE,fechavencimiento='$this->fecFechaVenc',fechacalculomora='$this->fecFechaCalculoMora' WHERE id='$this->intIdCuota'");
            $result_all=mysql_query($query);
            $num_rows = mysql_affected_rows();
            return ($result_all && $num_rows > 0);
    }

// ------------------------------------------------------------------------------------

    public function guardarCobro()
    {
        $query = ("UPDATE cuotas SET cobrado='$this->intCobrado',fechapago='$this->fecFechaPago',saldo='$this->intSaldo',idusrmod='$this->intIdUsrMod',
                        fechaultmod=CURRENT_DATE,interesmora='$this->intInteresMora',observacion='$this->txtObservacion',conceptos='$this->intConceptos' WHERE id='$this->intIdCuota'");
        $result_all=mysql_query($query);
        $num_rows = mysql_affected_rows();
        return ($result_all && $num_rows > 0);
    }

// ------------------------------------------------------------------------------------

    public function altaCuota()
    {
        $query = ("INSERT INTO cuotas (idcuenta,numerocuota,montocuota,saldo,fechavencimiento,fechapago,idusrcreador,idusrmod,fechaultmod,interes,cobrado,capital,cuotaformalizacion)
                        VALUES ('$this->intIdCuenta','$this->intNroCuota','$this->intMontoCuota','$this->intSaldo','$this->fecFechaVenc','$this->fecFechaPago',
                        '$this->intIdUsrCreador','$this->intIdUsrMod',CURRENT_DATE,'$this->intInteres','$this->intCobrado','$this->intCapital','$this->intCuotaFormalizacion')");
        $result_all = mysql_query($query);
        $num_rows = mysql_affected_rows();
        return ($result_all && $num_rows > 0);
    }

// ------------------------------------------------------------------------------------

    public function guardarMora()
    {
        $query = ("UPDATE cuotas SET interesmora='$this->intInteresMora',idusrmod='$this->intIdUsrMod',fechaultmod=CURRENT_DATE,fechacalculomora='$this->fecFechaCalculoMora' WHERE id = '$this->intIdCuota'");
        $result_all = mysql_query($query);
        return ($result_all);
    }
// ------------------------------------------------------------------------------------

    public function guardarConceptos()
    {
       //Guarda el total que se cobro en conceptos de gastos administrativos en una cuota
        $query = ("UPDATE cuotas SET conceptos='$this->intConceptos' WHERE id = '$this->intIdCuota'");
        $result_all=mysql_query($query);
        return ($result_all);
    }

//==============================================================================================
  
    public function cargarresultados($resultado)
    //coloca los datos del query en las variables de la clase
    {
        while ($cons = mysql_fetch_object($resultado))
        {
            $this->putIdCuota($cons->id);
            $this->putIdCuenta($cons->idcuenta);
            $this->putNroCuota($cons->numerocuota);
            $this->putNroCuenta($cons->nrocuenta);
            $this->putMontoCuota($cons->montocuota);
            $this->putSaldo($cons->saldo);
            $this->putFechaVenc($cons->fechavencimiento);
            $this->putFechaPago($cons->fechapago);
            $this->putIdUsrCreador($cons->idusrcreador);
            $this->putIdUsrMod($cons->idusrmod);
            $this->putUltMod($cons->fechaultmod);
            $this->putInteres($cons->interes);
            $this->putCapital($cons->capital);
            $this->putSolicitante($cons->apellido.", ".$cons->nombres);
            $this->putCobrado($cons->cobrado);
            $this->putAnioExpediente($cons->anioexpediene);
            $this->putNroExpediente($cons->nroexpediente);
            $this->putCuentaCorriente($cons->cuentacorriente);
            $this->putInteresMora($cons->interesmora);
            $this->putFechaCalculoMora($cons->fechacalculomora);
            $this->putEsCuotaFormalizacion($cons->cuotaformalizacion);
            $this->putObservacion($cons->observacion);
            $this->putConceptos($cons->conceptos);
        }
    }
 
// ------------------------------------------------------------------------------------
	
    public function ultimaCuotaPaga() 
    //retorna la consulta de la ultima cuota paga
    {
        $query = ("SELECT cuotas.* FROM cuotas WHERE cuotas.idcuenta='$this->intIdCuenta' && cuotas.fechapago<>'0000-00-00' ORDER BY fechavencimiento DESC LIMIT 1");
        $result_all = mysql_query($query);
        $num_rows = mysql_affected_rows();
        if($result_all && $num_rows > 0)
        {
            $varres = mysql_fetch_object($result_all);
            //llenar el array 	  
            $arrcuotas = array("id"=>$varres->id,
                            "nrocuota"=>$varres->numerocuota,
                            "montocuota"=>$varres->montocuota,
                            "fechavenc"=>$this->fechaACadena($varres->fechavencimiento),
                            "fechapago"=>$this->fechaACadena($varres->fechapago),
                            "saldo"=>$varres->saldo,
                            "cobrado"=>$varres->cobrado,
                            "interes"=>$varres->interes,
                            "cuotaformalizacion"=>$varres->cuotaformalizacion,
            );
        }
        return($arrcuotas);
    }
  
// ------------------------------------------------------------------------------------
	
    public function proximaCuotaPagar() 
    //retorna la consulta de la proxima cuota a pagar
    {
        $query = ("SELECT cuotas.* FROM cuotas WHERE cuotas.idcuenta='$this->intIdCuenta' && cuotas.fechapago='0000-00-00' LIMIT 1 ");
        $result_all = mysql_query($query);
        $num_rows = mysql_affected_rows();
        if($result_all && $num_rows > 0)
        {
            $varres = mysql_fetch_object($result_all);
            //llenar el array
            $arrcuotas = array("id"=>$varres->id,
                            "nrocuota"=>$varres->numerocuota,
                            "montocuota"=>$varres->montocuota,
                            "fechavenc"=>$this->fechaACadena($varres->fechavencimiento),
                            "fechapago"=>$this->fechaACadena($varres->fechapago),
                            "saldo"=>$varres->saldo,
                            "cobrado"=>$varres->cobrado,
                            "interes"=>$varres->interes,
                            "intmora"=>$varres->interesmora
            );
        }					 
        return($arrcuotas);
    }

// ------------------------------------------------------------------------------------

    public function proximaCuotaVencer()
    //retorna la consulta de la proxima cuota a a vencer
    {
        $query = ("SELECT cuotas.* FROM cuotas WHERE cuotas.idcuenta='$this->intIdCuenta' && cuotas.fechavencimiento>CURDATE() LIMIT 1 ");
        $result_all = mysql_query($query);
        $num_rows = mysql_affected_rows();
        if($result_all && $num_rows > 0)
        {
            $varres = mysql_fetch_object($result_all);
            //llenar el array
            $arrcuotas = array("id"=>$varres->id,
                            "nrocuota"=>$varres->numerocuota,
                            "montocuota"=>$varres->montocuota,
                            "fechavenc"=>$this->fechaACadena($varres->fechavencimiento),
                            "saldo"=>$varres->saldo,
                            "cobrado"=>$varres->cobrado,
                            "interes"=>$varres->interes,
                            "intmora"=>$varres->interesmora
            );
        }
        return($arrcuotas);
    }

// ------------------------------------------------------------------------------------

    function fechaACadena($parfecha)
    {
        if( empty($parfecha) ) 
        { 
            $auxFecha='';
        } 
        else 
        {
            list($y,$m,$d,$h,$n,$s) = split( "[- :]", $parfecha);
            if (($y=="0000") && ($m=="00") && ($d=="00"))
                    $auxFecha='';
            else
            $auxFecha = sprintf("%02s",$d).'/'.sprintf("%02s",$m).'/'.sprintf("%04s",$y);
        }
        return $auxFecha;
    }

// ------------------------------------------------------------------------------------

    public function calcularCobradoTotal($id)
    //calcula la suma del capital cobrado de cada cuota de una misma cuenta
    {
        $query = ("SELECT cuotas.cobrado FROM cuotas WHERE cuotas.idcuenta='$id'");
	    $result_all = mysql_query($query);
	    $num_rows = mysql_affected_rows();
        $cobrado_total = 0;
		if($result_all)
		{
			while ($varres = mysql_fetch_object($result_all))
			{
				//calcular el monto cobrado total
				$cobrado_total += $varres->cobrado;
			}

        }
        return $cobrado_total;
    }

// ------------------------------------------------------------------------------------

    public function calcularDeudaVencida($id)
    {
        $query = ("SELECT saldo,fechavencimiento,interesmora FROM cuotas WHERE cuotas.idcuenta='$id' && fechavencimiento < CURDATE()");
	    $result_all = mysql_query($query);
	    $num_rows = mysql_affected_rows();
        $deuda_vencida = 0;
		if($result_all)
		{
			while ($varres = mysql_fetch_object($result_all))
			{
				//calcular la deuda vencida
				$deuda_vencida += $varres->saldo;
			}
        }
        return $deuda_vencida;
    }

// ------------------------------------------------------------------------------------

    public function calcularSaldoNoVencido($id)
    {
        $query = ("SELECT saldo,fechavencimiento FROM cuotas WHERE cuotas.idcuenta='$id' && fechavencimiento >= CURDATE()");
	    $result_all = mysql_query($query);
	    $num_rows = mysql_affected_rows();
        $deuda_no_vencida = 0;
		if($result_all)
		{
			while ($varres = mysql_fetch_object($result_all))
			{
				//calcular la deuda vencida
				$deuda_no_vencida += $varres->saldo;
			}
        }
        return $deuda_no_vencida;
    }

// ------------------------------------------------------------------------------------

    public function calcularInteresTotal($id)
    {
        $query = ("SELECT interes FROM cuotas WHERE cuotas.idcuenta='$id'");
	    $result_all = mysql_query($query);
	    $num_rows = mysql_affected_rows();
        $interes_total = 0;
        while ($varres = mysql_fetch_object($result_all))
		{
			//calcular el interes por financiacion total
			$interes_total += $varres->interes;
		}
        return $interes_total;
    }

// ------------------------------------------------------------------------------------

    public function calcularMoraTotal($id)
    {
        $query = ("SELECT interesmora FROM cuotas WHERE cuotas.idcuenta='$id'");
	    $result_all = mysql_query($query);
	    $num_rows = mysql_affected_rows();
        $interes_total = 0;
        while ($varres = mysql_fetch_object($result_all))
		{
			//calcular el interes por mora total
			$interes_total += $varres->interesmora;
		}
        return $interes_total;
    }

// ------------------------------------------------------------------------------------

    public function calcularTotalFinanciacion($id)
    {
        $query = ("SELECT montocuota FROM cuotas WHERE cuotas.idcuenta='$id'");
	    $result_all = mysql_query($query);
	    $num_rows = mysql_affected_rows();
        $interes_total = 0;
        while ($varres = mysql_fetch_object($result_all))
		{
			//calcular la deuda vencida
			$interes_total += $varres->montocuota;
		}
        return $interes_total;
    }

// ------------------------------------------------------------------------------------

	public function calcularGastosCobrados($id)
    //devuelve el total de gastos fijos cobrados a una cuota
	{
		$query = ("SELECT sum(montogastos) as gatosfijos FROM movimientocuotas WHERE idcuota='$id'");
		$result_all = mysql_query($query);
		if($result_all)
		{
			$varres = mysql_fetch_object($result_all);
			return $varres->gatosfijos;
		}else{
			return false;
		}
	}

// ------------------------------------------------------------------------------------
    public function cuotas() 
    //retorna la consulta de todos las cuotas
    {       
        $query =("SELECT cuotas.*, pobladores.apellido,pobladores.nombres, cuentas.cantcuotas,cuentas.cuentacorriente,cuentas.convenio FROM cuotas,cuentas,pobladores,solicitudes WHERE cuotas.idcuenta='$this->intIdCuenta' && cuentas.id='$this->intIdCuenta'&& cuentas.idsolicitud=solicitudes.id && solicitudes.idpoblador=pobladores.id ORDER BY numerocuota ");
        $result_all=mysql_query($query);
        if($result_all){
            while ($varres = mysql_fetch_object($result_all)) {
                //llenar el array 	  
                $arrcuotas[]=array("id"=>$varres->id,
                                "numerocuota"=>$varres->numerocuota,
                                "montocuota"=>$varres->montocuota,
                                "capital"=>$varres->capital,
                                "interes"=>$varres->interes,
                                "fechavenc"=>$this->fechaACadena($varres->fechavencimiento),
                                "fechapago"=>$this->fechaACadena($varres->fechapago),
                                "saldo"=>$varres->saldo,
                                "interes"=>$varres->interes,
                                "tipointeres"=>$varres->tipointeres,
                                "solicitante"=>$varres->apellido.", ".$varres->nombres,
                                "cuenta"=>$varres->cuentacorriente,
                                "cantcuotas"=>$varres->cantcuotas,
                                "convenio"=>$varres->convenio,
                                "cuotaformalizacion"=>$varres->cuotaformalizacion,
                );
            }
            return($arrcuotas);
        }else{	
            return(false);
	}
    }
	
//---------------------------------------------------------------------------------------
	
	public function calcularMoraRango($cuotainicial, $cuotafinal, $idcuenta)
	{                        
	    $parParametros=new modeloparametro();
            $parParametros->putId('1');
            $parParametros->traerParametro();
            $interesbch=0;
            $interesbch=$parParametros->getInteresMora();            

            if(($cuotafinal >= 0) && ($cuotainicial >= 0) && ($cuotafinal >= $cuotainicial))
            {                            
                $j = 0;
                for($i=$cuotainicial; $i<=$cuotafinal; $i++)
                {                    
                    $cuota = new ModeloCuota();
                    $cuota->putNroCuota($i);
                    $cuota->putIdCuenta($idcuenta);                    
                    $ok = $cuota->traerCuotaCuenta(); 
                    
                    if($ok)
                    {                        
                        $array_cuotas[$j] = $this->morachubut($cuota, $interesbch);
                        $j++;
                    }else{                        
                        return false;
                    }
                    
                }                
                return $array_cuotas;
            }else{
                    return false;
            }
	}


//---------------------------------------------------------------------------------------

    public function morachubut($parcuota, $interesbch)
    {
        $saldo = $parcuota->getSaldo();
        $conceptos = $parcuota->getConceptos();
        $fechavenc = fechaACadena($parcuota->getFechaCalculoMora());       
        
        if($fechavenc == '')
            $fechavenc = fechaACadena($parcuota->getFechaVenc());
            $fechadeposito = date('d/m/Y');
            $interesxmora = 0;
            $dias = 0;
            $diastotales = 0;

        if(isset($_GET['fechamora']))
        {
            $fechadeposito = $_GET['fechamora']; // es la fecha en que el poblador piensa hacer el pago de la cuota esta variable llega por detalles cuota mora
        }
		 if(isset($_GET['fechapago']))
        {
            $fechadeposito = $_GET['fechapago']; // es la fecha en que el poblador piensa hacer el pago de la cuota esta variable llega por pago parcial
        }
		 if(isset($_POST['fechapagoglobal']))
        {
            $fechadeposito = $_POST['fechapagoglobal']; // es la fecha en que el poblador piensa hacer el pago de la cuota esta variable llega por pago global
        }

		
        $dias = $this->diasmora($fechadeposito,$fechavenc);
        if(($dias > 0) && ($saldo > 0))
        {
            $interesxmora = ($saldo+$conceptos)*(($interesbch/100)/365)*$dias;
        }
		
        $arrcuotas = array("id"=>$parcuota->getIdCuota(),
                            "nrocuota"=>$parcuota->getNroCuota(),
                            "deudamora"=>$parcuota->getSaldo()+$interesxmora+$parcuota->getInteresMora(),
                            "capital"=>$parcuota->getCapital(),
                            "interes"=>$parcuota->getInteres(),
                            "fechavencimiento"=>fechaACadena($parcuota->getFechaVenc()),
                            "fechamora"=>$fechadeposito,
                            "intmora"=>redondeoCincoCent($interesxmora, 1),		//redondeo
                            "moraanterior"=>redondeoCincoCent($parcuota->getInteresMora(), 1),	//redondeo
                            "diasmora"=>$dias,
                            "saldo"=>$parcuota->getSaldo(),
                            );
        return($arrcuotas);
    }


//=======================================================================================

	public function diasmora($fecha_pago, $fecha_vto)
	{
		$diasrango = 0;
        //si el resultado es < 0, la fecha de pago es menor a la fecha de vto y => no hay mora
        if(comparar_fechas($fecha_pago, $fecha_vto) < 0)
            return 0;

		return $this->restaFechas($fecha_vto, $fecha_pago);
	}

//=======================================================================================
   
	public function sumaDia($fecha,$dia)
	// suma dia a la fecha
	{
		// suma $dia dias a una fecha y retorna la fecha resultante
    	list($day,$mon,$year) = explode('/',$fecha);
		$day=($day*1)+$dia;
		$mon=$mon*1;
		$year=$year*1;
		
		$nuevafecha=date('d/m/Y',mktime(0,0,0,$mon,$day,$year));
			//echo $fecha."  ".$nuevafecha." mes ".$mon2."<br />\n";
       //echo "neueva fecha ".$nuevafecha;
		return($nuevafecha);			
	}

//=======================================================================================

	public function restaFechas($dFecIni, $dFecFin)
	// Fecha en formato dd/mm/yyyy o dd-mm-yyyy retorna la diferencia en dias
	{
		$dFecIni = str_replace("-","",$dFecIni);
		$dFecIni = str_replace("/","",$dFecIni);
		$dFecFin = str_replace("-","",$dFecFin);
		$dFecFin = str_replace("/","",$dFecFin);

		ereg("([0-9]{1,2})([0-9]{1,2})([0-9]{2,4})", $dFecIni, $aFecIni);
		ereg("([0-9]{1,2})([0-9]{1,2})([0-9]{2,4})", $dFecFin, $aFecFin);

		$date1 = mktime(0,0,0,$aFecIni[2], $aFecIni[1], $aFecIni[3]);
		$date2 = mktime(0,0,0,$aFecFin[2], $aFecFin[1], $aFecFin[3]);

		return round(($date2 - $date1) / (60 * 60 * 24));
	}
//=======================================================================================

	public function tienePagoFormalizacion()
	//devuelve por verdadero o por falso si la formalizaci�n tiene al menos un cobro realizado
	{
		//$query = "SELECT id FROM cuotas WHERE idcuenta='$this->intIdCuenta' && numerocuota=0 && fechapago<>'0000-00-00'";
                $query = "SELECT id FROM cuotas WHERE idcuenta='$this->intIdCuenta' && (numerocuota=0 || cuotaformalizacion=1) && fechapago<>'0000-00-00'";
		$result_all = mysql_query($query);
		$num_rows = mysql_affected_rows();
		return ($result_all && $num_rows > 0);
	}

// ------------------------------------------------------------------------------------

	public function cargaManualCuota()
	{
		$query = ("UPDATE cuotas SET montocuota='$this->intMontoCuota',saldo='$this->intSaldo',idusrmod='$this->intIdUsrMod',fechaultmod=CURRENT_DATE,
				fechavencimiento='$this->fecFechaVenc',fechapago='$this->fecFechaPago',interes='$this->intInteres',capital='$this->intCapital',
				fechacalculomora='$this->fecFechaCalculoMora ',interesmora='$this->intInteresMora',cobrado='$this->intCobrado',conceptos='$this->intConceptos',
				observacion='$this->txtObservacion'	WHERE id = '$this->intIdCuota'");
		$result_all = mysql_query($query);
      	return $result_all;
	}

}