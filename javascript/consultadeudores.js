function enviarDatos(form)
{
    if(validarDatos(form)){
        form.submit();
        return true;
    }
    return false;
}

function validarDatos(form)
{
	var desde = form.fechadesde.value;
	var hasta = form.fechahasta.value;
	if(!esvacio(desde))
		if(!esFecha(desde)){
			alert("Debe ingresar una 'fecha desde' v�lida");
			form.fechadesde.focus();
			return false;
		}
	if(!esvacio(hasta))
		if(!esFecha(hasta)){
			alert("Debe ingresar una 'fecha hasta' v�lida");
			form.fechahasta.focus();
			return false;
		}
	if(!esvacio(desde) && !esvacio(hasta))
		if(!compararfechas(desde, hasta)){
			alert("La 'fecha desde' no puede ser mayor que la 'fecha hasta'");
			form.fechadesde.focus();
			return false;
		}
    return true;
}

function cancelarEnvio()
{
    document.location = "index.php?controlador=index&&accion=nada";
    return true;
}
