function enviarSolicitud(op, id, nroexp, anioexp, apellido, nombre, tipo)
{
	solicitante = apellido+", "+nombre;
	window.opener.cargarSolicitud(id, nroexp, anioexp, solicitante, tipo);
    setTimeout("nada()",200);
}

function nada()
{
     self.close();
}

function verRespuesta(rta)
{
// Esta funcion trae una respuesta del html encontroinspeccion.html si la respuesta es '0' el informe de inspeccion no existe,
// si la respuesta es mayor a '0' quiere decir que el informe YA existe lo carga en pantalla.

	if(rta>0){	
	    //document.location = "index.php?controlador=emprendedor&&accion=veremprendedor&&operacion="+2+"&&idemp="+rta;
		alert("El expediente ya tiene un informe de inspección.");
		document.getElementById('documento').value = 0;
		document.getElementById('Guardar').disabled = true; //enable button
	    document.getElementById('documento').focus();
		return false;
	} else {
		document.getElementById('Guardar').disabled = false; //enable button
		document.getElementById('telefono').focus();  
		return true; 
	}
}