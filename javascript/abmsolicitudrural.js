function validarDatosRurales(form)
{
 cambiaValorRural(form);
 return (true);   
                 

}

function enviarDatosRurales(form)
{
if(validarDatosTierraRural(form)){
   if(validarDatosRurales(form)){
	   document.getElementById('observacionanttr').disabled=false;
	   	   
	   form.submit()
	   }
	else{   
       return (false);   
	}
}
else{   
    return (false);   
}
 

}

function validarRadio(){
	
var residentetierra=document.getElementById('residentetierra').value;
var integrasociedad=document.getElementById('integrasociedad').value;
var padresrurales=document.getElementById('padresrurales').value;
var estecnico=document.getElementById('estecnico').value;
var certificadoproductor=document.getElementById('certificadoproductor').value;
var otrastierras=document.getElementById('otrastierras').value;
var ocupantetierrafiscal=document.getElementById('ocupantetierrafiscal').value;
var otrasadjudicaciones=document.getElementById('otrasadjudicaciones').value;
var transfiriotierras=document.getElementById('transfiriotierras').value;

if(residentetierra==1){
	 document.getElementById('ESresidentetierra1').checked="checked";
	}
else{
	 document.getElementById('ESresidentetierra0').checked="checked";
	}	
	
if(integrasociedad==1){
	 document.getElementById('integra1').checked="checked";
	}
else{
	 document.getElementById('integra0').checked="checked";
	}	
if(padresrurales==1){
	 document.getElementById('padres1').checked="checked";
	}
else{
	 document.getElementById('padres0').checked="checked";
	}	
if(estecnico==1){
	 document.getElementById('tecnico1').checked="checked";
	}
else{
	 document.getElementById('tecnico0').checked="checked";
	}	

if(certificadoproductor==1){
	 document.getElementById('certificado1').checked="checked";
	}
else{
	 document.getElementById('certificado0').checked="checked";
	}	

if(otrastierras==1){
	 document.getElementById('otras1').checked="checked";
	}
else{
	 document.getElementById('otras0').checked="checked";
	}	

if(ocupantetierrafiscal==1){
	 document.getElementById('ocupante1').checked="checked";
	}
else{
	 document.getElementById('ocupante0').checked="checked";
	}	

if(otrasadjudicaciones==1){
	 document.getElementById('adjudicaciones1').checked="checked";
	}
else{
	 document.getElementById('adjudicaciones0').checked="checked";
	}	

if(transfiriotierras==1){
	 document.getElementById('transfirio1').checked="checked";
	}
else{
	 document.getElementById('transfirio0').checked="checked";
	}	

}




function validarfechas(form)
{
	fechasol=form.fechasolicitud.value;
		if(esFecha(fechasol)){
		    return(true);
		}
	
       else{
	      alert("La fecha de la solicitud no es correcta");
	      form.fechasolicitud.focus();
          return(false);
   }
}




function cambiaValorRural(form){

    var i;
	
	for (i=0;i<form.ESresidentetierra.length;i++){
       if (form.ESresidentetierra[i].checked){
          break;
	   }
    }
		
    form.residentetierra.value =form.ESresidentetierra[i].value;
	
    for (i=0;i<form.padres.length;i++){
       if (form.padres[i].checked)
          break;
    }
    form.padresrurales.value =form.padres[i].value;
	
	
	
	for (i=0;i<form.tecnico.length;i++){
       if (form.tecnico[i].checked)
          break;
    }
    form.estecnico.value =form.tecnico[i].value;
	
	
	
	for (i=0;i<form.integra.length;i++){
       if (form.integra[i].checked)
          break;
    }
    form.integrasociedad.value =form.integra[i].value;
	
	for (i=0;i<form.certificado.length;i++){
       if (form.certificado[i].checked)
          break;
    }
    form.certificadoproductor.value =form.certificado[i].value;
	
	for (i=0;i<form.otras.length;i++){
       if (form.otras[i].checked)
          break;
    }
    form.otrastierras.value =form.otras[i].value;

	for (i=0;i<form.ocupante.length;i++){
       if (form.ocupante[i].checked)
          break;
    }
    form.ocupantetierrafiscal.value =form.ocupante[i].value;

	for (i=0;i<form.adjudicaciones.length;i++){
       if (form.adjudicaciones[i].checked)
          break;
    }
    form.otrasadjudicaciones.value =form.adjudicaciones[i].value;

	for (i=0;i<form.transfirio.length;i++){
       if (form.transfirio[i].checked)
          break;
    }
    form.transfiriotierras.value =form.transfirio[i].value;
} 



function comprobarCheckRural()
{
 var residente;
 var empleado;
 
 residente=document.getElementById('residente').value;
 empleado=document.getElementById('empleadopublico').value;

  if(empleado==1){

	  document.getElementById('checkempleadopublico').checked="checked";
	  }
   if(residente==1){
	  document.getElementById('checkresidente').checked="checked";
	  } 	
  
   return(true);
}

function validarCheck(este,valormodif)
{
 
	if (este.checked){
		
	 valormodif.value=1;
	 
    }
	else{
	
	valormodif.value=0;
  }
			
}


function validarDatosTierraRural(form)
{
	if((validarseccion() && validarfraccion()) || (validarColonia() && validarEnsanche())){
		if(validarLote()){
			return true;
		}else{
			alert("Debe ingresar el n�mero de lote");
			document.getElementById('nrolote').focus();
		}
	}else{
		alert("Debe elegir la secci�n y fracci�n o la colonia y ensanche de la tierra");
		document.getElementById('ListaSeccion').focus();
		return false;
	}
}

function validarseccion()
{
	if(parseInt(document.getElementById('idseccion').value) > 0){
		return(true);
	}
	else{
	    return(false);
	}
}

function validarfraccion()
{
	if(parseInt(document.getElementById('idfraccion').value) > 0){
		return(true);
	}
	else{
	    return(false);
	}
}

function validarColonia()
{
	if(parseInt(document.getElementById('idcolonia').value) > 0){
		return(true);
	}
	else{
	    return(false);
	}
}

function validarEnsanche()
{
	if(parseInt(document.getElementById('idensanche').value) > 0){
		return(true);
	}
	else{
	    return(false);
	}
}

function validarLote()
{
	if(parseInt(document.getElementById('nrolote').value) > 0){
		return(true);
	}else{
	    return(false);
	}
}

function datosArrendamiento(form)
{    
     var idsolicitudrural=0;
	 var idarrendamiento=0;
	 idsolicitudrural=form.idsolicitudrural.value;
	 idarrendamiento=form.idarrendamiento.value;
	 url ="base.php?controlador=arrendamiento&&accion=verarrendamiento&&idsolicitudrural="+idsolicitudrural;	
	 open(url, "verarrendamentorural", "width=550, height=600, toolbar=no, top=200, left=200 ");

}


	
function traerArrendamiento(id, nombre, propietario, superficie, idunidad, explotacion)
{
	
	
	var titulo = "<h4>Datos del Arrendamiento</h4>";
	var linea = "<br/><h6></h6>";
	var campos = "<div class='campos'>" +
			"<br /><label>Nombre : </label><label id='labelnombre'></label>" +
			"<br /><label>Propietario: </label><label id='labelpropietario'></label>" +
			"<br /><label>Superficie: </label><label id='labelsuperficie'></label>" +
			"<br />	<label>Unidad : </label><label id='labelidunidad'></label>" +
			"<br />	<label>Explotacion:</label><label id='labelexplotacion'></label></div>";

    var inputs="<input name=\"nombre\"  id=\"nombre\" type=\"hidden\"   value=\"\"  size=\"10\" maxlength=\"40\" />"+
	  "<input name=\"propietario\"  id=\"propietario\" type=\"hidden\"   value=\"\"  size=\"10\" maxlength=\"40\" />"+  
	  "<input name=\"superficie\"  id=\"superficie\" type=\"hidden\"   value=\"\"  size=\"10\" maxlength=\"40\" /> "+   
	  "<input name=\"idunidad\" id=\"idunidad\"  type=\"hidden\"   value=\"\" />"+ 
      "<input name=\"explotacion\"  id=\"explotacion\" type=\"hidden\"   value=\"\"  size=\"10\" maxlength=\"40\"   {SOLOLECTURA} />";
	  
    document.getElementById("arrendamiento").innerHTML = linea + titulo + campos + inputs + linea;
    
	form = document.getElementById("formulariotr");
    document.getElementById("labelnombre").innerHTML =form.nombre.value = nombre;
    document.getElementById("labelpropietario").innerHTML =form.propietario.value = propietario;
    document.getElementById("labelsuperficie").innerHTML =form.superficie.value = superficie;
    document.getElementById("labelidunidad").innerHTML =form.idunidad.value = idunidad;
   	document.getElementById("labelexplotacion").innerHTML =form.explotacion.value = explotacion;
	document.getElementById("idarrendamiento").value = id;
    
    return true;
}



