function enviarDatos(form)
{
	if (validarDatos(form))
	    form.submit();
}
	
function validarDatos()
{
	return true;   
}

function cancelarEnvio()
{
	document.location = "./index.php?controlador=cuota&&accion=vercuota";
	return true;
}

function buscarCuenta(form)
{
	var anio = form.anioexpediente.value;
	var letra = form.letraexpediente.value;
	var nro = form.nroexpediente.value;
	var url = "base.php?controlador=cuenta&&accion=elegirCuenta&&anioexpediente="+anio+"&&letraexpediente="+letra+"&&nroexpediente="+nro;  
	open(url, "listacuentas", "width=620, height=250, toolbar=no, top=200, left=200 ");
	return true;
}

function mostrarDatos(idcuenta)
{    
	window.document.formcuota.idcuenta.value = idcuenta;
	document.location = "./index.php?controlador=cuota&&accion=vercuota&&idcuenta="+idcuenta;
}

function calcularMora()
{
	var idcuenta = window.document.formcuota.idcuenta.value;
	var idcuota = window.document.formcuota.idcuota.value;
	if(idcuenta > 0 && idcuota > 0){
		var fechacalculomora = document.getElementById('fechamora').value;
		document.location = "./index.php?controlador=cuota&&accion=vercuota&&idcuenta="+idcuenta+"&&idcuota="+idcuota+"&&fechamora="+fechacalculomora;
	}else{
		alert("Debe seleccionar la cuota para calcular el inter�s");
		return false;
	}
	return true;
}

function imprimirTalon()
{  
	var idcuenta = window.document.formcuota.idcuenta.value;
	var idcuota = window.document.formcuota.idcuota.value;
	if(idcuenta > 0 && idcuota > 0){
	    var solicitante = window.document.formcuota.solicitante.value;
		var cuentacorriente = window.document.formcuota.cuentacorriente.value;
		var nrocuota = window.document.formcuotamora.nrocuota.value;
		var montocuota = window.document.formcuotamora.deudamora.value;
		var fechavenc = window.document.formcuotamora.fechamora.value;
		var intmora = window.document.formcuotamora.intmora.value;
	    var url = "./reports/talonunico.php?idcuenta="+idcuenta+"&&idcuota="+idcuota+"&&solicitante="+solicitante+"&&cuenta="+cuenta+"&&nrocuota="+nrocuota+"&&montocuota="+montocuota+"&&fechavenc="+fechavenc+"&&intmora="+intmora;  
		open(url, "imprtalon", "width=650, height=250, toolbar=no, top=200, left=200 ");
	}else{
		alert("Debe seleccionar la cuota para imprimir el tal�n de pago");
		return false;
	}
	return true;
}

function verMovimiento(idcuota)
{
	if(idcuota > 0){
		var url = "./base.php?controlador=movimiento&&accion=vermovimiento&&idcuota="+idcuota;
		open(url, "vermov", "width=450, height=250, toolbar=no, top=200, left=200,location=No,directories=No,menubar=No ");
	}else{
		alert("Debe seleccionar la cuota para calcular el inter�s");
		return false;
	}
	return true;
}

function editarCuota(idcuota)
{    
	if( idcuota > 0){
		document.location = "./index.php?controlador=cuota&&accion=editarCuota&&idcuota="+idcuota+"&&operacion="+'2';
	}else{
		alert("Debe seleccionar la cuota para calcular el inter�s");
		return false;
	}
	return true;
}