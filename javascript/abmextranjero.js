function validarDatos(form)
{
	if(!validarFechas(form))
		return false;
	return(true); 
}


function cancelarEnvio(form)
{
	cerrar();
}


function validarFechas(form)
{
	var fechacarta = form.fechacarta.value;
	var fechallegada = form.fechallegada.value;
	
	if (fechacarta != "" && !esFecha(fechacarta)) {
		alert("La fecha de emisión es incorrecta");
		form.fechacarta.focus();
		return(false);
	}
	if (fechallegada != "" && !esFecha(fechallegada)) {
		alert("La fecha de llegada es incorrecta");
		form.fechallegada.focus();
		return(false);
	}
	// si estan ambas fechas cargadas
	if (fechacarta != "" && fechallegada != "")
		// si devuelve falso es porque la fechadesde (1ra) es mayor a la fechahasta (2da)
		if (!compararfechas(fechallegada, fechacarta)) {
			alert("CUIDADO! La fecha de expedición de la carta no puede ser anterior a la fecha de llegada");
			form.fechallegada.focus();
			return false;
	}
	return true;
}

function cerrar() 
{
	self.close();
}

function enviarExtranjero(form)
{
	if (validarDatos(form)) {
		window.opener.traerExtranjero(form.id.value, form.carta.value, form.fechacarta.value, form.juzgado.value, form.visadopor.value, form.fechallegada.value, form.procedencia.value, form.medio.value);
		cerrar();
	} else {
		return false;
	}
}

