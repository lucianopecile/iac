function enviarDatos(form)
{
   if (validarDatos(form)) {
     form.submit();
   }
}
	
function validarDatos(form)
{
	if(validarNombreCompleto(form))
	    return (true);
	return (false);
}

function validarNombreCompleto(form)
{
	var q=form.nombrecompleto.value;
	if ((form.nombrecompleto.value.length < 1)) {
		alert("Debe ingresar algun nombre para el empleado ");
		form.nombrecompleto.focus();
        return (false); 
    }else{
		for(var i = 0; i < q.length; i++ ) {
                if ( q.charAt(i) != " " ) {   
                        return (true);   
                }   
        }   
  
  // Agregar las validaciones.-
    alert("No se acepta el nombre"); 
	form.nombrecompleto.focus();
	return(false);
	
   }
}

function cancelarEnvio()
{
	document.location = "./index.php?controlador=empleado&&accion=mostrarempleado";
	return(true);
}