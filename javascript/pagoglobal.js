function enviarDatos(form)
{
	if (validarDatos(form)){
		form.submit();
	}
}
	
function validarDatos(form)
{
	if(!controlarvalor())
		return false;
	if(!validarFechas(form))
		return false;
	return true;
}

function cancelarEnvio()
{
	document.location = "./index.php?controlador=cuota&&accion=ingresartalon";
	return(true);
}

function buscarCuenta(form)
{
	var anio = form.anioexpediente.value;
	var nro = form.nroexpediente.value;
	var nrocuenta = form.nrocuenta.value;
	var url = "base.php?controlador=cuenta&&accion=elegircuenta&&anioexpediente="+anio+"&&nroexpediente="+nro+"&&nrocuenta="+nrocuenta;
	open(url, "listacuentas", "width=620,height=250,toolbar=no,top=200,left=200");
	return true;
}


function ConceptosCuenta(form)
{
	var idsolicitud = form.idsolicitud.value;
	var url = "base.php?controlador=liquidacion&&accion=conceptoscuenta&&idsolicitud="+idsolicitud;
	open(url, "listaconceptoscuentas", "width=620,height=550,toolbar=no,top=200,left=200");
	return true;
}

function mostrarDatos(idcuenta)
{
	document.location = "./index.php?controlador=cuota&&accion=pagoglobal&&idcuenta="+idcuenta;
}


function calcularpagoglobal()
{    
	var idcuenta = window.document.formcuota.idcuenta.value;
	var cuotainicial = 0;
	var cuotafinal = 0;
	cuotainicial = window.document.formpglobal.cuotainicial.value*1;
	cuotafinal = window.document.formpglobal.cuotafinal.value*1;
	if(idcuenta > 0 && (cuotainicial <= cuotafinal)){
		var fechapagoglobal = document.getElementById('fechapago').value;
		document.location = "./index.php?controlador=cuota&&accion=pagoglobal&&idcuenta="+idcuenta+"&&fechapago="+fechapagoglobal+"&&cuotainicial="+cuotainicial+"&&cuotafinal="+cuotafinal;
		return true;
	}else{
		alert("Debe seleccionar la cuota ");
		return false;
	}
}

function controlarvalor()
{  
	var tope = Math.ceil(window.document.formpglobal.montoglobal.value*1);
	var montoapagar = window.document.formpglobal.montoapagar.value*1;
	if(montoapagar > tope || montoapagar > 999999.99){
		alert("El monto del talon no puede superar el saldo de las cuotas elegidas o ser mayor a 999.999,99 ");
		window.document.globalformpglobal.montoglobal.value=0;
		return false;
	}else{
		if(montoapagar<=0){
			alert("El monto del talon no puede ser menor o igual a 0 ");
			window.document.formpglobal.montoglobal.value=0;
			return false;
		}else{
			return true;
		}
	}
}

function validarFechas(form)
{
	var fechapagoglobal = form.fechapagoglobal.value;
	if (fechapagoglobal == "")
		return true;
	if(esFecha(fechapagoglobal))
		return(true);
	else{
		alert("La fecha es incorrecta");
		form.fechapagoglobal.focus();
		return(false);
	}
}

function validarFechaPago(form)
{
	var fechapago = form.fechapago.value;
	
	if (fechapago == "")
		return true;
	if(esFecha(fechapago))
		return(true);
	else{
		alert("La fecha es incorrecta");
		form.fechapago.focus();
		return(false);
	}
}
