function comprobarCheck()
{
	var espejoagua = document.getElementById('espejoagua').value;
	var rioarroyo = document.getElementById('rioarroyo').value;
	var altascumbres = document.getElementById('altascumbres').value;
	var centrourbano = document.getElementById('centrourbano').value;
	var accesos = document.getElementById('accesos').value;
		
	if(espejoagua == 1)
		document.getElementById('checkespejosagua').checked = "checked";
	if(rioarroyo == 1)
		document.getElementById('checkrioarroyo').checked = "checked";
	if(altascumbres == 1)
		document.getElementById('checkaltascumbres').checked = "checked";
	if(centrourbano == 1)
		document.getElementById('checkcentrourbano').checked = "checked";
	if(accesos == 1)
		document.getElementById('checkaccesos').checked = "checked";
	return true;
}

function validarCheck(este, valormodif)
{
	if (este.checked)
		valormodif.value = 1;
	else
		valormodif.value = 0;
}

function enviarDatos(form)
{
	if (validarDatos(form)) 
	{
		form.observacionant.disabled = false;
		form.submit();
	}
}

function validarDatos(form)
{
	if(!validarFechas(form))
		return false;
	if(!validarSuperficie(form))
		return false;
	if(!validarSolicitud(form))
		return false;
	if(!validarDistanciayReciptividad(form))
		return false;
	if(!formatearSuperficie(form))
		return false;
		
	return true;
}

function validarFechas(form)
{
	var fecha = form.fechainspeccion.value;
	if(esFecha(fecha))
	{
		return(true);
	}else{
		alert("La fecha del informe no es v�lida");
		form.fechainspeccion.focus();
		return(false);
	}
}

function validarSuperficie(form)
{
	//si no ingreso la superficie o la unidad no permite la carga
	if (form.sup.value < 1)
	{
		alert("Debe indicar la superficie de la tierra en hect�reas-centi�reas-�reas-dm2");
		form.sup.focus();
		return false;
	}
	return true;
}

function validarSolicitud(form)
{
	if (form.idsolicitud.value < 1)
	{
		alert("Debe seleccionar la solicitud de tierra asociada al informe.");
		return false;
	}
	return true;
}

function validarDistanciayReciptividad(form)
{
	if(form.listaDistancia.selectedIndex < 1){
		alert("Debe seleccionar la distacia al punto de embarque m�s cercano");
		form.distanciaembarque.focus();
		return false;
	}
	if(form.listaReceptividad.selectedIndex < 1){
		alert("Debe seleccionar la reciptividad de la tierra");
		form.capacidadganadera.focus();
		return false;
	}
	return true;
}

function formatearSuperficie(form)
{
	var ver = 0;
	var inv = 0;
	var sup = form.sup.value;
	var new_sup = formato(sup);
	if(!new_sup){
		alert("Debe indicar la superficie de la tierra en hect�reas-centi�reas-�reas-dm2");
		form.sup.focus();
		return false;
	}
	//si hay superficie veranada formatea
	if(form.veranada.value != ""){
		ver = formato(form.veranada.value);
		if(!ver){
			alert("Debe indicar la superficie de veranada en hect�reas-centi�reas-�reas-dm2");
			form.veranada.focus();
			return false;
		}
		form.supveranada.value = ver; 
	}
	//si hay superficie invernada formatea	
	if(form.invernada.value != ""){
		inv = formato(form.invernada.value);
		if(!inv){
			alert("Debe indicar la superficie de invernada en hect�reas-centi�reas-�reas-dm2");
			form.invernada.focus();
			return false;
		}
		form.supinvernada.value = inv;
	}
	if((parseInt(ver) + parseInt(inv)) > parseInt(new_sup)){
		alert("La superficie de veranada o invernada no puede ser superior a la superficie total de la tierra");
		form.sup.focus();
		return false;
	}
	form.superficie.value = new_sup;	/*cargo la superficie que sera enviada*/
	return true;
}

function formato(valor)
{
	var arr = valor.split("-");
	if(arr.length > 4){
		return false;
	}

	//cargo el valor de hectareas
	var sup = arr[0];
	
	//si tiene valor de centiarea lo controlo y asigno, si no tiene termina el formateo
	if(!(typeof(arr[1]) == 'undefined')){
		if(arr[1].length > 2){
			return false;
		}else{
			if(arr[1] >= 0) sup = sup+'.'+arr[1];
		}
	}
	
	//si tiene valor de area lo controlo y asigno, si no tiene termina el formateo
	if(!(typeof(arr[2]) == 'undefined')){
		if(arr[2].length > 2){
			return false;
		}else{
			if(arr[2] >= 0) sup = sup+arr[2];
		}
	}

	//si tiene valor de dm2lo controlo y asigno, si no tiene termina el formateo
	if(!(typeof(arr[3]) == 'undefined')){
		if(arr[3].length > 2){
			return false;
		}else{
			if(arr[3] >= 0) sup = sup+arr[3];
		}
	}
	return sup;
}


function comprobarSuperficie()
{
	var sup = document.getElementById("sup").value;
	document.getElementById("sup").value = comprobar(sup);
	var ver = document.getElementById("veranada").value;
	document.getElementById("veranada").value = comprobar(ver);
	var inv = document.getElementById("invernada").value;
	document.getElementById("invernada").value = comprobar(inv);
	return true;
}

function comprobar(valor)
{
	if(valor == 0){
		return 0;
	}else{
		var arr = valor.split('.');
		var nuevo_valor = arr[0];
		nuevo_valor = nuevo_valor+'-'+arr[1].substring(0,2)+'-'+arr[1].substring(2,4)+'-'+arr[1].substring(4);
		return nuevo_valor;
	}
}


function cancelarEnvio()
{
	document.location = "./index.php?controlador=inspeccionbasico&&accion=mostrarinspeccion";
	return(true);
}


function buscarSolicitudRural(form)
{    
	var expe = form.expediente.value;
	var solicitante = form.solicitante.value;
	
	url = "base.php?controlador=solicitud&&accion=elegirsolicitudrural&&expediente="+expe+"&&solicitante="+solicitante;  
	open(url, "listasolicitudrural", "width=500, height=400, toolbar=no, top=200, left=200 ");
	return true;
} 

function cargarSolicitudRural(id, fecha, expediente, solicitante)
{
	form = document.getElementById("formulario");
	form.idsolicitud.value = id;
	form.solicitante.value = solicitante;
	form.fechasolicitud.value = fecha;
	form.expediente.value = expediente;
	return true;
}

function seleccionarItem(combo, clave)
{
	var j=0;
	for (j = 0; j <combo.length; j++) {
        if (combo.options[j].value==clave) combo.options[j].selected=true;
    }
}

function comprobarListas()
{
	var recept = document.getElementById("capacidadganadera").value;
	var dist = document.getElementById("distanciaembarque").value;
	var listaRec = document.getElementById("listaReceptividad");
	var listaDist = document.getElementById("listaDistancia");

	seleccionarItem(listaRec,recept);
	seleccionarItem(listaDist,dist);
}

