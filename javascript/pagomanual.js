function enviarDatos(form)
{
    if(validarDatos(form)){
        form.submit();
        return true;
    }
    return false;
}

function validarDatos(form)
{
    if(!validarFechas(form))
        return false;

	if(!validarCobrado(form))
		return false;

	document.getElementById("observacionant").disabled = false;
    return true;
}

function cancelarEnvio(idcuenta)
{
    document.location = "./index.php?controlador=cobro&&cuentapagomanual&&idcuenta="+idcuenta;
    return true;
}

function validarCobrado(form)
{
	var monto = form.montocobrado.value*1;
	var deuda = form.saldo_sf.value*1 + form.interesmora_sf.value*1;
	if(monto > deuda || monto == 0){
		alert("El monto cobrado no puede superar la deuda de la cuota o ser cero");
		form.montocobrado.focus();
		return false;
	}
	return true;
}

function validarFechas(form)
{
	var fechapago = form.fechapago.value;
	var fechavenc = form.fechavenc.value;
	var fechamora = form.fechacalculomora.value;
	var hoy = diadehoy();
	if(!esFecha(fechapago)){
		alert("La fecha de cobro no es v�lida");
		form.fechapago.focus();
		return false;
	}
	//si la fecha de pago es superior al dia de hoy
	if(!fechasiguales(fechapago, hoy) && !compararfechas(fechapago, hoy)){
		alert("La fecha de cobro no puede ser superior al d�a de hoy");
		form.fechapago.focus();
		return false;
	}
	//si la cuota esta vencida
	/* if(compararfechas(fechavenc, fechapago)){
		//si la fecha de pago es superior a la de calculo de mora
		if(!esFecha(fechamora) || (!fechasiguales(fechamora, fechapago) && compararfechas(fechamora, fechapago))){
			alert("La fecha de cobro de una cuota vencida no puede ser superior al ultimo c�lculo de mora. Ingrese manualmente el inter�s por mora");
			form.fechapago.focus();
			return false;
		}
	} */
	return true;
}
