<?php
class View
{
    var $htmlText;
	var $htmlTemplate;
	var $tpl_file;
    var $fileReaded;
	
	
    function __construct() 
	{
	}
 
	public function show($name, $vars = array()) 
	{
		//$name es el nombre de nuestra plantilla, por ej, listado.php
		//$vars es el contenedor de nuestras variables, es un arreglo del tipo llave => valor, opcional.
 
		//Traemos una instancia de nuestra clase de configuracion.
		$config = Config::singleton();
        $this->htmlText=""; 
		
		
		
		
		
		//Armamos la ruta a la plantilla
		$path = $config->get('viewsFolder') . $name;
        $this->htmlTemplate=$path; 
		//Si no existe el fichero en cuestion, tiramos un 404
		if (file_exists($path) == false) 
		{
			trigger_error ('Template `' . $path . '` does not exist.', E_USER_NOTICE);
			return false;
		}
        
		$this->tpl_file = $path;
        $this->fileReaded = $this->fileData = @fopen($this->tpl_file, 'r');
		 $this->htmlTemplate = fread($this->fileData, filesize($this->tpl_file));
         $this->htmlTemplate = str_replace ("'", "\'", $this->htmlTemplate);
         fclose($this->fileData);
		
		
		
		//Si hay variables para asignar, las pasamos una a una.
		if(is_array($vars)){
		   $this->asignarVariables($vars);
		
		//{
         //           foreach ($vars as $key => $value) 
         //           {
          //     	$$key = $value;
         //           }
                }
       
	   
		//Finalmente, incluimos la plantilla.
		//include($path);
		echo ($this->htmlText!="")?$this->htmlText:$this->htmlTemplate;
		
	}
	
	
	
	public function showframe($name, $vars = array()) 
	{
		//$name es el nombre de nuestra plantilla, por ej, listado.php
		//$vars es el contenedor de nuestras variables, es un arreglo del tipo llave => valor, opcional.
 
		//Traemos una instancia de nuestra clase de configuracion.
		$config = Config::singleton();
        $this->htmlText=""; 
		
		
		//Armamos la ruta a la plantilla
		$path = '../'.$config->get('viewsFolder') . $name;
        $this->htmlTemplate=$path; 
		//Si no existe el fichero en cuestion, tiramos un 404
		if (file_exists($path) == false) 
		{
			trigger_error ('Template `' . $path . '` does not exist.', E_USER_NOTICE);
			return false;
		}
        
		$this->tpl_file = $path;
        $this->fileReaded = $this->fileData = @fopen($this->tpl_file, 'r');
		 $this->htmlTemplate = fread($this->fileData, filesize($this->tpl_file));
         $this->htmlTemplate = str_replace ("'", "\'", $this->htmlTemplate);
         fclose($this->fileData);
		
		
		
		//Si hay variables para asignar, las pasamos una a una.
		if(is_array($vars)){
		   $this->asignarVariables($vars);
		
		//{
         //           foreach ($vars as $key => $value) 
         //           {
          //     	$$key = $value;
         //           }
                }
       
	   
		//Finalmente, incluimos la plantilla.
		//include($path);
		echo ($this->htmlText!="")?$this->htmlText:$this->htmlTemplate;
		
	}
	
	
	
	public function show1($name, $vars = array()) 
	{
		//$name es el nombre de nuestra plantilla, por ej, listado.php
		//$vars es el contenedor de nuestras variables, es un arreglo del tipo llave => valor, opcional.
 
		//Traemos una instancia de nuestra clase de configuracion.
		$config = Config::singleton();
 
		//Armamos la ruta a la plantilla
		$path = $config->get('viewsFolder') . $name;
 
		//Si no existe el fichero en cuestion, tiramos un 404
		if (file_exists($path) == false) 
		{
			trigger_error ('Template `' . $path . '` does not exist.', E_USER_NOTICE);
			return false;
		}
        
		//Si hay variables para asignar, las pasamos una a una.
		if(is_array($vars)){
		  
                    foreach ($vars as $key => $value) 
                    {
             	$$key = $value;
                    }
                }
       
	   
		//Finalmente, incluimos la plantilla.
		include($path);
		
		
	}
	
	public  function asignarVariables($vars )
	{
        
			$this->vars = $vars;
			$this->htmlText = preg_replace('#\{([a-z0-9\-_]*?)\}#is', "' . $\\1 . '", $this->htmlTemplate);
            reset ($this->vars);
            while (list($key, $val) = each($this->vars)) 
			{
			   switch (substr($key,0,5)){
			          case "TABLA":
         				 	 
         				 	 $$key =$this->ConvTabla ( $val ," <td  width='auto'>%s</td>\n");
							 break 1;
        				case "LISTA":
         				 	 
         				 	 $$key =$this->ConvLista ( $val ,"<option value='%s' %s >%s</option>\n");
							 break 1;
                        				
					    default:
						
         				 	 $$key = $val;
							 
         				 	 break 1;							 
							 }
            }
        eval("\$this->htmlText = '$this->htmlText';");
            reset ( $this->vars);
          while (list($key, $val) = each($this->vars)) 
			{
               unset($$key);
            }
            $this->htmlText = str_replace ("\'", "'", $this->htmlText);
            return true;
        
    }
	
    

  public	function ConvLista($parArray,$txtAll)
	{   
        $texto="";
		if (is_array($parArray)) 
		{
		    reset ($parArray);
			//
			
			if( array_key_exists('txtAll',$parArray) ) {
				$txtAll = $parArray['txtAll'];
				unset($parArray['txtAll']);
			}
			if( array_key_exists('selected',$parArray) ) {
				$s=$parArray['selected'];
				
				unset($parArray['selected']);
				if( !is_array($s) ) $s = array($s);
			} else {
				$s = array();
			}
			//
		    foreach($parArray as $arrElemento)
		    {
				$cero=array_shift($arrElemento);
				
				array_unshift( $arrElemento,$cero,( in_array( $cero, $s) ) ? " selected " : "");
				eval("\$texto .= sprintf(\$txtAll, '".implode( "','", $arrElemento )."' );");
				
		    }
        }
		
		return $texto;
	}

  public	function ConvTabla($parArray,$txtAll)
	{   
	
	//Array (               [0] => Array ( [0] => 1 [1] => Administracion ) 
	        //              [1] => Array ( [0] => 7 [1] => PEPIN ) 
			//			    [2] => Array ( [0] => 4 [1] => Socios ) ) 
	
	
	//Array ( [0] => Array ( [id] => 1 [nombrecompleto] => administrador        [usuario] => administrador [sucursal] => Rawson ) 
	//        [1] => Array ( [id] => 3 [nombrecompleto] => Estela               [usuario] => eperfumo      [sucursal] => Rawson ) 
	//		  [2] => Array ( [id] => 7 [nombrecompleto] => fffffffffffffftttttt [usuario] => rrrrr         [sucursal] => Rawson ) ) 
	
	 //Array ( [nombrecompleto] => administrador [usuario] => administrador [sucursal] => Rawson ) 
	 //Array ( [nombrecompleto] => Estela [usuario] => eperfumo [sucursal] => Rawson ) 
	 //Array ( [nombrecompleto] => fffffffffffffftttttt [usuario] => rrrrr [sucursal] => Rawson )  
	  
        $texto="";
		if (is_array($parArray)) 
		{
		    reset ($parArray);
			//
			
			if( array_key_exists('txtAll',$parArray) ) {
				$txtAll = $parArray['txtAll'];
				unset($parArray['txtAll']);
			}
			if( array_key_exists('selected',$parArray) ) {
				$s=$parArray['selected'];
				
				unset($parArray['selected']);
				if( !is_array($s) ) $s = array($s);
			} else {
				$s = array();
			}
			//
			$i=0;
			$otroarray=$parArray;
		   
				foreach($parArray as $arrElemento => $resultado){
				$idres=$resultado['id'];
                   $texto.="<tr id=".$resultado['id']."  onmouseover=\"colorear(this);\"  onmouseout=\"soloblanco(this);\" onclick=\" marcacelda(this);\" >\n";
			       $j=0;
				   $cero=array_shift($resultado);
			   foreach($resultado as $id_parametro => $parametro){
			    $j=$j+1;
				 eval("\$texto .= sprintf(\$txtAll, '".$parametro."' );");
                } 
				 $texto.="</tr><br/>";
       
		    }
        }
		return $texto;
	}
	
	
	public function verconsulta($name, $vars = array()) 
	{
		//$name es el nombre de nuestra plantilla, por ej, listado.php
		//$vars es el contenedor de nuestras variables, es un arreglo del tipo llave => valor, opcional.
 
		//Traemos una instancia de nuestra clase de configuracion.
		$config = Config::singleton();
        $this->htmlText=""; 
		
		
		
		
		
		//Armamos la ruta a la plantilla
		$path = $config->get('viewsFolder') . $name;
        $this->htmlTemplate=$path; 
		//Si no existe el fichero en cuestion, tiramos un 404
		if (file_exists($path) == false) 
		{
			trigger_error ('Template `' . $path . '` does not exist.', E_USER_NOTICE);
			return false;
		}
        
		$this->tpl_file = $path;
        $this->fileReaded = $this->fileData = @fopen($this->tpl_file, 'r');
		 $this->htmlTemplate = fread($this->fileData, filesize($this->tpl_file));
         $this->htmlTemplate = str_replace ("'", "\'", $this->htmlTemplate);
         fclose($this->fileData);
		
		
		
		//Si hay variables para asignar, las pasamos una a una.
		if(is_array($vars)){
		   $this->asignarVariables($vars);
		
		//{
         //           foreach ($vars as $key => $value) 
         //           {
          //     	$$key = $value;
         //           }
                }
       
	   
		//Finalmente, incluimos la plantilla.
		//include($path);
		
		echo(($this->htmlText!="")?$this->htmlText:$this->htmlTemplate);
		
	}
	
	
}

?>