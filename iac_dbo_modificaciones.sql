/* Archivo de modificaciones a la base de datos iac_dbo */
/* Tec. Nicolás H. Fernández */
/* Sistema IAC - Neurosoft */ 

/* 2018-08-05 */
ALTER TABLE  `parametros` ADD  `interescuotas` DECIMAL( 6, 3 ) UNSIGNED NULL DEFAULT NULL;
ALTER TABLE  `parametros` ADD  `recuperomensura` DECIMAL( 6, 3 ) UNSIGNED NULL DEFAULT NULL;
UPDATE  `iac_dbo`.`menu` SET  `descripcion` =  'Modificar intereses',`callaccion` =  'verintereses' WHERE  `menu`.`id` =501 LIMIT 1;

/* 2018-08-18 */
CREATE TABLE  `iac_dbo`.`log` (
`id` INT NOT NULL AUTO_INCREMENT ,
`fecha` DATETIME NOT NULL ,
`idusuario` INT NOT NULL ,
`accion` VARCHAR( 254 ) NOT NULL ,
PRIMARY KEY (  `id` )
) ENGINE = INNODB;

/* 2018-09-10 */
ALTER TABLE  `cuentas` ADD  `recuperomensura` DECIMAL( 5, 2 ) UNSIGNED NULL DEFAULT  '20.00' COMMENT  'Porcentaje de Recupero de Mensura aplicado al momento de crear la cuenta' AFTER  `formalizacion`;

/* 2018-09-17 */
ALTER TABLE  `cuotas` ADD  `cuotaformalizacion` TINYINT NULL DEFAULT  '0' COMMENT  'Define si la cuota es parte del plan de la formalizacion (1) o no (0)' AFTER  `fechacalculomora`;

/* 2018-09-19 */
ALTER TABLE  `cuentas` ADD  `sumarintereses` TINYINT NOT NULL DEFAULT  '1' COMMENT  'Booleano que determina si los intereses se suman en un periodo definido (1) o no (0)';
ALTER TABLE  `cuentas` ADD  `cantcuotasformalizacion` INT UNSIGNED NOT NULL DEFAULT  '1' COMMENT  'Determina la cantidad de cuotas estipuladas para la formalizacion';
ALTER TABLE  `cuentas` ADD  `periododiferidoformalizacion` INT UNSIGNED NOT NULL DEFAULT  '1' COMMENT  'Guarda la cantidad del periodo diferido para el tipo de periodo elegido';
ALTER TABLE  `cuentas` ADD  `fechavencimientoformalizacion` DATE NOT NULL COMMENT  'Guardo la fecha de vencimiento de la primer cuota de la formalizacion';
ALTER TABLE  `cuentas` ADD  `cuotasparalelas` TINYINT NOT NULL DEFAULT  '0' COMMENT  'Define si las cuotas del plan se debe cobrar de forma paralela a la formalizacion (1) o no (0)';
ALTER TABLE  `cuentas` ADD  `periododiferidoplan` INT UNSIGNED NOT NULL DEFAULT  '1' COMMENT  'Guarda la cantidad del periodo diferido para el tipo de periodo elegido para el plan de cuotas';
ALTER TABLE  `cuentas` ADD  `fechavencimientoplan` DATE NOT NULL COMMENT  'Guardo la fecha de vencimiento de la primer cuota del plan';

/* 2018-09-26 */
UPDATE cuentas SET cantcuotasformalizacion = 0 WHERE formalizacion = 0;