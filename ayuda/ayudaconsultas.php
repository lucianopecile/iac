<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

<link rel="stylesheet" type="text/css" href="./css/paginasayuda.css" media="all"/>

<title>Ayuda a Sistema IAC</title>
</head>

<body>

<div id="inicio">
	<h1>Consultas</h1>
	
	<ul>
		<li><a href=#estado>Estado de cuenta</a></li>
		<li><a href=#deudores>Listado de deudores a una fecha</a></li>
		<li><a href=#localidad>Listado de cuentas por localidad</a></li>
		<li><a href=#saldos>Listado de saldos totales</a></li>
		<li><a href=#ingresos>Listado de ingresos</a></li>
	</ul>
</div>

<div class="descripciones" > 
	<p>
	Esta secci&oacute;n detalla c&oacute;mo realizar consultas acerca de las deudas, cobros y saldos de las cuentas cargadas en el sistema.
	Todos los listados surgidos de estas consultas pueden ser impresos en formato PDF para luego ser enviados a impresi&oacute;n en papel.
	</p>
</div>

<div class="descripciones" id="estado">
	<h2>Estado de cuenta</h2>
	<p>Esta consulta presenta un resumen del estado de una cuenta a una fecha determinada presentando el total financiado, el inter&eacute;s
		por financiaci&oacute;n, la deuda vencida, deuda no vencida, intereses punitorios y el listado de las cuotas entre otros datos.
	</p>
	<p>Para consultar el estado de una cuenta acceda a la opci&oacute;n <b>estado de cuenta</b> del men&uacute;.<br/>
		Seleccione de la lista la cuenta que desea consultar.<br/>
		A continuaci&oacute;n ver&aacute; el resultado de la consulta que detalla los montos totales de la cuenta y debajo el listado de
		cuotas con sus montos individuales.
	</p>
	<p>Si desea hacer una consulta de estado de cuenta a una fecha diferente, ingrese la fecha en el campo de fecha al final de la pantalla y
		presione el bot&oacute;n <img alt="boton consultar" src="css/images/boton-consultar.png" />.
	</p>
	<p>Por &uacute;ltimo, para enviar el resultado de la consulta a imprimir presione el bot&oacute;n
		<img alt="boton imprimir listado" src="css/images/boton-imprimir-listado.png" />.
	</p>
</div>

<div class="descripciones" id="deudores">
	<h2>Listado de deudores a una fecha</h2>
	<p>Esta consulta lista la deuda de cada cuenta cuyo vencimiento cae dentro del periodo indicado.</p>
	<p>Para esto seleccione la opci&oacute;n <b>listar deudores</b> del men&uacute; principal.<br/>
		A continuaci&oacute;n ingrese las fechas <i>desde</i> y <i>hasta</i> para determinar el rango de la consulta que por defecto toma el
		corriente a&ntilde;o.
		Luego presione el bot&oacute;n <img alt="boton consultar" src="css/images/boton-consultar-2.png" />
	</p>
	<p>Por &uacute;ltimo, para enviar el resultado de la consulta a imprimir presione el bot&oacute;n
		<img alt="boton imprimir listado" src="css/images/boton-imprimir-listado.png" />.
	</p>
</div>


<div class="descripciones" id="localidad">
	<h2>Listado de cuentas por localidad</h2>
	<p>Esta consulta muestra un listado de las cuentas registradas para una determinada localidad o un conjunto de localidades y dentro de un
		periodo espec&iacute;fico.</p>
	<p>Ingrese a la opci&oacute;n <b>cuentas por localidad</b> del men&uacute; principal.<br/>
		En la siguiente pantalla seleccione de la lista la/s localidad/es que desea consultar y env&iacute;elas a la lista de <i>elegidas</i>
		presionando el bot&oacute;n <img alt="boton flechita" src="css/images/boton-flechita.png" />. Si no selecciona ninguna se
		tomar&aacute;n por defecto todas la localidades.<br/>
		Luego ingrese las fechas <i>desde</i> y <i>hasta</i>. Se listar&aacute;n las cuentas que fueron creadas dentro de dichas fechas. Por
		defecto se toman los &uacute;ltimos 10 a&ntilde;os.
		Finalmente presione el bot&oacute;n <img alt="boton consultar" src="css/images/boton-consultar.png" />.
	</p>
	<p>Por &uacute;ltimo, para enviar el resultado de la consulta a imprimir presione el bot&oacute;n
		<img alt="boton imprimir consulta" src="css/images/boton-imprimir-consulta.png" />
	</p>

</div>

<div class="descripciones" id="saldos">
	<h2>Listado de saldos totales</h2>
	<p>Esta consulta presenta un listado de todas las cuentas con saldo pendiente, indicando la deuda total a final del corriente a&ntilde;o
		adem&aacute;s del saldo actual. Tambi&eacute;n muestra el total de saldos, deudas y monto cobrado para todas las cuentas pendientes
		del organismo.
	</p>
	<p>Para esto seleccione la opci&oacute;n <b>listado de saldos</b> del men&uacute; principal y autom&aacute;ticamente se presentar&aacute;
		en pantalla el listado.
	</p>
	<p>Para consultar los saldos y deudas calculadas a una fecha diferente, ingrese la fecha en campo <i>consultar deudas al d&iacute;a</i> y
		presione el bot&oacute;n <img alt="boton consultar" src="css/images/boton-consultar.png" />.
	</p>
	<p>Por &uacute;ltimo, para enviar el resultado de la consulta a imprimir presione el bot&oacute;n
		<img alt="boton imprimir listado" src="css/images/boton-imprimir-listado.png" />.
	</p>
</div>

<div class="descripciones" id="ingresos">
	<h2>Listado de ingresos</h2>
	<p>Esta consulta muestra un listado de todos los archivos de cobros enviados por el banco que ya han sido procesados por el sistema. Para
		cada archivo se indica la fecha de procesamiento, el nombre del archivo, el monto total ingresado al organismo en ese procesamiento y
		finalmente el resultado del procesamiento.
	</p>
	<p>Para esta consulta simplemente seleccione del men&uacute; la opci&oacute;n <b>consultar archivos importados</b>.<br/>
	Autom&aacute;ticamente el sistema presentar&aacute; el listado de los archivos procesados ordenados seg&uacute;n su fecha de procesamiento en orden
	descendiente.
	</p>
	<img alt="consulta archivos" src="css/images/consulta-archivos.png"></img>
	<p>Para consultar los ingresos en m&aacute;s detalle haga doble clic en el archivo que desea consultar y el sistema presentar&aacute; un listado de
		todos los cobros procesados dentro de ese archivo. <br/>
	Por cada cobro se detalla la fecha en la que se realiz&oacute;, el monto cobrado y el n&uacute;mero de cuenta y de cuota a la cual corresponde el cobro.
	</p>
</div>

<div id="footer"><a href=#inicio>Volver al inicio</a></div>
</body>
</html>
