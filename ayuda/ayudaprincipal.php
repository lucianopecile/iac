<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

<link rel="stylesheet" type="text/css" href="./css/paginasayuda.css" media="all"/>

<title>Ayuda a Sistema IAC</title>
</head>

<body>

<div id="inicio">
	<h1>Pantalla principal</h1>
	<ul>
		<li><a href=#ingreso>C&oacute;mo ingresar al sistema.</a></li>
		<li><a href=#salir>C&oacute;mo salir del sistema</a></li>
		<li><a href=#proceso>C&oacute;mo dar de alta una nueva cuenta</a></li>
		<li><a href=#reliquidar>C&oacute;mo reliquidar/modificar una cuenta</a></li>
	</ul>
</div>


<div class="descripciones" >
	<p>En las siguientes l&iacute;neas se explica qu&eacute; pasos debe seguir el usuario para ingresar al sistema con contrase&ntilde;a y 
		para salir y bloquear la sesi&oacute;n de usuario. Tambi&eacute;n se detalla c&oacute;mo es el circuito completo de tareas para la
		creaci&oacute;n de una nueva cuenta y qu&eacute; pasos son necesarios para reliquidar una cuenta existente.</p>
</div>

<div class="descripciones" id="ingreso">
	<h2>C&oacute;mo ingresar al sistema</h2>
	<p>En el cuadro de <i>log-in</i> a la izquierda de la pantalla, ingrese su nombre de usuario y su contrase&ntilde;a y haga clic el
		bot&oacute;n <img src="css/images/boton-ingresar.png" alt="boton ingresar"/> o presione la tecla ENTER.
	</p>
	<img alt="login-box" src="css/images/login.png"></img>
	<p>A la izquierda de la ventana se desplegar&aacute; un men&uacute; con todas las operaciones que tiene permiso para realizar el usuario.
	</p>
</div>

<div class="descripciones" id="salir">
	<h2>C&oacute;mo salir del sistema</h2>
	<p>Para salir del sistema simplemente debe hacer clic en el bot&oacute;n <img src="css/images/boton-salir.png" alt="boton salir"/> que
		se encuentra en la parte superior de la pantalla. Una vez fuera del sistema, la sesi&oacute;n del usuario queda cerrada y protegida
		con contrase&ntilde;a.</p>
</div>

<div class="descripciones" id="proceso">
	<h2>C&oacute;mo dar de alta una nueva cuenta</h2>
	<p>El proceso para dar de alta una nueva cuenta es el siguiente:</p>
	<ul>
		<li>El primer paso es tener el poblador solicitante cargado en el sistema con sus datos personales correspondientes. Si no lo tiene 
			consulte la secci&oacute;n <a href="inicio.php?controlador=ayuda&&accion=ver_pagina&&pagina=ayudapobladores.php&&sector=#alta">
			c&oacute;mo cargar un nuevo poblador.</a> </li>
		<li>Luego es necesario tener el expediente con la solicitud de tierra que dio lugar a la creaci&oacute;n de la cuenta. Si no existe
			la solicitud en el sistema lea la secci&oacute;n
			<a href="inicio.php?controlador=ayuda&&accion=ver_pagina&&pagina=ayudarurales.php&&sector=#alta">c&oacute;mo cargar una nueva
			solicitud rural.</a> para tierras rurales o
			<a href="inicio.php?controlador=ayuda&&accion=ver_pagina&&pagina=ayudaurbanas.php&&sector=#alta">c&oacute;mo cargar una nueva
				solicitud urbana.</a> para tierras urbanas.</li>
		<li>El pr&oacute;ximo paso es dar de alta un nuevo informe de inspecci&oacute;n correspondiente a la solicitud en cuesti&oacute;n.
			Si la solicitud es de tierra urbana, este paso no es necesario ya que los informes de inspecci&oacute;n corresponden solamente a
			las tierras rurales. Consulte la secci&oacute;n
			<a href="inicio.php?controlador=ayuda&&accion=ver_pagina&&pagina=ayudainspeccion.php&&sector=#alta">c&oacute;mo cargar un nuevo
			informe de inspecci&oacute;n.</a> Tambi&eacute;n debe asignar a dicha tierra el valor de la hect&aacute;rea seg&uacute;n su
			capacidad ganadera y su distancia al punto de embarque m&aacute;s cercano, para esto consulte
			<a href="inicio.php?controlador=ayuda&&accion=ver_pagina&&pagina=ayudainspeccion.php&&sector=#precio">c&oacute;mo definir el
			precio por hect&aacute;rea.</a> </li>
		<li>Por &uacute;ltimo se debe crear la nueva cuenta correspondiente a la solicitud, liquidar la cuenta y generar las cuotas. Para esto
			consulte la secci&oacute;n <a href="inicio.php?controlador=ayuda&&accion=ver_pagina&&pagina=ayudapobladores.php&&sector=#alta">
			c&oacute;mo cargar una nueva cuenta.</a></li>
	</ul>
	<p>Si todos los pasos fueron concretados con &eacute;xito, la nueva cuenta estar&aacute; creada con sus cuotas correspondientes.</p>
</div>

<div class="descripciones" id="reliquidar">
	<h2>C&oacute;mo reliquidar/modificar una cuenta</h2>
	<p>El proceso para reliquidar una cuenta es el siguiente:</p>
	<ul>
		<li>Si es necesario, debe realizar las modificaciones correspondientes a la solicitud. Si es de tierra urbana consulte la
			secci&oacute;n <a href="inicio.php?controlador=ayuda&&accion=ver_pagina&&pagina=ayudaurbanas.php&&sector=#mod">c&oacute;mo
			modificar una solicitud urbana.</a> Si es una solicitud de tierra rural consulte
			<a href="inicio.php?controlador=ayuda&&accion=ver_pagina&&pagina=ayudainspeccion.php&&sector=#mod">c&oacute;mo modificar un
			informe	de inspecci&oacute;n.</a> y luego debe ajustar nuevamente el precio de la hect&aacute;rea seg&uacute;n se detalla en
			<a href="inicio.php?controlador=ayuda&&accion=ver_pagina&&pagina=ayudainspeccion.php&&sector=#precio">c&oacute;mo definir el
			precio por hect&aacute;rea.</a> </li>
		<li>Luego debe modificar los datos de liquidaci&oacute;n de la cuenta si corresponde y reliquidar la cuenta con los nuevos datos de
			solicitud. Consulte la secci&oacute;n
			<a href="inicio.php?controlador=ayuda&&accion=ver_pagina&&pagina=ayudacuentas.php&&sector=#reliquidar">c&oacute;mo
			reliquidar/modificar una cuenta.</a></li>
	</ul>
	<p>Si el procedimiento fue realizado correctamente, la cuenta tendr&aacute; un nuevo valor de liquidaci&oacute;n y por supuesto nuevos
		montos en las cuotas.<br/>
	Es importante destacar que las cuotas que ya recibieron un pago (parcial o total) no ser&aacute;n modificadas.</p>
</div>

<div id="footer"><a href=#inicio>Volver al inicio</a> </div>
</body>
</html>
