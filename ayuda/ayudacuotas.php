<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link rel="stylesheet" type="text/css" href="./css/paginasayuda.css" media="all"/>

<title>Ayuda a Sistema IAC</title>
</head>

<body>

<div id="inicio">
	<h1>Cuotas</h1>
	
	<ul>
		<li><a href=#editar>C&oacute;mo editar una cuota</a></li>
		<li><a href=#impr>Imprimir una boleta de pago a t&eacute;rmino</a></li>
		<li><a href=#pago>Reimprimir una boleta de pago vencida</a></li>
		<li><a href=#pagoparcial>Imprimir una boleta de pago parcial</a></li>
		<li><a href=#gastos>C&oacute;mo modificar los gastos fijos de una cuenta</a></li>
		<li><a href=#diferir>C&oacute;mo diferir los vencimientos de las cuotas</a></li>
	</ul>
</div>

<div class="descripciones" > 
	<p>En esta secci&oacute;n se detalla c&oacute;mo se realizan las tareas t&iacute;picas sobre las cuotas de una cuenta, tales como la
		impresi&oacute;n de chequeras y la edici&oacute;n de cuotas.
	</p>
</div>

<div class="descripciones" id="editar">
	<h2>C&oacute;mo editar una cuota</h2>
	<p>En el men&uacute; de opciones se encuentra la opci&oacute;n <b>edici&oacute;n de cuotas</b> la cual brinda la posibilidad de modificar
		el monto de inter&eacute;s por mora de una cuota y as&iacute; poder descontar o incrementar una deuda.</p>
	<p>Ingrese a la opci&oacute;n <b>edici&oacute;n de cuotas</b> y seleccione de la lista la cuenta que desea editar.<br/>
		Luego ver&aacute; los datos de la cuenta y la lista de cuotas correspondiente. Seleccione la cuota que necesita modificar haciendo
		doble clic sobre la misma.<br/>
		Acceder&aacute; entonces a la pantalla de edici&oacute;n de cuotas, un formulario similar al siguiente:
	</p>
	<img src="css/images/editar-cuota.png" alt="editar cuota"></img>
	<p>
		Una vez all&iacute;, puede modificar manualmente el monto de inter&eacute;s por mora.<br/>
		Cabe mencionar que la fecha en la cual se realiz&oacute; la modificaci&oacute;n queda registrada en el sistema como "fecha del
		&uacute;ltimo c&aacute;lculo de mora" y ser&aacute; utilizada para luego calcular nuevamente el inter&eacute;s por mora a una fecha
		posterior.<br/>
		Tambi&eacute;n puede ingresar all&iacute; cualquier observaci&oacute;n que crea conveniente.<br/>
		Por &uacute;ltimo presione <img src="css/images/boton-guardar-cambios.png" alt="boton guardar cambios"/> para salvar las
		modificaciones sobre la cuota.
	</p>

</div>

<div class="descripciones" id="impr">
	<h2>Imprimir una boleta de pago a t&eacute;rmino</h2>
	<p>Imprimir una boleta de pago a t&eacute;rmino significa que la boleta o chequera de pago que se va a imprimir implica cuotas que se van
		a pagar antes de su fecha de vencimiento, por el monto total de la cuota y que no cuentan con intereses punitorios acumulados.<br/>
		Adem&aacute;s se imprimir&aacute; una boleta individual por cada cuota.
		Si necesita imprimir boletas por pagos atrasados o una boleta global para el pago de varias cuotas consulte la secci&oacute;n
		<a href=#pago>reimprimir una boleta de pago vencida</a>.
		Si necesita imprimir una boleta por un pago parcial de una cuota consulte la secci&oacute;n <a href=#pagoparcial>imprimir una boleta
			de pago parcial</a>.
	</p>
	<p>Seleccione del men&uacute; de opciones la opci&oacute;n <b>imprimir chequera</b>.<br/>
		Ingrese el n&uacute;mero de expediente y presione ahora el bot&oacute;n
		<img src="css/images/boton-buscar-cuenta.png" alt="boton buscar cuenta"/> para buscar la cuenta correspondiente.<br/>
		Seleccione la cuenta de la lista haciendo doble clic.<br/>
	</p>
	<p>Ahora se encuentra en una pantalla con los datos de la cuenta y las cuotas correspondientes.<br/>
		<img style="border:thin solid;" alt="rango cuotas" src="css/images/rango-cuotas.png" /><br/>
		Ingrese en los campos <i>cuota inicial</i> y <i>cuota final</i> el rango de cuotas para las cuales desea imprimir las boletas.<br/>
		Puede ingresar el mismo n&uacute;mero de cuota en ambos campos si desea imprimir s&oacute;lo una boleta. Recuerde que se
		imprimir&aacute; una boleta individual para cada cuota.<br/>
		Si es necesario ingrese otros titulares de la cuenta en el campo correspondiente. Puede ingresar cada titular separado por '-' con
		el formato "Apellidos, Nombres" o el nombre de una firma si corresponde.<br/>
		Por una cuesti&oacute;n de espacio en la boleta el campo de otros titulares est&aacute; limitado a un m&aacute;ximo de 60 caracteres.
	</p>
	<p>
		Por &uacute;ltimo presione el bot&oacute;n <img src="css/images/boton-imprimir-chequera.png" alt="boton imprimir chequera"/> para
		enviar a imprimir la chequera.
	</p>
</div>

<div class="descripciones" id="pago">
	<h2>Reimprimir una boleta de pago vencida</h2>
	<p>Esta funcionalidad permite la reimpresi&oacute;n de la boleta de una cuota ya vencida por lo cual calcula y adiciona al monto los
		intereses punitorios a la fecha.<br/>
		En este caso se imprimir&aacute; un solo tal&oacute;n o boleta de pago por el monto global de la/s cuota/s seleccionada/s. Es decir,
		que si el poblador desea pagar varias cuotas o cancelar la deuda debe utilizar esta funcionalidad.
	</p>
	<p>Desde el men&uacute; de opciones acceda a la funci&oacute;n <b>cobrar cuotas</b> y seleccione de la lista la cuenta correspondiente.
		<br/>
		Una vez all&iacute;, ver&aacute; los datos de la cuenta y las cuotas correspondientes.<br/>
		Ingrese en los campos <i>cuota inicial</i> y <i>cuota final</i> el rango de las cuotas que desea cobrar y en el campo <i>fecha de
			dep&oacute;sito</i> la fecha en la cual vence el pago de la boleta.<br/>
		<img style="border:thin solid;" alt="rango cuotas fecha" src="css/images/rango-cuotas-fecha.png" /><br/>
		Luego presione el bot&oacute;n <img src="css/images/boton-cobrar.png" alt="boton cobrar"/>.<br/>
		El sistema le mostrar&aacute; las cuotas individuales y los montos totales que se estar&aacute;n cobrando. Puede modificar
		manualmente el monto total a pagar en el campo <i>monto total del tal&oacute;n</i>.<br/>
		Si es necesario ingrese otros titulares de la cuenta en el campo correspondiente. Puede ingresar cada titular separado por '-' con
		el formato "Apellidos, Nombres" o el nombre de una firma si corresponde.<br/>
		Por una cuesti&oacute;n de espacio en la boleta el campo de otros titulares est&aacute; limitado a un m&aacute;ximo de 60 caracteres.
	</p>
	<p>
		Por &uacute;ltimo, si los valores son correctos, presione el bot&oacute;n
		<img src="css/images/boton-imprimir-talon.png" alt="boton imprimir talon"/> para enviar la boleta a impresi&oacute;n.
	</p>

</div>

<div class="descripciones" id="pagoparcial">
	<h2>Imprimir una boleta de pago parcial</h2>
	<p>Imprimir una boleta para pago parcial es una operaci&oacute;n similar a la de <a href=#pago>cobro de cuotas atrasadas</a>. Se utiliza
		para cobrar una cuota atrasada o no, pero que el monto que se desea cobrar no alcanza al monto total de la deuda por una cuota
		espec&iacute;fica.<br/>
	</p>
	<p>Desde el men&uacute; de opciones acceda a la funci&oacute;n <b>cobrar cuotas</b> y seleccione de la lista la cuenta correspondiente.
		<br/>
		Una vez all&iacute;, ver&aacute; los datos de la cuenta y las cuotas correspondientes.<br/>
		Ingrese en los campos <i>cuota inicial</i> y <i>cuota final</i> el n&uacute;mero de cuota a la que corresponde el pago parcial y en
		el campo <i>fecha de dep&oacute;sito</i> la fecha en la cual vence el pago de la boleta.<br/>
		<img style="border:thin solid;" alt="rango cuotas fecha" src="css/images/rango-cuotas-fecha.png" /><br/>
		Luego presione el bot&oacute;n <img src="css/images/boton-cobrar.png" alt="boton cobrar"/>.<br/>
		El sistema le mostrar&aacute; la cuota y los montos que se est&aacute; cobrando y los montos totales por intereses y deuda atrasada
		si existen.<br/>
		Ingrese manualmente el monto que se va cobrar al poblador en el campo <i>monto total del tal&oacute;n</i>. Tenga
		en cuenta que para que sea un pago parcial, dicho monto debe ser menor a la deuda total de la cuota.<br/>
		Si es necesario ingrese otros titulares de la cuenta en el campo correspondiente. Puede ingresar cada titular separado por '-' con
		el formato "Apellidos, Nombres" o el nombre de una firma si corresponde.<br/>
		Por una cuesti&oacute;n de espacio en la boleta el campo de otros titulares est&aacute; limitado a un m&aacute;ximo de 60 caracteres.
	</p>
	<p>
		Por &uacute;ltimo, si los valores son correctos, presione el bot&oacute;n
		<img src="css/images/boton-imprimir-talon.png" alt="boton imprimir talon"/> para enviar la boleta a impresi&oacute;n.
	</p>
</div>

<div class="descripciones" id="gastos">
	<h2>C&oacute;mo modificar los gastos fijos de una cuenta</h2>
	<p>Los gastos fijos de una cuenta implican montos extra tales como gastos administrativos, gastos de inspecci&oacute;n, derechos de
		pastaje, etc. Estos conceptos se definen al momento de la liquidaci&oacute;n de la cuenta pero pueden ser modificados posteriormente.
	</p>
	<p>Para modificar los gastos fijos de la cuenta acceda a la opci&oacute;n <b>cobrar cuotas</b> del men&uacute; de opciones.<br/>
		Seleccione la cuenta que desea modificar haciendo doble clic.<br/>
		Una vez all&iacute;, ver&aacute; los datos de la cuenta y las cuotas correspondientes.<br/>
		<img style="border:thin solid; float:right; margin-left:5px;" alt="gatos fijos" src="css/images/gastos-fijos.png"></img>
		Antes de realizar cualquier otra operaci&oacute;n, presione el bot&oacute;n <img src="css/images/boton-radio.png" alt="boton radio"/>
		que se encuentra junto a cualquiera de las cuotas de la cuenta.<br/>
		El sistema presentar&aacute; una nueva ventana con los gastos fijos posibles de agregar a la cuenta, tilde los que correspondan y
		seleccione <img src="css/images/boton-guardar.png" alt="boton guardar"/> para salvar los cambios.
	</p>
	<p>
		Los cambios en los gastos fijos afectan y se modifican para la cuenta en general, es decir que se aplicar&aacute;n a todas las cuotas
		al momento de imprimir las boletas.
		Por &uacute;ltimo, si es necesario puede proceder para <a href=#pago>imprimir las boletas de pago</a>.
	</p>
</div>

<div class="descripciones" id="diferir">
	<h2>C&oacute;mo diferir los vencimientos de las cuotas</h2>
	<p>La funci&oacute;n de diferir vencimientos permite retrasar o adelantar el vencimiento de un conjunto de cuotas. Si bien es posible
		retrasar los vencimientos, los montos por intereses punitorios calculados y registrados para una cuota no ser&aacute;n alterados
		mediante esta funcionalidad.
	</p>
	<p>Para diferir los vencimientos de la cuotas de una cuenta acceda a la opci&oacute;n <b>diferir vencimientos</b> del men&uacute;
		principal.<br/>
		Seleccione de la lista la cuenta que desea modificar haciendo doble clic.<br/>
		Una vez all&iacute;, ver&aacute; los datos de la cuenta y las cuotas correspondientes.<br/>
		En el campo <i>nueva fecha de vencimiento</i> ingrese la nueva fecha de vencimiento que se le asignar&aacute; la cuota indicada en el
		campo <i>nro cuota</i>.<br/>
		<img style="border:thin solid;" alt="nuevo vencimiento" src="css/images/nuevo-vencimiento.png" /><br/><br/>
		Por &uacute;ltimo, presione el bot&oacute;n <img src="css/images/boton-diferir.png" alt="boton diferir"/>
	</p>
	<p>Es importante aclarar que la funci&oacute;n de diferir vencimientos difiere las fechas de vencimiento de la cuota indicada y todas las
		cuotas posteriores seg&uacute;n los periodos de pago indicados al momento de crear la cuenta (anual, semestral, mensual, etc).
	</p>
</div>


<div id="footer"><a href=#inicio>Volver al inicio</a> </div>
</body>
</html>
