<?php


class ControlMenuAyuda
{

	function __construct()
	{
	    //Creamos una instancia de nuestro mini motor de plantillas
	    $this->view = new View();
	}


    public function rebuild_link($link, $controller_name, $page_name, $sector_name) {
   
		 // $link = es la pagina donde vamos a ir
		 // $controller_name es el nombre del controlador que se esta llamando
		 // $page_name es el nombre de la pagina html que se esta llamando
		 // $sector_name es el sector dentro de la pagina solicitada
		 // la accion es por defecto y siempre 'ver_pagina'.
	   
		$link_parts = explode("?", $link);
		$base_var = "?controlador=".$controller_name."&&accion=ver_pagina&&pagina=".$page_name."&&sector=".$sector_name;
		
		if (!empty($link_parts[1]))
		{
			$link_parts[1] = str_replace("&amp;", "##", $link_parts[1]);
			$parts = explode("##", $link_parts[1]);
			$newParts = array();
			
			foreach ($parts as $val)
			{
				$val_parts = explode("=", $val);
				if ($val_parts[0] != $parent_var)
				{
					array_push($newParts, $val);
				}
			}
			if (count($newParts) != 0) {
				$qs = "&amp;".implode("&amp;", $newParts);
			} 
			return $link_parts[0].$base_var.$qs;
		}
		else
		{
			return $link_parts[0].$base_var;
		}

   }

 
	
	

	public function dyn_menu()
	{
		require 'modelomenuayuda.php';

		$consulta = new ModeloMenuAyuda();
		
		//Le pedimos al modelo todos los items
		$result= $consulta->getmenu();
		if(!$result){
		   return(false);
		}
		
		while ($itemsmenu = mysql_fetch_object($result))
		{
			if ($itemsmenu->IdParent == 0)
				{
					$parent_menu[$itemsmenu->id]['descripcion'] = $itemsmenu->descripcion;
					$parent_menu[$itemsmenu->id]['link'] =  $itemsmenu->link_url;
					$parent_menu[$itemsmenu->id]['callcontrolador'] =$itemsmenu->callcontrolador;
					$parent_menu[$itemsmenu->id]['callpagina'] = $itemsmenu->callpagina;
					$parent_menu[$itemsmenu->id]['callsector'] = $itemsmenu->callsector;

				}
				else
				{
					$sub_menu[$itemsmenu->id]['parent'] = $itemsmenu->IdParent;
					$sub_menu[$itemsmenu->id]['descripcion'] = $itemsmenu->descripcion;
					$sub_menu[$itemsmenu->id]['link'] = $itemsmenu->link_url;
					$sub_menu[$itemsmenu->id]['callcontrolador'] =$itemsmenu->callcontrolador;
					$sub_menu[$itemsmenu->id]['callpagina'] = $itemsmenu->callpagina;
					$sub_menu[$itemsmenu->id]['callsector'] = $itemsmenu->callsector;
					$parent_menu[$itemsmenu->IdParent]['count']++;
				}
		}
		$arreglo[0]=$parent_menu;
		$arreglo[1]=$sub_menu;
		return $arreglo;
	
	}


	
	public function armarmenu($qs_val,$qs_accion,$c_menusolo,$c_menusub,$c_submenu,$c_glossy)
	{
		$arreglo_menu = $this->dyn_menu();

		if(!$arreglo_menu)
		{
			$menu = "";
			return $menu;
		}
	
		$parent_array = $arreglo_menu[0];
	   	$sub_array = $arreglo_menu[1]	; 
		$totalparent = count($parent_array);
		if ($totalparent <= 0)
		{
			$menu = "";
			return $menu;
		} 
		else
		{
			$menu = " <div class=\"".$c_glossy."\">\n";
			foreach ($parent_array as $pkey => $pval)
			{
				if (!empty($pval['count']))
				{
					$menu .= "<a class=\"".$c_menusub."\" href=\"".$pval['link']."?".$qs_val."=".$pval['callcontrolador']."&&accion=ver_pagina&&pagina=".$pval['callpagina']."&&sector=".$pval['callsector']."\">".$pval['descripcion']."<br/></a>\n";
					$_REQUEST[$qs_val]=$pkey;
				}
				else
				{
					$menu .= "<a  class=\"".$c_menusolo."\" href=\"".$pval['link']."\">".$pval['descripcion']."<br/></a>\n"; 
					$_REQUEST[$qs_val]=NULL;
				}
			
		        
				if (!empty($qs_val) && !($_REQUEST[$qs_val]==NULL))
				{
					$menu .= "<div class=\"".$c_submenu."\">\n";
					$menu .= "<ul type=none >\n";
					foreach ($sub_array as $sval)
					{
						if ($pkey == $_REQUEST[$qs_val] && $pkey == $sval['parent'])
						{
							
							if (empty($sval['callcontrolador']) && empty($sval['callpagina']))
							{
								$menu .= "<li><a href=\"".$this->rebuild_link($sval['link'], "", "", "")."\">".$sval['descripcion']."</a></li>\n";
							}
							else
							{
								$menu .= "<li><a href=\"".$this->rebuild_link($sval['link'], $sval['callcontrolador'], $sval['callpagina'], $sval['callsector'])."\">".$sval['descripcion']."</a></li>\n";
							}
	
						}
					}
					$menu .= "</ul>\n";
					$menu .= "</div>\n";
				}
			}
			$menu .= "</div> \n";
			return $menu;
		}

	}

}
?>


