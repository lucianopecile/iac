<?php
require_once 'modelos/modeloextranjero.php';
require_once 'modelos/modelopoblador.php';


class ControlExtranjero
{
  	function __construct()
	{

	    $this->view = new View();
	}
 
	
//============================================================================
	
	public function verextranjero()
	//retorna los datos de un poblador extranjero si se carg� el id, sino retorna campos en blanco
	{	
		$ext = new ModeloExtranjero();
		$pob = new ModeloPoblador();

		if (isset($_GET['idpob']) && ($_GET['idpob'] > 0)) //si es modificacion o eliminacion
		{			
		    $ext->putIdPoblador($_GET['idpob']);
			$empent = $ext->traerExtranjeroAsociado();
			$pob->putIdPoblador($_GET['idpob']);
			$pob_ok = $pob->traerpoblador();
			
			if (!$empent || !$pob_ok)
			{
			    $mensaje = htmlentities("En este momento no se puede realizar la operaci�n, int�ntelo m�s tarde");
			    $data['mensaje'] = $mensaje;
		    	$this->view->show1("mostrarerror.html", $data);
				return;
		    }
	    }   
		$data = $this->cargarPlantillaModificar($ext, $pob);
		$this->view->show("abmextranjero.html", $data);
	}

//============================================================================
	
	public function altaextranjero($idpoblador=null)
	// carga el nuevo poblador extranjero en el modelo y le a�ade el id del poblador asociado
	{
		$alta = new ModeloExtranjero();
		$this->cargavariables($alta, ALTA);
		$alta->putIdPoblador($idpoblador);
		$result = $alta->altaextranjero();
		return $result;
	}

//============================================================================	

	public function modificarextranjero()
	{
		$modifica = new ModeloExtranjero();
		$this->cargavariables($modifica, MODIFICAR);
		$modificado = $modifica->modificarextranjero();
		return $modificado;
	}
	
//============================================================================
	
	public function borrarextranjero()
	{
		$borra = new ModeloExtranjero();
		$borra->putIdExtranjero($_GET['id']);
		$borrado = $borra->borrarextanjero();
		if (!$borrado)
		{
			$mensaje = htmlentities("En este momento no se puede realizar la operaci�n, int�ntelo m�s tarde");
			$data['mensaje'] = $mensaje;
			$this->view->show1("mostrarerror.html", $data);
			return;
		}
		return $borrado;
	}
	
//============================================================================

	public function cargarPlantillaModificar($parExtranjero, $parPoblador) 
	{  
		$idext= $parExtranjero->getIdExtranjero();
		
		$quehacer = "";
		if ($idext == 0)
			$quehacer = ALTA;
		else
			$quehacer = MODIFICAR;
		
		switch($quehacer)
		{
			case ALTA:	      
	        $nombreboton="Guardar";
		    $nombreaccion="altaextranjero";
			break;	 
			
			case MODIFICAR:
			$nombreboton="Guardar";
			$nombreaccion="modificarextranjero";
			break;
			
			case BAJA:
			$nombreboton="Eliminar";
			$nombreaccion="borrarextranjero";  
			break;

			default:  
			$nombreboton="";
			$nombreaccion="";
		}
	 
		switch ($quehacer)
		{
			case MODIFICAR:
				$parametros = array(
                    "TITULO" => "Modificar",
                    "ID" => $parExtranjero->getIdExtranjero(),
					"IDPOBLADOR" => $parPoblador->getIdPoblador(),
					"CARTA" => $parExtranjero->getCartaCiudadania(), 
					"FECHACARTA"=> fechaACadena($parExtranjero->getFechaCarta()),
					"JUZGADO" => $parExtranjero->getJuzgado(),
					"VISADOPOR" => $parExtranjero->getVisadoPor(),
					"FECHALLEGADA" => fechaACadena($parExtranjero->getFechaLlegada()),
					"PROCEDENCIA" => $parExtranjero->getProcedencia(),
					"MEDIO" => $parExtranjero->getMedio(),
					"APELLIDO"=>$parPoblador->getApellido(),
					"NOMBRES"=>$parPoblador->getNombres(),
					"nombreaccion"=>$nombreaccion,
					"CONFIGURACION"=>"",
					"SOLOLECTURA"=>"",
					"ENAB_DISA"=>"",
					"nombreboton"=>$nombreboton,
					);
        			break;

        case BAJA:
			$parametros = array(
					"TITULO" => "Eliminar",
                    "ID" => $parExtranjero->getIdExtranjero(),
					"IDPOBLADOR" => $parPoblador->getIdPoblador(),
					"CARTA" => $parExtranjero->getCartaCiudadania(), 
					"FECHACARTA"=> fechaACadena($parExtranjero->getFechaCarta()),
					"JUZGADO" => $parExtranjero->getJuzgado(),
					"VISADOPOR" => $parExtranjero->getVisadoPor(),
					"FECHALLEGADA" => fechaACadena($parExtranjero->getFechaLlegada()),
					"PROCEDENCIA" => $parExtranjero->getProcedencia(),
					"MEDIO" => $parExtranjero->getMedio(),
					"APELLIDO"=>$parPoblador->getApellido(),
					"NOMBRES"=>$parPoblador->getNombres(),			
					"nombreaccion"=>$nombreaccion,
					"CONFIGURACION"=>"",
					"SOLOLECTURA"=>"readonly='readonly'",
					"ENAB_DISA"=>"disabled='disabled'",
					"nombreboton"=>$nombreboton,
					);
					break;
		 
		case ALTA:
		 	$parametros = array(
					"TITULO" => "Alta",
                    "ID" => 0,
					"IDPOBLADOR" => $parPoblador->getIdPoblador(),
					"CARTA" => "", 
					"FECHACARTA"=> "",
					"JUZGADO" => "",
					"VISADOPOR" => "",
					"FECHALLEGADA" => "",
					"PROCEDENCIA" => "",
					"MEDIO" => "",
		 			"APELLIDO"=>$parPoblador->getApellido(),
					"NOMBRES"=>$parPoblador->getNombres(),
					"CONFIGURACION"=>"",
					"SOLOLECTURA"=>"",
					"ENAB_DISA"=>"",					
					"nombreaccion"=>$nombreaccion,
					"nombreboton"=>$nombreboton,
					);
					break;

		default:
			$parametros = array(
                    "TITULO" => "Consultar",
                    "ID" => $parExtranjero->getIdExtranjero(),
					"IDPOBLADOR" => $parPoblador->getIdPoblador(),
					"CARTA" => $parExtranjero->getCartaCiudadania(), 
					"FECHACARTA"=> fechaACadena($parExtranjero->getFechaCarta()),
					"JUZGADO" => $parExtranjero->getJuzgado(),
					"VISADOPOR" => $parExtranjero->getVisadoPor(),
					"FECHALLEGADA" => fechaACadena($parExtranjero->getFechaLlegada()),
					"PROCEDENCIA" => $parExtranjero->getProcedencia(),
					"MEDIO" => $parExtranjero->getMedio(),
					"APELLIDO"=>$parPoblador->getApellido(),
					"NOMBRES"=>$parPoblador->getNombres(),
					"nombreaccion"=>$nombreaccion,
					"CONFIGURACION"=>"",
					"SOLOLECTURA"=>"readonly='readonly'",
					"ENAB_DISA"=>"disabled='disabled'",
					"nombreboton"=>$nombreboton,
					);
		} 				
        return $parametros;
	}


//============================================================================

	public function cargavariables($clasecarga, $op=null)
	//carga las variables de la clase en el alta o en la modificacion
	{
		if ($op == MODIFICAR)
			$clasecarga->putIdExtranjero($_POST["idextranjero"]);

		$clasecarga->putCartaCiudadania($_POST["carta"]);
        $clasecarga->putFechaCarta(cadenaAFecha($_POST["fechacarta"]));
        $clasecarga->putJuzgado($_POST["juzgado"]);
        $clasecarga->putVisadoPor($_POST["visadopor"]);
        $clasecarga->putFechaLlegada(cadenaAFecha($_POST["fechallegada"]));
		$clasecarga->putProcedencia($_POST["procedencia"]);
		$clasecarga->putMedio($_POST["medio"]);
	}

}

?>