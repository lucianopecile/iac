<?php
require_once 'modelos/modelosolicitud.php';
require_once 'modelos/modelolocalidad.php';
require_once 'modelos/modelogradotenencia.php';
require_once 'modelos/modeloempleado.php';
require_once 'modelos/modelounidad.php';

class ControlSolicitud
{
 
 	function __construct()
	{
	    $this->view = new View();
	}
 
//---------------------------------------------------------------------------------
	 
	public function mostrarsolicitud()
	// muestra todas las solicitudes en un html con una tabla
	{
		$solicitudes = new modelosolicitud();
		$liztado = $solicitudes->listadoTotal();
		$data['liztado'] = $liztado;
		$this->view->show1("solicitud.html", $data);
 	}
//---------------------------------------------------------------------------------
	 
	public function mostrarsolicitudrural()
	// muestra todas las solicitudes en un html con una tabla
	{
		$solicitudes = new modelosolicitud();
		$liztado = $solicitudes->listadoTotalRurales();
		$data['liztado'] = $liztado;
		$this->view->show1("solicitudrural.html", $data);
 	}

//---------------------------------------------------------------------------------
	 
	public function mostrarsolicitudurbana()
	// muestra todas las solicitudes en un html con una tabla
	{
		$solicitudes = new modelosolicitud();
		$liztado = $solicitudes->listadoTotalUrbanas();
		$data['liztado'] = $liztado;
		$this->view->show1("solicitudurbana.html", $data);
 	}


//---------------------------------------------------------------------------------------
	
	public function altasolicitud()
	{
		$alta= new modelosolicitud();
		$this->cargavariables($alta, ALTA);
		$tiposol=$_POST["tiposolicitud"];
		$altaok = $alta->altasolicitud();
		if (!$altaok)
		{
			$mensaje = htmlentities("No se pudo dar de alta la solicitud, int�ntelo m�s tarde");
			$data['mensaje'] = $mensaje;
			$this->view->show1("mostrarerror.html", $data);
			return;
			
		}
		else{
		   $nuevoid=$alta->getIdSolicitud();
	       $data['controlador']="solicitud";
		    if($tiposol==RURAL)
		       $data['accion']="vertabsolicitudrural&&idsol=".$nuevoid;
			else
		       $data['accion']="vertabsolicitudurbana&&idsol=".$nuevoid;
			
		}
		$this->view->show1("bridgecustom.html",$data);
	}
	
//---------------------------------------------------------------------------------------
	
	public function modificarsolicitud()
	{
		$modifica= new modelosolicitud();
		$this->cargavariables($modifica,MODIFICAR);
		$tiposol=$_POST["tiposolicitud"];	
        $modificado=$modifica->modificarsolicitud();
	    
        
	   if (!$modificado){
	      $mensaje = htmlentities("No se pudo modificar la solicitud, int�ntelo m�s tarde");
	      $data['mensaje']=$mensaje;
    	  $this->view->show1("mostrarerror.html", $data);
		  return;
        }
	  if($tiposol==RURAL)
		    $this->vertabsolicitudrural();		 
		else
		    $this->vertabsolicitudurbana();		 	
			
	}
		
//---------------------------------------------------------------------------------------
	
	public function borrarsolicitud()
	{
		$borra= new modelosolicitud();
		$borra->putIdSolicitud($_POST['id']);
        $tiposol=$_POST["tiposolicitud"];		
		$borrado=$borra->borrarsolicitud();
		if (!$borrado)
		{
			$mensaje = htmlentities("No se pudo eliminar la solicitud, int�ntelo m�s tarde");
			$data['mensaje'] = $mensaje;
			$this->view->show1("mostrarerror.html", $data);
			return;
		}
		if($tiposol==RURAL)
		    $this->mostrarsolicitudrural();		 
		else
		    $this->mostrarsolicitudurbana();		 
		   	
	}



//-----------------------------------------------------------------------------------

//retorna los datos de una solicitud si se carg� el id, sino retorna campos en blanco para hacer un alta
    public function versolicitud()
	{
		
	$solicitudes = new modelosolicitud();
    if (isset($_GET['idsol'])) { 
	
        $solicitudes->putIdSolicitud($_GET['idsol']);
	
       	$empent = $solicitudes->traersolicitud();
	
     	if (!$empent){
 	       $mensaje = htmlentities("En este momento no se puede consultar la solicitud, int�ntelo m�s tarde");
	       $data['mensaje']=$mensaje;
           $this->view->show1("mostrarerror.html", $data);
		   return;
         }
	
    } 
		
	
	$localidad=new modelolocalidad;
	$empleado=new modeloempleado;
	$gradotenencia=new modelogradotenencia;
	$data=$this->cargarPlantillaModificar($solicitudes,$localidad,$empleado,$gradotenencia);
	  $this->view->show("abmsolicitud.html", $data);
	
	}	
//-----------------------------------------------------------------------------------

    public function vertabsolicitudrural()
	{
		
	$solicitudes = new modelosolicitud();
   if (isset($_GET['idsol']) || (isset($_POST['idsol'])) ) {

	   if (isset($_GET['idsol'])){
           $solicitudes->putIdSolicitud($_GET['idsol']);
		}
       if (isset($_POST['idsol'])){  
	        $solicitudes->putIdSolicitud($_POST['idsol']);
		}
	
       	$empent = $solicitudes->traersolicitud();
	
     	if (!$empent){
 	       $mensaje = htmlentities("En este momento no se puede consultar la solicitud, int�ntelo m�s tarde");
	       $data['mensaje']=$mensaje;
           $this->view->show1("mostrarerror.html", $data);
		   return;
         }
	
    } 
		
	$unidad=new modelounidad;
	$localidad=new modelolocalidad;
	$empleado=new modeloempleado;
	$gradotenencia=new modelogradotenencia;
	$data=$this->cargarPlantillaModificar($solicitudes,$localidad,$empleado,$gradotenencia,$unidad);
	  $this->view->show("tabsolicitudrural.html", $data);
	
	}	
//-----------------------------------------------------------------------------------

    public function vertabsolicitudurbana()
	{
		
	$solicitudes = new modelosolicitud();
   if (isset($_GET['idsol']) || (isset($_POST['idsol'])) ) {

	   if (isset($_GET['idsol'])){
           $solicitudes->putIdSolicitud($_GET['idsol']);
		}
       if (isset($_POST['idsol'])){  
	        $solicitudes->putIdSolicitud($_POST['idsol']);
		}
	
       	$empent = $solicitudes->traersolicitud();
	
     	if (!$empent){
 	       $mensaje = htmlentities("En este momento no se puede consultar la solicitud, int�ntelo m�s tarde");
	       $data['mensaje']=$mensaje;
           $this->view->show1("mostrarerror.html", $data);
		   return;
         }
	
    } 
		
	
	$localidad=new modelolocalidad;
	$unidad=new modelounidad;
	$empleado=new modeloempleado;
	$gradotenencia=new modelogradotenencia;
	$data=$this->cargarPlantillaModificar($solicitudes,$localidad,$empleado,$gradotenencia,$unidad);
	  $this->view->show("tabsolicitudurbana.html", $data);
	
	}		
//-----------------------------------------------------------------------------------
	 public function cargarPlantillaModificar($parSolicitud,$parLocalidad,$parEmpleado,$parGradoTenencia,$parUnidad) 
{  
   
	
	$vl= $parLocalidad->TraerTodosL();
	$vl['selected']=  $parSolicitud->getIdLocalidad();
	$listaunidad= $parUnidad->TraerTodosTierra();
	$listaunidad['selected']=  $parSolicitud->getIdUnidad();
	$ve= $parEmpleado->TraerTodos();
	$ve['selected']=  $parSolicitud->getIdEmpleado();
	$vgt= $parGradoTenencia->TraerTodos();
	$vgt['selected']=  $parSolicitud->getIdGradoTenencia();
	$lista_ts = array(
		array('1','RURAL'), 
		array('2','URBANA'), 
		'selected' => $parSolicitud->getTipoSolicitud()
	);
	
	
	$idsolicitud = $parSolicitud->getIdSolicitud();
		
		$quehacer = "";
		if ($idsolicitud == 0)
			$quehacer = ALTA;
		else
			if (isset($_GET['operacion']))
			{
				if ($_GET['operacion'] == 2) $quehacer = MODIFICAR;
				if ($_GET['operacion'] == 3) $quehacer = BAJA;
			}
	
	  
	switch($quehacer)
	{
      case ALTA:
		
		$parSolicitud->putIdSolicitud("");
		
        $nombreboton="Guardar";
	    $nombreaccion="altasolicitud";
	 
      break;	 
      case MODIFICAR:
	     
        $nombreboton="Guardar";
	    $nombreaccion="modificarsolicitud";
	  break;
	  case BAJA:
	     
         $nombreboton="Eliminar";
         $nombreaccion="borrarsolicitud";  
      break;
      default:  
		  $nombreboton="Guardar";
	      $nombreaccion="modificarsolicitud";
		  
   }
	
		  
        switch ($quehacer)
       {

       	case MODIFICAR:
		$idsoli=$parSolicitud->getIdSolicitud();
		$parametros = array(
                    "TITULO" =>  "Solicitudes -> Modificar",
                    "ID" => $parSolicitud->getIdSolicitud(),
					"IDSOLICITUD" => $parSolicitud->getIdSolicitud(),
					"IDOPERACION" => MODIFICAR,
					"IDPOBLADOR" =>$parSolicitud->getIdPoblador(), 
					"POBLADOR" =>$parSolicitud->getApellido().",".$parSolicitud->getNombres(),
    				"USR_MOD"=>$parSolicitud->getUsrMod(),
					"FECHASOLICITUD"=> fechaACadena($parSolicitud->getFechaSolicitud()),
					"LISTALOCALIDAD"=>$vl,
					"LISTASOLICITUD"=>$lista_ts,
					"TIPOSOLICITUD"=>$parSolicitud->getTipoSolicitud(),
					"IDLOCALIDAD"=>$parSolicitud->getIdLocalidad(),
					"OBSERVACION"=>$parSolicitud->getObservacion(),
					"IDESTADOSOLICITUD"=>  $parSolicitud->getIdEstadoSolicitud(), 
					"IDGRADOTENENCIA"=>$parSolicitud->getIdGradoTenencia(), 
					"LISTAGT"=>$vgt,
					"LISTAEMPLEADO"=>$ve,
					"ANIOEXPEDIENTE"=>$parSolicitud->getAnioExpediente(),
					"NROEXPEDIENTE"=>$parSolicitud->getNroExpediente(),
					"LETRAEXPEDIENTE"=>$parSolicitud->getLetraExpediente(),
					"RESIDENTE"=>$parSolicitud->getResidente(),
					"FECHARESIDEDESDE"=>$parSolicitud->getFechaResideDesde(),
					"EMPLEADOPUBLICO"=>$parSolicitud->getEmpleadoPublico(),
					"REPARTICION"=>$parSolicitud->getReparticion(),
					"IDEMPLEADO"=>$parSolicitud->getIdEmpleado(),
					"SUPERFICIE"=>$parSolicitud->getSuperficie(),
					"IDUNIDADSOL"=>$parSolicitud->getIdUnidad(),
					"LISTAUNIDADSOL"=>$listaunidad,
					"NOVER"=>"style='visibility:hidden'",

					"nombreaccion"=>$nombreaccion,
					"CONFIGURACION"=>"",
					"SOLOLECTURA"=>"",
					"ENAB_DISA"=>"",
					"DISA_MODI"=>"disabled='disabled'",
					"nombreboton"=>$nombreboton,
					"tabdefault1" => "",
					"tabdefault" => "tabbertabdefault",
					"tabdefault2" => ""
                  
                    );
        break;
		case BAJA:
	    $parametros = array(
                   "TITULO" =>  "Solicitudes -> Eliminar",
                    "ID" => $parSolicitud->getIdSolicitud(),
					"IDSOLICITUD" => $parSolicitud->getIdSolicitud(),
					"IDOPERACION" => BAJA,
					"IDPOBLADOR" =>$parSolicitud->getIdPoblador(), 
					"POBLADOR" =>$parSolicitud->getApellido().",".$parSolicitud->getNombres(),
    				"USR_MOD"=>$parSolicitud->getUsrMod(),
					"FECHASOLICITUD"=> fechaACadena($parSolicitud->getFechaSolicitud()),
					"LISTALOCALIDAD"=>$vl,
					"LISTASOLICITUD"=>$lista_ts,
					"TIPOSOLICITUD"=>$parSolicitud->getTipoSolicitud(),
    				"IDLOCALIDAD"=>$parSolicitud->getIdLocalidad(),
					"OBSERVACION"=>$parSolicitud->getObservacion(),
					"IDESTADOSOLICITUD"=>  $parSolicitud->getIdEstadoSolicitud(), 
					"IDGRADOTENENCIA"=>$parSolicitud->getIdGradoTenencia(), 
					"LISTAGT"=>$vgt,
					"LISTAEMPLEADO"=>$ve,
					"ANIOEXPEDIENTE"=>$parSolicitud->getAnioExpediente(),
					"NROEXPEDIENTE"=>$parSolicitud->getNroExpediente(),
					"LETRAEXPEDIENTE"=>$parSolicitud->getLetraExpediente(),
					"RESIDENTE"=>$parSolicitud->getResidente(),
					"FECHARESIDEDESDE"=>$parSolicitud->getFechaResideDesde(),
					"EMPLEADOPUBLICO"=>$parSolicitud->getEmpleadoPublico(),
					"REPARTICION"=>$parSolicitud->getReparticion(),
					"IDEMPLEADO"=>$parSolicitud->getIdEmpleado(),
					"SUPERFICIE"=>$parSolicitud->getSuperficie(),
					"IDUNIDADSOL"=>$parSolicitud->getIdUnidad(),
					"LISTAUNIDADSOL"=>$listaunidad,
			        "nombreaccion"=>$nombreaccion,
					"NOVER"=>"style='visibility:hidden'",

					"DISA_MODI"=>"",
					"SOLOLECTURA"=>"readonly='readonly'",
					"ENAB_DISA"=>"disabled='disabled'",
					"nombreboton"=>$nombreboton,
					"tabdefault1" => "",
					"tabdefault" => "tabbertabdefault",
					"tabdefault2" => ""
                    );
	     break;
		 case ALTA:
		 
 
         
  
	     $parametros = array(
	                "TITULO" =>  "Solicitudes -> Alta",
                    "ID" => NULL,
					"IDSOLICITUD" => $parSolicitud->getIdSolicitud(),
					"IDOPERACION" => ALTA,
					"LISTASOLICITUD"=>$lista_ts,
					"TIPOSOLICITUD"=>"",
					"IDPOBLADOR" =>NULL, 
					"POBLADOR" =>"",
    				"USR_MOD"=>"",
					"FECHASOLICITUD"=> date('d/m/Y'),
					"LISTALOCALIDAD"=>$vl,
					"IDLOCALIDAD"=>NULL,
					"OBSERVACION"=>"",
					"IDESTADOSOLICITUD"=> '1', 
					"IDGRADOTENENCIA"=>NULL, 
					"LISTAGT"=>$vgt,
					"LISTAEMPLEADO"=>$ve,
					"ANIOEXPEDIENTE"=>0,
					"NROEXPEDIENTE"=>0,
					"LETRAEXPEDIENTE"=>"IAC",
					"RESIDENTE"=>0,
					"FECHARESIDEDESDE"=>"",
					"EMPLEADOPUBLICO"=>0,
					"REPARTICION"=>"",
					"IDEMPLEADO"=>NULL,
					"SUPERFICIE"=>0,
					"IDUNIDADSOL"=>0,
					"LISTAUNIDADSOL"=>$listaunidad,
					"CONFIGURACION"=>"",
				    "SOLOLECTURA"=>"",
					"ENAB_DISA"=>"",					
	                "DISA_MODI"=>"",
					"nombreaccion"=>$nombreaccion,
					"nombreboton"=>$nombreboton,
					"tabdefault1" => "",
					"tabdefault" => "tabbertabdefault",
					"tabdefault2" => ""
					   );
	
	     break;
		 default :
		$parametros = array(
                    "TITULO" =>  "Solicitudes -> Ver",
                    "ID" => $parSolicitud->getIdSolicitud(),
					"IDSOLICITUD" => $parSolicitud->getIdSolicitud(),
					"IDOPERACION" => MODIFICAR,
					"IDPOBLADOR" =>$parSolicitud->getIdPoblador(), 
					"POBLADOR" =>$parSolicitud->getApellido().",".$parSolicitud->getNombres(),
    				"USR_MOD"=>$parSolicitud->getUsrMod(),
					"FECHASOLICITUD"=> fechaACadena($parSolicitud->getFechaSolicitud()),
					"LISTALOCALIDAD"=>$vl,
					"LISTASOLICITUD"=>$lista_ts,
					"TIPOSOLICITUD"=>$parSolicitud->getTipoSolicitud(),
					"IDLOCALIDAD"=>$parSolicitud->getIdLocalidad(),
					"OBSERVACION"=>$parSolicitud->getObservacion(),
					"IDESTADOSOLICITUD"=>  $parSolicitud->getIdEstadoSolicitud(), 
					"IDGRADOTENENCIA"=>$parSolicitud->getIdGradoTenencia(), 
					"LISTAGT"=>$vgt,
					"LISTAEMPLEADO"=>$ve,
					"ANIOEXPEDIENTE"=>$parSolicitud->getAnioExpediente(),
					"NROEXPEDIENTE"=>$parSolicitud->getNroExpediente(),
					"LETRAEXPEDIENTE"=>$parSolicitud->getLetraExpediente(),
					"RESIDENTE"=>$parSolicitud->getResidente(),
					"FECHARESIDEDESDE"=>$parSolicitud->getFechaResideDesde(),
					"EMPLEADOPUBLICO"=>$parSolicitud->getEmpleadoPublico(),
					"REPARTICION"=>$parSolicitud->getReparticion(),
					"IDEMPLEADO"=>$parSolicitud->getIdEmpleado(),
                    "SUPERFICIE"=>$parSolicitud->getSuperficie(),
					"IDUNIDADSOL"=>$parSolicitud->getIdUnidad(),
					"LISTAUNIDADSOL"=>$listaunidad,
					"nombreaccion"=>$nombreaccion,
					"NOVER"=>"style='visibility:hidden'",
					"CONFIGURACION"=>"",
				    "SOLOLECTURA"=>"",
					"ENAB_DISA"=>"",					
	                "DISA_MODI"=>"disabled='disabled'",

					"nombreaccion"=>$nombreaccion,
					"nombreboton"=>$nombreboton,
					"tabdefault1" => "",
					"tabdefault" => "tabbertabdefault",
					"tabdefault2" => ""
                    );
		 					
		 
		} 				



        return $parametros;
  }

//----------------------------------------------------------------------------------
//Carga las variables del html para volcarlas en la tabla


public function cargavariables($clasecarga,$oper){
       
	 ///carga las variables de la clase 
	   
	   if ($oper==MODIFICAR){  
	    
        $clasecarga->putIdSolicitud($_POST["id"]);
		}
        $clasecarga->putIdPoblador($_POST["idpoblador"]);
        $clasecarga->putIdUsrCreador($_SESSION["s_idusr"]);
		$clasecarga->putIdUsrMod($_SESSION["s_idusr"]);
		$clasecarga->putFechaSolicitud(cadenaAFecha($_POST["fechasolicitud"]));
		$obs=$_POST["observacion"];
		if(!empty($obs)){
		   $nuevaobservacion=$_POST["observacionant"]."\n".$_POST["observacion"]." (".$_SESSION['s_username'].")";
		 } 
		 else{
		    $nuevaobservacion=$_POST["observacionant"];
		 } 
		$clasecarga->putObservacion($nuevaobservacion);


		$clasecarga->putIdEstadoSolicitud($_POST["idestadosolicitud"]);
		$clasecarga->putIdGradoTenencia($_POST["idgradotenencia"]);
		
		$clasecarga->putAnioExpediente($_POST["anioexpediente"]);
		$clasecarga->putNroExpediente($_POST["nroexpediente"]);
		$clasecarga->putLetraExpediente($_POST["letraexpediente"]);

		$clasecarga->putResidente($_POST["residente"]);
		$clasecarga->putFechaResideDesde($_POST["fecharesidedesde"]);

		$clasecarga->putEmpleadoPublico($_POST["empleadopublico"]);
		$clasecarga->putReparticion($_POST["reparticion"]);
		if($_POST["idempleado"]==0){
		   $clasecarga->putIdEmpleado('NULL');       
		}
		else{
		   $clasecarga->putIdEmpleado($_POST["idempleado"]);
		}
	    if($_POST["idlocalidad"] == 0){
		   $clasecarga->putIdLocalidad('NULL');       
		}
		else{
		   $clasecarga->putIdLocalidad($_POST["idlocalidad"]);
		}
	    $clasecarga->putTipoSolicitud($_POST["tiposolicitud"]);
		
		if($_POST["idunidadsol"] == 0){
		   $clasecarga->putIdUnidad('NULL');       
		}
		else{
		   $clasecarga->putIdUnidad($_POST["idunidadsol"]);
		}
	    $clasecarga->putSuperficie($_POST["superficie"]);
   }	

//---------------------------------------------------------------------------------------

	public function elegirSolicitud()
	// envia a la vista las solicitudes elegidas segun el numero de expdte.
	{
		$solicitud = new modelosolicitud;
		$solicitud ->putNroExpediente(0);
		$solicitud ->putApellido("");

		if (isset($_GET['expediente'])){
			if(!empty($_GET['expediente'])){
				$solicitud ->putNroExpediente($_GET['expediente']);
			}
		}

		if (isset($_GET['anio'])){
			if(!empty($_GET['anio'])){
				$solicitud ->putAnioExpediente($_GET['anio']);
			}
		}

		if (isset($_GET['solicitante'])){
			if(!empty($_GET['solicitante'])){
				$solicitud ->putApellido($_GET['solicitante']);
			}
		}
	    $liztado = $solicitud ->listadoExpedienteNombre();
	    $data['liztado'] = $liztado;
		$this->view->show1("elegirsolicitud.html", $data);
	}

//---------------------------------------------------------------------------------------

	public function elegirSolicitudRural()
	// envia a la vista las solicitudes rurales elegidas segun el numero de expdte.
	{
		$solicitud = new modelosolicitud;
		$solicitud ->putNroExpediente(0);
		$solicitud ->putApellido("");

		if (isset($_GET['expediente'])){
			if(!empty($_GET['expediente'])){
				$solicitud ->putNroExpediente($_GET['expediente']);
			}
		}
		
		if (isset($_GET['solicitante'])){
			if(!empty($_GET['solicitante'])){
				$solicitud ->putApellido($_GET['solicitante']);
			}
		}

	    $liztado = $solicitud ->listadoRuralesExpedienteSinInspeccion();

	    $data['liztado'] = $liztado;
		
		$this->view->show1("elegirsolicitudruralbasico.html", $data);
	}
   
//---------------------------------------------------------------------------------------

	public function elegirSolicitudRuralPrivilegios()
	// envia a la vista las solicitudes rurales elegidas segun el numero de expdte.
	{
		$solicitud = new modelosolicitud;
		$solicitud ->putNroExpediente(0);
		$solicitud ->putApellido("");

		if (isset($_GET['expediente'])){
			if(!empty($_GET['expediente'])){
				$solicitud ->putNroExpediente($_GET['expediente']);
			}
		}
		
		if (isset($_GET['solicitante'])){
			if(!empty($_GET['solicitante'])){
				$solicitud ->putApellido($_GET['solicitante']);
			}
		}

	    $liztado = $solicitud ->listadoRuralesExpedienteSinInspeccion();

	    $data['liztado'] = $liztado;
		
		$this->view->show1("elegirsolicitudruralprivilegios.html", $data);
	} 

//---------------------------------------------------------------------------------------

	public function elegirSolicitudSinCuenta()
	// envia a la vista las solicitudes elegidas segun el numero de expdte.
	{
		$solicitud = new modelosolicitud;
		$solicitud ->putNroExpediente(0);
		$solicitud ->putApellido("");

		if (isset($_GET['expediente']) || isset($_GET['anio']))
		{
			if(!empty($_GET['expediente']))
				$solicitud ->putNroExpediente($_GET['expediente']);
			
			if(!empty($_GET['anio']))
				$solicitud ->putAnioExpediente($_GET['anio']);
		}
	    $liztado = $solicitud->listadoExpedienteNombreSinCuenta();
	    $data['liztado'] = $liztado;
		$this->view->show1("elegirsolicitud.html", $data);
	}

//---------------------------------------------------------------------------------------

	public function verificarNroExpediente()
	//verifica que el nro de expediente cargado no exista para otro expediente, se usa con ajax
	{
		$solicitud = new ModeloSolicitud();
		$solicitud->putNroExpediente("");
		
		if(isset($_GET['nroexpediente']) && $_GET['nroexpediente'] > 0)
			$solicitud->putNroExpediente($_GET['nroexpediente']);
		
		$existe = $solicitud->existeNroExpediente();
		
		if($existe)
			echo "_{true}_";
					
		return;		
	}
	
	
}

?>