<?php

require_once 'modelos/modelomenu.php';

class controlmenu
{
	function __construct()
	{
	    //Creamos una instancia de nuestro mini motor de plantillas
	    $this->view = new View();
	}

    public function rebuild_link($link, $parent_var, $parent_val,$qs_accion,$parent_accion) {
   
     // link = es la pagina donde vamos a ir
	 // parent_var =el la variable donde inidicamos el proximo controlador a llamar "controlador"
	 // parent_val=trae el  controlador a llamar
	 // parent_accion=es la accion que se va a realizar
   
    if(($parent_accion=="") && ($parent_val=="")){
	     $link_parts = explode("?", $link);
	
         $base_var = "";
	}
	else
	{
    $link_parts = explode("?", $link);
	
    $base_var = "?".$parent_var."=".$parent_val."&&".$qs_accion."=".$parent_accion;
	}
	
    if (!empty($link_parts[1])) {
        $link_parts[1] = str_replace("&amp;", "##", $link_parts[1]);
        $parts = explode("##", $link_parts[1]);
        $newParts = array();
        foreach ($parts as $val) {
            $val_parts = explode("=", $val);
            if ($val_parts[0] != $parent_var) {
                array_push($newParts, $val);
            }
        }
        if (count($newParts) != 0) {
            $qs = "&amp;".implode("&amp;", $newParts);
        } 
        return $link_parts[0].$base_var.$qs;
    } else {
        return $link_parts[0].$base_var;
    }
	
	
   }

 
	
	

	public function dyn_menu()
	{
	    require_once 'modelos/modelomenu.php';
		
		$consulta = new modelomenu();
		//$_SESSION["s_idusr"]=1;
		//$consulta->putCambiaClave($_SESSION["cambiaclave"]);
	    $consulta->putIdUsuario($_SESSION["s_idusr"]);
		//Le pedimos al modelo todos los items
		$result= $consulta->getmenu();
		if(!$result){
			return(false);
		}

		while ($itemsmenu = mysql_fetch_object($result))
		{
		   
			if ($itemsmenu->idparent == 0)
			{
				$parent_menu[$itemsmenu->id]['descripcion'] = $itemsmenu->descripcion;
				$parent_menu[$itemsmenu->id]['link'] =  $itemsmenu->link_url;
			} else {
				$sub_menu[$itemsmenu->id]['parent'] = $itemsmenu->idparent;
				$sub_menu[$itemsmenu->id]['descripcion'] = $itemsmenu->descripcion;
				$sub_menu[$itemsmenu->id]['link'] = $itemsmenu->link_url;
				$sub_menu[$itemsmenu->id]['callcontrolador'] =$itemsmenu->callcontrolador;
				$sub_menu[$itemsmenu->id]['callaccion'] = $itemsmenu->callaccion;
				$parent_menu[$itemsmenu->idparent]['count']++;
			}
		}
		$arreglo[0] = $parent_menu;
		$arreglo[1] = $sub_menu;
		
		return $arreglo;
 
	}
	

	public function armarmenu($qs_val,$qs_accion,$c_menusolo,$c_menusub,$c_submenu,$c_glossy)
	{
	

		$arreglo_menu = $this->dyn_menu();
		if(!$arreglo_menu)
		{
			$menu = "";
			return $menu;
		}
        
		$parent_array = $arreglo_menu[0];
		$sub_array = $arreglo_menu[1]	; 
		$totalparent = count($parent_array);
		if ($totalparent <= 0)
		{
			$menu = "";
			return $menu;
		} else {
			$menu = "<div id=\"".$c_glossy."\">\n";
			
			foreach ($parent_array as $pkey => $pval)
			{
				if (!empty($pval['count']))
				{
					$menu .= "<a class=\"".$c_menusub."\" href=\"".$pval['link']."?".$qs_val."=".$pkey."\">".$pval['descripcion']."</a>\n";
					$_REQUEST[$qs_val] = $pkey;
				} else {
					$menu .= "<a  class=\"".$c_menusolo."\" href=\"".$pval['link']."\">".$pval['descripcion']."</a>\n"; 
					$_REQUEST[$qs_val] = NULL;
				}
				if (!empty($qs_val) && ($_REQUEST[$qs_val] != NULL))
				{
					$menu .= "<div class=\"".$c_submenu."\">\n";
					$menu .= "<ul>\n";
					foreach ($sub_array as $sval)
					{
						if ($pkey == $_REQUEST[$qs_val] && $pkey == $sval['parent'])
						{
							if (empty($sval['callcontrolador']) && empty($sval['callaccion']))
								$menu .= "<li><a href=\"".$this->rebuild_link($sval['link'], $qs_val, "",$qs_accion,"")."\">".$sval['descripcion']."</a></li>\n";
							else
								$menu .= "<li><a href=\"".$this->rebuild_link($sval['link'], $qs_val, $sval['callcontrolador'],$qs_accion,$sval['callaccion'])."\">".$sval['descripcion']."</a></li>\n";
						}
	            	}
	            $menu .= "</ul>\n";
				$menu .= "</div>\n";
	        	}
	    	}
			$menu .= "</div>\n";
		    return $menu;
		}
 
	}

/*-------------------------------------------------------------------------------------*/
/*-------------------------------------------------------------------------------------*/
public function mostrarmenu()
    {
        $clasemenu = new modelomenu();
        $liztado = $clasemenu->listadoTotal();
        $data['liztado'] = $liztado;
        $this->view->show1("menu.html", $data);
		
 	}

/*-------------------------------------------------------------------------------------*/
/*-------------------------------------------------------------------------------------*/
public function vermenu()
	{

    $clasemenu= new modelomenu();
   
    if (isset($_GET['id'])) { 
       $clasemenu->putIdMenu($_GET['id']);
	 
	   $locent=$clasemenu->traermenu();
       if (!$locent){
	          $mensaje= "No se encontro el item de menu";
	          $data['mensaje']=$mensaje;
    	      $this->view->show1("mostrarerror.html", $data);
		      return;
       }
	}   
	$data=$this->cargarPlantillaModificar($clasemenu);
	$this->view->show("abmmenu.html", $data);
	}

/*-------------------------------------------------------------------------------------*/
/*-------------------------------------------------------------------------------------*/
public function altamenu()
	{
	   $alta= new modelomenu();
	   
	    
       $this->cargavariables($alta,ALTA);
	   
	   $altaok=$alta->altamenu();
	   if (!$altaok){
	          $mensaje= "No se pudo dar de alta el item del menu ";
	          $data['mensaje']=$mensaje;
    	      $this->view->show1("mostrarerror.html", $data);
		      return;
        }
	  
		 
	}
/*-------------------------------------------------------------------------------------*/
/*-------------------------------------------------------------------------------------*/
public function modificarmenu()
	{
		   
       $modifica= new modelomenu();
	   
	   $this->cargavariables($modifica,MODIFICAR);
		
	    $modificado=$modifica->modificarmenu();
        
	   if (!$modificado){
	          $mensaje= "No se pudo modificar el  item del menju";
	          $data['mensaje']=$mensaje;
    	      $this->view->show1("mostrarerror.html", $data);
		      return;
        }
	    $this->mostrarmenu();
			
	}
	
/*-------------------------------------------------------------------------------------*/
/*-------------------------------------------------------------------------------------*/

    //*Esta funcion carga los valores en la vista*/
    public function cargarPlantillaModificar($parmenu) 
    {  
    /*En esta instancia se cargan toods los valores que son generales para todo  tipo de accion*/
	   $listapadre = $parmenu->traertodospadres();
       $listapadre['selected']=$parmenu->getIdParent();
       $listalink = array(
		array('#','#'),
		array('index.php','index.php'), 
		'selected' => $parmenu->getLink_Url()
	);
       if(isset($_GET['operacion'])){
	    $quehacer=$_GET['operacion'];
	}else{
		$quehacer=ALTA;
	}
    
	switch($quehacer)
	{
      case ALTA:
      
        $nombreboton="Guardar";
	    $nombreaccion="altamenu";
	 
      break;	 
      case MODIFICAR:
        $nombreboton="Guardar";
	    $nombreaccion="modificarmenu";
	  break;
	 
      default:  
		     $nombreboton="";
             $nombreaccion="";  
		  
   }
		  
  
	  switch ($quehacer)
       {

       	case MODIFICAR:

	      $parametros = array(
                    "TITULO" =>  "Editando Item Menu",
                    "ID" => $parmenu->getIdMenu(),
					"DESCRIPCION" => $parmenu->getDescripcion(),
					"IDPARENT"=> $parmenu->getIdParent(),
					"IDACCESO"=> $parmenu->getIdAcceso(),
					"LINK_URL"=> $parmenu->getLink_Url(),
					"CALLCONTROLADOR"=> $parmenu->getCallControlador(),
					"LISTAPADRE"=>$listapadre,
					"LISTALINK"=>$listalink,
					"CALLACCION"=> $parmenu->getCallAccion(),
					"DISA_MODI" =>"readonly='readonly'",
					"nombreaccion"=>$nombreaccion,
					"nombreboton"=>$nombreboton
                    );
					
	    break;
      case ALTA:
	     $parametros = array(
		 
					"TITULO" =>  "Alta de Item Menu",
                    "ID" => 0,
					"DESCRIPCION" => "",
					"IDPARENT"=> 0,
					"LISTAPADRE"=>$listapadre,
					"IDACCESO"=> 1,
					"LINK_URL"=> "",
					"LISTALINK"=>$listalink,
					"CALLCONTROLADOR"=> "",
					"CALLACCION"=> "",
					"DISA_MODI" =>"readonly='readonly'",
					"nombreaccion"=>$nombreaccion,
					"nombreboton"=>$nombreboton
	
                    );
	    break;
		default :

		 $parametros = array(
  
                    "ID" => $parmenu->getIdMenu(),
					"DESCRIPCION" => $parmenu->getDescripcion(),
					"IDPARENT"=> $parmenu->getIdParent(),
					"IDACCESO"=> $parmenu->getIdAcceso(),
					"LINK_URL"=> $parmenu->getLink_Url(),
					"CALLCONTROLADOR"=> $parmenu->getCallControlador(),
					"CALLACCION"=> $parmenu->getCallAccion(),
					"LISTAPADRE"=>$listapadre,
					"LISTALINK"=>$listalink,
					"DISA_MODI" =>"readonly='readonly'",
					"nombreaccion"=>$nombreaccion,
					"nombreboton"=>$nombreboton,
					"CONFIGURACION"=>"style='visibility:hidden'",
					"SOLOLECTURA"=>"readonly='readonly'",
					"ENAB_DISA"=>"disabled='disabled'"
                    );
	  }				

        return $parametros;
  }
 

/*-------------------------------------------------------------------------------------*/
/*-------------------------------------------------------------------------------------*/
public function cargavariables($clasecarga,$oper){
       
	 ///carga las variables de la clase 
	   
	   if ($oper==MODIFICAR){  
	    
        $clasecarga->putIdMenu($_POST["id"]);
		}
        $clasecarga->putDescripcion($_POST["descripcion"]);
        $clasecarga->putIdParent($_POST["idparent"]);
        $clasecarga->putIdAcceso($_POST["idacceso"]);
        $clasecarga->putLink_Url($_POST["link_url"]);
        $clasecarga->putCallControlador($_POST["callcontrolador"]);
        $clasecarga->putCallAccion($_POST["callaccion"]);
     
   }

	
   
	
  
}
?>


