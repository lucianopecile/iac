<?php
require_once 'modelos/modelohijo.php';
require_once 'modelos/modelotipodoc.php';



class ControlHijo
{
  	function __construct()
	{
	    $this->view = new View();
	}
 
//============================================================================
	 
	public function mostrarhijo()
	// envia a la vista un listado de todos los hijos y su tipo y nro de documento
	{
		$hijos = new modelohijo();
		$liztado = $hijos->listadototal();
		$data['liztado'] = $liztado;
		$this->view->show1("hijo.html", $data);
 	}

//============================================================================
	 
	public function mostrarhijospoblador()
	//retorna una lista de los hijos de un poblador
	{	
		$hijos = new modelohijo();
		$tipodoc = new modelotipodoc();

		if (isset($_GET['idpob']) || isset($_POST['idpob'])) //si es modificacion o eliminacion
		{			
			if (isset($_GET['idpob']))
				$idpob = $_GET['idpob'];
			else 
				$idpob = $_POST['idpob'];
	    	$hijos->putIdPoblador($idpob);
			$liztado = $hijos->listarhijospoblador();
			$listatipodoc = $tipodoc->listadoTotal();
		} else {
	    		$this->mostrarhijo();
	    		return; 
	    }
	    
	    $data = $this->cargarPlantillaModificar($hijos);
	    $data['liztado'] = $liztado;
	    $data['listatipodoc'] = $listatipodoc;
	    $data['idpob'] = $idpob;
	    $data['tabdefault1'] = "";
		$data['tabdefault2'] = "tabbertabdefault";
		$data['tabdefault'] = "";
	    
		$this->view->show1("tabpoblador.html", $data);
	}
	 	
//============================================================================

	public function listahijo()
	//lista los hijos con un mismo apellido, sino recibe apellido lista todos los hijos
	{
		$hijos = new modelohijo();

		if (isset($_POST['apellidohijo']))
	    {
			$hijos->putApellido($_POST['apellidohijo']);
			$liztado = $hijos->listadonombre();
		} else {
			$liztado = $hijos->listadototal();
		}   

		$data['liztado'] = $liztado;
		
		$this->view->show1("hijo.html", $data);
	}	
	
//============================================================================
	
	public function elegirhijo()
	// envia a la vista los hijos elegidos segun el apellido.
	{
		$hijos = new modelohijo();
		
	    if (isset($_GET['apellidohijo']))
			$hijos->putApellido($_GET['apellidohijo']);
		else
			$hijos->putApellido("");
		 
	    $liztado = $hijos->listadonombre();

	    $data['liztado'] = $liztado;
		
		$this->view->show1("elegirhijo.html", $data);
	}
	
//============================================================================
	
	public function altahijo()
	// carga el nuevo hijo en el modelo
	{
		$hijo = new modelohijo();
		$this->cargavariables($hijo, ALTA);
		$altaok = $hijo->altahijo();

		if (!$altaok)
		{	
			$mensaje = htmlentities("En este momento no se puede realizar la operaci�n, int�ntelo m�s tarde");
			$data['mensaje'] = $mensaje;
			$this->view->show1("mostrarerror.html", $data);
			return;
        }
//		$vista = new view();
//		$vista->show1("bridge.html","");
		$this->mostrarhijospoblador();
	}
	
//============================================================================	

	public function modificarhijo()
	{
		$modifica = new modelohijo();
		$this->cargavariables($modifica, MODIFICAR);
		$modificado = $modifica->modificarhijo();
        
		if (!$modificado)
		{
			$mensaje = htmlentities("En este momento no se puede realizar la operaci�n, int�ntelo m�s tarde");
			$data['mensaje'] = $mensaje;
			$this->view->show1("mostrarerror.html", $data);
			return;
		}
	    $this->mostrarhijospoblador();
	}
	
//============================================================================
	
	public function borrarhijo()
	{
		$borra = new modelohijo();
		$borra->putIdHijo($_POST['idhijo']);
		$borra->putIdPoblador($_POST['idpob']);
		$borrado = $borra->borrarhijo();
		if (!$borrado)
		{
			$mensaje = htmlentities("En este momento no se puede realizar la operaci�n, int�ntelo m�s tarde");
			$data['mensaje'] = $mensaje;
			$this->view->show1("mostrarerror.html", $data);
			return;
		}
		$this->mostrarhijospoblador(); 
	}
	
//============================================================================
	
	public function verhijo()
	//retorna los datos de un hijo segun el id para modificacion sino retorna campos en blanco
	{	
		$hijo = new modelohijo();

		if (isset($_GET['idhijo'])) //si es modificacion o eliminacion
		{			
		    $hijo->putIdHijo($_GET['idhijo']);
			$result = $hijo->traerhijo();
			
			if (!$result)
			{
			    $mensaje = htmlentities("En este momento no se puede realizar la operaci�n, int�ntelo m�s tarde");
			    $data['mensaje'] = $mensaje;
		    	$this->view->show1("mostrarerror.html", $data);
				return;
		    }
	    }   
		$tipodoc = new modelotipodoc;
		$data = $this->cargarPlantillaModificar($hijo, $tipodoc);
		$this->view->show("modificarhijo.html", $data);
	}

//============================================================================

	public function cargarPlantillaModificar($parHijo)
	// carga el id del poblador para la vista
	{  
		$parametros = array(
 					"ID" => $parHijo->getIdPoblador(),
					);
		 				
        return $parametros;
	}

//============================================================================

	public function cargavariables($clasecarga, $op)
	//carga las variables de la clase modelo para alta o modificacion
	{
		if ($op == MODIFICAR)
			$clasecarga->putIdHijo($_POST["idhijo"]);

		$clasecarga->putIdTipodoc($_POST["idtipodoc"]);
		$clasecarga->putIdPoblador($_POST["idpob"]);
        $clasecarga->putDocumento($_POST["documento"]);
        $clasecarga->putNombres($_POST["nombres"]);
        $clasecarga->putApellido($_POST["apellido"]);
        $clasecarga->putFechaNac(cadenaAFecha($_POST["fechanac"]));
	}

//=================================================================================================	 
	
	public function verificardocumento()
	// verifica que el numero de documento ingresado no se repita en el modelo
	{
		$hijo = new modelohijo();
	    //$hijo->putIdTipoDoc("");
		$hijo->putDocumento("");

		if (isset($_GET['val']) && !empty($_GET['val']))
			$hijo->putDocumento($_GET['val']);
	
	    $idpob = $hijo->buscardocumento();
	    echo 'ID= '.$idpob." DOC= ".$_GET['val'];
	}	

}

?>