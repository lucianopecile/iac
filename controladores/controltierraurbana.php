<?php
require_once 'modelos/modelotierraurbana.php';
require_once 'modelos/modelolocalidad.php';
require_once 'modelos/modelozonaurbana.php';


class ControlTierraUrbana
{
 
 	function __construct()
	{
	    $this->view = new View();
	}
 
//---------------------------------------------------------------------------------
	 
	public function mostrartierraurbana()
	// muestra todas las tierraurbana en un html con una tabla
	{
		$tierraurbana = new modelotierraurbana();
		$liztado = $tierraurbana->listadoTotal();
		$data['liztado'] = $liztado;
		$this->view->show1("tierraurbana.html", $data);
 	}

//---------------------------------------------------------------------------------------
	
	public function altatierraurbana()
	{
		$alta= new modelotierraurbana();
		$this->cargavariables($alta, ALTA);
		$altaok = $alta->altatierraurbana();
		if (!$altaok)
		{
			$mensaje = "En este momento no se puede realizar la operaci�n, int�ntelo m�s tarde";
			$data['mensaje'] = $mensaje;
			$this->view->show1("mostrarerror.html", $data);
			return(0);
		}
		else{
		
		  return($altaok);
		}
	}
	
//---------------------------------------------------------------------------------------
	
	public function modificartierraurbana()
	{
		$modifica= new modelotierraurbana();
		$this->cargavariables($modifica,MODIFICAR);
		
        $modificado=$modifica->modificartierraurbana();
	    
        
	   if (!$modificado){
	      $mensaje= "En este momento no se puede modificar la tierra rural";
	      $data['mensaje']=$mensaje;
    	  $this->view->show1("mostrarerror.html", $data);
		  return(false);
        }
		else{
	      return(true);
		}	
	}
		
		
//---------------------------------------------------------------------------------------
	
	public function borrartierraurbana()
	{
		$borra= new modelotierraurbana();
		$borra->putIdTierraUrbana($_POST['id']);
		$borrado=$borra->borrartierraurbana();
		if (!$borrado)
		{
			$mensaje ="En este momento no se puede realizar la operaci�n, int�ntelo m�s tarde";
			$data['mensaje'] = $mensaje;
			$this->view->show1("mostrarerror.html", $data);
			return;
		}
		$this->mostrartierrarural();		 
	}




//-----------------------------------------------------------------------------------

//retorna los datos de una tierraurbana si se carg� el id, sino retorna campos en blanco para hacer un alta
    public function vertierraurbana()
	{
		
	$tierraurbana = new modelotierraurbana();
    if (isset($_GET['idtierra'])) { 
	
        $tierraurbana->putIdTierraUrbana($_GET['idtierra']);
	
       	$empent = $tierraurbana->traertierraurbana();
	
     	if (!$empent){
 	       $mensaje= "En este momento no se puede realizar la operacion para ver solicitud urbana, intentelo mas tarde";
	       $data['mensaje']=$mensaje;
           $this->view->show1("mostrarerror.html", $data);
		   return;
         }
	
    } 
		
	
	$localidad=new modelolocalidad;
	$zonaurbana=new modelozonaurbana;
	
	$data=$this->cargarPlantillaModificar($tierraurbana,$localidad,$zonaurbana);
	  $this->view->show("abmtierraurbana.html", $data);
	
	}	
//-----------------------------------------------------------------------------------

//retorna los datos de una tierra urbana si se carg� el id, sino retorna campos en blanco para hacer un alta
    public function vertierraurbanaasociada($tierraurbana)
	{
		
	    $empent = $tierraurbana->traertierraurbanaasociada();

     	if (!$empent){
 	       $mensaje= "No se pudo localizar la tierra urbana asociada";
	       $data['mensaje']=$mensaje;
           $this->view->show1("mostrarerror.html", $data);
		   return;
         }
       
	
  
	
	
	$localidad=new modelolocalidad;
	$zonaurbana=new modelozonaurbana;
	
	
	$data=$this->cargarPlantillaModificar($tierraurbana,$localidad,$zonaurbana);
	return($data);
	
	}	
	
	
//-----------------------------------------------------------------------------------
	 public function cargarPlantillaModificar($parTierraUrbana,$parLocalidad,$parZonaUrbana) 
{  
   
	
	$vlocalidad= $parLocalidad->TraerTodos();
	$vlocalidad['selected']=  $parTierraUrbana->getIdLocalidad();
    $vzonaurbana= $parZonaUrbana->TraerTodos();
	$vzonaurbana['selected']=  $parTierraUrbana->getIdZonaUrbana();

	
	$idtierraurbana =  $parTierraUrbana->getIdTierraUrbana();
	$quehacer = "";
	if ($idtierraurbana == 0)
			$quehacer = ALTA;
	else
		if (isset($_GET['operacion']))
			{
				if ($_GET['operacion'] == 2) $quehacer = MODIFICAR;
				if ($_GET['operacion'] == 3) $quehacer = BAJA;
			}
		  
	switch($quehacer)
	{
      case ALTA:
		
		$parTierraUrbana->putIdTierraUrbana("");
		
        $nombreboton="Guardar";
	    $nombreaccion="altatierraurbana";
	 
      break;	 
      case MODIFICAR:
	     
        $nombreboton="Guardar";
	    $nombreaccion="modificartierraurbana";
	  break;
	  case BAJA:
	     
         $nombreboton="Eliminar";
         $nombreaccion="borrartierraurbana";  
      break;
      default:  
		     $nombreboton="";
             $nombreaccion="";  
		  
   }
	
		  
        switch ($quehacer)
       {

       	case MODIFICAR:
		$idsoli=$parTierraUrbana->getIdTierraUrbana();
		$parametros = array(
                    "TITULOFORM" =>  "Tierras Urbanas -> Modificar",
                    "ID" => $parTierraUrbana->getIdTierraUrbana(),
					"IDLOCALIDADTU" => $parTierraUrbana->getIdLocalidad(),
					"IDZONAURBANA" =>$parTierraUrbana->getIdZonaUrbana(), 
					"NROZONA" =>$parTierraUrbana->getNroZona(),
    				"NROSOLAR"=>$parTierraUrbana->getNroSolar(),
					"FRACCION"=>$parTierraUrbana->getFraccion(),
					"OBSERVACION"=>$parTierraUrbana->getObservacion(),
					"LISTALOCALIDAD"=>$vlocalidad,
					"LISTAZONAURBANA"=>$vzonaurbana,
					"NOVER"=>"style='visibility:hidden'",
					"nombreacciontu"=>$nombreaccion,
					"CONFIGURACION"=>"",
					"SOLOLECTURA"=>"",
					"ENAB_DISA"=>"",
					"DISA_MODI"=>"disabled='disabled'",
					"nombrebotontu"=>$nombreboton
                  
                    );
        break;
		case BAJA:
        $idsoli=$parTierraUrbana->getIdTierraUrbana();
	    $parametros = array(
                   "TITULOFORM" =>  "Tierras Urbanas -> Eliminar",
                    "ID" => $parTierraUrbana->getIdTierraUrbana(),
					"IDLOCALIDADTU" => $parTierraUrbana->getIdLocalidad(),
					"IDZONAURBANA" =>$parTierraUrbana->getIdZonaUrbana(), 
					"NROZONA" =>$parTierraUrbana->getNroZona(),
    				"NROSOLAR"=>$parTierraUrbana->getNroSolar(),
					"FRACCION"=>$parTierraUrbana->getFraccion(),
					"OBSERVACION"=>$parTierraUrbana->getObservacion(),
					"LISTALOCALIDAD"=>$vlocalidad,
					"LISTAZONAURBANA"=>$vzonaurbana,
					"NOVER"=>"style='visibility:hidden'",
					"nombreacciontu"=>$nombreaccion,
					"DISA_MODI"=>"",
					"SOLOLECTURA"=>"readonly='readonly'",
					"ENAB_DISA"=>"disabled='disabled'",
					"nombrebotontu"=>$nombreboton
                    );
	     break;
		 case ALTA:
		 
 
         
  
	     $parametros = array(
	                "TITULOFORM" =>  "Tierras Urbanas -> Agregar",
                    "ID" => 0,
					"IDLOCALIDADTU" => 0,
					"IDZONAURBANA" =>0, 
					"NROZONA" =>0,
    				"NROSOLAR"=>0,
					"FRACCION"=>"",
					"OBSERVACION"=>"",
					"LISTALOCALIDAD"=>$vlocalidad,
					"LISTAZONAURBANA"=>$vzonaurbana,
					"CONFIGURACION"=>"",
				    "SOLOLECTURA"=>"",
					"ENAB_DISA"=>"",					
	                 "DISA_MODI"=>"",
					"nombreacciontu"=>$nombreaccion,
					"nombrebotontu"=>$nombreboton,
					   );
	
	     break;
		 default :
		 $idsoli=$parTierraUrbana->getIdTierraUrbana();
		$parametros = array(
                    "TITULOFORM" =>  "Tierras Urbanas -> Ver",
                    "ID" => $parTierraUrbana->getIdTierraUrbana(),
					"IDLOCALIDADTU" => $parTierraUrbana->getIdLocalidad(),
					"IDZONAURBANA" =>$parTierraUrbana->getIdZonaUrbana(), 
					"NROZONA" =>$parTierraUrbana->getNroZona(),
    				"NROSOLAR"=>$parTierraUrbana->getNroSolar(),
					"FRACCION"=>$parTierraUrbana->getFraccion(),
					"OBSERVACION"=>$parTierraUrbana->getObservacion(),
					"LISTALOCALIDAD"=>$vlocalidad,
					"LISTAZONAURBANA"=>$vzonaurbana,
					"NOVER"=>"style='visibility:hidden'",
					"nombreacciontu"=>$nombreaccion,
					"CONFIGURACION"=>"",
					"SOLOLECTURA"=>"",
					"ENAB_DISA"=>"",
					"DISA_MODI"=>"disabled='disabled'",
					"nombrebotontu"=>$nombreboton
                    );
		 					
		 
		} 				



        return $parametros;
  }

//----------------------------------------------------------------------------------
//Carga las variables del html para volcarlas en la tabla


public function cargavariables($clasecarga,$oper){
       
	 ///carga las variables de la clase 
	   
	   if ($oper==MODIFICAR){  
	    
        $clasecarga->putIdTierraUrbana($_POST["id"]);
		}
		
   
   
	$clasecarga->putNroZona($_POST["nrozona"]);
	$clasecarga->putNroSolar($_POST["nrosolar"]);
	$clasecarga->putFraccion($_POST["fraccion"]);
     $obs=$_POST["observaciontu"];
		if(!empty($obs)){
		   $nuevaobservacion=$_POST["observacionanttu"]."\n".$_POST["observaciontu"];
		 } 
		 else{
		    $nuevaobservacion=$_POST["observacionanttu"];
		 } 
		 $clasecarga->putObservacion($nuevaobservacion);

		if($_POST["idzonaurbana"]==0){
		    $clasecarga->putIdZonaUrbana('NULL');       
		}
		else{
	       $clasecarga->putIdZonaUrbana($_POST["idzonaurbana"]);
		}
	    if($_POST["idlocalidadtu"]==0){
		     $clasecarga->putIdLocalidad('NULL');       
		}
		else{
	        $clasecarga->putIdLocalidad($_POST["idlocalidadtu"]);
		}
   
   }	
	
	


   



}

?>