<?php

require_once 'modelos/modeloestadocivil.php';


class ControlEstadoCivil
{
 
 	function __construct()
	{
	    $this->view = new View();
	}
 
/*-------------------------------------------------------------------------------------*/

	public function mostrartipodoc()
	{
		$estadocivil = new modeloestadocivil();
		$liztado = $estadocivil->listadoTotal();
		$data['liztado'] = $liztado;
		$this->view->show1("estadocivil.html", $data);
 	}

/*-------------------------------------------------------------------------------------*/
	public function verestadocivil()
	{
		$estadocivil = new modeloestadocivil();
   
		if (isset($_GET['id']))
		{ 
       		$estadocivil->putId($_GET['id']);
			$locent = $estadocivil->traerestadocivil();
	       if (!$locent){
		          $mensaje = "No se encontro el estado civil solicitado";
		          $data['mensaje'] = $mensaje;
	    	      $this->view->show1("mostrarerror.html", $data);
			      return;
			}
		}
		$data=$this->cargarPlantillaModificar($estadocivil);
		$this->view->show("abmestadocivil.html", $data);
	}

/*-------------------------------------------------------------------------------------*/

	public function altaestadocivil()
	{
		$alta= new modeloestadocivil();
		$this->cargavariables($alta,ALTA);
		$altaok=$alta->altaestadocivil();
		
		if (!$altaok)
		{
			$mensaje = "No se pudo dar de alta el nuevo estado civil";
	    	$data['mensaje'] = $mensaje;
    		$this->view->show1("mostrarerror.html", $data);
			return;
		}
	}
	
/*-------------------------------------------------------------------------------------*/

	public function modificarestadocivil()
	{
		$modifica= new modeloestadocivil();
		$this->cargavariables($modifica,MODIFICAR);
		$modificado=$modifica->modificarestadocivil();
        
		if (!$modificado)
		{
			$mensaje = "No se pudo modificar el estado civil";
			$data['mensaje'] = $mensaje;
			$this->view->show1("mostrarerror.html", $data);
			return;
		}
		$this->mostrarestadocivil();	
	}
	
/*-------------------------------------------------------------------------------------*/
	
	public function borrarestadocivil()
	{
		$borra= new modeloestadocivil();
		$borra->putId($_POST['id']);
		$borrado=$borra->borrarestadocivil();

		if (!$borrado)
		{
			$mensaje= "No se puede borrar el estado civil";
			$data['mensaje'] = $mensaje;
		    $this->view->show1("mostrarerror.html", $data);
			return;
		}
		$this->mostrarestadocivil();
	}

/*-------------------------------------------------------------------------------------*/

    public function cargarPlantillaModificar($parestadocivil)
    // Esta funcion carga los valores en la vista 
    {
		if(isset($_GET['operacion']))
	    	$quehacer=$_GET['operacion'];
		else
			$quehacer=ALTA;
		
		switch($quehacer)
		{
			case ALTA:
				$nombreboton="Guardar";
				$nombreaccion="altaestadocivil";
				break;	 

			case MODIFICAR:
				$nombreboton="Guardar";
				$nombreaccion="modificarestadocivil";
				break;

			case BAJA:
				$nombreboton="Eliminar";
				$nombreaccion="borrarestadocivil";
				break;

			default:
				$nombreboton="";
				$nombreaccion="";  
		}
  
		switch ($quehacer)
		{

       	case MODIFICAR:

	      $parametros = array(
                    "TITULO" =>  "Editando estado civil",
                    "ID" => $parestadocivil->getId(),
					"DESCRIPCION" => $parestadocivil->getDescripcion(),
					
					"DISA_MODI" =>"readonly='readonly'",
					"nombreaccion"=>$nombreaccion,
					"nombreboton"=>$nombreboton
                    );
					
	    break;
		case BAJA:
		  $parametros = array(
                    "TITULO" =>  "Eliminando estado civil",

                   "ID" => $parestadocivil->getId(),
					"DESCRIPCION" => $parestadocivil->getDescripcion(),
					
					"nombreaccion"=>$nombreaccion,
					"nombreboton"=>$nombreboton,
					"CONFIGURACION"=>"",
					"SOLOLECTURA"=>"readonly='readonly'",
					"ENAB_DISA"=>"disabled='disabled'",
                    );
	    break;
		case ALTA:
	     $parametros = array(
		 
					"TITULO" =>  "Alta de estado civil",
                    "ID" => 0,
					"DESCRIPCION" => "",
					
                 	"nombreaccion"=>$nombreaccion,
					"nombreboton"=>$nombreboton
					
                    );
	    break;
		default :

		 $parametros = array(
  
                    "ID" => $parestadocivil->getId(),
					"DESCRIPCION" => $parestadocivil->getDescripcion(),
					
					 
					"nombreaccion"=>$nombreaccion,
					"nombreboton"=>$nombreboton,
					"CONFIGURACION"=>"style='visibility:hidden'",
					"SOLOLECTURA"=>"readonly='readonly'",
					"ENAB_DISA"=>"disabled='disabled'"
                    );
	  }				

        return $parametros;
  }
 
/*-------------------------------------------------------------------------------------*/
	public function cargavariables($clasecarga,$oper)
	{
    // carga las variables de la clase
	   
		if ($oper == MODIFICAR)  
	    	$clasecarga->putId($_POST["id"]);

        $clasecarga->putDescripcion($_POST["descripcion"]);
	}


}

?>