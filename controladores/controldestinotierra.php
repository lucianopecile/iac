<?php

require_once 'modelos/modelodestinotierra.php';
require_once 'modelos/modelolocalidad.php';




class controldestinotierra
{
 
 
 	function __construct()
	{

	    $this->view = new View();
	}
 
	/*Muestra un listado con las destinotierraes exitentes en la base de datos*/ 
  
  
/*-------------------------------------------------------------------------------------*/
  
    public function mostrardestinotierra()
    {
        $destinotierra = new modelodestinotierra();
        $destino = $destinotierra->listadoTotal();
        $data['destino'] = $destino;
        $this->view->show1("destinotierra.html", $data);
		
 	}

/*-------------------------------------------------------------------------------------*/

	
	public function verdestinotierra()
	{

    $destinotierra= new modelodestinotierra();
   
    if (isset($_GET['id'])) { 
       $destinotierra->putId($_GET['id']);
	 
	   $locent=$destinotierra->traerdestinotierra();
       if (!$locent){
	          $mensaje = htmlentities("No se encontr� el destino de la tierra");
	          $data['mensaje']=$mensaje;
    	      $this->view->show1("mostrarerror.html", $data);
		      return;
       }
	}   
	$localidad=new modelolocalidad();
	$data=$this->cargarPlantillaModificar($destinotierra,$localidad);
	$this->view->show("abmdestinotierra.html", $data);
	}

/*-------------------------------------------------------------------------------------*/

	public function altadestinotierra()
	{
	   $alta= new modelodestinotierra();
	   
	    
       $this->cargavariables($alta,ALTA);
	   
	   $altaok=$alta->altadestinotierra();
	   if (!$altaok){
	          $mensaje = htmlentities("No se pudo dar de alta el valor de la tierra");
	          $data['mensaje']=$mensaje;
    	      $this->view->show1("mostrarerror.html", $data);
		      return;
        }
	  
	   else{
		 $this->mostrardestinotierra();
	   }	 
	  
	  
	  
		 
	}
/*-------------------------------------------------------------------------------------*/
/*-------------------------------------------------------------------------------------*/

	public function modificardestinotierra()
	{
		   
       $modifica= new modelodestinotierra();
	   
	   $this->cargavariables($modifica,MODIFICAR);
		
	    $modificado=$modifica->modificardestinotierra();
        
	   if (!$modificado){
	          $mensaje = htmlentities("No se pudo modificar el valor de la tierra");
	          $data['mensaje']=$mensaje;
    	      $this->view->show1("mostrarerror.html", $data);
		      return;
        }
		else{
	      $this->mostrardestinotierra();
		 } 
			
	}
	
/*-------------------------------------------------------------------------------------*/
/*-------------------------------------------------------------------------------------*/

	
	public function borrardestinotierra()
	{
	 
       $borra= new modelodestinotierra();
	   $borra->putId($_POST['id']);
	   $borrado=$borra->borrardestinotierra();
       if (!$borrado){
	          $mensaje = htmlentities("No se puede borrar el valor de la tierra");
	          $data['mensaje']=$mensaje;
    	      $this->view->show1("mostrarerror.html", $data);
		      return;
        }
	    $this->mostrardestinotierra();
		 
	}

/*-------------------------------------------------------------------------------------*/
/*-------------------------------------------------------------------------------------*/

    //*Esta funcion carga los valores en la vista*/
    public function cargarPlantillaModificar($pardestinotierra,$parLocalidad) 
    {  
	$listalocalidad= $parLocalidad->TraerTodosL();
	$listalocalidad['selected']=  $pardestinotierra->getIdLocalidad();
	
       if(isset($_GET['operacion'])){
	    $quehacer=$_GET['operacion'];
	}else{
		$quehacer=ALTA;
	}
    
	switch($quehacer)
	{
      case ALTA:
      
        $nombreboton="Guardar";
	    $nombreaccion="altadestinotierra";
	 
      break;	 
      case MODIFICAR:
        $nombreboton="Guardar";
	    $nombreaccion="modificardestinotierra";
	  break;
	  case BAJA:
         $nombreboton="Eliminar";
         $nombreaccion="borrardestinotierra";  
      break;
      default:  
		     $nombreboton="";
             $nombreaccion="";  
		  
   }
		  
  
	  switch ($quehacer)
       {

       	case MODIFICAR:

	      $parametros = array(
                    "TITULO" =>  "Editando Destino de la tierra",
                    "ID" => $pardestinotierra->getId(),
					"DESCRIPCION" => $pardestinotierra->getDescripcion(),
					"PRECIO" => number_format($pardestinotierra->getPrecio(),2,',','.'),
					"IDLOCALIDAD" => $pardestinotierra->getIdLocalidad(),
					"LISTALOCALIDAD" => $listalocalidad,
					"DISA_MODI" =>"readonly='readonly'",
					"nombreaccion"=>$nombreaccion,
					"nombreboton"=>$nombreboton
                    );
					
	    break;
		case BAJA:
		  $parametros = array(
                    "TITULO" =>  "Eliminar Destino de la tierra",
                    "ID" => $pardestinotierra->getId(),
					"DESCRIPCION" => $pardestinotierra->getDescripcion(),
					"PRECIO" => number_format($pardestinotierra->getPrecio(),2,',','.'),
					"IDLOCALIDAD" => $pardestinotierra->getIdLocalidad(),
					"LISTALOCALIDAD" => $listalocalidad,
					"nombreaccion"=>$nombreaccion,
					"nombreboton"=>$nombreboton,
					"CONFIGURACION"=>"",
					"SOLOLECTURA"=>"readonly='readonly'",
					"ENAB_DISA"=>"disabled='disabled'",
                    );
	    break;
		case ALTA:
	     $parametros = array(
		 
                    "TITULO" =>  "Agregar Destino de la tierra",
                    "ID" => 0,
					"DESCRIPCION" =>"",
					"PRECIO" => number_format(0,2,',','.'),
					"IDLOCALIDAD" => 0,
					"LISTALOCALIDAD" => $listalocalidad,
					
                 	"nombreaccion"=>$nombreaccion,
					"nombreboton"=>$nombreboton
					
                    );
	    break;
		default :

		 $parametros = array(
  
                    "TITULO" =>  "Ver Destino de la tierra",
                    "ID" => $pardestinotierra->getId(),
					"DESCRIPCION" => $pardestinotierra->getDescripcion(),
					"PRECIO" => number_format($pardestinotierra->getPrecio(),2,',','.'),
					"IDLOCALIDAD" => $pardestinotierra->getIdLocalidad(),
					"LISTALOCALIDAD" => $listalocalidad,
					"nombreaccion"=>$nombreaccion,
					"nombreboton"=>$nombreboton,
					"CONFIGURACION"=>"style='visibility:hidden'",
					"SOLOLECTURA"=>"readonly='readonly'",
					"ENAB_DISA"=>"disabled='disabled'"
                    );
	  }				

        return $parametros;
  }
 

/*-------------------------------------------------------------------------------------*/
/*-------------------------------------------------------------------------------------*/


   public function cargavariables($clasecarga,$oper){
       
	 ///carga las variables de la clase 
	   
	   if ($oper==MODIFICAR){  
	    
        $clasecarga->putId($_POST["id"]);
		}
        $clasecarga->putDescripcion($_POST["descripcion"]);
        $clasecarga->putPrecio($_POST["precio"]);
        $clasecarga->putIdLocalidad($_POST["idlocalidad"]);
   }

}

?>