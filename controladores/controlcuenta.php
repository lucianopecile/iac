<?php
require_once 'modelos/modelocuenta.php';
require_once 'modelos/modelosolicitud.php';
require_once 'modelos/modelocuota.php';
require_once 'modelos/modelotipoperiodo.php';
require_once 'modelos/modeloparametro.php';
require_once 'modelos/modelolog.php';

class ControlCuenta
{
 
 	function __construct()
	{
	    $this->view = new View();
	}
 
//---------------------------------------------------------------------------------

	public function mostrarCuenta()
	// muestra todas las cuentas en un html con una tabla
	{
            $cuentas = new ModeloCuenta();
            $liztado = $cuentas->listadoTotal();
            $data['liztado'] = $liztado;
            $this->view->show1("cuenta.html", $data);
 	}
 
//---------------------------------------------------------------------------------

    public function altacuenta()
    {
        //creo un nuevo objeto de LOG
        $log = new ModeloLog();
        //traigo los parametros seteados de intereses
        $parametros = new ModeloParametro();
        $alta = new ModeloCuenta();
        $this->cargavariables($alta, ALTA);
        if($alta->existeNroCuenta())
        {
            $mensaje = htmlentities("El numero de cuenta ingresado ya existe para otra cuenta. Corrobore los datos.");
            $data['mensaje'] = $mensaje;
            $this->view->show1("mostrarerror.html", $data);
            return;
        }
        $idcta = $alta->altacuenta();
        if ($idcta == 0)
        {
            $mensaje = htmlentities("En este momento no se puede realizar la operacion, intentelo mas tarde");
            $data['mensaje'] = $mensaje;
            $this->view->show1("mostrarerror.html", $data);
            return;
        }                       
        //guardo el LOG de creacion de cuenta
        $log->altaLog("Se crea la cuenta Nro: ".$idcta);
        $alta->putIdCuenta($idcta);
        //traigo la cuenta que acabo de dar de alta para generar las cuotas
        $alta->traerCuenta();
        //genero todas las cuotas segun los datos grabados de la cuenta
        $this->generarCuotasSistemaFrances($alta);
        $vista = new view();
        $data['controlador'] = "cuenta";
        $data['accion'] = "vercuenta&&idcta=".$idcta."&&operacion=4";
        $vista->show1("bridgecustom.html",$data);
    }

//---------------------------------------------------------------------------------------
	
	public function modificarcuenta()
	{
            //creo un nuevo objeto de LOG
            $log = new ModeloLog();
            
            $modifica = new ModeloCuenta();
            $this->cargavariables($modifica, MODIFICAR);
                        
            //no permito reliquidar la cuenta por un monto inferior a lo ya pagado
            $pagado = $modifica->sumaCapitalCuotasPagas();
            if($pagado-$_POST['valorliquidacion'] >= 1 )
            {
                    $mensaje = htmlentities("El monto cobrado a la cuenta ya supera el nuevo valor de la tierra. Se cancel� la liquidaci�n.");
                    $data['mensaje'] = $mensaje;
                    $this->view->show1("mostrarerror.html", $data);
                    return;
            }
            $modificado = $modifica->modificarCuenta();
            if (!$modificado)
            {
                    $mensaje = htmlentities("En este momento no se puede realizar la operaci�n, int�ntelo m�s tarde");
                    $data['mensaje'] = $mensaje;
                    $this->view->show1("mostrarerror.html", $data);
                    return;
            }
            //guardo el LOG de modificacion de cuenta
            $log->altaLog("Se modifica la cuenta Nro: ".$modifica->getIdCuenta());
            //traigo todos los campos de la cuenta
            $modifica->traerCuenta();
            if(!$this->recalcularCuotas($modifica))
                return false;
            $idcta = $modifica->getIdCuenta();
            $vista = new view();
            $data['controlador'] = "cuenta";
            $data['accion'] = "vercuenta&&idcta=".$idcta."&&operacion=4";
            $vista->show1("bridgecustom.html",$data);
	}

//---------------------------------------------------------------------------------------

	public function borrarcuenta()
	{
            //creo un nuevo objeto de LOG
            $log = new ModeloLog();
            $borra = new ModeloCuenta();
            $borra->putIdCuenta($_POST['idcta']);
            if($borra->tieneCuotasPagas())
            {
                $mensaje = htmlentities("No es posible dar de baja esta cuenta. Ya cuenta con al menos una cuota paga");
                $data['mensaje'] = $mensaje;
                $this->view->show1("mostrarerror.html", $data);
                return;
            }
		$borrar_cuotas = $borra->borrarCuotasCuenta();
		if (!$borrar_cuotas)
		{
                    $mensaje = htmlentities("En este momento no se puede realizar la operaci�n, int�ntelo m�s tarde");
                    $data['mensaje'] = $mensaje;
                    $this->view->show1("mostrarerror.html", $data);
                    return;
		}
		$borrado = $borra->borrarCuenta();
		if (!$borrado)
		{
			$mensaje = htmlentities("En este momento no se puede realizar la operaci�n.\nCompruebe los resultados e int�ntelo m�s tarde");
			$data['mensaje'] = $mensaje;
			$this->view->show1("mostrarerror.html", $data);
			return;
		}
                //guardo el LOG de borrado de cuenta
                $log->altaLog("Se elimina la cuenta Nro: ".$borra->getIdCuenta()." con todas sus cuotas.");
		$this->mostrarcuenta();
	}

//---------------------------------------------------------------------------------------

   	public function mostrarAjustarCuenta()
	// muestra todas las cuentas en un html con una tabla
	{
            $cuentas = new ModeloCuenta();
            $liztado = $cuentas->listadoTotal();
            $data['liztado'] = $liztado;
            $this->view->show1("listaajustecuenta.html", $data);
 	}

//---------------------------------------------------------------------------------------

	public function ajustarLiquidacionCuenta()
	{
		$cuenta = new ModeloCuenta();
		$this->cargavariables($cuenta, MODIFICAR);
		//no permito reliquidar la cuenta por un monto inferior a lo ya pagado
		$pagado = $cuenta->sumaCapitalCuotasPagas();
		if($pagado-$_POST['valorliquidacion'] >= 1 )
		{
                    $mensaje = htmlentities("El monto cobrado a la cuenta ya supera el nuevo valor de la tierra. Se cancel� la liquidaci�n.");
                    $data['mensaje'] = $mensaje;
                    $this->view->show1("mostrarerror.html", $data);
                    return;
		}
		$modificado = $cuenta->ajustarLiquidacion();
		if (!$modificado)
		{
                    $mensaje = htmlentities("En este momento no se puede realizar la operaci�n, int�ntelo m�s tarde");
                    $data['mensaje'] = $mensaje;
                    $this->view->show1("mostrarerror.html", $data);
                    return;
		}
        //traigo todos los campos de la cuenta
        $cuenta->traerCuenta();
        if(!$this->recalcularCuotas($cuenta))
			return false;
        $idcta = $cuenta->getIdCuenta();
		$vista = new view();
        $data['controlador'] = "cuenta";
        $data['accion'] = "vercuenta&&idcta=".$idcta."&&operacion=4";
		$vista->show1("bridgecustom.html",$data);
	}

//---------------------------------------------------------------------------------------

	public function verCuenta()
	//retorna los datos de una cuenta si se carg� el id, sino retorna campos en blanco para hacer un alta
	{	
            $cuentas = new ModeloCuenta();
            if (isset($_GET['idcta']))
            { 
                $cuentas->putIdCuenta($_GET['idcta']);
                $c_ok = $cuentas->traerCuenta();
                if (!$c_ok)
                {
                    $mensaje = htmlentities("En este momento no se puede realizar la operaci�n, int�ntelo m�s tarde");
                    $data['mensaje'] = $mensaje;
                    $this->view->show1("mostrarerror.html", $data);
                    return;
                }
                $solicitud = new ModeloSolicitud();	
                $solicitud->putIdSolicitud($cuentas->getIdSolicitud());
                $s_ok = $solicitud->traersolicitud();
                if (!$s_ok)
                {
                    $mensaje = htmlentities("En este momento no se puede realizar la operaci�n, int�ntelo m�s tarde");
                    $data['mensaje'] = $mensaje;
                    $this->view->show1("mostrarerror.html", $data);
                    return;
                }
            }
            $periodo = new ModeloTipoPeriodo();
            $data = $this->cargarPlantillaModificar($cuentas, $solicitud, $periodo);
            $this->view->show("abmcuenta.html", $data);
	}

//---------------------------------------------------------------------------------------

	public function verAjusteCuenta()
	//igual a verCuenta() pero para acceder a la pantalla de ajuste manual de liquidacion
	{
		$cuentas = new ModeloCuenta();
		if (isset($_GET['idcta']))
		{
                    $cuentas->putIdCuenta($_GET['idcta']);
                    $c_ok = $cuentas->traerCuenta();
                    if (!$c_ok)
                    {
                            $mensaje = htmlentities("En este momento no se puede realizar la operaci�n, int�ntelo m�s tarde");
                            $data['mensaje'] = $mensaje;
                            $this->view->show1("mostrarerror.html", $data);
                            return;
                    }
                    $solicitud = new ModeloSolicitud();
                    $solicitud->putIdSolicitud($cuentas->getIdSolicitud());
                    $s_ok = $solicitud->traersolicitud();
                    if (!$s_ok)
                    {
                            $mensaje = htmlentities("En este momento no se puede realizar la operaci�n, int�ntelo m�s tarde");
                            $data['mensaje'] = $mensaje;
                            $this->view->show1("mostrarerror.html", $data);
                            return;
                    }
		}
		$periodo = new ModeloTipoPeriodo();
		$data = $this->cargarPlantillaAjustar($cuentas, $solicitud, $periodo);
		$this->view->show("ajustecuenta.html", $data);
	}

//==================================================================================================	
	
    public function cargarPlantillaModificar($parCuenta, $parSolicitud, $parPeriodo) 
    {  
        $parParametros=new modeloparametro();
        $parParametros->putId('1');
        $parParametros->traerParametro();

        $vperiodo = $parPeriodo->TraerTodos();
        $vperiodo['selected'] = $parCuenta->getPeriodoCuota()."-".$parCuenta->getCantMeses();

        if(isset($_GET['operacion'])){
            $quehacer=$_GET['operacion'];
        }else{
            $quehacer=ALTA;
        }

        switch($quehacer)
        {
                case ALTA:
                        $nombreboton="Generar cuotas";
                        $nombreaccion="altacuenta";
                        break;	 
                case MODIFICAR:
                        $nombreboton="Recalcular cuotas";
                        $nombreaccion="modificarcuenta";
                        break;
                case BAJA:
                        $nombreboton="Eliminar";
                        $nombreaccion="borrarcuenta";  
                        break;
                default:
                        //por defecto es la consulta de la cuenta
                        $nombreboton="";
                        $nombreaccion="";  
        }

        switch ($quehacer)
        {
                case MODIFICAR:
                    //si ya existe la cuenta traigo estos datos para controles con JS
                    $cuota = new ModeloCuota();
                    $cuota->putIdCuenta($parCuenta->getIdCuenta());
                    $arr_cobrado = $cuota->calcularCapitalCobrado();
                    if($cuota->tienePagoFormalizacion())
                            $cant_cuotas_cobradas = $arr_cobrado['cantcuotas']-1;	//si tiene paga la formalizacion, es una cuota menos
                    else
                            $cant_cuotas_cobradas = $arr_cobrado['cantcuotas'];
                    $cant_cuotas_cobradas = $arr_cobrado['cantcuotas']-1;
                    $capital_cobrado = $arr_cobrado['capitalcobrado'];
                    $arr_ultima_paga = $cuota->ultimaCuotaPaga();
                    $ultima_cuota_paga = $arr_ultima_paga['nrocuota'];

                    $parametros = array(
                            "TITULO"=>"Reliquidar",
                            "IDCUENTA"=>$parCuenta->getIdCuenta(),
                            "IDSOLICITUD"=>$parCuenta->getIdSolicitud(),
                            "NROCUENTA"=>$parCuenta->getNroCuenta(),
                            "SOLICITANTE"=>$parSolicitud->getNombres().", ".$parSolicitud->getApellido(),
                            "NROEXPEDIENTE"=>$parSolicitud->getNroExpediente(),
                            "ANIOEXPEDIENTE"=>$parSolicitud->getAnioExpediente(),
                            "FECHACUENTA"=>fechaACadena($parCuenta->getFechaCuenta()),
                            "TIPO"=>$parSolicitud->getTipoSolicitud(),
                            "CUENTACORRIENTE"=>$parCuenta->getCuentaCorriente(), 
                            "CONVENIO"=>$parCuenta->getConvenio(),
                            "VALORLIQUIDACION"=>number_format($parCuenta->getValorLiquidacion(),2,',','.'),
                        
                            //defino el % de formalización para el cálculo de las cuotas                            
                            "FORMALIZACION"=>number_format($parCuenta->getFormalizacion(),2,',','.'),
                            //agrego el % de recupera de la mensura mensura
                            "RECUPEROMENSURA" => number_format($parCuenta->getRecuperoMensura(),2,',','.'),
                        
                            "LIQ_SIN_FORMATO"=>$parCuenta->getValorLiquidacion(),
                            "CANTCUOTAS"=>$parCuenta->getCantCuotas(),
                            "CANTMESES"=>$parCuenta->getCantMeses(),
                            "IDPERIODO"=>$parCuenta->getPeriodoCuota(),
                            "LISTAPERIODO"=>$vperiodo,
                            "TASAINTERES"=>number_format($parCuenta->getTasaInteres(),3,',','.'),
                            "OBSERVACION"=>$parCuenta->getObservacion(),
                        
                            "SUMARINTERESES"=>($parCuenta->getSumarIntereses()==1)?"checked":"",
                            "CHECKFORMALIZACION"=>($parCuenta->getFormalizacion()>0)?"":"checked",
                            "CUOTASFORMALIZACION"=>$parCuenta->getCantCuotasFormalizacion(),
                            "PERIODODIFERIDOFORMALIZACION"=>$parCuenta->getPeriodoDiferidoFormalizacion(),
                            "PRIMERVENCIMIENTOFORMALIZACION"=>fechaACadena($parCuenta->getFechaVencimientoFormalizacion()),
                            "CHECKCUOTASPARALELAS"=>($parCuenta->getCuotasParalelas()==1)?"checked":"",
                            "PERIODODIFERIDOPLAN"=>$parCuenta->getPeriodoDiferidoPlan(),
                            "PRIMERVENCIMIENTOPLAN"=>fechaACadena($parCuenta->getFechaVencimientoPlan()),
                        
                            "NROEXPEDIENTE"=>$parSolicitud->getNroExpediente(),
                            "ANIOEXPEDIENTE"=>$parSolicitud->getAnioExpediente(),
                            "CANTCUOTASCOBRADAS"=>$cant_cuotas_cobradas,		//necesario para controles en el reliquidar
                            "CAPITALCOBRADO"=>$capital_cobrado,					//necesario para controles en el reliquidar
                            "ULTIMACUOTAPAGA"=>$ultima_cuota_paga,				//necesario para controles en el reliquidar
                            "DISA_PERIODO"=>($capital_cobrado > 0)?"disabled":"", //necesario para controles en el reliquidar
                            //"CONFIGURACION"=>'style="visibility:hidden"',
                            "CONFIGURACION"=>'',
                            "DISA_MODI"=>"disabled='disabled'",
                            "ENAB_DISA"=>"",				
                            "VISIBLE_MODI"=>'',
                            "OPERACION"=>MODIFICAR,     //necesario para verificar o no el primer vencimiento
                            "nombreaccion"=>$nombreaccion,
                            "nombreboton"=>$nombreboton
                    );
                    break;					
                case BAJA:
                    $parametros = array(
                            "TITULO"=>"Eliminar",
                            "IDCUENTA"=>$parCuenta->getIdCuenta(),
                            "IDSOLICITUD"=>$parCuenta->getIdSolicitud(),
                            "NROCUENTA"=>$parCuenta->getNroCuenta(),
                            "SOLICITANTE"=>$parSolicitud->getNombres().", ".$parSolicitud->getApellido(),
                            "NROEXPEDIENTE"=>$parSolicitud->getNroExpediente(),
                            "ANIOEXPEDIENTE"=>$parSolicitud->getAnioExpediente(),
                            "FECHACUENTA"=>fechaACadena($parCuenta->getFechaCuenta()),
                            "TIPO"=>$parSolicitud->getTipoSolicitud(),
                            "CUENTACORRIENTE"=>$parCuenta->getCuentaCorriente(),
                            "CONVENIO"=>$parCuenta->getConvenio(),
                            "VALORLIQUIDACION"=>number_format($parCuenta->getValorLiquidacion(),2,',','.'),
                        
                            //defino el % de formalización para el cálculo de las cuotas                            
                            "FORMALIZACION"=>number_format($parCuenta->getFormalizacion(),2,',','.'),
                            //agrego el % de recupera de la mensura mensura
                            "RECUPEROMENSURA" => number_format($parCuenta->getRecuperoMensura(),2,',','.'),
                        
                            "LIQ_SIN_FORMATO"=>$parCuenta->getValorLiquidacion(),
                            "CANTCUOTAS"=>$parCuenta->getCantCuotas(),
                            "CANTMESES"=>$parCuenta->getCantMeses(),
                            "IDPERIODO"=>$parCuenta->getPeriodoCuota(),
                            "LISTAPERIODO"=>$vperiodo,
                            "TASAINTERES"=>number_format($parCuenta->getTasaInteres(),3,',','.'),
                            "OBSERVACION"=>$parCuenta->getObservacion(),
                        
                            "SUMARINTERESES"=>($parCuenta->getSumarIntereses()==1)?"checked":"",
                            "CHECKFORMALIZACION"=>($parCuenta->getFormalizacion()>0)?"":"checked",
                            "CUOTASFORMALIZACION"=>$parCuenta->getCantCuotasFormalizacion(),
                            "PERIODODIFERIDOFORMALIZACION"=>$parCuenta->getPeriodoDiferidoFormalizacion(),
                            "PRIMERVENCIMIENTOFORMALIZACION"=>fechaACadena($parCuenta->getFechaVencimientoFormalizacion()),
                            "CHECKCUOTASPARALELAS"=>($parCuenta->getCuotasParalelas()==1)?"checked":"",
                            "PERIODODIFERIDOPLAN"=>$parCuenta->getPeriodoDiferidoPlan(),
                            "PRIMERVENCIMIENTOPLAN"=>fechaACadena($parCuenta->getFechaVencimientoPlan()),
                       
                            "NROEXPEDIENTE"=>$parSolicitud->getNroExpediente(),
                            "ANIOEXPEDIENTE"=>$parSolicitud->getAnioExpediente(),
                            "CONFIGURACION"=>'style="visibility:hidden"',
                            "DISA_MODI"=>"disabled='disabled'",
                            "ENAB_DISA"=>"disabled='disabled'",
                            "VISIBLE_MODI"=>"",
                            "VISIBLE_OBS"=>"style='visibility:hidden'",
                            "OPERACION"=>BAJA,     //necesario para verificar o no el primer vencimiento
                            "nombreaccion"=>$nombreaccion,
                            "nombreboton"=>$nombreboton
                    );
                    break;

                case ALTA:
                    $parametros = array(
                            "TITULO"=>"Nueva",
                            "IDCUENTA"=>0,
                            "IDSOLICITUD"=>0,
                            "NROCUENTA"=>"",
                            "SOLICITANTE"=>"",
                            "NROEXPEDIENTE"=>0,
                            "ANIOEXPEDIENTE"=>0,
                            "FECHACUENTA"=>"",
                            "TIPO"=>0,
                            "CUENTACORRIENTE"=>$parParametros->getCuentaCorriente(),
                            "CONVENIO"=>$parParametros->getConvenio(),
                            "VALORLIQUIDACION"=>number_format(0,2,',','.'),
                            "LIQ_SIN_FORMATO"=>0,
                            //defino el % de formalización para el cálculo de las cuotas
                            "FORMALIZACION"=>number_format(10,2,',','.'),
                            //agrego el % de recupera de la mensura mensura
                            "RECUPEROMENSURA" => number_format($parParametros->getRecuperoMensura(),2,',','.'),
                            "CANTCUOTAS"=>0,
                            "CANTMESES"=>0,
                            "IDPERIODO"=>0,
                            "LISTAPERIODO"=>$vperiodo,
                            //"TASAINTERES"=>number_format(5.07,3,',','.'),	/* valor de interes por defecto */
                            "TASAINTERES"=>number_format($parParametros->getInteresCuotas(),3,',','.'),
                            "OBSERVACION"=>"",
                        
                            "SUMARINTERESES"=>"checked",
                            "CHECKFORMALIZACION"=>"",
                            "CUOTASFORMALIZACION"=>1,
                            "PERIODODIFERIDOFORMALIZACION"=>1,
                            "PRIMERVENCIMIENTOFORMALIZACION"=>null,
                            "CHECKCUOTASPARALELAS"=>"",
                            "PERIODODIFERIDOPLAN"=>1,
                            "PRIMERVENCIMIENTOPLAN"=>null,
                        
                            "NROEXPEDIENTE"=>"",
                            "ANIOEXPEDIENTE"=>"",
                            "CANTCUOTASCOBRADAS"=>0,		//necesario para controles en el reliquidar
                            "CAPITALCOBRADO"=>0,			//necesario para controles en el reliquidar
                            "ULTIMACUOTAPAGA"=>0,			//necesario para controles en el reliquidar
                            "OPERACION"=>ALTA,     //necesario para verificar o no el primer vencimiento
                            "VISIBLE_OBS"=>"style='visibility:hidden'",
                            "nombreaccion"=>$nombreaccion,
                            "nombreboton"=>$nombreboton,
                    );
                    break;
                default:
                    //el default es la consulta de una cuenta
                    $parametros = array(
                            "TITULO"=>"Consultar",
                            "IDCUENTA" => $parCuenta->getIdCuenta(),
                            "IDSOLICITUD" =>$parCuenta->getIdSolicitud(),
                            "NROCUENTA"=>$parCuenta->getNroCuenta(),
                            "SOLICITANTE"=>$parSolicitud->getNombres().", ".$parSolicitud->getApellido(),
                            "NROEXPEDIENTE"=>$parSolicitud->getNroExpediente(),
                            "ANIOEXPEDIENTE"=>$parSolicitud->getAnioExpediente(),
                            "FECHACUENTA"=>fechaACadena($parCuenta->getFechaCuenta()),
                            "TIPO"=>$parSolicitud->getTipoSolicitud(),
                            "CUENTACORRIENTE"=>$parCuenta->getCuentaCorriente(),
                            "CONVENIO"=>$parCuenta->getConvenio(),
                            "VALORLIQUIDACION"=>number_format($parCuenta->getValorLiquidacion(),2,',','.'),
                            
                            //defino el % de formalización para el cálculo de las cuotas                            
                            "FORMALIZACION"=>number_format($parCuenta->getFormalizacion(),2,',','.'),
                            //agrego el % de recupera de la mensura mensura
                            "RECUPEROMENSURA" => number_format($parCuenta->getRecuperoMensura(),2,',','.'),
                        
                            "LIQ_SIN_FORMATO"=>$parCuenta->getValorLiquidacion(),
                            "CANTCUOTAS"=>$parCuenta->getCantCuotas(),
                            "CANTMESES"=>$parCuenta->getCantMeses(),
                            "IDPERIODO"=>$parCuenta->getPeriodoCuota(),
                            "LISTAPERIODO"=>$vperiodo,
                            "TASAINTERES"=>number_format($parCuenta->getTasaInteres(),3,',','.'),
                            "OBSERVACION"=>$parCuenta->getObservacion(),
                            "NROEXPEDIENTE"=>$parSolicitud->getNroExpediente(),
                            "ANIOEXPEDIENTE"=>$parSolicitud->getAnioExpediente(),
                        
                            "SUMARINTERESES"=>($parCuenta->getSumarIntereses()==1)?"checked":"",
                            "CHECKFORMALIZACION"=>($parCuenta->getFormalizacion()>0)?"":"checked",
                            "CUOTASFORMALIZACION"=>$parCuenta->getCantCuotasFormalizacion(),
                            "PERIODODIFERIDOFORMALIZACION"=>$parCuenta->getPeriodoDiferidoFormalizacion(),
                            "PRIMERVENCIMIENTOFORMALIZACION"=>fechaACadena($parCuenta->getFechaVencimientoFormalizacion()),
                            "CHECKCUOTASPARALELAS"=>($parCuenta->getCuotasParalelas()==1)?"checked":"",
                            "PERIODODIFERIDOPLAN"=>$parCuenta->getPeriodoDiferidoPlan(),
                            "PRIMERVENCIMIENTOPLAN"=>fechaACadena($parCuenta->getFechaVencimientoPlan()),
                            
                            "CONFIGURACION"=>"style='visibility:hidden'",
                            "DISA_MODI"=>"disabled='disabled'",
                            "ENAB_DISA"=>"disabled='disabled'",
                            "VISIBLE_MODI"=>"style='visibility:hidden'",
                            "VISIBLE_OBS"=>"style='visibility:hidden'",
                            "OPERACION"=>VER,     //necesario para verificar o no el primer vencimiento
                            "nombreaccion"=>$nombreaccion,
                            "nombreboton"=>$nombreboton
                    );
        } 				
        return $parametros;
    }

//==================================================================================================

    public function cargarPlantillaAjustar($parCuenta, $parSolicitud, $parPeriodo)
    {
        $vperiodo = $parPeriodo->TraerTodos();
        $vperiodo['selected'] = $parCuenta->getPeriodoCuota()."-".$parCuenta->getCantMeses();
        //si ya existe la cuenta traigo estos datos para controles con JS
        $cuota = new ModeloCuota();
        $cuota->putIdCuenta($parCuenta->getIdCuenta());
        $arr_cobrado = $cuota->calcularCapitalCobrado();
        if($cuota->tienePagoFormalizacion())
            $cant_cuotas_cobradas = $arr_cobrado['cantcuotas']-1;	//si tiene paga la formalizacion, es una cuota menos
        else
            $cant_cuotas_cobradas = $arr_cobrado['cantcuotas'];
        $capital_cobrado = $arr_cobrado['capitalcobrado'];
        $arr_ultima_paga = $cuota->ultimaCuotaPaga();
        $ultima_cuota_paga = $arr_ultima_paga['nrocuota'];

        $parametros = array(
            "TITULO"=>"Ajustar liquidaci&oacute;n",
            "IDCUENTA"=>$parCuenta->getIdCuenta(),
            "IDSOLICITUD"=>$parCuenta->getIdSolicitud(),
            "NROCUENTA"=>$parCuenta->getNroCuenta(),
            "SOLICITANTE"=>$parSolicitud->getNombres().", ".$parSolicitud->getApellido(),
            "NROEXPEDIENTE"=>$parSolicitud->getNroExpediente(),
            "ANIOEXPEDIENTE"=>$parSolicitud->getAnioExpediente(),
            "FECHACUENTA"=>fechaACadena($parCuenta->getFechaCuenta()),
            "TIPO"=>$parSolicitud->getTipoSolicitud(),
            "CUENTACORRIENTE"=>$parCuenta->getCuentaCorriente(),
            "CONVENIO"=>$parCuenta->getConvenio(),
            "VALORLIQUIDACION"=>number_format($parCuenta->getValorLiquidacion(),2,',','.'),
            "LIQ_SIN_FORMATO"=>$parCuenta->getValorLiquidacion(),
            "CANTCUOTAS"=>$parCuenta->getCantCuotas(),
            "CANTMESES"=>$parCuenta->getCantMeses(),
            "IDPERIODO"=>$parCuenta->getPeriodoCuota(),
            "LISTAPERIODO"=>$vperiodo,
            "TASAINTERES"=>number_format($parCuenta->getTasaInteres(),3,',','.'),
            "OBSERVACION"=>$parCuenta->getObservacion(),
            "NROEXPEDIENTE"=>$parSolicitud->getNroExpediente(),
            "ANIOEXPEDIENTE"=>$parSolicitud->getAnioExpediente(),
            "CANTCUOTASCOBRADAS"=>$cant_cuotas_cobradas,		//necesario para controles en el reliquidar
            "CAPITALCOBRADO"=>$capital_cobrado,					//necesario para controles en el reliquidar
            "ULTIMACUOTAPAGA"=>$ultima_cuota_paga,				//necesario para controles en el reliquidar
            "CONFIGURACION"=>'style="visibility:hidden"',
            "DISA_MODI"=>"disabled='disabled'",
            "nombreaccion"=>"ajustarliquidacioncuenta",
            "nombreboton"=>"Recalcular cuotas"
        );
        return $parametros;
    }

//----------------------------------------------------------------------------------

    public function cargavariables($clasecarga, $oper)
    // Carga las variables del html para volcarlas en la tabla
    {
        if ($oper!=ALTA)
            $clasecarga->putIdCuenta($_POST["idcta"]);
            $clasecarga->putNroCuenta(strtoupper($_POST["nrocuenta"]));
            $clasecarga->putIdSolicitud($_POST["idsolicitud"]);
            $clasecarga->putIdUsrCreador($_SESSION["s_idusr"]);
            $clasecarga->putIdUsrMod($_SESSION["s_idusr"]);
            $clasecarga->putFechaCuenta(cadenaAFecha($_POST["fechacuenta"]));
            $clasecarga->putCuentaCorriente($_POST["cuentacorriente"]);
            $clasecarga->putConvenio($_POST["convenio"]);            
            $clasecarga->putValorLiquidacion($_POST["valorliquidacion"]);
            $clasecarga->putValorHectareaFinal($_POST["valorhectareafinal"]);                        
            $clasecarga->putCantCuotas($_POST["cantcuotas"]);
            $clasecarga->putPeriodoCuota($_POST["idperiodo"]);
            $clasecarga->putTasaInteres($_POST["tasainteres"]);
            $clasecarga->putFormalizacion($_POST["formalizacion"]);
            $clasecarga->putRecuperoMensura($_POST["porcentajerecupero"]);
            $nuevaobservacion = $_POST["observacionant"]."\n".$_POST["observacion"];
            $clasecarga->putObservacion($nuevaobservacion);
            
            $clasecarga->putSumarIntereses($_POST["sumarinteres"]);
            $clasecarga->putCantCuotasFormalizacion($_POST["cantcuotasformalizacion"]);
            $clasecarga->putPeriodoDiferidoFormalizacion($_POST["perdifformalizacion"]);
            $clasecarga->putFechaVencimientoFormalizacion(cadenaAFecha($_POST["primervencimientoformalizacion"]));
            $clasecarga->putCuotasParalelas($_POST["checkcuotasjuntas"]);
            $clasecarga->putPeriodoDiferidoPlan($_POST["perdifplan"]);
            $clasecarga->putFechaVencimientoPlan(cadenaAFecha($_POST["primervencimientoplan"]));
                        
            //$clasecarga->putPrimerVenc(cadenaAFecha($_POST["primervencimientoplan"]));
    }	
	
//==================================================================================================

    public function generarCuotas($cuenta)
    {
        //creo un nuevo objeto de LOG
        $log = new ModeloLog();
        //creo el objeto de la cuota que se va a ir generando
        $cuota = new modelocuota();
        //a la cuota le coloco el nro de cuenta a la que pertenece
        $cuota->putIdCuenta($cuenta->getIdCuenta());
        //a la cuota le coloco el id del usuario creador
        $cuota->putIdUsrCreador($cuenta->getIdUsrCreador());
        //a la cuota le coloco el id del usuario modificador
        $cuota->putIdUsrMod($_SESSION["s_idusr"]);
        //obtengo la cantidad de cuotas del plan
        $cantcuotas = $cuenta->getCantCuotas();	
        //obtengo la cantidad de meses segun el periodo elegido
        $cantmeses = $cuenta->getCantMeses();
        //obtengo el % de recupero de mensura de la cuenta
        $porct_recupero = $cuenta->getRecuperoMensura();        
        //calculo el interes anual (sin periodo diferido) dividido por la cantidad de cuotas por año
        $porcentaje_int_cuota = $cuenta->getTasaInteres() / (12/$cantmeses); //1,54                
                                        
        //genero el resto a liquidar
        //$liquidacion_resto = $cuenta->getValorLiquidacion() - $cuenta->getValorLiquidacion()*($cuenta->getFormalizacion()/100) + $cuenta->getValorLiquidacion()*0.20;
        $liquidacion_resto = $cuenta->getValorLiquidacion() - $cuenta->getValorLiquidacion()*($cuenta->getFormalizacion()/100) + $cuenta->getValorLiquidacion() * ($porct_recupero/100);
        $varmontoaFinanciar = $liquidacion_resto ;
        //calculo el capital de la cuota pura
        $capital_cuota = redondeoCincoCent($liquidacion_resto / $cantcuotas);	//redondeado                 
        
        //$interes_cuota = redondeoCincoCent($liquidacion_resto * ($porcentaje_int_cuota / 100));	//redondeado 
        
        //inicializo las valiables
        $monto_cuota = $capital_cuota;
        $cobrado = 0;
        $nroCuota = 1;
        //guardo la fecha de vencimiento del plan
        $fechavenc = $cuenta->getFechaVencimientoPlan();
        
        /* ############### INICIO del calculo las cuotas de formalizacion ############### */
        //si tiene formalizacion y se paga en cuotas
        if($cuenta->getFormalizacion() > 0){
            //calculo la formalizacion = (LiquidacionTotal * % de formalizacion)/100
            $monto_formalizacion = redondeoCincoCent($cuenta->getValorLiquidacion() * $cuenta->getFormalizacion() / 100);
            //obtengo la cantidad de coutas especificadas de la formalizacion            
            $cantCuotasFormalizacion = $cuenta->getCantCuotasFormalizacion();
            //obtengo fecha de vencimiento de la 1er cuota de formalizacion
            $fecha_formalizacion = $cuenta->getFechaVencimientoFormalizacion();                        
            //genero las cuotas puras de formalizacion del credito
            $montoCuotaFormalizacion = $monto_formalizacion / $cantCuotasFormalizacion;                        
            //obtengo la cantidad de cuotas de formalizacion a multiplicar para sacar el interes
            $totCuotasFormConPeriodoDif = ($cantCuotasFormalizacion == 1)?$cantCuotasFormalizacion-1:$cantCuotasFormalizacion;            
            //Si se deben sumar los intereses por periodos diferidos
            if($cuenta->getSumarIntereses()==1){
                ($cuenta->getPeriodoDiferidoFormalizacion() == 1 )? $peridoDifForm = $cuenta->getPeriodoDiferidoFormalizacion()-1 : $peridoDifForm = $cuenta->getPeriodoDiferidoFormalizacion();
                ($cantCuotasFormalizacion == 1) ? $cuotasForm = $cantCuotasFormalizacion-1 : $cuotasForm = $cantCuotasFormalizacion;
                //obtengo la cantidad de cuotas de la formalizacion + periodo diferido
                $totCuotasFormConPeriodoDif = ($peridoDifForm)+($cuotasForm);
            }            
            //Comienzo a generar las cuotas de la formalizacion
            for ($nroCuota; $nroCuota<=$cantCuotasFormalizacion;$nroCuota++)
            {                
                //calculo el montoInteres por cuota
                $montoInteres = (($porcentaje_int_cuota * ($totCuotasFormConPeriodoDif - $nroCuota + 1) ) * $montoCuotaFormalizacion)/100;                
                //guardo los datos de la cuota
                $cuota->putNroCuota($nroCuota);
                $cuota->putFechaVenc($fecha_formalizacion);
                $cuota->putCapital($montoCuotaFormalizacion);
                $cuota->putMontoCuota($montoCuotaFormalizacion + $montoInteres);
                //$cuota->putInteres(0.00);
                $cuota->putInteres($montoInteres);
                $cuota->putSaldo($montoCuotaFormalizacion + $montoInteres);
                $cuota->putEsCuotaFormalizacion(1); //defino que esta cuenta es de formalizacion y no del plan
                $cuota->putCobrado($cobrado);
                //doy de alta la cuota en la base de datos
                $altaok = $cuota->altacuota();
                //si hubo algun error en el alta de la cuota => error
                if (!$altaok)
                {
                    $mensaje = htmlentities("No se generaron correctamente las cuotas");
                    $data['mensaje'] = $mensaje;
                    //si falla una cuota elimino todas las anteriores
                    $cuenta->borrarCuotasCuenta();
                    $this->view->show1("mostrarerror.html", $data);
                    return false;
                }
                //calculo la nueva fecha de vencimiento de la formalizacion segun los meses
                for ($j = 1; $j <= $cantmeses; $j++)
                {
                    $fecha_formalizacion = $this->sumaMes($fecha_formalizacion);
                }                
            }
        }
        /* ###############  FIN del calculo las cuotas de formalizacion ############### */
        
        /* ###############  INICIO del calculo las cuotas del plan ############### */
        //obtengo el total de cuotas del plan + foralizacion
        $totalCuotasConDiferidas = $cantcuotas+$cantCuotasFormalizacion;
        if($cuenta->getSumarIntereses()==1){
            //obtengo el total de cuotas del plan + formalizacion + diferidas
            $totalCuotasConDiferidas = ((($cuenta->getPeriodoDiferidoPlan() == 1 ? $cuenta->getPeriodoDiferidoPlan() - 1 : $cuenta->getPeriodoDiferidoPlan()) +$cantcuotas) + (($cuenta->getPeriodoDiferidoFormalizacion() == 1 ? $cuenta->getPeriodoDiferidoFormalizacion()-1 : $cuenta->getPeriodoDiferidoFormalizacion())+$cantCuotasFormalizacion));
        }
        //Comienzo a generar las cuotas del plan
        //for($i=1; $i<=$cantcuotas; $i++)
        for($nroCuota; $nroCuota<=($cantcuotas+$cantCuotasFormalizacion); $nroCuota++)
        {
             //calculo el montoInteres por cuota
            $montoInteres = (($porcentaje_int_cuota * (($totalCuotasConDiferidas) - ($nroCuota-$cantCuotasFormalizacion) + 1) )
                                 * $monto_cuota)/100;
            //echo "<br>IntDifPlan (".$porcentaje_int_cuota.")* (".$totalCuotasConDiferidas." - ".($nroCuota-$cantCuotasFormalizacion)." + 1 ) * ". $monto_cuota.") /100";
            //if(($nroCuota-$cantCuotasFormalizacion)==2){
            //    throw new Exception;
            //}
            //guardo los datos de la cuota
            //$cuota->putNroCuota($i);
            $cuota->putNroCuota($nroCuota);
            $cuota->putFechaVenc($fechavenc);
            $cuota->putCapital($capital_cuota);
            //$cuota->putMontoCuota($monto_cuota);
            $cuota->putMontoCuota($monto_cuota + $montoInteres);
            //$cuota->putInteres($interes_cuota);
            $cuota->putInteres($montoInteres);
            $cuota->putSaldo($monto_cuota + $montoInteres);
            $cuota->putCobrado($cobrado);
            $cuota->putEsCuotaFormalizacion(0); //defino que esta cuenta es del plan y no de la formalizacion
            //guardo la cuota en la base de datos
            $altaok = $cuota->altacuota();
            //si se produce un error al guarda => error!
            if (!$altaok)
            {
                $mensaje = htmlentities("No se generaron correctamente las cuotas");
                $data['mensaje'] = $mensaje;
                //si falla una cuota elimino todas las anteriores
                $cuenta->borrarCuotasCuenta();
                $this->view->show1("mostrarerror.html", $data);
                return false;
            }
            //calculo la nueva fecha de vencimiento de la proxima cuota
            for ($j=1; $j<=$cantmeses; $j++)
            {
                $fechavenc = $this->sumaMes($fechavenc);
            }
            //calculo el interes y el monto de la prox cuota segun el saldo restante
            //$liquidacion_resto -= $capital_cuota;
            //$interes_cuota = redondeoCincoCent($liquidacion_resto * ($porcentaje_int_cuota / 100));	//redondeo a 1 decimal
            //if ($i==($cantcuotas-1))  {
            if ($nroCuota==(($cantcuotas+$cantCuotasFormalizacion)-1))  {
                $capital_cuota=$capital_cuota+($varmontoaFinanciar-($capital_cuota*$cantcuotas));
            }
            //$monto_cuota = $capital_cuota + $interes_cuota;
        }        
        //guardo el LOG de creacion de todas las cuotas de la cuenta
        $log->altaLog("Se crearon todas las ". $cuenta->getCantCuotas() ." cuotas de la cuenta Nro: ".$cuenta->getIdCuenta());
        return true;
    }

    public function generarCuotasSistemaFrances($cuenta)
    {


        //creo un nuevo objeto de LOG
        $log = new ModeloLog();
        //creo el objeto de la cuota que se va a ir generando
        $cuota = new modelocuota();
        //a la cuota le coloco el nro de cuenta a la que pertenece
        $cuota->putIdCuenta($cuenta->getIdCuenta());
        //a la cuota le coloco el id del usuario creador
        $cuota->putIdUsrCreador($cuenta->getIdUsrCreador());
        //a la cuota le coloco el id del usuario modificador
        $cuota->putIdUsrMod($_SESSION["s_idusr"]);


        //obtengo la cantidad de cuotas del plan
        $cantcuotas = $cuenta->getCantCuotas();
        //obtengo la cantidad de meses segun el periodo elegido
        $cantmeses = $cuenta->getCantMeses();
        //obtengo el % de recupero de mensura de la cuenta
        $porct_recupero = $cuenta->getRecuperoMensura();

        //variables para poder sacar los valores de los
        $interes_taza = ($cuenta->getTasaInteres() / 100); //55.44=>0.5544
        $subperiodos = 12 / $cantmeses; //si es trimestral es 4(en un año entran 4 cuotas= 12/3->4)
        $porcentaje_int_cuota = $interes_taza / $subperiodos; //1,54              

        //genero el resto a liquidar
        //$liquidacion_resto = $cuenta->getValorLiquidacion() - $cuenta->getValorLiquidacion()*($cuenta->getFormalizacion()/100) + $cuenta->getValorLiquidacion()*0.20;
        $liquidacion_resto = $cuenta->getValorLiquidacion() - $cuenta->getValorLiquidacion() * ($cuenta->getFormalizacion() / 100) + $cuenta->getValorLiquidacion() * ($porct_recupero / 100);
        $varmontoaFinanciar = $liquidacion_resto;
        //calculo el capital de la cuota pura
        $saldo_inicial = $liquidacion_resto; //es el capital que va quedando. de mas a menos


    
        $amortizacion_total = 0;

        //$monto_cuota = $cuota_R;
        $cobrado = 0;
        $nroCuota = 1;
        //guardo la fecha de vencimiento del plan
        $fechavenc = $cuenta->getFechaVencimientoPlan();

        /* ############### INICIO del calculo las cuotas de formalizacion ############### */
        //si tiene formalizacion y se paga en cuotas
        if ($cuenta->getFormalizacion() > 0) {
            //calculo la formalizacion = (LiquidacionTotal * % de formalizacion)/100
            $monto_formalizacion = redondeoCincoCent($cuenta->getValorLiquidacion() * ($cuenta->getFormalizacion() / 100));

            //obtengo la cantidad de coutas especificadas de la formalizacion            
            $cantCuotasFormalizacion = $cuenta->getCantCuotasFormalizacion();
            //obtengo fecha de vencimiento de la 1er cuota de formalizacion
            $fecha_formalizacion = $cuenta->getFechaVencimientoFormalizacion();
            //genero las cuotas puras de formalizacion del credito
            //$montoCuotaFormalizacion = $monto_formalizacion / $cantCuotasFormalizacion;


            $a = ($porcentaje_int_cuota * pow((1 + $porcentaje_int_cuota), ($cantCuotasFormalizacion)));
            $b = pow((1 + $porcentaje_int_cuota), ($cantCuotasFormalizacion)) - 1;


            $montoCuotaFormalizacion = $monto_formalizacion * ($a)    / ($b);

            /*  $montoCuotaFormalizacion =  $monto_formalizacion * (
                ($interes_taza * (pow((1 + $interes_taza), ( $cantCuotasFormalizacion * $subperiodos)))) / ((pow((1 + $interes_taza), ( $cantCuotasFormalizacion * $subperiodos))) - 1)); //cuota fija es la misma pero cambia la proporcion
            */
            $montoCuotaFormalizacion = redondeoCincoCent($montoCuotaFormalizacion);



            //obtengo la cantidad de cuotas de formalizacion a multiplicar para sacar el interes
            $totCuotasFormConPeriodoDif = ($cantCuotasFormalizacion == 1) ? $cantCuotasFormalizacion - 1 : $cantCuotasFormalizacion;
            //Si se deben sumar los intereses por periodos diferidos
            if ($cuenta->getSumarIntereses() == 1) {
                ($cuenta->getPeriodoDiferidoFormalizacion() == 1) ? $peridoDifForm = $cuenta->getPeriodoDiferidoFormalizacion() - 1 : $peridoDifForm = $cuenta->getPeriodoDiferidoFormalizacion();
                ($cantCuotasFormalizacion == 1) ? $cuotasForm = $cantCuotasFormalizacion - 1 : $cuotasForm = $cantCuotasFormalizacion;
                //obtengo la cantidad de cuotas de la formalizacion + periodo diferido
                $totCuotasFormConPeriodoDif = ($peridoDifForm) + ($cuotasForm);
            }

            for ($nroCuota; $nroCuota <= $cantCuotasFormalizacion; $nroCuota++) {


                $interes_cuota = redondeoCincoCent($porcentaje_int_cuota * $monto_formalizacion); //el interes por cuota que se hace taza_interes por el saldo inical(parte de la cuota)


                $amortizacion_cuota = $montoCuotaFormalizacion - $interes_cuota; //capital que va pagando (parte de la cuota). 

                $saldo_final = $monto_formalizacion - $amortizacion_cuota; //lo que va quedando de capital para pagar


                $monto_formalizacion = $saldo_final;

                $amortizacion_total = $amortizacion_total + $amortizacion_cuota;



                //calculo el montoInteres por cuota
                // $montoInteres = (($porcentaje_int_cuota * ($totCuotasFormConPeriodoDif - $nroCuota + 1)) * $montoCuotaFormalizacion) / 100;
                //guardo los datos de la cuota
                $cuota->putNroCuota($nroCuota);
                $cuota->putFechaVenc($fecha_formalizacion);
                $cuota->putCapital($amortizacion_cuota);
                $cuota->putMontoCuota($montoCuotaFormalizacion);
                //$cuota->putInteres(0.00);
                $cuota->putInteres($interes_cuota);
                $cuota->putSaldo($amortizacion_cuota + $interes_cuota);
                $cuota->putEsCuotaFormalizacion(1); //defino que esta cuenta es de formalizacion y no del plan
                $cuota->putCobrado($cobrado);
                //doy de alta la cuota en la base de datos
                $altaok = $cuota->altacuota();
                //si hubo algun error en el alta de la cuota => error
                if (!$altaok) {
                    $mensaje = htmlentities("No se generaron correctamente las cuotas");
                    $data['mensaje'] = $mensaje;
                    //si falla una cuota elimino todas las anteriores
                    $cuenta->borrarCuotasCuenta();
                    $this->view->show1("mostrarerror.html", $data);
                    return false;
                }
                //calculo la nueva fecha de vencimiento de la formalizacion segun los meses
                for ($j = 1; $j <= $cantmeses; $j++) {
                    $fecha_formalizacion = $this->sumaMes($fecha_formalizacion);
                }
            }
        }
        /* ###############  FIN del calculo las cuotas de formalizacion ############### */

        /* ###############  INICIO del calculo las cuotas del plan ############### */
        //obtengo el total de cuotas del plan + foralizacion
        $totalCuotasConDiferidas = $cantcuotas + $cantCuotasFormalizacion;
        if ($cuenta->getSumarIntereses() == 1) {
            //obtengo el total de cuotas del plan + formalizacion + diferidas
            $totalCuotasConDiferidas = ((($cuenta->getPeriodoDiferidoPlan() == 1 ? $cuenta->getPeriodoDiferidoPlan() - 1 : $cuenta->getPeriodoDiferidoPlan())
                + $cantcuotas) + (($cuenta->getPeriodoDiferidoFormalizacion() == 1 ? $cuenta->getPeriodoDiferidoFormalizacion() - 1 : $cuenta->getPeriodoDiferidoFormalizacion())
                + $cantCuotasFormalizacion));
        }
        //Comienzo a generar las cuotas del plan
      

        //comienzo de las cuotas 
        $interes_taza = ($cuenta->getTasaInteres() / 100); //55.44=>0.5544
        $subperiodos = 12 / $cantmeses; //si es trimestral es 4(en un año entran 4 cuotas= 12/3->4)
        $porcentaje_int_cuota = $interes_taza / $subperiodos; //1,54 
        $saldo_inicial = $liquidacion_resto; //es el capital que va quedando. de mas a menos
        $cantcuotas = $cantcuotas * 1;

        $a = ($porcentaje_int_cuota * pow((1 + $porcentaje_int_cuota), ($cantcuotas)));
        $b = pow((1 + $porcentaje_int_cuota), ($cantcuotas)) - 1;


        $cuota_R = $saldo_inicial * ($a)    / ($b);


       
        $cuota_R = redondeoCincoCent($cuota_R);
        //en la proporcion de la cuota fija(cuota_R), al principio se paga mas de interes que de capital

        $saldo_I = $cuenta->getValorLiquidacion() + $cuenta->getValorLiquidacion() * ($porct_recupero / 100);
       
        $amortizacion_total = 0;
        for ($nroCuota; $nroCuota <= ($cantcuotas + $cantCuotasFormalizacion); $nroCuota++) {
            $interes_cuota = redondeoCincoCent($porcentaje_int_cuota * $saldo_inicial); //el interes por cuota que se hace taza_interes por el saldo inical(parte de la cuota)


            $amortizacion_cuota = $cuota_R - $interes_cuota; //capital que va pagando (parte de la cuota). 

            $saldo_final = $saldo_inicial - $amortizacion_cuota; //lo que va quedando de capital para pagar


            $saldo_inicial = $saldo_final;

            $amortizacion_total = $amortizacion_total + $amortizacion_cuota;


            $cuota->putNroCuota($nroCuota);
            $cuota->putFechaVenc($fechavenc);
            $cuota->putCapital($amortizacion_cuota);
            //$cuota->putMontoCuota($monto_cuota);
            $cuota->putMontoCuota($cuota_R);
            //$cuota->putInteres($interes_cuota);
            $cuota->putInteres($interes_cuota);
            $cuota->putSaldo($amortizacion_cuota + $interes_cuota);
            $cuota->putCobrado($cobrado);
            $cuota->putEsCuotaFormalizacion(0); //defino que esta cuenta es del plan y no de la formalizacion
            //guardo la cuota en la base de datos
            $altaok = $cuota->altacuota();
            //si se produce un error al guarda => error!
            if (!$altaok) {
                $mensaje = htmlentities("No se generaron correctamente las cuotas");
                $data['mensaje'] = $mensaje;
                //si falla una cuota elimino todas las anteriores
                $cuenta->borrarCuotasCuenta();
                $this->view->show1("mostrarerror.html", $data);
                return false;
            }
            //calculo la nueva fecha de vencimiento de la proxima cuota
            for ($j = 1; $j <= $cantmeses; $j++) {
                $fechavenc = $this->sumaMes($fechavenc);
            }

            if ($nroCuota == (($cantcuotas + $cantCuotasFormalizacion) - 1)) {
                if ($amortizacion_total == $liquidacion_resto) {
                    echo 'Son iguales';
                }
            }
        }
        //guardo el LOG de creacion de todas las cuotas de la cuenta
        $log->altaLog("Se crearon todas las " . $cuenta->getCantCuotas() . " cuotas de la cuenta Nro: " . $cuenta->getIdCuenta());
        return true;
    }
	

//==================================================================================================	 

    public function recalcularCuotas($cuenta)
    //recalcula nuevas cuotas de una cuenta con un nuevo valor de liquidacion y nueva financiacion
    //pero manteniendo las cuotas con un pago tal como estan
    {
        //creo un nuevo objeto de LOG
        $log = new ModeloLog();
            
        $cuota = new ModeloCuota();
        $cuenta_aux = new ModeloCuenta();

       
        $cuota->putIdUsrCreador($cuenta->getIdUsrCreador());
        $cuota->putIdUsrMod($_SESSION["s_idusr"]);
        $idcuenta = $cuenta->getIdCuenta();
        $cuota->putIdCuenta($idcuenta);

    //variables necesarias para el calculo de las cuotas
        $cantmeses = $cuenta->getCantMeses();
        
        $interes_taza = ($cuenta->getTasaInteres() / 100);
        $subperiodos = 12 / $cantmeses;
        $porcentaje_int_cuota = $interes_taza / $subperiodos; //1,54 
       


        //calculo lo cobrado de la liquidacion anterior
        $estado_anterior = $cuota->calcularCapitalCobrado();
        $cobrado_anterior = $estado_anterior['capitalcobrado']; //lo que ya se cobro
        
        //var_dump($cobrado_anterior);
        //si no hay cobros anteriores
        if($cobrado_anterior <= CERO_DECIMAL)
        {
            //obtengo los datos de la cuenta desde la DB
            $cuenta_aux->putIdCuenta($cuenta->getIdCuenta());
            $cuenta_aux->traerCuenta();

            //Obtengo el listado total de cuotas para esa cuenta
            $listadoTotalCuotas = $cuota->listadoTotal();
            
            //traigo la primer cuota para obtener el primer vencimiento
            $cuota->putNroCuota($listadoTotalCuotas[0]['nrocuota']);
            $c_ok = $cuota->traerCuotaCuenta();
            if (!$c_ok)
            {
                $mensaje = htmlentities("No se generaron correctamente las cuotas. Intentelo de nuevo.");
                $data['mensaje'] = $mensaje;
                $this->view->show1("mostrarerror.html", $data);
                return false;
            }
            
            $cuenta->putFechaVencimientoPlan($cuenta_aux->getFechaVencimientoPlan());
            $cuenta->putFormalizacion($cuenta_aux->getFormalizacion());
            //borro todas las cuotas y genero nuevas
            $cuenta->borrarCuotasCuenta();
            return ($this->generarCuotasSistemaFrances($cuenta));
        }

        $fecha_vencimiento= $cuenta->getFechaVencimientoPlan();
        $fecha_vencimiento_form=$cuenta->getFechaVencimientoFormalizacion();


        //SI HAY COBROS ANTERIORES

        //FORMALIZACION
        $cantCuotasFormalizacion = $cuenta->getCantCuotasFormalizacion() *1; //cantidad de cuotas de formalizacion

        if($cuota->tienePagoFormalizacion()){ //si se realizó algun pago
            //$cantcuotas_pagas = $estado_anterior['cantcuotas']-1; //si tiene paga la formalizacion, se la resto a la cant cuotas pagas
            $cantcuotas_pagas = $estado_anterior['cantcuotas']-$cantCuotasFormalizacion; //si tiene paga la formalizacion, se la resto a la cant cuotas pagas
        }else{
            $cantcuotas_pagas = $estado_anterior['cantcuotas'];
        }

        
        ($cuota->tienePagoFormalizacion())?$aux="True":$aux="False";
        echo "<br>TieneForm?: ".$aux.", cantCuotasPagas: ".$cantcuotas_pagas;
                
        echo "<br>Hay cobros anteriores, asiq hay que recorrer y recalcular formalizacion y plan";
        //si hay algun cobro ya realizado de alguna cuota
        //controlo que la cantidad de cuotas no sea menor a las ya pagas ni menor a a la ulitma cobrada
        $cantcuotas = $cuenta->getCantCuotas();
        $cantCuotasTotalCuenta = $cantcuotas + $cantCuotasFormalizacion;
       
        $arr_ultima_paga = $cuota->ultimaCuotaPaga();
        if($cantcuotas < $arr_ultima_paga['nrocuota'])
        {
            $mensaje = htmlentities("No es posible recalcular la cuenta por una cantidad de cuotas menor a la ultima cuota con un pago");
            $data['mensaje'] = $mensaje;
            $this->view->show1("mostrarerror.html", $data);
            return false;
        }


        //NUEVO VALOR DE LIQUIDACION    

        //resto la deuda anterior a la nueva liquidacion, redondeo a 1 decimal
        $nuevo_valor_liquidacion = redondeoCincoCent($cuenta->getValorLiquidacion() /* - $cobrado_anterior */);
        //si el nuevo valor es igual a lo ya pagado (margen 0.05 pesos) en las cuotas, es decir, cuenta cancelada
        //pongo todas las cuotas adeudadas con fecha de pago hoy y cobrado cero.
        $fecha_pago = "";

        
      
        if($nuevo_valor_liquidacion <= CERO_DECIMAL){
            $fecha_pago = date('d/m/Y');
        }else{
            //si hay monto para calcular, debo asegurarme que la cantidad de cuotas sea mayor a la cantidad de cuotas pagadas
            //if($cantcuotas <= $cantcuotas_pagas)
            if($cantCuotasTotalCuenta <= $cantcuotas_pagas)
            {
                $mensaje = htmlentities("No es posible recalcular la cuenta por una cantidad de cuotas menor o igual a la cantidad de cuotas pagadas");
                $data['mensaje'] = $mensaje;
                $this->view->show1("mostrarerror.html", $data);
                return false;
            }
        }
        //PRIMERO modifico la formalizacion de venta si no tiene pagos
        $op_ok = true;	//inicializo variable
        
        //Obtengo el listado total de cuotas para esa cuenta
        $listadoTotalCuotas = $cuota->listadoTotal();        
        //traigo la primer cuota para obtener el primer vencimiento
        //$cuota->putNroCuota(0);
        $cuota->putNroCuota($listadoTotalCuotas[0]['nrocuota']);
        
              
        //guardo la fecha de vencimiento por si la necesita mas adelante para nuevas cuotas
        $fechavenc = $cuota->getFechaVenc();        
        
        //$monto_form = redondeoCincoCent($cuenta->getValorLiquidacion()*10/100);
        $monto_form = redondeoCincoCent( $nuevo_valor_liquidacion*($cuenta->getFormalizacion()/100));

        //var_dump($monto_form);
        //genero las cuotas de formalizacion del credito
        
        //obtengo el tasa nominal mensual para las cuotas del plan (sin periodos diferidos)
        //$inttnm = $cuenta->getTasaInteres() / (12/$cantmeses);
        
        if($cuenta->getSumarIntereses()==1){            
            $totalCuotasConDiferidas = (($cuenta->getPeriodoDiferidoPlan()-1+$cuenta->getCantCuotas()) + ($cuenta->getPeriodoDiferidoFormalizacion()-1+$cuenta->getCantCuotasFormalizacion()));
            $inttntDiferida = $porcentaje_int_cuota * $totalCuotasConDiferidas;                        
        }else{
            $inttntDiferida = $porcentaje_int_cuota * ($cuenta->getCantCuotas()+$cuenta->getCantCuotasFormalizacion());
        }
        //obtengo la tasa nominal mensual diferida según la cantidad de cuotas de formalizacion
        $inttnmDiferida = $inttntDiferida / $cuenta->getCantCuotasFormalizacion(); //da false

        //var_dump( $cuenta->getCantCuotasFormalizacion());
        
        ($cuenta->getCantCuotasFormalizacion()>0)?$i=1:$i=0;

        if( $listadoTotalCuotas[0]['nrocuota']==0){
            $cantCuotasFormalizacion=1;
        }
        //genero las cuotas de formalizacion nuevamente
        //$montoCuotaFormalizacion = $monto_form / $cantCuotasFormalizacion;
        /* if($i==0){
            $cantCuotas_form=1;
        }else{
            $cantCuotas_form= $cuenta->getCantCuotasFormalizacion();
        } */
        
        
        $a = ($porcentaje_int_cuota * pow((1 + $porcentaje_int_cuota), ($cantCuotasFormalizacion)));
        $b = pow((1 + $porcentaje_int_cuota), ($cantCuotasFormalizacion)) - 1;


        $cuota_R_Form = $monto_form * ($a)  /  ($b);


        
        $cuota_R_Form = redondeoCincoCent( $cuota_R_Form);


        //CALCULO CUANTAS CUOTAS DE FORMALIZACIÓN ESTAN PAGADAS
       // $cantcouta_for_pagadas = 0;
       /*  for ($nroCuota = $i; $nroCuota <= $cantCuotasFormalizacion; $nroCuota++) {
             if(!$cuota->getFechaPago() == "0000-00-00" && !($cuota->getCobrado() <= CERO_DECIMAL))
            {
                $cantcouta_for_pagadas = $cantcouta_for_pagadas + 1;
            }
        }

        $monto_form_descontar = $cuota_R_Form * $cantcouta_for_pagadas;
        $monto_form = $monto_form-$monto_form_descontar; */
          
       // $porcentaje_int_cuota = $interes_taza / $subperiodos; //1,54 
        $amortizacion_total=0;

        $periodo_diferido= $cuenta->getPeriodoDiferidoFormalizacion();

        if($periodo_diferido>=1){
            for ($periodo = 1; $periodo <= $periodo_diferido; $periodo++) {
                for ($j = 1; $j <= 12; $j++) { //mensual max tres, pero todavia esa restriccion no esta
                    $fecha_vencimiento_form = $this->sumaMes($fecha_vencimiento_form);
                }
            }
        }

        
     
        for ($nroCuota=$i;$nroCuota<$cantCuotasFormalizacion;$nroCuota++){
            $cuota = new ModeloCuota();
            $cuota->putNroCuota($i);
            $cuota->putIdCuenta( $idcuenta);
            $c_ok = $cuota->traerCuotaCuenta();

            if ($c_ok) {
                

                $montoInteres = redondeoCincoCent($porcentaje_int_cuota * $monto_form); //el interes por cuota que se hace taza_interes por el saldo inical(parte de la cuota)


                $amortizacion_cuota = $cuota_R_Form - $montoInteres; //capital que va pagando (parte de la cuota). 

                $saldo_final = $monto_form - $amortizacion_cuota; //lo que va quedando de capital para pagar


                $monto_form = $saldo_final;

                $amortizacion_total = $amortizacion_total + $amortizacion_cuota;

                //si NO tiene cobro la recalculo, caso contrario, no se toca
                if ($cuota->getFechaPago() == "0000-00-00" && ($cuota->getCobrado() <= CERO_DECIMAL)) {



                   


                    //calculo el interes por cuota de formalizacion
                    //$montoInteres = (($inttnmDiferida * ($cantCuotasFormalizacion - $nroCuota + 1) ) * $montoCuotaFormalizacion)/100;
                    $cuota->putFechaVenc( $fecha_vencimiento_form);
                    $cuota->putCapital($amortizacion_cuota);
                    $cuota->putMontoCuota($cuota_R_Form);
                    //$cuota->putInteres(0);
                    $cuota->putInteres($montoInteres);
                    $cuota->putSaldo($amortizacion_cuota + $montoInteres);
                    //$cuota->putCobrado($cobrado);
                    $cuota->putCobrado(0);
                    $cuota->putFechaPago(cadenaAFecha($fecha_pago));    //si el monto es cero la fecha de pago es 0000-00-00
                    //$cuota->putInteresMora(0);
                    $cuota->putInteresMora(0);
                    $cuota->putFechaCalculoMora(cadenaAFecha(""));
                    $cuota->putEsCuotaFormalizacion(1); //defino que esta cuenta es de formalizacion y no del plan
                    $nueva_obs = $cuota->getObservacion() . "\n" . date("d/m/Y") . " Modificaci�n por reliquidaci�n de cuenta. Usuario [" . $_SESSION['s_username'] . "].";
                    $cuota->putObservacion($nueva_obs);
                    $op_ok = $cuota->modificarCuota();
                    //actualizo el valor restando la formalizacion ya cobrada
                    //$monto_form -= $cuota_R_Form;

                    for ($j = 1; $j <= 12; $j++) { //mensual max tres, pero todavia esa restriccion no esta
                        $fecha_vencimiento_form = $this->sumaMes($fecha_vencimiento_form);
                    }
                } elseif ($cuota->getFechaPago() != "0000-00-00") {


                    if (($cuota->getSaldo() > CERO_DECIMAL)) {

                        /* $cuota->putCapital($amortizacion_cuota);
                        $cuota->putMontoCuota($cuota_R_Form);
                        //$cuota->putInteres(0);
                        $cuota->putInteres($montoInteres);
                        $cuota->putSaldo($amortizacion_cuota + $montoInteres - $cuota->getCobrado()); */
                    } else {
                        //var_dump( $amortizacion_cuota);
                       /*  $cuota->putCapital($amortizacion_cuota);
                        $cuota->putMontoCuota($cuota_R_Form);
                        $cuota->putCobrado($cuota_R_Form);
                        //$cuota->putInteres(0);
                        $cuota->putInteres($montoInteres); */
                        //$cuota->putSaldo($amortizacion_cuota + $montoInteres - $cuota->getCobrado());
                    }




                    //calculo el interes por cuota de formalizacion
                    //$montoInteres = (($inttnmDiferida * ($cantCuotasFormalizacion - $nroCuota + 1) ) * $montoCuotaFormalizacion)/100;


                    //$cuota->putCobrado($cobrado);
                    //$cuota->putCobrado(0);
                    //$cuota->putFechaPago(cadenaAFecha($fecha_pago));    //si el monto es cero la fecha de pago es 0000-00-00
                    //$cuota->putInteresMora(0);
                    //$cuota->putInteresMora(0);
                    //$cuota->putFechaCalculoMora(cadenaAFecha(""));
                    //$cuota->putEsCuotaFormalizacion(1); //defino que esta cuenta es de formalizacion y no del plan
                   /*  $nueva_obs = $cuota->getObservacion() . "\n" . date("d/m/Y") . " Modificaci�n por reliquidaci�n de cuenta. cuota con saldo. Usuario [" . $_SESSION['s_username'] . "].";
                    $cuota->putObservacion($nueva_obs);
                    $op_ok = $cuota->modificarCuota(); */
                }

                if (!$op_ok) {
                    $mensaje = htmlentities("No se generaron correctamente las cuotas de formalizacion. Verifique los resultados e intentelo de nuevo.");
                    $data['mensaje'] = $mensaje;
                    $this->view->show1("mostrarerror.html", $data);
                    return false;
                }
            } /* else {  //si la cuota no existe tengo que generar nuevas

                //$montoInteres = (($inttnmDiferidaPlan * (($cantcuotas+$cantCuotasFormalizacion) - $nroCuota + 1) ) * $monto_cuota)/100;
                $montoInteres = redondeoCincoCent($porcentaje_int_cuota * $liquidacion_resto); //el interes por cuota que se hace taza_interes por el saldo inical(parte de la cuota)


                $amortizacion_cuota = $cuota_R - $montoInteres; //capital que va pagando (parte de la cuota). 

                $saldo_final = $liquidacion_resto - $amortizacion_cuota; //lo que va quedando de capital para pagar


                $liquidacion_resto = $saldo_final;

                $amortizacion_total = $amortizacion_total + $amortizacion_cuota;


                //calculo el proximo vencimiento
                for ($j = 1; $j <= $cantmeses; $j++) {
                    $fechavenc = $this->sumaMes($fechavenc);
                }
                $cuota->putIdUsrCreador($cuenta->getIdUsrCreador());
                $cuota->putIdUsrMod($_SESSION["s_idusr"]);
                $cuota->putFechaVenc($fechavenc);
                $cuota->putCapital($amortizacion_cuota);
                $cuota->putMontoCuota($cuota_R);
                //$cuota->putInteres($interes_cuota);
                $cuota->putInteres($montoInteres);
                $cuota->putSaldo($amortizacion_cuota + $montoInteres);
                $cuota->putCobrado($cobrado);
                $cuota->putEsCuotaFormalizacion(0); //defino que esta cuenta es de formalizacion y no del plan
                $nueva_obs = $cuota->getObservacion() . "\n" . date("d/m/Y") . " Modificacion por reliquidacion de cuenta. Usuario [" . $_SESSION['s_username'] . "].";
                $cuota->putObservacion($nueva_obs);
                $op_ok = $cuota->altacuota();
                //si agregue cuota actualizo el interes para la proxima cuota
                //$liquidacion_resto -= $capital_cuota;
                //$interes_cuota = redondeoCincoCent($liquidacion_resto * ($porcentaje_int_cuota / 100));	//redondeo a un decimal
                //$monto_cuota = $capital_cuota + $interes_cuota;
            }
            if (!$op_ok) {
                $mensaje = htmlentities("No se generaron correctamente las cuotas 3. Verifique los resultados e int�ntelo de nuevo.");
                $data['mensaje'] = $mensaje;
                $this->view->show1("mostrarerror.html", $data);
                return false;
            } */
            
          
        }

        //MONTO QUE SE DESCUENTA DE LA FORMALIZACION
        
        //FIN modificacion de formalizacion de venta

        //interes anual (sin periodo diferido) dividido por la cantidad de cuotas por año

     

        
      
        $porct_recupero = $cuenta->getRecuperoMensura();

        $monto_formalizacion= redondeoCincoCent($cuenta->getValorLiquidacion() * ($cuenta->getFormalizacion() / 100));
        $monto_recupero= redondeoCincoCent($cuenta->getValorLiquidacion() * ($porct_recupero / 100));
        $liquidacion_resto = ( $cuenta->getValorLiquidacion()- $monto_formalizacion  + $monto_recupero );
        //las cuotas del nuevo valor se calculan descontando las cuotas pagas
        $liquidacion_resto = redondeoCincoCent( $liquidacion_resto);

        //$cantcuotas_pagas = sizeof($cuota->listadoCuotasCuentaPagas());
        $cantmeses = $cuenta->getCantMeses();

        $interes_taza = ($cuenta->getTasaInteres() / 100);
        $subperiodos = 12 / $cantmeses;
        $porcentaje_int_cuota = $interes_taza / $subperiodos; //1,54 
        $cantcuotasR= $cantcuotas;

        
        //$liquidacion_resto = redondeoCincoCent($liquidacion_resto / ($cantcuotas - $cantcuotas_pagas));        //redondeo a un decimal
        
        $a = ($porcentaje_int_cuota * pow((1 + $porcentaje_int_cuota), ( $cantcuotasR)));
        $b = pow((1 + $porcentaje_int_cuota), ( $cantcuotasR)) - 1;


        $cuota_R = $liquidacion_resto * ($a)   / ($b);
        $cuota_R = redondeoCincoCent($cuota_R);
        

        /* var_dump( $cuota_R);
        return; */
       
        $cobrado = 0;
        $interes_mora = 0;

        //saco el nuevo liquidacion
        $canti=$nroCuota;
        $cantidad_cuotas_pagar=0;
        $cuotas_pagadas=0;

        /* for ($i = $canti; $i <= ($cantcuotas); $i++) {
            $op_ok = true;    //inicializo variable
            $cuota = new ModeloCuota();
            $cuota->putNroCuota($i);
            $cuota->putIdCuenta($idcuenta);
            $c_ok = $cuota->traerCuotaCuenta();
            //si la cuota ya existe y no tiene cobro la modifico, si ya tiene cobro la salteo
            if ($c_ok) {
                if ($cuota->getFechaPago() != "0000-00-00" && ($cuota->getSaldo() <= CERO_DECIMAL)) {
                    /*  $montoInteres_p = redondeoCincoCent($porcentaje_int_cuota * $liquidacion_resto); //el interes por cuota que se hace taza_interes por el saldo inical(parte de la cuota)
 

                    $amortizacion_cuota_p = $cuota_R - $montoInteres_p; //capital que va pagando (parte de la cuota). 

                    $saldo_final_p = $liquidacion_resto - $amortizacion_cuota_p; //lo que va quedando de capital para pagar


                    $liquidacion_resto = $saldo_final_p; 

                    $cuotas_pagadas = $cuotas_pagadas+1;
                }else{
                    $cantidad_cuotas_pagar = $cantidad_cuotas_pagar + 1;


                }
            }

        } */

        // $cuota_pareja = ($liquidacion_resto / $cantcuotas);
        /* var_dump($cuota_pareja);
        return; */
        // $liquidacion_resto= $liquidacion_resto - $cuota_pareja* $cuotas_pagadas ;

        /*   $a = ($porcentaje_int_cuota * pow((1 + $porcentaje_int_cuota), ($cantidad_cuotas_pagar)));
        $b = pow((1 + $porcentaje_int_cuota), ($cantidad_cuotas_pagar)) - 1;


        $cuota_R = $liquidacion_resto * ($a)   / ($b);
        $cuota_R = redondeoCincoCent($cuota_R); */

        //var_dump( $liquidacion_resto);
        $periodo_diferido = $cuenta->getPeriodoDiferidoPlan();

        if ($periodo_diferido >= 1) {
            for ($periodo = 1; $periodo <= $periodo_diferido; $periodo++) {
                for ($j = 1; $j <= 12; $j++) { //mensual max tres, pero todavia esa restriccion no esta
                    $fecha_vencimiento = $this->sumaMes($fecha_vencimiento);
                }
            }
        }

   
        for($i=$nroCuota; $i<=($cantcuotas); $i++)
        {
            $op_ok = true;	//inicializo variable
            $cuota = new ModeloCuota();
            $cuota->putNroCuota($i);
            $cuota->putIdCuenta($idcuenta);
            $c_ok = $cuota->traerCuotaCuenta();
            //si la cuota ya existe y no tiene cobro la modifico, si ya tiene cobro la salteo
            if($c_ok)
            {
                $montoInteres = redondeoCincoCent($porcentaje_int_cuota * $liquidacion_resto); //el interes por cuota que se hace taza_interes por el saldo inical(parte de la cuota)


                $amortizacion_cuota = $cuota_R - $montoInteres; //capital que va pagando (parte de la cuota). 

                $saldo_final = $liquidacion_resto - $amortizacion_cuota; //lo que va quedando de capital para pagar


                $liquidacion_resto = $saldo_final;

                $amortizacion_total = $amortizacion_total + $amortizacion_cuota;

                if($cuota->getFechaPago() == "0000-00-00" && ($cuota->getCobrado() <= CERO_DECIMAL))
                {
                    //guardo la fecha de vencimiento por si la necesita mas adelante para nuevas cuotas
                    //calculo el proximo vencimiento
                   

                    
                    //si NO tiene cobro la recalculo

                    //$montoInteres = (($inttnmDiferidaPlan * (($cantcuotas+$cantCuotasFormalizacion) - $nroCuota + 1) ) * $monto_cuota)/100;



                    $cuota->putFechaVenc( $fecha_vencimiento);
                    $cuota->putCapital( $amortizacion_cuota);
                    $cuota->putMontoCuota( $cuota_R);
                    $cuota->putInteres($montoInteres);
                    $cuota->putSaldo( $amortizacion_cuota + $montoInteres);
                    $cuota->putCobrado($cobrado);
                    $cuota->putFechaPago(cadenaAFecha($fecha_pago));	//si el monto es cero la fecha de pago es 0000-00-00
                    $cuota->putInteresMora($interes_mora);
                    $cuota->putFechaCalculoMora(cadenaAFecha(""));
                    $cuota->putEsCuotaFormalizacion(0); //defino que esta cuenta es de formalizacion y no del plan
                    $nueva_obs = $cuota->getObservacion()."\n".date("d/m/Y")." Modificacion por reliquidacion de cuenta. Usuario [".$_SESSION['s_username']."].";
                    $cuota->putObservacion($nueva_obs);
                    $op_ok = $cuota->modificarCuota();
                    //si modifique cuota actualizo el interes para la proxima cuota
                    // $liquidacion_resto -= $couta_R;
                    //$interes_cuota = redondeoCincoCent($liquidacion_resto * ($porcentaje_int_cuota / 100));	//redondeo a un decimal
                    //$monto_cuota = $capital_cuota + $interes_cuota;
                    for ($j = 1; $j <= $cantmeses; $j++) {
                        $fecha_vencimiento = $this->sumaMes($fecha_vencimiento);
                    }
                } else{

                    
                    if($cuota->getSaldo()<=0){
                        
                        /* $cuota->putCapital($amortizacion_cuota);
                        $cuota->putMontoCuota($cuota_R);
                        $cuota->putInteres($montoInteres);
                        $cuota->putSaldo($amortizacion_cuota + $montoInteres - $cuota->getCobrado());
                        $op_ok = $cuota->modificarCuota(); */
                    }
                    



                   
                    //$cuota->putCobrado($cobrado);
                   // $cuota->putFechaPago(cadenaAFecha($fecha_pago));    //si el monto es cero la fecha de pago es 0000-00-00
                   // $cuota->putInteresMora($interes_mora);
                   // $cuota->putFechaCalculoMora(cadenaAFecha(""));
                   // $cuota->putEsCuotaFormalizacion(0); //defino que esta cuenta es de formalizacion y no del plan
                    /* $nueva_obs = $cuota->getObservacion() . "\n" . date("d/m/Y") . " Modificacion por reliquidacion de cuenta. tenia cobro Usuario [" . $_SESSION['s_username'] . "].";
                    $cuota->putObservacion($nueva_obs);
                    $op_ok = $cuota->modificarCuota(); */

                }
            }else{  //si la cuota no existe tengo que generar nuevas
                
                //$montoInteres = (($inttnmDiferidaPlan * (($cantcuotas+$cantCuotasFormalizacion) - $nroCuota + 1) ) * $monto_cuota)/100;
                $montoInteres = redondeoCincoCent($porcentaje_int_cuota * $liquidacion_resto); //el interes por cuota que se hace taza_interes por el saldo inical(parte de la cuota)


                $amortizacion_cuota = $cuota_R - $montoInteres; //capital que va pagando (parte de la cuota). 

                $saldo_final = $liquidacion_resto - $amortizacion_cuota; //lo que va quedando de capital para pagar


                $liquidacion_resto = $saldo_final;

                $amortizacion_total = $amortizacion_total + $amortizacion_cuota;


                //calculo el proximo vencimiento
                for ($j=1; $j<=$cantmeses; $j++)
                {
                    $fechavenc = $this->sumaMes($fecha_vencimiento);
                }
                $cuota->putIdUsrCreador($cuenta->getIdUsrCreador());
                $cuota->putIdUsrMod($_SESSION["s_idusr"]);
                $cuota->putFechaVenc($fechavenc);
                $cuota->putCapital( $amortizacion_cuota);
                $cuota->putMontoCuota( $cuota_R);
                //$cuota->putInteres($interes_cuota);
                $cuota->putInteres($montoInteres);
                $cuota->putSaldo($amortizacion_cuota + $montoInteres);
                $cuota->putCobrado($cobrado);
                $cuota->putEsCuotaFormalizacion(0); //defino que esta cuenta es de formalizacion y no del plan
                $nueva_obs = $cuota->getObservacion()."\n".date("d/m/Y")." Modificacion por reliquidacion de cuenta. Usuario [".$_SESSION['s_username']."].";
                $cuota->putObservacion($nueva_obs);
                $op_ok = $cuota->altacuota();
                //si agregue cuota actualizo el interes para la proxima cuota
                //$liquidacion_resto -= $capital_cuota;
                //$interes_cuota = redondeoCincoCent($liquidacion_resto * ($porcentaje_int_cuota / 100));	//redondeo a un decimal
                //$monto_cuota = $capital_cuota + $interes_cuota;
            }
            if (!$op_ok)
            {
                $mensaje = htmlentities("No se generaron correctamente las cuotas 3. Verifique los resultados e int�ntelo de nuevo.");
                $data['mensaje'] = $mensaje;
                $this->view->show1("mostrarerror.html", $data);
                return false;
            }

           
        }
        //return;
        //borro las cuotas restante si las hay
        $hay_cuotas = true;
        $nro = ($cantcuotas+$cuenta->getCantCuotasFormalizacion())+1;
        $cuota->putNroCuota($nro);
        $hay_cuotas = $cuota->traerCuotaCuenta();
        while($hay_cuotas)
        {
            $op_ok = $cuota->borrarCuota();
            if(!$op_ok)
            {
                $mensaje = htmlentities("No se borraron correctamente las cuotas antiguas. Verifique los resultados e int�ntelo de nuevo.");
                $data['mensaje'] = $mensaje;
                $this->view->show1("mostrarerror.html", $data);
                return false;
            }
            $nro++;
            $cuota->putNroCuota($nro);
            $hay_cuotas = $cuota->traerCuotaCuenta();
        }
        
        //guardo el LOG de re-liquidacion de cuenta
        $log->altaLog("Se modifica la cuenta Nro: ".$cuenta->getIdCuenta());
        
        return true;
    }

//==================================================================================================

	public function elegirCuenta()
	{
		$cuentas = new ModeloCuenta();
		$cuentas->putAnioExpediente(0);
		$cuentas->putNroExpediente(0);
                $cuentas->putNroCuenta(0);

		if (isset($_GET['anioexpediente']))
			$cuentas->putAnioExpediente($_GET['anioexpediente']);

		if( isset($_GET['nroexpediente']))		   
			$cuentas->putNroExpediente($_GET['nroexpediente']);

		if (isset($_GET['nrocuenta']))
			$cuentas->putNroCuenta($_GET['nrocuenta']);

		$liztado = $cuentas->traerCuentaExpediente();
		$data['liztado'] = $liztado;
		$this->view->show1("elegircuenta.html", $data);		
 	}

//=================================================================================================	 
	
	public function verificarNroCuenta()
	// verifica que el numero de cuenta ingresado no se repita en otra cuenta
	{
		$cta = new ModeloCuenta();
		$cta->putNroCuenta("");

		if (isset($_GET['nrocuenta']) && $_GET['nrocuenta'] > 0)
			$cta->putNroCuenta($_GET['nrocuenta']);
			
		$existe = $cta->existeNroCuenta();
	    
	    if ($existe)
	    	echo "CUIDADO! Este n&uacute;mero de cuenta ya existe para otra cuenta.";
	    
	    return;
	}

//==============================================================================================

	public function sumaMes($fecha)
	//suma un mes a la fecha
	{
		// suma $dia dias a una fecha y retorna la fecha resultante
		list($year,$mon,$day) = explode('-',$fecha);
		$nuevafecha = date('Y-m-d',mktime(0,0,0,$mon+1,$day,(int)$year));
		return($nuevafecha);
	}

//======================================================================================================================

    public function sumarCapitalArray($arr_cuotas)
    //suma la deuda total de un arreglo de cuotas
    {
        foreach ($arr_cuotas as $c)
        {
            $capital = ($c['montocuota'] - $c['interes']);
        }
    }
	
//=======================================================================================================================

	public function cancelarUltimaCuota($cuenta)
	// si la ultima cuenta tiene saldo le modifica el monto y elimina el saldo y el interes por mora para que cierre la deuda en cero
	{
		$cuota = new ModeloCuota();
		$cuota->putIdCuenta($cuenta->getIdCuenta());
		$cuota->putNroCuota($cuenta->getCantCuotas());
		$cuota->traerCuotaCuenta();
		$cuota->putInteresMora(0.00);
		$cuota->putMontoCuota($cuota->getCobrado());
		$cuota->putSaldo(0.00);
		$c_ok = $cuota->editarCuota();
		return $c_ok;
	}

//=======================================================================================================================

	public function modificarObservacion()
	{
		if(isset($_GET['idcuenta']) && $_GET['idcuenta'] > 0)
		{
			$cuenta = new ModeloCuenta();
			$cuenta->putIdCuenta($_GET['idcuenta']);
			$cuenta->traerCuenta();
			$obs = $cuenta->getObservacion()."\n".$_GET['observacion'];
			$cuenta->putObservacion($obs);
			$ok = $cuenta->guardarobservacion();
			if(!$ok)
			{
				$mensaje = htmlentities("No se pudo modificar la observaci�n. Int�ntelo nuevamente.");
				$data['mensaje'] = $mensaje;
				$this->view->show1("mostrarerror.html", $data);
				return false;
			}
			$vista = new view();
			$data['controlador'] = "cuenta";
		    $data['accion'] = "vercuenta&&idcta=".$_GET['idcuenta']."&&operacion=2";
			$vista->show1("bridgecustom.html",$data);
		}else{
			$mensaje = htmlentities("Error interno, no se recibi� el ID de la cuenta. Int�ntelo nuevamente.");
			$data['mensaje'] = $mensaje;
			$this->view->show1("mostrarerror.html", $data);
			return false;
		}
	}
}

?>
