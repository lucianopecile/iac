<?php
require_once 'modelos/modelopoblador.php';
require_once 'modelos/modelotipodoc.php';
require_once 'modelos/modelolocalidad.php';
require_once 'modelos/modeloestadocivil.php';


class ControlConyuge
{
  	function __construct()
	{
		$this->view = new View();
	}
 
//============================================================================
	 
	public function mostrarconyuge()
	// envia a la vista un listado de todos los conyuges (pobladores) y su localidad
	{
		$conyuges = new modelopoblador();
	 
		$liztado = $conyuges->listadototal();
	      
		$data['liztado'] = $liztado;
		
		$this->view->show1("conyuge.html", $data);
 	}
 	
//============================================================================

	public function listaconyuge()
	//lista los conyuges (pobladores) con un mismo apellido, sino recibe apellido lista todos los conyuges
	{
		$conyuges = new modelopoblador();

		if (isset($_POST['nombrepob']))
	    {
			$conyuges->putApellido($_POST['nombrepob']);
			$liztado = $conyuges->listadonombre();
		} else {
			$liztado = $conyuges->listadototal();
		}   

		$data['liztado'] = $liztado;
		
		$this->view->show1("conyuge.html", $data);
	}	
	
//============================================================================
	
	public function elegirconyuge()
	// envia a la vista los conyuges elegidos segun el apellido.
	{
		$conyuges = new modelopoblador();
		
		if (isset($_GET['apellido']))
			$conyuges->putApellido($_GET['apellido']);
		else
			$conyuges->putApellido("");
		
	    $liztado = $conyuges->listadonombreconyuges();

		$data['liztado'] = $liztado;

		$this->view->show1("elegirconyuge.html", $data);
	}
	
//============================================================================
	
	public function vertabconyuge()
	//retorna los datos de una conyuge si se carg� el id, sino retorna campos en blanco
	{	
		$conyuges = new modelopoblador();
		$conyuges->putIdPoblador($_GET['idpob']); /* si llega hasta aca si o si hay un id de poblador */

		$empent = $conyuges->traerconyugepoblador();
		
		$conyuges->putIdConyuge($_GET['idpob']);
			
			if (!$empent)
			{
			    $mensaje = htmlentities("En este momento no se puede realizar la operaci�n, intent�lo m�s tarde");
			    $data['mensaje'] = $mensaje;
		    	$this->view->show1("mostrarerror.html", $data);
				return;
		    }
		

	    $tipodoc = new modelotipodoc;
		$localidad = new modelolocalidad;
		$estadocivil = new modeloestadocivil;
		$data = $this->cargarPlantillaModificar($conyuges, $tipodoc, $localidad, $estadocivil);
		$this->view->show("tabpoblador.html", $data);
	}

//============================================================================
	
	public function altaconyuge()
	// carga el nuevo conyuge en el modelo
	{
		$alta = new modelopoblador();
		$this->cargavariables($alta, ALTA);

		if ($alta->existedocumento())
		{
			$mensaje = htmlentities("El n�mero de documento ".$alta->getDocumento()." ya existe para otro poblador en la base de datos.");
			$data['mensaje'] = $mensaje;
			$this->view->show1("mostrarerror.html", $data);
			return;
		}

		$altaok = $alta->altaconyugepoblador();
				
		if (!$altaok)
		{	
			$mensaje = htmlentities("En este momento no se puede realizar la operaci�n, int�ntelo m�s tarde");
			$data['mensaje'] = $mensaje;
			$this->view->show1("mostrarerror.html", $data);
			return;
        }
        $vista = new view();
        $data['controlador'] = "conyuge";
        $data['accion'] = "vertabconyuge&&idpob=".$_POST['idconyuge'];
		$vista->show1("bridgecustom.html",$data);		
	}
	
//============================================================================	

	public function modificarconyuge()
	{
		$modifica = new modelopoblador();
		$this->cargavariables($modifica, MODIFICAR);
		$modificado = $modifica->modificarpoblador();
	    	    
		if (!$modificado)
		{
		    $mensaje = htmlentities("En este momento no se puede realizar la operaci�n, int�ntelo m�s tarde");
		    $data['mensaje'] = $mensaje;
    		$this->view->show1("mostrarerror.html", $data);
			return;
		}
//	    $this->mostrarconyuge();
        $vista = new view();
        $data['controlador'] = "conyuge";
        $data['accion'] = "vertabconyuge&&idpob=".$_POST['idconyuge'];
		$vista->show1("bridgecustom.html",$data);		
	}
	
//============================================================================
	
	public function borrarconyuge()
	{
		$borra = new modelopoblador();
		$borra->putIdPoblador($_POST['id']);
		$borrado = $borra->borrarconyugepoblador();
		if (!$borrado)
		{
			$mensaje = htmlentities("En este momento no se puede realizar la operaci�n, int�ntelo m�s tarde");
			$data['mensaje'] = $mensaje;
			$this->view->show1("mostrarerror.html", $data);
			return;
		}
		$this->mostrarconyuge(); 
	}

//============================================================================
	
	public function desvincularconyuge()
	{
		$pob = new modelopoblador();
		$this->cargavariables($pob, MODIFICAR);
		$ok = $pob->desvincularconyugepoblador();
		if (!$ok)
		{
			$mensaje = htmlentities("En este momento no se puede realizar la operaci�n, int�ntelo m�s tarde");
			$data['mensaje'] = $mensaje;
			$this->view->show1("mostrarerror.html", $data);
			return;
		}
		$this->mostrarconyuge(); 
	}
	
//============================================================================

	public function cargarPlantillaModificar($parconyuge, $parTipodoc, $parLocalidad, $parEstadoCivil) 
	{  
		$vd = $parTipodoc->TraerTodos();
		$vd['selected'] = $parconyuge->getIdTipodoc();
		$vl = $parLocalidad->TraerTodos();
		$vl['selected'] = $parconyuge->getIdLocalidad();
		$ve = $parEstadoCivil->TraerTodos();
		$ve['selected'] = $parconyuge->getIdEstadoCivil();
		
		$idpoblador = $parconyuge->getIdPoblador(); /* el conyuge es el poblador en el objeto en este momento */
		
		$quehacer = "";
		if ($idpoblador == 0)
			$quehacer = ALTA;
		else
			if (isset($_GET['operacion']))
			{
				if ($_GET['operacion'] == 2) $quehacer = MODIFICAR;
				if ($_GET['operacion'] == 3) $quehacer = BAJA;
			}

		switch($quehacer)
		{
			case ALTA:	      
	        $nombreboton="Guardar";
		    $nombreaccion="altaconyuge";
			break;	 
			
			case MODIFICAR:
			$nombreboton="Guardar";
			$nombreaccion="modificarconyuge";
			break;
			
			case BAJA:
			$nombreboton="Eliminar";
			$nombreaccion="borrarconyuge";
			break;
			
			// el default es igual al modificar
			default:
			$nombreboton="Guardar";
			$nombreaccion="modificarconyuge";
					   }
	 
		switch ($quehacer)
		{
			case MODIFICAR:
				$parametros = array(
                    "TITULO" => "Datos",
					"IDPOBLADOR" => $parconyuge->getIdConyuge(), /* variable id del poblador que alcanza a las tres pesta�as */
                    "ID" => $parconyuge->getIdPoblador(), /* ID del poblador conyuge */
					"IDCONYUGE" => $parconyuge->getIdConyuge(),
					"IDTIPODOC" => $parconyuge->getIdTipodoc(), 
					"LISTATIPODOC" => $vd,
					"DOCUMENTO"=> $parconyuge->getDocumento(),
					"NOMBRE" => $parconyuge->getNombres(),
					"APELLIDO" => $parconyuge->getApellido(),
					"DOMICILIOLEGAL" => $parconyuge->getDomicilioLegal(),
					"DOMICILIOPOSTAL" => $parconyuge->getDomicilioPostal(),
					"NACIONALIDAD" => $parconyuge->getNacionalidad(),
					"LUGARNACIMIENTO" => $parconyuge->getLugarNac(),
					"FECHANAC" => fechaACadena($parconyuge->getFechaNac()),				
					"TELEFONO" => $parconyuge->getTelefono(),
					"CARACTERISTICA" => $parconyuge->getCaracTel(),
					"IDLOCALIDAD" => $parconyuge->getIdLocalidad(),
					"LISTALOCALIDAD" => $vl,
					"IDESTADOCIVIL" => $parconyuge->getIdEstadoCivil(),
					"LISTAESTADOCIVIL" => $ve,
					"nombreaccion"=>$nombreaccion,
					"CONFIGURACION"=>"style='visibility:hidden';",
					"SOLOLECTURA"=>"",
					"ENAB_DISA"=>"",
					"nombreboton"=>$nombreboton,
					"tabdefault1" => "tabbertabdefault",
					"tabdefault" => "",
					"tabdefault2" => "",
                    );
        			break;

        case BAJA:
			$parametros = array(
                    "TITULO" => "Datos",
					"IDPOBLADOR" => $parconyuge->getIdConyuge(), /* variable id del poblador que alcanza a las tres pesta�as */
                    "ID" => $parconyuge->getIdPoblador(), /* ID del poblador conyuge */
					"IDCONYUGE" => $parconyuge->getIdConyuge(),
					"IDTIPODOC" => $parconyuge->getIdTipodoc(), 
					"LISTATIPODOC" => $vd,
					"DOCUMENTO"=> $parconyuge->getDocumento(),
					"NOMBRE" => $parconyuge->getNombres(),
					"APELLIDO" => $parconyuge->getApellido(),
					"DOMICILIOLEGAL" => $parconyuge->getDomicilioLegal(),
					"DOMICILIOPOSTAL" => $parconyuge->getDomicilioPostal(),
					"NACIONALIDAD" => $parconyuge->getNacionalidad(),
					"LUGARNACIMIENTO" => $parconyuge->getLugarNac(),
					"FECHANAC" => fechaACadena($parconyuge->getFechaNac()),			
					"TELEFONO" => $parconyuge->getTelefono(),
					"CARACTERISTICA" => $parconyuge->getCaracTel(),
					"IDLOCALIDAD" => $parconyuge->getIdLocalidad(),
					"LISTALOCALIDAD" => $vl,
					"IDESTADOCIVIL" => $parconyuge->getIdEstadoCivil(),
					"LISTAESTADOCIVIL" => $ve,					
					"nombreaccion" => $nombreaccion,
					"CONFIGURACION" => "",
					"SOLOLECTURA" => "readonly='readonly'",
					"ENAB_DISA" => "disabled='disabled'",
					"nombreboton" => $nombreboton,
					"tabdefault1" => "tabbertabdefault",
					"tabdefault" => "",
					"tabdefault2" => "",
					);
					break;
		 
		case ALTA:
		 	$parametros = array(
					"TITULO" => "Nuevo",
		 			"IDPOBLADOR" => $parconyuge->getIdConyuge(), /* variable id del poblador que alcanza a las tres pesta�as */
                    "ID" => 0, /* ID del poblador conyuge*/
					"IDCONYUGE" => $parconyuge->getIdConyuge(),
					"IDTIPODOC" => 0, 
					"LISTATIPODOC" => $vd,
					"DOCUMENTO"=> "",
					"NOMBRE" => "",
					"APELLIDO" => "",
					"DOMICILIOLEGAL" => "",
					"DOMICILIOPOSTAL" => "",
					"NACIONALIDAD" => "",
					"LUGARNACIMIENTO" => "",
					"FECHANAC" => "",		 	
					"NOMBREPADRE" => "",
					"NOMBREMADRE" => "",
					"VIVEPADRE" => 0,
					"VIVEMADRE" => 0,
					"NACIONALIDADPADRE" => "",
					"NACIONALIDADMADRE" => "",
					"TELEFONO" => "",
					"CARACTERISTICA" => "",
					"IDLOCALIDAD" => 0,
					"LISTALOCALIDAD" => $vl,
					"IDESTADOCIVIL" => 2,	/* por ser conyuge, por defecto es casado */
					"LISTAESTADOCIVIL" => $ve,
					"CONFIGURACION" => "",
					"SOLOLECTURA" => "",
					"ENAB_DISA" => "",					
					"nombreaccion" => $nombreaccion,
					"nombreboton" => $nombreboton,
					"tabdefault1" => "tabbertabdefault",
					"tabdefault" => "",
					"tabdefault2" => "",
		 		 	);
					break;

		// el default es igual al modificar
		default:
			$parametros = array(
                    "TITULO" => "Datos",
					"IDPOBLADOR" => $parconyuge->getIdConyuge(), /* variable id del poblador que alcanza a las tres pesta�as */
                    "ID" => $parconyuge->getIdPoblador(), /* ID del poblador conyuge */
					"IDCONYUGE" => $parconyuge->getIdConyuge(),
					"IDTIPODOC" => $parconyuge->getIdTipodoc(), 
					"LISTATIPODOC" => $vd,
					"DOCUMENTO"=> $parconyuge->getDocumento(),
					"NOMBRE" => $parconyuge->getNombres(),
					"APELLIDO" => $parconyuge->getApellido(),
					"DOMICILIOLEGAL" => $parconyuge->getDomicilioLegal(),
					"DOMICILIOPOSTAL" => $parconyuge->getDomicilioPostal(),
					"NACIONALIDAD" => $parconyuge->getNacionalidad(),
					"LUGARNACIMIENTO" => $parconyuge->getLugarNac(),
					"FECHANAC" => fechaACadena($parconyuge->getFechaNac()),			
					"TELEFONO" => $parconyuge->getTelefono(),
					"CARACTERISTICA" => $parconyuge->getCaracTel(),
					"IDLOCALIDAD" => $parconyuge->getIdLocalidad(),
					"LISTALOCALIDAD" => $vl,
					"IDESTADOCIVIL" => $parconyuge->getIdEstadoCivil(),
					"LISTAESTADOCIVIL" => $ve,
					"nombreaccion"=>$nombreaccion,
					"CONFIGURACION"=>"style='visibility:hidden';",
					"SOLOLECTURA"=>"",
					"ENAB_DISA"=>"",
					"nombreboton"=>$nombreboton,
					"tabdefault1" => "tabbertabdefault",
					"tabdefault" => "",
					"tabdefault2" => "",
					);
		} 				
        return $parametros;
	}


//============================================================================

	public function cargavariables($clasecarga, $op)
	//carga las variables de la clase
	{
		if ($op == MODIFICAR)
			$clasecarga->putIdPoblador($_POST["id"]);

        $clasecarga->putIdTipodoc($_POST["idtipodoc"]);
        $clasecarga->putDocumento($_POST["documento"]);
        $clasecarga->putNombres(strtoupper($_POST["nombres"]));
        $clasecarga->putApellido(strtoupper($_POST["apellido"]));
        $clasecarga->putNacionalidad(strtoupper($_POST["nacionalidad"]));
        $clasecarga->putLugarNac(strtoupper($_POST["lugarnacimiento"]));
        $clasecarga->putFechaNac(cadenaAFecha($_POST["fechanacimiento"]));
        $clasecarga->putNombrePadre(strtoupper($_POST["nombrepadre"]));
        $clasecarga->putNombreMadre(strtoupper($_POST["nombremadre"]));
        $clasecarga->putVivePadre($_POST["vivepadre"]);
        $clasecarga->putViveMadre($_POST["vivemadre"]);
        $clasecarga->putNacionalidadPadre(strtoupper($_POST["nacpadre"]));
        $clasecarga->putNacionalidadMadre(strtoupper($_POST["nacmadre"]));
        $clasecarga->putTelefono($_POST["telefono"]);
        $clasecarga->putCaracTel($_POST["caracteristica"]);
        $clasecarga->putDomicilioPostal($_POST["domiciliopostal"]);
        $clasecarga->putDomicilioLegal($_POST["domiciliolegal"]);        
		$clasecarga->putIdLocalidad($_POST["idlocalidad"]);
		$clasecarga->putIdEstadoCivil($_POST["idestadocivil"]);

		if($_POST["idconyuge"] == 0)
			$clasecarga->putIdConyuge('NULL');
		else
			$clasecarga->putIdConyuge($_POST["idconyuge"]);	     
	}

//=================================================================================================	 
	
	public function verificardocumento()
	// verifica que el numero de documento ingresado no se repita en el modelo
	{
		$poblador = new modelopoblador();
		$poblador->putDocumento("");

		if (isset($_GET['doc']) && !empty($_GET['doc']))
			$poblador->putDocumento($_GET['doc']);

	    $existe = $poblador->existedocumento();
	    
	    if ($existe)
	    	echo "CUIDADO! Ya existe un poblador con ese n&uacute;mero de documento";

	    return;
	}	
	
//=================================================================================================

	public function vincularConyuge()
	// vincula un poblador como conyuge de otro segun su id
	{
		$pob = new ModeloPoblador();
		if(($_GET['idpob'] > 0) && ($_GET['idconyuge'] > 0))
		{
			//vinculo al conyugue con el poblador
			$pob->putIdPoblador($_GET['idpob']);
			$pob->traerpoblador();
			$pob->putIdConyuge($_GET['idconyuge']);
			$pob->vincularConyugePoblador();
		}
        $vista = new view();
        $data['controlador'] = "conyuge";
        $data['accion'] = "vertabconyuge&&idpob=".$_GET['idpob'];
		$vista->show1("bridgecustom.html",$data);		
	}


}

?>