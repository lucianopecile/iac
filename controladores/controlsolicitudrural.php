<?php
require_once 'modelos/modelosolicitudrural.php';
require_once 'modelos/modelosolicitud.php';
require_once 'modelos/modeloprovincia.php';
require_once 'modelos/modelotrabajocampo.php';
require_once 'modelos/modeloarrendamiento.php';
require_once 'modelos/modelotierrarural.php';
require_once 'controladores/controltierrarural.php';
require_once 'controladores/controlarrendamiento.php';

class ControlSolicitudRural
{
 
 	function __construct()
	{
	    $this->view = new View();
	}
 
//---------------------------------------------------------------------------------
	 
	public function mostrarsolicitudrural()
	// muestra todas las solicitudrural en un html con una tabla
	{
		$solicitudrural = new modelosolicitudrural();
		$liztado = $solicitudrural->listadoTotal();
		$data['liztado'] = $liztado;
		$this->view->show1("solicitudrural.html", $data);
 	}

//---------------------------------------------------------------------------------------
	
	public function altasolicitudrural()
	{
	   $tierrarural=new modelotierrarural();
	   $clasetierrarural = new controltierrarural();
	   $altatirural=$clasetierrarural->altatierrarural($tierrarural);
	   $idaltaarr ='NULL';
	   
	   
	   
		
       if($altatirural>0){ // si se pudo dar de alta tierra rural
		
		
		    // si vienen cargados datos de arrendamiento
		    if (isset($_POST['nombre']) || isset($_POST['propietario']) || isset($_POST['superficie']) || isset($_POST['idunidad']) || isset($_POST['explotacion']))
		  {
			$ctrl_arrendamiento= new ControlArrendamiento();
			$idaltaarr = $ctrl_arrendamiento->altaarrendamiento(); // alta de arrendamiento
			if (!$idaltaarr)
			  { 	
				$mensaje = htmlentities("No se pudo completar la operaci�n, compruebe los resultados e int�ntelo nuevamente m�s tarde");
				$tierrarural->putIdTierraRural($altatirural);
				$tierrarural->borrartierrarural(); // no se pudo dar de alta borro tierra rural
				$data['mensaje'] = $mensaje;
				$this->view->show1("mostrarerror.html", $data);
				return;
	          }
		   }
		
		
		
		  $alta= new modelosolicitudrural();
		  $this->cargavariables($alta, ALTA);
		  $alta->putIdTierraRural($altatirural);
		  $alta->putIdArrendamiento($idaltaarr);
		  $altaok = $alta->altasolicitudrural();
		  if (!$altaok)
		  {
			 $mensaje = htmlentities("En este momento no se pudo dar de alta la solicitud rural");
			 $data['mensaje'] = $mensaje;
			 $this->view->show1("mostrarerror.html", $data);
			 return;
		  }
		   else{
		    $nuevoid=$alta->getIdSolicitud();
	        $data['controlador']="solicitudrural";
		    $data['accion']="vertabsolicitudrural&&idsol=".$nuevoid;
		    $this->view->show1("bridgecustom.html",$data);	
		  }	
		}      
		else{
		
             $mensaje = htmlentities("No se pudo dar de alta la tierra rural");
			 $data['mensaje'] = $mensaje;
			 $this->view->show1("mostrarerror.html", $data);
			 return;

		
		}
		 
	}
	
//---------------------------------------------------------------------------------------
	
	public function modificarsolicitudrural()
	{
	    $arrenda= new modeloarrendamiento();
	    $tierrarural=new modelotierrarural();
		$modifica= new modelosolicitudrural();
		$this->cargavariables($modifica,MODIFICAR);
		$idaltaarr ='NULL';

        $arrenda->putIdArrendamiento($modifica->getIdArrendamiento());
        // si vienen cargados datos de arrendamiento
		 if (isset($_POST['nombre']) || isset($_POST['propietario']) || isset($_POST['superficie']) || isset($_POST['idunidad']) || isset($_POST['explotacion']))
		  {
		   $ctrl_arrendamiento= new ControlArrendamiento();
		    if($arrenda->getIdArrendamiento()>0){           // si ya existe el arrendamiento lo modifica
			  $ctrl_arrendamiento->modificararrendamiento();
			}
			else {                                          //si no existe el arrendamiento lo da de alta
			  $idaltaarr = $ctrl_arrendamiento->altaarrendamiento(); // alta de arrendamiento
			  $modifica->putIdArrendamiento($idaltaarr);
			  if (!$idaltaarr)
			    { 	
				 $mensaje = htmlentities("No se pudo agregar datos de arrendamiento");
				 $data['mensaje'] = $mensaje;
			     $this->view->show1("mostrarerror.html", $data);
				 return;
	            }
			}	
		   }



		
        $modificado=$modifica->modificarsolicitudrural();
	    $tierrarural->putIdTierraRural($modifica->getIdTierraRural());
	    $clasetierrarural = new controltierrarural();
	    $tierrarural=$clasetierrarural->modificartierrarural($tierrarural);
		
		  
		
		
	   if (!$modificado){
	      $mensaje = htmlentities("En este momento no se puede realizar la operacion, intentelo mas tarde");
	      $data['mensaje']=$mensaje;
    	  $this->view->show1("mostrarerror.html", $data);
		  return;
        }
	    $this->vertabsolicitudrural();
			
	}
		
//---------------------------------------------------------------------------------------
	
	public function borrarsolicitudrural()
	{
		$borra= new modelosolicitudrural();
		$borra->putIdSolicitudRural($_POST['id']);
		$borrado=$borra->borrarsolicitudrural();
		if (!$borrado)
		{
			$mensaje = htmlentities("En este momento no se puede realizar la operaci�n, int�ntelo m�s tarde");
			$data['mensaje'] = $mensaje;
			$this->view->show1("mostrarerror.html", $data);
			return;
		}
		$this->mostrarsolicitudrural();		 
	}



//-----------------------------------------------------------------------------------

//retorna los datos de una solicitudrural si se carg� el id, sino retorna campos en blanco para hacer un alta
    public function versolicitudrural()
	{
		
	$solicitudrural = new modelosolicitudrural();
    if (isset($_GET['idsol'])) { 
	
        $solicitudrural->putIdSolicitudRural($_GET['idsol']);
	
       	$empent = $solicitudrural->traersolicitudrural();
	
     	if (!$empent){
 	       $mensaje = htmlentities("En este momento no se puede realizar la operaci�n para ver solicitud, int�ntelo m�s tarde");
	       $data['mensaje']=$mensaje;
           $this->view->show1("mostrarerror.html", $data);
		   return;
         }
	
    } 
		
	
	$solicitud=new modelosolicitud;
	$provincia=new modeloprovincia;
	$trabajocampo=new modelotrabajocampo;
	$arrendamiento=new modeloarrendamiento;
	
	$data=$this->cargarPlantillaModificar($solicitudrural,$solicitud,$provincia,$arrendamiento,$trabajocampo);
	  $this->view->show("abmsolicitudrural.html", $data);
	
	}	


//-----------------------------------------------------------------------------------

//retorna los datos de una solicitudrural si se carg� el id, sino retorna campos en blanco para hacer un alta
    public function vertabsolicitudrural()
	{
	$tierrarural = new modelotierrarural();	
	$solicitudrural = new modelosolicitudrural();
	if (isset($_GET['idsol']) || (isset($_POST['idsolicitud'])) ) {

	   if (isset($_GET['idsol'])){
           $solicitudrural->putIdSolicitud($_GET['idsol']);
	   	   $solicitudrural->putIdPoblador($_GET['idpob']);
		}
       if (isset($_POST['idsolicitud'])){  
	        $solicitudrural->putIdSolicitud($_POST['idsolicitud']);
		}
		
		$empent = $solicitudrural->traersolicitudruralasociada();
		       $tierrarural->putIdSolicitudRural($solicitudrural->getIdSolicitudRural());
	           $clasetierrarural = new controltierrarural();
	           $parTierraRural=$clasetierrarural->vertierraruralasociada($tierrarural);
	            if (isset($_GET['idsol'])){
                   $solicitudrural->putIdSolicitud($_GET['idsol']);
	   	            $solicitudrural->putIdPoblador($_GET['idpob']);
		}
       if (isset($_POST['idsolicitud'])){  
	        $solicitudrural->putIdSolicitud($_POST['idsolicitud']);
		}

     	if (!$empent){
 	       $mensaje = htmlentities("En este momento no se puede realizar la operaci�n para ver solicitud, int�ntelo m�s tarde");
	       $data['mensaje']=$mensaje;
           $this->view->show1("mostrarerror.html", $data);
		   return;
         }
	
    } 
		
	
	$solicitud=new modelosolicitud;
	$provincia=new modeloprovincia;
	$trabajocampo=new modelotrabajocampo;
	$arrendamiento=new modeloarrendamiento;
	
	$data=$this->cargarPlantillaModificar($solicitudrural,$solicitud,$provincia,$arrendamiento,$trabajocampo);
	$nuevoarray=$data;
	if(is_array( $parTierraRural)){
     	$nuevoarray=array_merge($data,$parTierraRural);
	}
	  $this->view->show("tabsolicitudrural.html", $nuevoarray);
	
	}	
	
//-----------------------------------------------------------------------------------
	 public function cargarPlantillaModificar($parSolicitudRural,$parSolicitud,$parProvincia,$parArrendamiento,$parTrabajoCampo) 
{  
   
	
	$vlp= $parProvincia->TraerTodos();
	$vlp['selected']=  $parSolicitudRural->getIdProvinciaAnterior();
	$vla= $parArrendamiento->TraerTodos();
	$vla['selected']=  $parSolicitudRural->getIdArrendamiento();
	$vlt= $parTrabajoCampo->TraerTodos();
	$vlt['selected']=  $parSolicitudRural->getIdTrabajoCampo();
	
	
	$idsolicitudrural =  $parSolicitudRural->getIdSolicitudRural();
	$quehacer = "";
	if ($idsolicitudrural == 0)
			$quehacer = ALTA;
	else
		if (isset($_GET['operacion']))
			{
				if ($_GET['operacion'] == 2) $quehacer = MODIFICAR;
				if ($_GET['operacion'] == 3) $quehacer = BAJA;
			}
			  
	switch($quehacer)
	{
      case ALTA:
		
		$parSolicitudRural->putIdSolicitudRural("");
		
        $nombreboton="Guardar";
	    $nombreaccion="altasolicitudrural";
	 
      break;	 
      case MODIFICAR:
	     
        $nombreboton="Guardar";
	    $nombreaccion="modificarsolicitudrural";
	  break;
	  case BAJA:
	     
         $nombreboton="Eliminar";
         $nombreaccion="borrarsolicitudrural";  
      break;
      default:  
	    $nombreboton="Guardar";
	    $nombreaccion="modificarsolicitudrural";
		  
   }
	
		  
        switch ($quehacer)
       {

       	case MODIFICAR:
		$idsoli=$parSolicitudRural->getIdSolicitudRural();
		$parametros = array(
                    "TITULOFORM" =>  "Solicitudes Rurales -> Modificar",
                    "ID" => $parSolicitudRural->getIdSolicitud(),
					"IDSOLICITUD" => $parSolicitudRural->getIdSolicitud(),
					"IDSOLICITUDRURAL" => $parSolicitudRural->getIdSolicitudRural(),
					"IDPOBLADOR"=>$parSolicitudRural->getIdPoblador(),
					"IDOPERACION"=>$quehacer,
					"IDTIERRARURAL"=>$parSolicitudRural->getIdTierraRural(),
					"IDSOLICITUD" => $parSolicitudRural->getIdSolicitud(),
					"RESIDENTETIERRA" =>$parSolicitudRural->getResidenteTierra(), 
					"FECHARESIDETIERRA" =>$parSolicitudRural->getFechaResideTierra(),
    				"USR_MOD"=>$parSolicitudRural->getUsrMod(),
					"RESIDENCIAANTERIOR"=>$parSolicitudRural->getResidenciaAnterior(),
					"DOMICILIOREAL"=>$parSolicitudRural->getDomicilioReal(),
					"PROFESION"=>$parSolicitudRural->getProfesion(),
					"OCUPACIONACTUAL"=>$parSolicitudRural->getOcupacionActual(),
					"INTEGRASOCIEDAD"=>  $parSolicitudRural->getIntegraSociedad(), 
					"DENOMINACIONSOCIEDAD"=>$parSolicitudRural->getDenominacionSociedad(), 
					"COMPONENTESSOCIEDAD"=>$parSolicitudRural->getComponentesSociedad(),
					"LISTATRABAJOCAMPO"=>$vlt,
					"IDTRABAJOCAMPO"=>$parSolicitudRural->getIdTrabajoCampo(),
					"PADRESRURALES"=>$parSolicitudRural->getPadresRurales(),
					"IDPROVINCIAANTERIOR"=>$parSolicitudRural->getIdProvinciaAnterior(),
					"LISTAPROVINCIAS"=>$vlp,
					"ESTECNICO"=>$parSolicitudRural->getEsTecnico(),
					"TITULO"=>$parSolicitudRural->getTitulo(),
					"EJERCIOPROFESIONEN"=>$parSolicitudRural->getEjercioProfesionEn(),
					"CERTIFICADOPRODUCTOR"=>$parSolicitudRural->getCertificadoProductor(),
					"OTRASTIERRAS"=>$parSolicitudRural->getOtrasTierras(),
					"TIERRASDECLARADAS"=>$parSolicitudRural->getTierrasDeclaradas(),
					"OCUPANTETIERRAFISCAL"=>$parSolicitudRural->getOcupanteTierraFiscal(),
					"UBICACIONTIERRAFISCAL"=>$parSolicitudRural->getUbicacionTierraFiscal(),
					"OTRASADJUDICACIONES"=>$parSolicitudRural->getOtrasAdjudicaciones(),
					"DETALLEADJUDICACIONES"=>$parSolicitudRural->getDetalleAdjudicaciones(),					
					"TRANSFIRIOTIERRAS"=>$parSolicitudRural->getTransfirioTierras(),
					"DESTINATARIO"=>$parSolicitudRural->getDestinatario(),										
					"IDARRENDAMIENTO"=>$parSolicitudRural->getIdArrendamiento(),
					"LISTAARRENDAMIENTO"=>$vla,
					"REFERENCIAS"=>$parSolicitudRural->getReferencias(),															
					"NOVER"=>"style='visibility:hidden'",

					"nombreaccion"=>$nombreaccion,
					"CONFIGURACION"=>"",
					"SOLOLECTURA"=>"",
					"ENAB_DISA"=>"",
					"DISA_MODI"=>"disabled='disabled'",
					"tabdefault1" => "tabbertabdefault",
					"tabdefault" => "",
					"tabdefault2" => "",

					"nombreboton"=>$nombreboton
                  
                    );
        break;
		case BAJA:
        $idsoli=$parSolicitudRural->getIdSolicitudRural();
	    $parametros = array(
                   "TITULOFORM" =>  "Solicitudes Rurales -> Eliminar",
                    "ID" => $parSolicitudRural->getIdSolicitud(),
					"IDSOLICITUDRURAL" => $parSolicitudRural->getIdSolicitudRural(),
					"IDSOLICITUD" => $parSolicitudRural->getIdSolicitud(),
					"IDPOBLADOR"=>$parSolicitudRural->getIdPoblador(),
					"IDOPERACION"=>$quehacer,
					"IDSOLICITUD" => $parSolicitudRural->getIdSolicitud(),
					"IDTIERRARURAL"=>$parSolicitudRural->getIdTierraRural(),

					"FECHARESIDETIERRA" =>$parSolicitudRural->getFechaResideTierra(),
    				"USR_MOD"=>$parSolicitudRural->getUsrMod(),
					"RESIDENCIAANTERIOR"=>$parSolicitudRural->getResidenciaAnterior(),
					"DOMICILIOREAL"=>$parSolicitudRural->getDomicilioReal(),
					"PROFESION"=>$parSolicitudRural->getProfesion(),
					"OCUPACIONACTUAL"=>$parSolicitudRural->getOcupacionActual(),
					"INTEGRASOCIEDAD"=>  $parSolicitudRural->getIntegraSociedad(), 
					"DENOMINACIONSOCIEDAD"=>$parSolicitudRural->getDenominacionSociedad(), 
					"COMPONENTESSOCIEDAD"=>$parSolicitudRural->getComponentesSociedad(),
					"LISTATRABAJOCAMPO"=>$vlt,
					"IDTRABAJOCAMPO"=>$parSolicitudRural->getIdTrabajoCampo(),
					"PADRESRURALES"=>$parSolicitudRural->getPadresRurales(),
					"IDPROVINCIAANTERIOR"=>$parSolicitudRural->getIdProvinciaAnterior(),
					"LISTAPROVINCIAS"=>$vlp,
					"ESTECNICO"=>$parSolicitudRural->getEsTecnico(),
					"TITULO"=>$parSolicitudRural->getTitulo(),
					"EJERCIOPROFESIONEN"=>$parSolicitudRural->getEjercioProfesionEn(),
					"CERTIFICADOPRODUCTOR"=>$parSolicitudRural->getCertificadoProductor(),
					"OTRASTIERRAS"=>$parSolicitudRural->getOtrasTierras(),
					"TIERRASDECLARADAS"=>$parSolicitudRural->getTierrasDeclaradas(),
					"OCUPANTETIERRAFISCAL"=>$parSolicitudRural->getOcupanteTierraFiscal(),
    				"UBICACIONTIERRAFISCAL"=>$parSolicitudRural->getUbicacionTierraFiscal(),
					"OTRASADJUDICACIONES"=>$parSolicitudRural->getOtrasAdjudicaciones(),
					"DETALLEADJUDICACIONES"=>$parSolicitudRural->getDetalleAdjudicaciones(),					
					"TRANSFIRIOTIERRAS"=>$parSolicitudRural->getTransfirioTierras(),
					"DESTINATARIO"=>$parSolicitudRural->getDestinatario(),										
					"IDARRENDAMIENTO"=>$parSolicitudRural->getIdArrendamiento(),
					"LISTAARRENDAMIENTO"=>$vla,
					"REFERENCIAS"=>$parSolicitudRural->getReferencias(),	
					"NOVER"=>"style='visibility:hidden'",
					"nombreaccion"=>$nombreaccion,
					"DISA_MODI"=>"",
					"SOLOLECTURA"=>"readonly='readonly'",
					"ENAB_DISA"=>"disabled='disabled'",
					"tabdefault1" => "tabbertabdefault",
					"tabdefault" => "",
					"tabdefault2" => "",

					"nombreboton"=>$nombreboton
                    );
	     break;
		 case ALTA:
		 
 
         
  
	     $parametros = array(
	                "TITULOFORM" =>  "Solicitudes Rurales -> Alta",
                    "ID" => $parSolicitudRural->getIdSolicitud(),
					"IDSOLICITUD" => $parSolicitudRural->getIdSolicitud(),
					"IDSOLICITUDRURAL" => 0,
					"IDTIERRARURAL"=>0,

					"IDPOBLADOR"=>$parSolicitudRural->getIdPoblador(),
					"IDOPERACION"=>$quehacer,
					"RESIDENTETIERRA" =>0, 
					"FECHARESIDETIERRA" =>0,
    				"USR_MOD"=>$parSolicitudRural->getUsrMod(),
					"RESIDENCIAANTERIOR"=>"",
					"DOMICILIOREAL"=>"",
					"PROFESION"=>"",
					"OCUPACIONACTUAL"=>"",
					"INTEGRASOCIEDAD"=>  0, 
					"DENOMINACIONSOCIEDAD"=>"", 
					"COMPONENTESSOCIEDAD"=>"",
					"LISTATRABAJOCAMPO"=>$vlt,
					"IDTRABAJOCAMPO"=>0,
					"PADRESRURALES"=>0,
					"IDPROVINCIAANTERIOR"=>0,
					"LISTAPROVINCIAS"=>$vlp,
					"ESTECNICO"=>0,
					"TITULO"=>0,
					"EJERCIOPROFESIONEN"=>"",
					"CERTIFICADOPRODUCTOR"=>0,
					"OTRASTIERRAS"=>0,
					"TIERRASDECLARADAS"=>"",
					"OCUPANTETIERRAFISCAL"=>0,
					"UBICACIONTIERRAFISCAL"=>"",
					"OTRASADJUDICACIONES"=>0,
					"DETALLEADJUDICACIONES"=>"",					
					"TRANSFIRIOTIERRAS"=>0,
					"DESTINATARIO"=>"",										
					"IDARRENDAMIENTO"=>0,
					"LISTAARRENDAMIENTO"=>$vla,
					"REFERENCIAS"=>"",	
					"CONFIGURACION"=>"",
				    "SOLOLECTURA"=>"",
					"ENAB_DISA"=>"",					
	                 "DISA_MODI"=>"",
					"tabdefault1" => "tabbertabdefault",
					"tabdefault" => "",
					"tabdefault2" => "",
					"nombreaccion"=>$nombreaccion,
					"nombreboton"=>$nombreboton,
					   );
	
	     break;
		 default :
		 $idsoli=$parSolicitudRural->getIdSolicitudRural();
		$parametros = array(
                   "TITULOFORM" =>  "Solicitudes Rurales -> Modificar",
                    "ID" => $parSolicitudRural->getIdSolicitud(),
					"IDSOLICITUD" => $parSolicitudRural->getIdSolicitud(),
					"IDSOLICITUDRURAL" => $parSolicitudRural->getIdSolicitudRural(),
					"IDPOBLADOR"=>$parSolicitudRural->getIdPoblador(),
					"IDOPERACION"=>$quehacer,
					"IDSOLICITUD" => $parSolicitudRural->getIdSolicitud(),
					"IDTIERRARURAL"=>$parSolicitudRural->getIdTierraRural(),
					"RESIDENTETIERRA" =>$parSolicitudRural->getResidenteTierra(), 
					"FECHARESIDETIERRA" =>$parSolicitudRural->getFechaResideTierra(),
    				"USR_MOD"=>$parSolicitudRural->getUsrMod(),
					"RESIDENCIAANTERIOR"=>$parSolicitudRural->getResidenciaAnterior(),
					"DOMICILIOREAL"=>$parSolicitudRural->getDomicilioReal(),
					"PROFESION"=>$parSolicitudRural->getProfesion(),
					"OCUPACIONACTUAL"=>$parSolicitudRural->getOcupacionActual(),
					"INTEGRASOCIEDAD"=>  $parSolicitudRural->getIntegraSociedad(), 
					"DENOMINACIONSOCIEDAD"=>$parSolicitudRural->getDenominacionSociedad(), 
					"COMPONENTESSOCIEDAD"=>$parSolicitudRural->getComponentesSociedad(),
					"LISTATRABAJOCAMPO"=>$vlt,
					"IDTRABAJOCAMPO"=>$parSolicitudRural->getIdTrabajoCampo(),
					"PADRESRURALES"=>$parSolicitudRural->getPadresRurales(),
					"IDPROVINCIAANTERIOR"=>$parSolicitudRural->getIdProvinciaAnterior(),
					"LISTAPROVINCIAS"=>$vlp,
					"ESTECNICO"=>$parSolicitudRural->getEsTecnico(),
					"TITULO"=>$parSolicitudRural->getTitulo(),
					"EJERCIOPROFESIONEN"=>$parSolicitudRural->getEjercioProfesionEn(),
					"CERTIFICADOPRODUCTOR"=>$parSolicitudRural->getCertificadoProductor(),
					"OTRASTIERRAS"=>$parSolicitudRural->getOtrasTierras(),
					"TIERRASDECLARADAS"=>$parSolicitudRural->getTierrasDeclaradas(),
					"OCUPANTETIERRAFISCAL"=>$parSolicitudRural->getOcupanteTierraFiscal(),
					"UBICACIONTIERRAFISCAL"=>$parSolicitudRural->getUbicacionTierraFiscal(),
					"OTRASADJUDICACIONES"=>$parSolicitudRural->getOtrasAdjudicaciones(),
					"DETALLEADJUDICACIONES"=>$parSolicitudRural->getDetalleAdjudicaciones(),					
					"TRANSFIRIOTIERRAS"=>$parSolicitudRural->getTransfirioTierras(),
					"DESTINATARIO"=>$parSolicitudRural->getDestinatario(),										
					"IDARRENDAMIENTO"=>$parSolicitudRural->getIdArrendamiento(),
					"LISTAARRENDAMIENTO"=>$vla,
					"REFERENCIAS"=>$parSolicitudRural->getReferencias(),															
					"NOVER"=>"style='visibility:hidden'",

					"nombreaccion"=>$nombreaccion,
					"CONFIGURACION"=>"",
					"SOLOLECTURA"=>"",
					"ENAB_DISA"=>"",
					"DISA_MODI"=>"disabled='disabled'",
					"tabdefault1" => "tabbertabdefault",
					"tabdefault" => "",
					"tabdefault2" => "",

					"nombreboton"=>$nombreboton
                    );
		 					
		 
		} 				



        return $parametros;
  }

//----------------------------------------------------------------------------------
//Carga las variables del html para volcarlas en la tabla


public function cargavariables($clasecarga,$oper){
       
	 ///carga las variables de la clase 
	   
	   if ($oper==MODIFICAR){  
	    
        $clasecarga->putIdSolicitudRural($_POST["idsolicitudrural"]);
		}
		
    $clasecarga->putIdSolicitud($_POST["idsolicitud"]);
    $clasecarga->putIdUsrCreador($_SESSION["s_idusr"]);
	$clasecarga->putIdUsrMod($_SESSION["s_idusr"]);
	
	$clasecarga->putResidenteTierra($_POST["residentetierra"]);
	$clasecarga->putFechaResideTierra($_POST["fecharesidetierra"]);
	$clasecarga->putResidenciaAnterior($_POST["residenciaanterior"]);
	$clasecarga->putDomicilioReal($_POST["domicilioreal"]);
	$clasecarga->putProfesion($_POST["profesion"]);
	$clasecarga->putOcupacionActual($_POST["ocupacionactual"]);
	$clasecarga->putIntegraSociedad($_POST["integrasociedad"]);
	$clasecarga->putDenominacionSociedad($_POST["denominacionsociedad"]);
	$clasecarga->putComponentesSociedad($_POST["componentessociedad"]);
		if($_POST["idtrabajocampo"]==0){
		   $clasecarga->putIdTrabajoCampo('NULL');       
		}
		else{
	      $clasecarga->putIdTrabajoCampo($_POST["idtrabajocampo"]);
		}

	$clasecarga->putPadresRurales($_POST["padresrurales"]);
		if($_POST["idprovinciaanterior"]==0){
		   $clasecarga->putIdProvinciaAnterior('NULL');       
		}
		else{
	      $clasecarga->putIdProvinciaAnterior($_POST["idprovinciaanterior"]);
		}

    $clasecarga->putEsTecnico($_POST["estecnico"]);
    $clasecarga->putTitulo($_POST["titulo"]);
    $clasecarga->putEjercioProfesionEn($_POST["ejercioprofesionen"]);
    $clasecarga->putCertificadoProductor($_POST["certificadoproductor"]);
    $clasecarga->putOtrasTierras($_POST["otrastierras"]);
    $clasecarga->putTierrasDeclaradas($_POST["tierrasdeclaradas"]);
    $clasecarga->putOcupanteTierraFiscal($_POST["ocupantetierrafiscal"]);
    $clasecarga->putUbicacionTierraFiscal($_POST["ubicaciontierrafiscal"]);
    $clasecarga->putOtrasAdjudicaciones($_POST["otrasadjudicaciones"]);
    $clasecarga->putDetalleAdjudicaciones($_POST["detalleadjudicaciones"]);
    $clasecarga->putTransfirioTierras($_POST["transfiriotierras"]);
    $clasecarga->putDestinatario($_POST["destinatario"]);
		if($_POST["idarrendamiento"]==0){
		   $clasecarga->putIdArrendamiento('NULL');       
		}
		else{
          $clasecarga->putIdArrendamiento($_POST["idarrendamiento"]);
		}
		if($_POST["idtierrarural"]==0){
		   $clasecarga->putIdTierraRural('NULL');       
		}
		else{
          $clasecarga->putIdTierraRural($_POST["idtierrarural"]);
		}
	$clasecarga->putReferencias($_POST["referencias"]);
    $clasecarga->putNombres($_POST["nombres"]);
    $clasecarga->putApellido($_POST["apellido"]);
   
   }	
	
//----------------------------------------------------------------------------------

	public function buscarInspeccionAsociada()
	// busca un informe de inspeccion asociado a la solicitud rural, si no tiene devuelve falso.
	{
		if (isset($_GET['idrural']))
		{
			$s_rural = new ModeloSolicitudRural();
			$s_rural->putIdSolicitudRural($_GET['idrural']);
			$s_rural->traersolicitudrural();

		}
	}


}

?>