<?php
define('FPDF_FONTPATH','../fpd153/font/');
require_once '../fpd153/fpdf.php' ;
require_once '../fpd153/diseniocuotas.php' ;
require_once '../librerias/config.php';
require_once '../modelos/modelocuota.php';
require_once '../modelos/modelocuenta.php';
require_once '../modelos/modeloparametro.php';
require_once '../librerias/funcionesphp.php';
require_once '../config.php'; //Archivo con configuraciones.

define('MAXLINEA', 60);

$cuotas = new modelocuota();
$cuenta = new ModeloCuenta();

if($_POST['idcuenta'])
    $cuotas->putIdCuenta($_POST['idcuenta']);
else
    return false;

$listado = $cuotas->listadoTotal();

//$cant_cuotas = count($listado)-1;
//obtengo el nro de la primer cuota del plan total (formalizacion + plan cuotas)
$nroPrimerCuota = $listado[0]['nrocuota'];            
//$cant_cuotas = count($liztado)-1;
$cant_cuotas = ($nroPrimerCuota==0)?count($listado)-1:count($listado);

$_GET['fechamora'] = $_POST['fechaestado'];
//$arr_mora = $cuotas->calcularMoraRango(0, $cant_cuotas ,$_POST['idcuenta']);
$arr_mora = $cuotas->calcularMoraRango($nroPrimerCuota, $cant_cuotas ,$_POST['idcuenta']);
if(!$arr_mora)
	return false;

//traigo las observaciones de la cuenta
$cuenta->putIdCuenta($_POST['idcuenta']);
$cuenta->traerCuenta();
$obs = $cuenta->getObservacion();

if($listado)
{
	$pdf = new APDF();
	$pdf->AliasNbPages();
	$pdf->AddPage('P', "Legal");
	$pdf->Setmargins(20,20,10);
	$pdf->SetLineWidth(0.1);
	$pdf->SetFillColor(192, 192, 192);
	$pdf->Setfont('times','',8);
	$fila=50;
	$columna=10;
	$pdf->SetFont('Times','B');

	//Encabezado datos cuenta
	$pdf->SetFont('Times','B',10);
	$pdf->SetXY($columna,$fila);
	$pdf->Write(4, "Titular: ".$_POST['solicitante']);
	$pdf->SetXY($columna,$fila+5);
	$pdf->Write(4, "Expediente: ".$_POST['nroexpediente']);
	$offset = (strlen($_POST['nroexpediente']) +10) * 2; /*offset es el espacio entre el nro de expediente y el a�o de expediente */
	$pdf->SetXY($columna+$offset,$fila+5);
        $anio = utf8_decode('Año');
	$pdf->Write(4, $anio.": ".$_POST['anioexpediente']);
	$pdf->SetXY($columna,$fila+10);
	$pdf->Write(4, "Nro de cuenta: ".$_POST['nrocuenta']);
	$offset = (strlen($_POST['nrocuenta']) +15) * 2; /*offset es el espacio entre el nro de cuenta y la superficie*/
	$pdf->SetXY($columna+$offset,$fila+10);
	$pdf->Write(4, "Superficie: ".$_POST['superficie']." ".$_POST['unidad']);
	$pdf->SetXY($pdf->GetX()+10,$pdf->GetY());
	$pdf->Write(4, $_POST['localidad']);

	//Fin datos cuenta

	// Encabezado Fila
	$pdf->SetFont('Times','B',8);
	$fila=$fila+21;
	$pdf->SetXY($columna,$fila);
	$pdf->Cell(195,10,'',1,1,'C',1);
	$pdf->SetXY($columna,$fila);
	$pdf->drawTextBox("Nro cuota", 10, 10,'C','M', 1);
	$pdf->SetXY($columna+10,$fila);
	$pdf->drawTextBox("Monto cuota", 20, 10,'C','M', 1);

	$pdf->SetXY($columna + 30, $fila);
	$pdf->drawTextBox("Capital", 20, 10, 'C', 'M', 1);

	$pdf->SetXY($columna+50,$fila);	
	$pdf->drawTextBox(utf8_decode("Interés por cuota"), 20, 10,'C','M', 1);
	$pdf->SetXY($columna+70,$fila);
	$pdf->drawTextBox("Saldo cuota", 20, 10,'C','M', 1);
	$pdf->SetXY($columna+90,$fila);
	$pdf->drawTextBox("Fecha vto.", 15, 10,'C','M', 1);
	 $pdf->SetXY($columna+105,$fila);
	$pdf->drawTextBox("Gastos fijos", 15, 10,'C','M', 1);
	$pdf->SetXY($columna+120,$fila);
	$pdf->drawTextBox("Fecha cobro", 15, 10,'C','M', 1);
	$pdf->SetXY($columna+135,$fila);
	$pdf->drawTextBox("Cobrado por cuota", 20, 10,'C','M', 1);
	 $pdf->SetXY($columna+155,$fila);
	$pdf->drawTextBox(utf8_decode("Interés por mora"), 20, 10,'C','M', 1);
	$pdf->SetXY($columna+175,$fila);
	$pdf->drawTextBox(utf8_decode("Interés mora al día ").$_POST['fechaestado'], 20, 10,'C','M', 1);
	$pdf->SetLineWidth(0.1);
	$fila=$fila+10;
	//Fin Encabezado de Fila
	$pdf->Setfont('times','',8);
	$i=0;
	foreach($listado as $cuota)
	{
		$pdf->SetXY($columna,$fila);
		$pdf->Cell(195,5,'',1,1,'C');
		//columna nro cuota
		$pdf->SetXY($columna,$fila);
		$pdf->drawTextBox($cuota['nrocuota'], 10, 5,'C','M', 1);
		//columna monto cuota
		$pdf->SetXY($columna+10,$fila);
		$valor = "$ ".number_format($cuota['montocuota'],2,",",".")." ";
		$pdf->drawTextBox($valor, 20, 5,'R','M', 1);
		//columna capital
		$pdf->SetXY($columna + 30, $fila);
		$valor = "$ " . number_format($cuota['capital'], 2, ",", ".") . " ";
		$pdf->drawTextBox($valor, 20, 5, 'R', 'M', 1);
		//columna interes
		$pdf->SetXY($columna+50,$fila);
		$valor = "$ ".number_format($cuota['interes'],2,",",".")." ";
		$pdf->drawTextBox($valor, 20, 5,'R','M', 1);
		//columna saldo
		$pdf->SetXY($columna+70,$fila);
		$valor = "$ ".number_format($cuota['saldo'],2,",",".")." ";
		$pdf->drawTextBox($valor, 20, 5,'R','M', 1);
		//columna fecha vencimiento
		$pdf->SetXY($columna+90,$fila);
		$pdf->drawTextBox($cuota['fechavencimiento'], 15, 5,'C','M', 1);
		//columna gastos fijos
		$pdf->SetXY($columna+105,$fila);
		$valor = "$ ".number_format($cuota['conceptos'],2,",",".")." ";
		$pdf->drawTextBox($valor, 15, 5,'R','M', 1);
		//columna fecha pago
		$pdf->SetXY($columna+120,$fila);
		$pdf->drawTextBox($cuota['fechapago'], 15, 5,'C','M', 1);
		//columna monto cobrado
		$pdf->SetXY($columna+135,$fila);
		$valor = "$ ".number_format($cuota['cobrado'],2,",",".")." ";
		$pdf->drawTextBox($valor, 20, 5,'R','M', 1);
		//columna interes por mora calculada actual
		$pdf->SetXY($columna+155,$fila);
		$valor = "$ ".number_format($cuota['interesmora'],2,",",".")." ";
		$pdf->drawTextBox($valor, 20, 5,'R','M', 1);
		//columna interes por mora ala fecha indicada
		$pdf->SetXY($columna+175,$fila);
		$valor = "$ ".number_format($cuota['interesmora']+$arr_mora[$i]['intmora'],2,",",".")." ";
		$pdf->drawTextBox($valor, 20, 5,'R','M', 1);
		$fila=$fila+5;
		$i++;

		// si hay salto de pagina
		if($fila>=300)
		{
			$pdf->Addpage('P', "Legal");
			$pdf->Setmargins(20,20,10);
			$pdf->SetLineWidth(0.1);
			$pdf->SetFillColor(192, 192, 192);
			$fila=50;
			$columna=10;
			//Encabezado datos cuenta nueva pagina
			$pdf->SetFont('Times','B',10);
			$pdf->SetXY($columna,$fila);
			$pdf->Write(4, "Titular: ".$_POST['solicitante']);
			$pdf->SetXY($columna,$fila+5);
			$pdf->Write(4, "Expediente: ".$_POST['nroexpediente']);
			$offset = (strlen($_POST['nroexpediente']) +10) * 2; /*offset es el espacio entre el nro de expediente y el a�o de expediente */
			$pdf->SetXY($columna+$offset,$fila+5);
			$pdf->Write(4, "A�o: ".$_POST['anioexpediente']);
			$pdf->SetXY($columna,$fila+10);
			$pdf->Write(4, "Nro de cuenta: ".$_POST['nrocuenta']);
			$offset = (strlen($_POST['nrocuenta']) +15) * 2; /*offset es el espacio entre el nro de cuenta y la superficie*/
			$pdf->SetXY($columna+$offset,$fila+10);
			$pdf->Write(4, "Superficie: ".$_POST['superficie']." ".$_POST['unidad']);
			//Fin datos cuenta nueva pagina
			// Encabezado Fila nueva pagina
			$pdf->SetFont('Times','B',8);
			$fila=$fila+21;
			$pdf->SetXY($columna,$fila);
			$pdf->Cell(175,10,'',1,1,'C',1);
			$pdf->SetXY($columna,$fila);
			$pdf->drawTextBox("Nro cuota", 10, 10,'C','M', 1);
			$pdf->SetXY($columna+10,$fila);
			$pdf->drawTextBox("Monto cuota", 20, 10,'C','M', 1);
			$pdf->SetXY($columna+30,$fila);
			$pdf->drawTextBox("Inter�s por cuota", 20, 10,'C','M', 1);
			$pdf->SetXY($columna+50,$fila);
			$pdf->drawTextBox("Saldo cuota", 20, 10,'C','M', 1);
			$pdf->SetXY($columna+70,$fila);
			$pdf->drawTextBox("Fecha vto.", 15, 10,'C','M', 1);
			$pdf->SetXY($columna+85,$fila);
			$pdf->drawTextBox("Gastos fijos", 15, 10,'C','M', 1);
			$pdf->SetXY($columna+100,$fila);
			$pdf->drawTextBox("Fecha cobro", 15, 10,'C','M', 1);
			$pdf->SetXY($columna+115,$fila);
			$pdf->drawTextBox("Cobrado por cuota", 20, 10,'C','M', 1);
			$pdf->SetXY($columna+135,$fila);
			$pdf->drawTextBox("Inter�s por mora", 20, 10,'C','M', 1);
			$pdf->SetXY($columna+155,$fila);
			$pdf->drawTextBox("Inter�s mora al d�a ".$_POST['fechaestado'], 20, 10,'C','M', 1);
			$pdf->SetLineWidth(0.1);
			$fila=$fila+10;
			//Fin Encabezado de Fila nueva pagina
			$pdf->SetFont('Times','',8);
		}
	}
	// Totales
	$pdf->SetFont('Times','B',10);
	//$tamanio_totales = 50 + $lineas_obs;
    $tamanio_totales = 60;
	//si necesito nueva pagina para los totales creo una nueva, sino utilizo la misma
	if(300 - $fila < $tamanio_totales)
	{
		$pdf->Addpage('P', "Legal");
		$pdf->Setmargins(20,20,10);
		$pdf->SetLineWidth(0.1);
		$pdf->SetFillColor(192, 192, 192);
		$fila=50;
		$columna=10;
		//Encabezado datos cuenta nueva pagina
		$pdf->SetFont('Times','B',10);
		$pdf->SetXY($columna,$fila);
		$pdf->Write(4, "Titular: ".$_POST['solicitante']);
		$pdf->SetXY($columna,$fila+5);
		$pdf->Write(4, "Expediente: ".$_POST['nroexpediente']);
		$offset = (strlen($_POST['nroexpediente']) +10) * 2; /*offset es el espacio entre el nro de expediente y el a�o de expediente */
		$pdf->SetXY($columna+$offset,$fila+5);
		$pdf->Write(4, "A�o: ".$_POST['anioexpediente']);
		$pdf->SetXY($columna,$fila+10);
		$pdf->Write(4, "Nro de cuenta: ".$_POST['nrocuenta']);
		$offset = (strlen($_POST['nrocuenta']) +15) * 2; /*offset es el espacio entre el nro de cuenta y la superficie*/
		$pdf->SetXY($columna+$offset,$fila+10);
		$pdf->Write(4, "Superficie: ".$_POST['superficie']." ".$_POST['unidad']);
		//Fin datos cuenta nueva pagina
		$fila = $fila+10;
	}
	$fila = $fila+10;
	//titulo
	$pdf->SetXY($columna,$fila);
	$pdf->Write(4, "TOTALES ACTUALES CALCULADOS");
	//valor liquidacion
	$valor = "$ ".$_POST['valorliquidacion'];
	$pdf->SetXY($columna,$fila+5);
	$pdf->Write(4, "Monto liquidado: ".$valor);
	//interes financiacion
	$valor = "$ ".$_POST['total'];
	$pdf->SetXY($columna+90,$fila+5);
	$pdf->Write(4, "Total financiado: ".$valor);
	//total financiacion
	$valor = "$ ".$_POST['cobrado'];
	$pdf->SetXY($columna,$fila+10);
	$pdf->Write(4, "Monto cobrado: ".$valor);
	//total cobrado
	$valor = "$ ".$_POST['gastosfijos'];
	$pdf->SetXY($columna+90,$fila+10);
	$pdf->Write(4, "Cobrado gastos fijos: ".$valor);
	//total saldo
	$valor = "$ ".$_POST['saldo'];
	$pdf->SetXY($columna,$fila+15);
	$pdf->Write(4, "Saldo de cuenta: ".$valor);
	//total deudavencida
	$valor = "$ ".$_POST['deudavencida'];
	$pdf->SetXY($columna+90,$fila+15);
	$pdf->Write(4, "Deuda vencida: ".$valor);
	//total deuda no vencida
	$valor = "$ ".$_POST['deudanovencida'];
	$pdf->SetXY($columna,$fila+20);
	$pdf->Write(4, "Deuda no vencida: ".$valor);
	//total interes por mora
	$valor = "$ ".$_POST['mora'];
	$pdf->SetXY($columna+90,$fila+20);
	$pdf->Write(4, "Intereses punitorios actuales: ".$valor);
	//total interes por mora a la fecha indicada
	$valor = "$ ".number_format($_POST['nuevamora'],2,',','.');
	$pdf->SetXY($columna,$fila+25);
	$pdf->Write(4, "Intereses punitorios al d�a ".$_POST['fechaestado'].": ".$valor);
	//total deuda con interes por mora a la fecha de hoy
	$valor = "$ ".number_format($_POST['nuevadeuda'],2,',','.');
	$pdf->SetXY($columna+90,$fila+25);
	$pdf->Write(4, "DEUDA TOTAL AL D�A ".$_POST['fechaestado'].": ".$valor);
	// Fin Totales

	//observaciones
   	$fila = $pdf->GetY() + 15;
	if(strlen($obs) > 1)
	{
	    $pdf->SetXY($columna,$fila);
        $pdf->SetFont("Times", 'BU');
	    $pdf->Write(4, "Observaciones:");
        $pdf->SetFont("Times", 'B');
	    $pdf->SetXY($columna,$fila+5);
        $pdf->MultiCell(0, 5, $obs);
	}

	$pdf->Output();
}
?>