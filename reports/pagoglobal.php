<?php
define('FPDF_FONTPATH','../fpd153/font/');
require_once '../fpd153/fpdf.php' ;
require_once '../librerias/config.php';
require_once '../librerias/funcionesphp.php';
require_once '../modelos/modelocuota.php';
require_once '../modelos/modelocuenta.php';
require_once '../modelos/modeloparametro.php';
require_once '../modelos/modelosolicitudconcepto.php';
require_once '../config.php'; //Archivo con configuraciones.
//Conceptos a cobrar en talon
$conceptos="";
$solicitudconcepto = new modelosolicitudconcepto();
$solicitudconcepto->putIdSolicitud($_POST['idsolicitud']);
$conceptosliq=$solicitudconcepto->TraerDetalle();
$totalgastos=0;

while ($varlab = mysql_fetch_object($conceptosliq))
{
$conceptos[]=array( "descripcion"=>$varlab->descripcion,
   					"valor"=>	$varlab->valor);
	$totalgastos=$totalgastos+$varlab->valor;

}	
//Fin Conceptos
//Guardar Nuevos Intereses Calculados	
$cuotas= new modelocuota();
if (isset($_POST['cuotainicial']) || isset($_POST['cuotafinal']))
{
$arrMora = $cuotas->calcularMoraRango($_POST['cuotainicial'], $_POST['cuotafinal'], $_POST['idcuenta']);
if(!$arrMora)
           return false;
foreach($arrMora as $intmora ){
           $cuotas->putIdCuota($intmora['id']);

           if($intmora['intmora']>0){
			 $traecuotas=$cuotas->traercuota();
			 $cuotas->putInteresMora($intmora['intmora']+$intmora['moraanterior']);
             $cuotas->putIdUsrMod($_POST["idusr"]);
			 $dif=comparar_fechas(fechaACadena($cuotas->getFechaCalculoMora()),$_POST['fechapagoglobal']);
             if($dif>0) // si fecha calculo de mora es mayor a la fecha de pago parcial dejar la fecha anterior
               $cuotas->putFechaCalculoMora($cuotas->getFechaCalculoMora());
             else
			   $cuotas->putFechaCalculoMora(cadenaAFecha($_POST['fechapagoglobal']));

             $cuotas->modificarmora();
		   }
		   if($intmora['nrocuota'] == $_POST['cuotainicial'])
		   {
				$cuotas->putConceptos($totalgastos);
				$cuotas->guardarConceptos();
		   }

}
$interesglobal="0,00";
$amortizacionventa=number_format($_POST['montoapagar'], 2,',','.');
$deudaatrasada=$_POST['deudaatrasada'];
$deudaglobal=$_POST['deudaglobal'];
//si la cuenta comienza con 'A' es de pastaje
if($_POST['nrocuenta'][0] == 'A')
    $taloncuota = "Pastaje";
else
    $taloncuota="Cuota ".$_POST['cuotainicial']." hasta ".$_POST['cuotafinal'];
}
if($arrMora){
$pdf=new FPDF();
$pdf->AddFont('barras','','barras.php');
$pdf->AddPage();
$fila=5;
$columna=5;
$valorcuota=$_POST['montoapagar']+$totalgastos;
if($valorcuota>999999.99){
echo "El valor a pagar mas los gastos fijos supera los $ 999999.99 - No puede emitirse el talon";
return(false);
}
$nrocuota=$_POST['cuotainicial'];
$stringcta=trim(sprintf("%s",$nrocuota));
$idcuenta=$_POST['idcuenta'];
$stringpmo=trim(sprintf("%s",$idcuenta));
$nropmo=str_pad($stringpmo,7, "0", STR_PAD_LEFT);
$nrocta=str_pad($stringcta,3, "0", STR_PAD_LEFT);
$oper="3";//operacion cobro de varias cuotas
$cuenta=$_POST['cuentadeposito'];
$convenio=$_POST['convenio'];
$productor=$_POST['solicitante']." - ".strtoupper($_POST['titulares']);
$fecha1=$vto1=$_POST['fechapagoglobal'];
$fechaemision="Fecha de Emision : ".date('d/m/Y');
include('talon.php');
$pdf->Output();
return true;}else
{echo "No se pudieron recuperar las cuotas";
return(false);}
?>