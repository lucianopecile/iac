<?php
define('FPDF_FONTPATH','../fpd153/font/');
require_once '../fpd153/fpdf.php' ;
require_once '../fpd153/diseniosaldos.php' ;
require_once '../librerias/config.php';
require_once '../modelos/modelocuota.php';
require_once '../modelos/modelocuenta.php';
require_once '../modelos/modeloparametro.php';
require_once '../librerias/funcionesphp.php';
require_once '../config.php'; //Archivo con configuraciones.

$cuota = new modelocuota();
$cuenta = new modelocuenta();
$cant_cuotas = count($listado)-1;
$fecha = $_POST['fechamora'];
$i=0;

$lista_cuentas = $cuenta->listadoTotal();
if(count($lista_cuentas) <= 0)
{
	$mensaje = htmlentities("No hay cuentas para listar");
	$data['mensaje'] = $mensaje;
	printf("%s", $mensaje);
	return false;
}
foreach ($lista_cuentas as $varc)
{
	$idcuenta = $varc['id'];
	$cuota->putIdCuenta($idcuenta);
	//obtengo las cuotas adeudadas y vencidas de la cuenta
	$lista_cuotas = $cuota->listadoCuotasCuentaDeuda("");
	if(count($lista_cuotas) > 0)
	{
		$saldo_total=$cobrado_total=$int_mora_total=$int_mora_fecha=0;
		foreach ($lista_cuotas as $c)
		{
			$cuota->putIdCuota($c['id']);
			$cuota->traerCuota();
			$saldo_total += $cuota->getSaldo()*1;
			$cobrado_total += $cuota->getCobrado()*1;
			$int_mora_total += $cuota->getInteresMora()*1;
		}
		//obtengo la mora total a la fecha indicada
		$_GET['fechamora'] = $fecha;
		$arr_mora = $cuota->calcularMoraRango($lista_cuotas[0]['nrocuota'], $lista_cuotas[count($lista_cuotas)-1]['nrocuota'], $idcuenta);
		if(!$arr_mora)
		{
			$mensaje = htmlentities("Error en el c�lculo de intereses, corrobore los datos.");
			$data['mensaje'] = $mensaje;
			printf("%s", $mensaje);
			return false;
		}
		foreach ($arr_mora as $c)
		{
			$int_mora_fecha += ($c['intmora'] + $c['moraanterior']);
		}
		if(($saldo_total>0) || ($int_mora_total>0))
		{
			//genero el arreglo con todos los datos
			$listado[$i]['saldototal'] = $saldo_total;
			$listado[$i]['cobradototal'] = $cobrado_total;
			$listado[$i]['morafecha'] = $int_mora_fecha;
			$listado[$i]['nrocuenta'] = $varc['nrocuenta'];
			$listado[$i]['expediente'] = $varc['expediente'];
			$listado[$i]['valorliquidacion'] = $varc['valorliquidacion'];
			$listado[$i]['titular'] = $varc['solicitante'];
			$listado[$i]['tipo'] = $varc['tipo'];
			$i++;
		}
	}
}
if($listado)
{
	$pdf = new APDF();
	$pdf->AliasNbPages();
	$pdf->AddPage('P', "Legal");
	$pdf->Setmargins(20,20,10);
	$pdf->SetLineWidth(0.1);
	$pdf->SetFillColor(192, 192, 192);
	$pdf->Setfont('times','',8);
	$fila=50;
	$columna=10;
	$pdf->SetFont('Times','B');

	//Encabezado
	$pdf->SetFont('Times','B',10);
	$pdf->SetXY($columna,$fila);
	$pdf->Write(4, "Fecha: ".date('d/m/Y'));
	$pdf->SetXY($columna,$fila+5);
	$pdf->Write(4, "Deudas exigibles calculados al ".$fecha);
	//Fin

	// Encabezado Fila
	$pdf->SetFont('Times','B',8);
	$fila=$fila+21;
	$pdf->SetXY($columna,$fila);
	$pdf->Cell(185,5,'',1,1,'C',1);
	$pdf->SetXY($columna,$fila);
	$pdf->drawTextBox("Cuenta", 15, 5,'C','M', 1);
	$pdf->SetXY($columna+15,$fila );
	//$pdf->drawTextBox("Expediente", 25, 5,'C','M', 1);
	//$pdf->SetXY($columna+40,$fila);
	$pdf->drawTextBox("Titular", 15, 5,'L','M', 0);
	//$pdf->SetXY($columna+85,$fila);
//	$pdf->drawTextBox("Tipo", 15, 5,'C','M', 1);
//	$pdf->SetXY($columna+100,$fila);
//	$pdf->drawTextBox("Monto liquidado", 20, 5,'C','M', 1);
//	$pdf->SetXY($columna+120,$fila);
//	$pdf->drawTextBox("Saldo total", 20, 5,'C','M', 1);
//	$pdf->SetXY($columna+140,$fila);
//	$pdf->drawTextBox("Cobrado total", 20, 5,'C','M', 1);
	$pdf->SetXY($columna+160,$fila);
	$pdf->drawTextBox("Deuda total al ".$fecha, 25, 5,'C','M', 1);
	$pdf->SetLineWidth(0.1);
	$fila=$fila+5;
	//Fin Encabezado de Fila

	$pdf->Setfont('times','',8);
	$i=0;
	foreach($listado as $c)
	{
		$pdf->SetXY($columna,$fila);
		$pdf->Cell(185,5,'',1,1,'C');
		//columna cuenta
		$pdf->SetXY($columna,$fila);
		$pdf->drawTextBox($c['nrocuenta'], 15, 5,'C','M', 1);
		//columna expediente
	//	$pdf->SetXY($columna+15,$fila);
	//	$pdf->drawTextBox($c['expediente'], 25, 5,'C','M', 1);
		//columna titular
		$pdf->SetXY($columna+15,$fila);
		$pdf->drawTextBox($c['titular'], 55, 5,'L','M', 0);
		//columna tipo
		//$pdf->SetXY($columna+85,$fila);
		//$pdf->drawTextBox($c['tipo'], 15, 5,'C','M', 1);
		//columna monto liquidado
		//$pdf->SetXY($columna+100,$fila);
		//$valor = "$ ".number_format($c['valorliquidacion'],2,",",".")." ";
		//$pdf->drawTextBox($valor, 20, 5,'R','M', 1);
		//columna deuda total a la fecha
		//$pdf->SetXY($columna+120,$fila);
		//$valor = "$ ".number_format($c['saldototal'],2,",",".")." ";
		//$pdf->drawTextBox($valor, 20, 5,'R','M', 1);
		//columna saldo total
		//$pdf->SetXY($columna+140,$fila);
		//$valor = "$ ".number_format($c['cobradototal'],2,",",".")." ";
		//$pdf->drawTextBox($valor, 20, 5,'R','M', 1);
		//columna cobrado total
		$pdf->SetXY($columna+160,$fila);
		$valor = "$ ".number_format($c['saldototal']+$c['morafecha'],2,",",".")." ";
		$pdf->drawTextBox($valor, 25, 5,'R','M', 1);
		$fila=$fila+5;
		$i++;

		// si hay salto de pagina
		if($fila>=300)
		{
			$pdf->Addpage('P', "Legal");
			$pdf->Setmargins(20,20,10);
			$pdf->SetLineWidth(0.1);
			$pdf->SetFillColor(192, 192, 192);
			$fila=50;
			$columna=10;
			//Encabezado nuva pagina
			$pdf->SetFont('Times','B',10);
			$pdf->SetXY($columna,$fila);
			$pdf->Write(4, "Fecha: ".date('d/m/Y'));
			$pdf->SetXY($columna,$fila+5);
			$pdf->Write(4, "Deudas exigibles calculados al ".$fecha);
			//Fin encabedazo

			// Encabezado Fila nueva pagina
			$pdf->SetFont('Times','B',8);
			$fila=$fila+21;
			$pdf->SetXY($columna,$fila);
			$pdf->Cell(185,5,'',1,1,'C',1);
			$pdf->SetXY($columna,$fila);
			$pdf->drawTextBox("Cuenta", 15, 5,'C','M', 1);
//			$pdf->SetXY($columna+15,$fila);
//			$pdf->drawTextBox("Expediente", 25, 5,'C','M', 1);
			$pdf->SetXY($columna+15,$fila);
	        $pdf->drawTextBox("Titular", 15, 5,'L','M', 0);
	//		$pdf->SetXY($columna+85,$fila);
	//		$pdf->drawTextBox("Tipo", 15, 5,'C','M', 1);
	//		$pdf->SetXY($columna+100,$fila);
	//		$pdf->drawTextBox("Monto liquidado", 20, 5,'C','M', 1);
	//		$pdf->SetXY($columna+120,$fila);
	//		$pdf->drawTextBox("Saldo total", 20, 5,'C','M', 1);
	//		$pdf->SetXY($columna+140,$fila);
	//		$pdf->drawTextBox("Cobrado total", 20, 5,'C','M', 1);
			$pdf->SetXY($columna+160,$fila);
			$pdf->drawTextBox("Deuda total al ".$fecha, 25, 5,'C','M', 1);
			$pdf->SetLineWidth(0.1);
			$fila=$fila+5;
			//Fin Encabezado de Fila nueva pagina
			$pdf->SetFont('Times','',8);
		}
	}
	// Totales
	$pdf->SetFont('Times','B',10);
	$tamanio_totales = 50;
	//si necesito nueva pagina para los totales creo una nueva, sino utilizo la misma
	if(300 - $fila < $tamanio_totales)
	{
		$pdf->Addpage('P', "Legal");
		$pdf->Setmargins(20,20,10);
		$pdf->SetLineWidth(0.1);
		$pdf->SetFillColor(192, 192, 192);
		$fila=50;
		$columna=10;
		//Encabezado datos cuenta nueva pagina
		$pdf->SetFont('Times','B',10);
		$pdf->SetXY($columna,$fila);
		$pdf->Write(4, "Fecha: ".date('d/m/Y'));
		$pdf->SetXY($columna,$fila+5);
		$pdf->Write(4, "Deudas exigibles calculados al ".$fecha);
		//Fin datos cuenta nueva pagina
	}
	$fila=$fila+30;
	//titulo
	$pdf->SetXY($columna,$fila);
	//$pdf->Write(4, "TOTALES");
	//total cantidad de cuentas
//	$valor = $_POST['cantcuentas'];
	//$pdf->SetXY($columna,$fila+10);
	//$pdf->Write(4, "Total ".$valor." cuentas abiertas");
	//valor suma de saldos
	//$valor = "$ ".$_POST['sumasaldos'];
	//$pdf->SetXY($columna,$fila+20);
	//$pdf->Write(4, "Total saldos: ".$valor);
	//valor suma de cobros
	//$valor = "$ ".$_POST['sumacobrados'];
	//$pdf->SetXY($columna,$fila+30);
	//$pdf->Write(4, "Total cobrado: ".$valor);
	//valor suma de deudas totales
	$valor = "$ ".$_POST['sumadeudas'];
//	$pdf->SetXY($columna,$fila+40);
	$pdf->Write(4, "Total acreencias: ".$valor);
	// Fin Totales

	$pdf->Output();
}

?>
