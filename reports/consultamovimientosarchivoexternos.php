<?php
define('FPDF_FONTPATH','../fpd153/font/');
require_once '../fpd153/fpdf.php' ;
require_once '../fpd153/diseniomovimientosexternos.php' ;
require_once '../librerias/config.php';
require_once '../librerias/funcionesphp.php';
require_once '../modelos/modelomovimientoexterno.php';
require_once '../config.php'; //Archivo con configuraciones.

$mov = new ModeloMovimientoExterno();
$mov->putIdArchivo($_GET['idarchivo']);
$marchvio=$_GET['nombrearchivo'];
$lista = $mov->listadoMovimientosArchivo();

if(count($lista) <= 0)
{
	$mensaje = htmlentities("No hay movimientos.");
	$data['mensaje'] = $mensaje;
	printf("%s", $mensaje);
	return false;
}

if($lista)
{
	$pdf = new APDF();
	$pdf->AliasNbPages('P', "Legal");
	$pdf->AddPage();
	$pdf->Setmargins(20,20,10);
	$pdf->SetLineWidth(0.1);
	$pdf->SetFillColor(192, 192, 192);
	$pdf->Setfont('times','',8);
	$fila=30;
	$columna=30;
	$pdf->SetFont('Times','B');
        
        // Encabezado Fila
	$pdf->SetFont('Times','B',8);
	
        $pdf->Write(5,"                                                 Archivo: ".$marchvio);
        $fila=$fila+21;
	
	
        
        $pdf->SetXY($columna,$fila);
	$pdf->Cell(160,5,'',1,1,'C',1);
	$pdf->SetXY($columna,$fila);
	$pdf->drawTextBox("Fecha", 20, 5,'C','M', 1);
	$pdf->SetXY($columna+20,$fila);
	$pdf->drawTextBox("Id Trans. Externa", 30, 5,'C','M', 1);
        
        $pdf->SetXY($columna+50,$fila);
	$pdf->drawTextBox("Fecha Cobro", 20, 5,'R','M', 1);
	
	$pdf->SetXY($columna+70,$fila);
	$pdf->drawTextBox("Cobrado", 20, 5,'R','M', 1);
	$pdf->SetXY($columna+90,$fila);
	$pdf->drawTextBox("Detalle Registro", 70, 5,'C','M', 1);
	$pdf->SetLineWidth(0.1);
	$fila=$fila+5;
	//Fin Encabezado de Fila

	$pdf->Setfont('times','',8);
	$i=0;
	foreach($lista as $m)
	{ 
		$pdf->SetXY($columna,$fila);
		$pdf->Cell(160,5,'',1,1,'C');
		//columna fecha
		$pdf->SetXY($columna,$fila);
		$pdf->drawTextBox($m['fecha'], 20, 5,'R','M', 1);
		//columna monto
		$pdf->SetXY($columna+20,$fila);
		$pdf->drawTextBox($m['idIdMovimientoExterno'], 30, 5,'R','M', 1);
                
		
                
                $pdf->SetXY($columna+50,$fila);
		$pdf->drawTextBox($m['fechacobro'], 20, 5,'R','M', 1);
                
                $valor = "$ ".number_format($m['cobrado'],2,",",".")." ";
		
                $pdf->SetXY($columna+70,$fila);
		$pdf->drawTextBox($valor, 20, 5,'R','M', 1);
		//columna nro cuota
		
                $pdf->SetXY($columna+90,$fila);
		$pdf->drawTextBox($m['detalleboleta'],  70, 5,'L','M', 1);
		$fila=$fila+5;
		$i++;
 
		// si hay salto de pagina
		if($fila>=300)
		{
			$pdf->Addpage('P', "Legal");
			$pdf->Setmargins(20,20,10);
			$pdf->SetLineWidth(0.1);
			$pdf->SetFillColor(192, 192, 192);
			$fila=30;
			$columna=30;

			// Encabezado Fila nueva pagina
			$pdf->SetFont('Times','B',8);
			$fila=$fila+21;
			$pdf->SetXY($columna,$fila);
			$pdf->Cell(160,5,'',1,1,'C',1);
			$pdf->SetXY($columna,$fila);
			$pdf->drawTextBox("Fecha", 20, 5,'C','M', 1);
	$pdf->SetXY($columna+20,$fila);
	$pdf->drawTextBox("Id Trans. Externa", 30, 5,'C','M', 1);
        
        $pdf->SetXY($columna+50,$fila);
	$pdf->drawTextBox("Fecha Cobro", 20, 5,'R','M', 1);
	
	$pdf->SetXY($columna+70,$fila);
	$pdf->drawTextBox("Cobrado", 20, 5,'R','M', 1);
	$pdf->SetXY($columna+90,$fila);
	$pdf->drawTextBox("Detalle Registro", 70, 5,'C','M', 1);
			$pdf->SetLineWidth(0.1);
			$fila=$fila+5;
			//Fin Encabezado de Fila nueva pagina
			$pdf->SetFont('Times','',8);
		}
	}
	$pdf->Output();
}

?>